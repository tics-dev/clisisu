<?php
/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Exceptions;

use Exception;
use GuzzleHttp\Psr7\Request;

/**
 * HttpException.
 *
 * @author Kris Wallsmith <kris@symfony.com>
 */
class WsError extends Exception
{
    public function render(Request $request)
    {
        return json_encode(
            array(
                "success" => false,
                "msj" => $this->getMessage(),
                'request' => $request,
                'out' => [
                    'code' => $this->getCode(),
                    'file' => basename($this->getFile()),
                    'line' => $this->getLine()
                ]
            )
        );
    }


    public function context()
    {
    }
}
