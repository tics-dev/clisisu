<?php

namespace App\Exceptions;

use Barryvdh\Debugbar\Facades\Debugbar;
use Exception;

class DebugException extends Exception
{
    static $errors = array();
    protected $orderId;

    public function context()
    {
        return ['order_id' => $this->orderId];
    }

    public function report()
    {
        Debugbar::addMessage('mensaje', $this->getMessage());
        Debugbar::addMessage('codigo', $this->getCode());
        Debugbar::addMessage('linea', $this->getLine());
        Debugbar::addMessage('archivo', $this->getFile());
    }

    public function render($request)
    {
        return response("");
    }

    public static function add($key,  $collect)
    {
        self::$errors[$key] = $collect;
    }

    public static function item($key)
    {
        return (isset(self::$errors[$key])) ? self::$errors[$key] : "";
    }
}
