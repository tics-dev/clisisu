<?php
namespace App\Exceptions;
use Exception;

class GlozaException extends Exception
{
    public static $errors = array();
    protected $nomsersat;
    protected $numtracc;
    protected $gloza;
    protected $asunto; 

    public function context()
    {
        return [
            'numtracc' => $this->numtracc,
            'gloza' => $this->gloza,
        ];
    }
    
    public function render()
    {
        $this->numtracc = trim($this->getMessage());
        $glozas = self::getGlozas();

        if(array_key_exists($this->numtracc, self::$errors))
        {
            $gloza = self::$errors["{$this->numtracc}"];
            $respuesta = array(
                "numtracc"=> $this->numtracc,
                "gloza" => $gloza,
                "mensaje" => $glozas["{$gloza}"] 
            );
            return response(json_encode($respuesta), 200, ["Content-Type"=> "application/json"]);
        }else{
            return false;
        }
    }

    public static function add($radicado, $key='')
    {
        $glozas = self::getGlozas();
        if(array_key_exists($key, $glozas))
        {
            self::$errors["$radicado"] = $key;
            return;
        }
        $alertas = self::getAlertas();
        if(array_key_exists($key, $alertas))
        {
            self::$errors["$radicado"] = $key;
        }
    }

    public static function item($key='')
    {
        return (!empty($key)) ? self::$errors[$key] : false;
    }

    public static function items()
    {
        return (!empty(self::$errors)) ? self::$errors : false;
    }

    public static function getGlozas()
    {
        return [
            "GN01"=> "Número de radicado ya existe para la CCF reportado en el SAT",
            "GN02"=> "El empleador no existe en las fuentes de referencia",
            "GN03"=> "El primer nombre del empleador persona natural no coincide con la información de referencia.",
            "GN04"=> "El primer apellido del empleador persona natural no coincide con la información de referencia.",
            "GN05"=> "La fecha de solicitud de la afiliación o desafiliación no puede ser superior a la fecha de efectividad de la afiliación o desafiliación",
            "GN06"=> "La razón social no coincide con la información de referencia",
            "GN07"=> "El empleador ya tiene una afiliación para el departamento reportado",
            "GN08"=> "El empleador tiene una solicitud de afiliación pendiente de respuesta para el departamento reportado",
            "GN09"=> "El empleador no ha tenido una afiliación previa para el departamento reportado",
            "GN10"=> "El código de la CCF anterior reportado no es el responsable de la última afiliación del empleador.",
            "GN11"=> "El empleador no tiene una afiliación vigente para el departamento reportado para la CCF que reporta",
            "GN12"=> "La Fecha de Paz y Salvo no puede ser inferior a la fecha de afiliación.",
            "GN13"=> "La fecha de pérdida de la afiliación no puede ser inferior a la fecha de la afiliación",
            "GN14"=> "La fecha de inicio de la relación laboral no puede ser inferior a la fecha de afiliación de la empresa en departamento.",
            "GN15"=> "El trabajador no existe en las fuentes de referencia.",
            "GN16"=> "El primer nombre del trabajador no coincide con la información de referencia.",
            "GN17"=> "El primer apellido del trabajador no coincide con la información de referencia.",
            "GN18"=> "No existe una relación laboral para el empleador en el departamento y CCF a la fecha reportada",
            "GN19"=> "La fecha de fin de la relación laboral no puede ser inferior a la fecha de inicio de la relación laboral",
            "GN20"=> "La fecha de novedad no puede ser inferior a la fecha de inicio de la relación laboral",
            "GN21"=> "No existe la novedad a interrumpir, prorrogar, o cancelar",
            "GN22"=> "No hay una novedad vigente la cual corregir",
            "GN23"=> "La novedad ya ha finalizado y por lo tanto no se puede interrumpir, prorrogar, o cancelar",
            "GN24"=> "La novedad ya ha finalizado y por lo tanto no se puede corregir",
            "GN25"=> "Cuando se realiza el registro, la interrupción, la prórroga, o la corrección de la novedad, la fecha fin de novedad queda menor que la fecha inicio de la novedad",
            "GN26"=> "La fecha de retiro definitivo del empleador al SSF no puede ser anterior a la fecha de efectividad de la última afiliación.",
            "GN27"=> "El empleador tiene relaciones laborales vigentes por lo cual no se puede aplicar un retiro definitivo",
            "GN28"=> "El empleador no se encuentra al día",
            "GN29"=> "El empleador no se encuentra en mora",
            "GN30"=> "La CCF solo puede reportar afiliaciones en el departamento donde opera",
            "GN31"=> "El empleador no tiene una afiliación terminada con causal desafiliación por causa grave para el departamento reportado para la CCF que reporta cuando el tipo de reporte es 2",
            "GN32"=> "El empleador tiene una afiliación en estado terminado con causa desafiliación por causa grave en el departamento reportado.",
            "GN33"=> "Ya existe una relación laboral registrada entre el empleador y el trabajador reportados",
            "GN34"=> "El empleador ya tiene una afiliación para el departamento reportado en estado diferente a activo",
            "GN35"=> "El número de radicado del SAT debe existir y corresponder a una solicitud de afiliación a una Caja de Compensación Familiar por primera vez en un departamento.",
            "GN36"=> "Se debe validar que la solicitud de afiliación que corresponde al campo Número de transacción de la afiliación enviado por SAT no se encuentre en estado desistida",
            "GN37"=> "Se debe validar que la solicitud de afiliación que corresponde al campo Número de transacción de la afiliación enviado por SAT se encuentre en estado pendiente.",
        ];
    }

    public static function getAlertas()
    {
        return [
            "AN01"=> "El tipo y número de documento del representante legal no existe en fuentes de referencia",
            "AN02"=> "El primer nombre del representante legal no coincide con la información de referencia.",
            "AN03"=> "El primer apellido del representante legal no coincide con la información de referencia."
        ];
    }
 
    public static function for($nomsersat, $asunto)
    {
        $instancia = new static("Error servicio ciente Api [{$nomsersat}]");
        $instancia->nomsersat = $nomsersat;
        $instancia->asunto = $asunto;
        return $instancia;
    }
}