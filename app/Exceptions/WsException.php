<?php
namespace App\Exceptions;
use Exception;

class WsException extends Exception
{
    protected $nomsersat;
    protected $asunto;
    
    public function context()
    {
        return [
            'nomsersat' => $this->nomsersat,
            'asunto' => $this->asunto,
        ];
    }

    public function render()
    {
        return json_encode(
            array(
                "success" => false,
                "msj" => $this->getMessage(),
                "servicio" => $this->nomsersat
            )
        );
    }

    public static function for($nomsersat, $asunto)
    {
        $instancia = new static("Error servicio ciente Api [{$nomsersat}]");
        $instancia->nomsersat = $nomsersat;
        $instancia->asunto = $asunto;
        return $instancia;
    }
}