<?php

namespace App\Exports;

use App\Models\Gener02;
use Maatwebsite\Excel\Concerns\FromCollection;

class UsersExport implements FromCollection
{
    public function collection()
    {
        return Gener02::all();
    }
}
