<?php

use App\Console\Servicios\ComfacaAfilia\AfiliaConyugue;
use App\Console\Servicios\ServicioSat\TokenGenerador\TokenGeneratorFactory;
use App\Models\Subsi02;
use Firebase\JWT\JWT as JWT;
use Firebase\JWT\Key as Key;
use App\Exceptions\GlozaException;
use App\Models\Gener09;
use App\Models\Sat01;
use App\Models\Sat29;
use App\Models\Subsi04;
use App\Models\Subsi12;
use App\Models\Subsi14;
use App\Models\Subsi15;
use App\Models\Subsi20;
use App\Models\Subsi21;
use App\Models\Subsi22;
use App\Models\Subsi30;
use App\Models\Subsi48;
use App\Models\Xml86;
use App\Models\Xml87;
use Illuminate\Support\Facades\DB;

class ProcesoPrueba
{
    protected $console;
    public function __construct($console)
    {
        $this->console = $console;
    }

    public function prueba($params)
    {
        $empresa = Subsi02::where("nit", '891190047')->get()->first();
        $sucursales = $empresa->getSucursales();
        foreach ($sucursales as $sucursal) {
            $this->console->info('Display :' . $sucursal->detalle);
        }
        $this->read_param();
        $comando = $this->console->getComando();
        var_export($comando);
    }

    public function read_param()
    {
        $key = "example_key";
        $payload = array(
            "iss" => "http://example.org",
            "aud" => "http://example.com",
            "iat" => 1356999524,
            "nbf" => 1357000000
        );
        $jwt = JWT::encode($payload, $key, 'HS256');
        $decoded = JWT::decode($jwt, new Key($key, 'HS256'));
        print_r($jwt);
        print_r($decoded);
    }

    public function probar_exception()
    {
        try {
            $codapl = 'SA';
            $tabla = 'sat02';
            $tokenGenerator = TokenGeneratorFactory::create();
            $sat01  = Sat01::where("codapl", $codapl)->get()->first();
            $sat29  = Sat29::where("referencia", $tabla)->where('codapl', $codapl)->get()->first();
            $tokenGenerator->setAtenticar(True);
            $tokenGenerator->generateToken($sat01, $sat29);
            $this->console->info($tokenGenerator->getToken());

            /*GlozaException::add("1212202323{$i}", "GN0{$i}"); */
        } catch (GlozaException $th) {
            $this->console->info($th->render());
        }
        if (collect(GlozaException::items())->count() > 0) {
            $this->console->info(print_r(GlozaException::items()));
        }
    }

    public function probar_trayectoria($argv = '', $user = '', $sistem = '', $env = '')
    {
        $trabajador = Subsi15::where('estado', 'A')->where('cedtra', '1110491951')->get()->first();
        $out = $trabajador->getTrayectoria()->toArray();
        $this->console->info(json_encode($out));
    }

    public function preparaCartentCategoria($argv = '', $user = '', $sistem = '', $env = '')
    {
        $mperiodo = Subsi12::where('girado', 'N')->orderBy('periodo', 'asc')->get()->first();
        $periodo = $mperiodo->periodo;

        $cantidad_salarios = 4000000 / $mperiodo->salmin;

        var_dump($cantidad_salarios);
        //categorias
        $categorias = Subsi30::all()->toArray();

        $codigo_categoria = 'C';
        $carnetiza =  'N';
        foreach ($categorias as $ai => $row) {
            if ($cantidad_salarios <= $row['cansal']) {
                $codigo_categoria  = ($row['codcat']) ? $row['codcat'] : 'C';
                $carnetiza =  'S';
                break;
            }
        }
        var_dump([$carnetiza, $codigo_categoria]);
    }

    public function procesarSaldosTarjeta($argv = '', $user = '', $sistem = '', $env = '')
    {
        $sql = "SELECT sum(saldo) as tsaldo, numtar FROM asopa07 where saldo > 0 group by numtar";
        $rqsaldos = DB::select($sql);

        $collection_saldos = array();
        $collection_saldos[0] = array();

        foreach ($rqsaldos as $i => $rsaldo) {

            $res = collect(DB::select("SELECT numtar, cedres FROM asopa03 where numtar='{$rsaldo->numtar}'"))->first();
            if ($res) {

                if (!isset($collection_saldos["{$res->cedres}"])) {
                    $collection_saldos["{$res->cedres}"] = 0;
                }
                $collection_saldos["{$res->cedres}"] += $rsaldo->tsaldo;
            } else {
                $collection_saldos[0][] = ["numtar" => "{$rsaldo->numtar}", "saldo" => $rsaldo->tsaldo];
            }
        }
        $res = null;
        $rqsaldos = null;
        $sql = null;
        $rsaldo = null;

        $data = array(
            'cedula',
            'saldo',
            'nombre',
            'nit',
            'estado',
            'codezona',
            'zona',
            'telefono',
            'cuenta',
            'tipo_pago'
        );
        $this->console->info(implode(';', $data));

        foreach ($collection_saldos as $key => $valor) {

            if ($key == 0) continue;

            $responsable = Subsi20::where('cedcon', $key)->get()->first();
            if (!$responsable) {
                $responsable = Subsi15::where('cedtra', $key)->get()->first();
            } else {

                $relation = Subsi21::where('cedcon', $responsable->cedcon)->get()->first();
                if ($relation) {
                    $trabajador = Subsi15::where('cedtra', $relation->cedtra)->get()->first();
                    if ($trabajador) {
                        $responsable->nit = $trabajador->nit;
                        $responsable->estado = $trabajador->estado;
                    } else {
                        $responsable->nit = '';
                    }
                } else {
                    $responsable->nit = '';
                }
            }

            if ($responsable) {

                $zonas = Gener09::where('codzon', $responsable->codzon)->get()->first();
                $data = array(
                    $key,
                    $valor,
                    $responsable->prinom . ' ' . $responsable->segnom . ' ' . $responsable->priape . '' . $responsable->segape,
                    $responsable->nit,
                    $responsable->estado,
                    $responsable->codzon,
                    ($zonas) ? $zonas->detzon : '',
                    $responsable->telefono,
                    $responsable->numcue,
                    $responsable->tippag
                );
            } else {
                $data = array(
                    $key,
                    $valor,
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    ''
                );
            }
            $this->console->info(implode(';', $data));
            $responsable = null;
            $data = null;
        }

        $data = array(
            'NUMTAR',
            'SALDO',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        );
        $this->console->info(implode(';', $data));

        if (isset($collection_saldos[0])) {

            foreach ($collection_saldos[0] as $key => $valor) {

                $data = array(
                    $valor['numtar'],
                    $valor['saldo'],
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    ''
                );
                $this->console->info(implode(';', $data));
            }
        }
    }

    public function procesarConsultaConyuges($nit = '', $user = '', $sistem = '', $env = '')
    {
        $afiliaConyugue = new AfiliaConyugue();
        $afiliaConyugue->init($this->console, $env);
        $data = $afiliaConyugue->listar($nit, ['A', 'I']);
        $this->console->info(json_encode($data));
    }

    public function procesar_estadistica($nit = '', $user = '', $sistem = '', $env = '')
    {
        ini_set('memory_limit', '3000M');

        $sql = "SELECT periodo, pergir, sum(valor) as valor, sum(numcuo) as numcuo, cedtra, cedres, codben, nit
        from subsi09 where periodo='202201' GROUP BY cedres, cedtra, codben, periodo, pergir";

        $header = [
            'documento', 'periodo', 'pergir', 'valor', 'numcuo', 'cedtra', 'cedres', 'codben', 'estadis', 'ajuste'
        ];

        $this->console->info(implode(';', $header));

        $rqs = DB::select($sql);
        $i = 0;
        foreach ($rqs as $ai => $row) {
            $beneficiario =  Subsi22::where("codben", $row->codben)->get()->first();

            $estadis = Xml87::where("cedtra", $row->cedtra)
                ->where("codben", $row->codben)
                ->where("nit", $row->nit)
                ->where("documento", $beneficiario->documento)
                ->where("periodo", $row->pergir)
                ->where("subliq", ">", 0)
                ->get()
                ->count();

            $data = [
                $beneficiario->documento,
                $row->periodo,
                $row->pergir,
                $row->valor,
                $row->numcuo,
                $row->cedtra,
                $row->cedres,
                $row->codben,
                $estadis,
                'Normal'
            ];
            $this->console->info(implode(';', $data));
        }

        $sql = "SELECT periodo, pergir, sum(valor) as valor, sum(numcuo) as numcuo, cedtra, cedres, codben, nit 
        from subsi09 where periodo='202200' GROUP BY cedres, cedtra, codben, periodo, pergir";

        $rqs = DB::select($sql);
        $i = 0;
        foreach ($rqs as $ai => $row) {
            $beneficiario =  Subsi22::where("codben", $row->codben)->get()->first();

            $estadis = Xml87::where("cedtra", $row->cedtra)
                ->where("codben", $row->codben)
                ->where("nit", $row->nit)
                ->where("documento", $beneficiario->documento)
                ->where("periodo", $row->pergir)
                ->where("subliq", ">", 0)
                ->get()
                ->count();

            $data = [
                $beneficiario->documento,
                $row->periodo,
                $row->pergir,
                $row->valor,
                $row->numcuo,
                $row->cedtra,
                $row->cedres,
                $row->codben,
                $estadis,
                'Ajuste'
            ];
            $this->console->info(implode(';', $data));
        }
        $this->console->info("Proceso Finalizado;;;;;;;");
    }

    public function procesar_estadistica_xml($nit = '', $user = '', $sistem = '', $env = '')
    {
        ini_set('memory_limit', '3000M');
        $estadisticas = Xml87::where("periodo", '202202')->get();

        $this->console->info(str_pad(';', 19, ';', STR_PAD_RIGHT));
        $this->console->info(str_pad('Poblacion Afiliados a Cargo', 19, ';', STR_PAD_RIGHT));
        $this->console->info(str_pad('Elaborado el 16 de Marzo del 2022', 19, ';', STR_PAD_RIGHT));
        $this->console->info(str_pad(';', 19, ';', STR_PAD_RIGHT));
        $header = [
            'Identificacion Empresa',
            'Nit',
            'Identificacion Trabajador',
            'Cedula',
            'Identificacion Beneficiario',
            'Documento',
            'Primer Nombre',
            'Segundo Nombre',
            'Primer Apellido',
            'Segundo Apellido',
            'Fecha Nacimiento',
            'Genero',
            'Parentesco',
            'Municipio Residencia',
            'Area Residencia',
            'Condicion Discapacidad',
            'Tipo Cuota',
            'Valor Subsidio',
            'Numero de cuotas',
            'Periodos',
            'Ajuste',
            'Miltiple'
        ];

        $this->console->info(implode(';', $header));

        foreach ($estadisticas as $row) {

            $multi =  Subsi22::where("documento", $row->documento)->get()->count();
            $sql = "SELECT 
                cedtra,  
                codben, 
                cedres,
                periodo, 
                pergir, 
                sum(valor) as valor, 
                sum(numcuo) as numcuo, 
                nit 
                from subsi09 
                where pergir IN('202200') and 
                cedtra='{$row->cedtra}' and 
                codben='{$row->codben}' 
                GROUP BY 1, 2;";

            $giros = DB::select($sql);
            if ($giros) {
                $giro = collect($giros)->first();
                $data = [
                    $row->docnit,
                    $row->nit,
                    $row->doctra,
                    $row->cedtra,
                    $row->coddoc,
                    $row->documento,
                    $this->sanetizar($row->prinom),
                    $this->sanetizar($row->segnom),
                    $this->sanetizar($row->priape),
                    $this->sanetizar($row->segape),
                    $row->fecnac,
                    $row->sexo,
                    "\"0{$row->parent}\"",
                    $row->codmun,
                    $row->area,
                    $row->discap,
                    "\"0{$row->tipcuo}\"",
                    intval($row->subliq) + intval($giro->valor),
                    $row->numcuo,
                    $row->periodos,
                    intval($giro->valor),
                    $multi
                ];
                $this->console->info(implode(';', $data));
            } else {
                $data = [
                    $row->docnit,
                    $row->nit,
                    $row->doctra,
                    $row->cedtra,
                    $row->coddoc,
                    $row->documento,
                    $this->sanetizar($row->prinom),
                    $this->sanetizar($row->segnom),
                    $this->sanetizar($row->priape),
                    $this->sanetizar($row->segape),
                    $row->fecnac,
                    $row->sexo,
                    "\"0{$row->parent}\"",
                    $row->codmun,
                    $row->area,
                    $row->discap,
                    "\"0{$row->tipcuo}\"",
                    intval($row->subliq),
                    $row->numcuo,
                    $row->periodos,
                    0,
                    $multi
                ];
                $this->console->info(implode(';', $data));
            }
        }
        $this->console->info("Proceso Finalizado;;;;;;;");
    }

    public function ajuste_categorias_estadistica($nit = '', $user = '', $sistem = '', $env = '')
    {
        ini_set('memory_limit', '3000M');
        $estadisticas = Xml86::where("periodo", '202301')->get();
        $year = date('Y');
        foreach ($estadisticas as $estadistica) {
            $trabajador =  Subsi15::where('cedtra', $estadistica->cedtra)->get()->first();
            $msalario = $trabajador->salario;
            $rqs  = DB::select("SELECT * FROM categoria_servicios where vigencia='{$year}' AND ($msalario >= valor_minimo AND $msalario <= valor_maximo) ");
            $categoria_servicio = collect($rqs)->first();

            $mcat = ($categoria_servicio) ? $categoria_servicio->categoria : 'D';
            if (intval($trabajador->tipcot) == 3 && $mcat == 'A') {
                $mcat = 'B';
            }

            if (intval($trabajador->tipcot) == 66) $mcat = 'A'; //POR FIDELIDAD

            $categoria = Subsi30::where('codcat', $mcat)->get()->first();
            $this->console->info($categoria->codcat_circular . '|' . $trabajador->cedtra . '|' . $trabajador->nit . '|' . $estadistica->codcat);
            $estadistica->codcat = $categoria->codcat_circular;
            $estadistica->save();
        }
    }

    public function validaSectorAgro($argv, $user = '', $sistem = '', $env = '')
    {
        ini_set('memory_limit', '3000M');

        $giroCuotas = DB::select("SELECT 
        subsi14.codben,
        subsi14.cedres,
        subsi14.cedtra,
        subsi14.numcuo as cuotas, 
        (subsi14.valor+subsi14.valaju-subsi14.valcre) AS valor,
        subsi14.tippag as tippag,
        subsi14.codsuc as codsuc,
        subsi14.nit as nit,
        subsi14.captra, 
        subsi14.propag,  
        subsi14.muetra 
        FROM subsi14 
        WHERE 
        subsi14.pergir='202301'");

        $header = [
            'Codben',
            'Cedres',
            'Cedtra',
            'Cuotas',
            'Valor',
            'Tipo_pago',
            'Sucursal',
            'Nit',
            'Estadistica',
            'AgroReal',
            'AgroTra'
        ];

        $this->console->info(implode('|', $header));

        foreach ($giroCuotas as $cuota) {
            $empresa = Subsi48::where('nit', $cuota->nit)->where('codsuc', $cuota->codsuc)->get()->first();
            $esAgro = Subsi04::where('codact', $empresa->codact)->where('esagro', '>', '0')->count();
            $esAgroTra = Subsi15::where('cedtra', $cuota->cedtra)->where('agro', 'S')->count();

            $num = 0;
            if ($cuota->captra == 'I' && $cuota->propag  != 'M' && $cuota->muetra != 'S') {
                $num++;
            }

            $data = [
                $cuota->codben,
                $cuota->cedres,
                $cuota->cedtra,
                $cuota->cuotas,
                $cuota->valor,
                $cuota->tippag,
                $cuota->codsuc,
                $cuota->nit,
                $num,
                $esAgro,
                $esAgroTra
            ];
            $this->console->info(implode('|', $data));
        }
    }

    public function sanetizar($string)
    {
        $string = str_replace("NÃ¯Â¿Â½", "N", $string);
        $string = str_replace("ALPÃ‰S", "ALPES", $string);
        $string = str_replace("URBANIZACIÃ“N", "URBANIZACION", $string);
        $string = str_replace("NÃ‚Âº", "NU", $string);
        $string = str_replace("CARRERA", "CR", $string);
        $string = str_replace(array('Ã±', "Ã`", "Ã‘", 'ÑOB', '?', 'Ñ'), 'N', $string);
        $string = str_replace("NÂ°", "NU", $string);
        $string = str_replace("N°", "NU", $string);
        $string = str_replace("NIÃAA½O", "NINO", $string);
        $string = str_replace("\t", " ", $string);
        $string = str_replace("\n", " ", $string);

        $string = str_replace(
            array(
                "¨", "º", "-", "~", "·", ",", "$", "%", "&", "/", "°", "*",
                "(", ")", "?", "'", "¡",
                "¿", "[", "^", "<code>", "]",
                "+", "}", "{", "¨", "´",
                ">", "< ", ";", ",", ":", "Ã", "Â", "¿"
            ),
            ' ',
            $string
        );
        return $string;
    }
}
