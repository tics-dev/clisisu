<?php

namespace App\Console\Servicios\Comandos;

class Comandos
{

    public static $proporcion = 0;
    public static $cantidad  = 0;
    public static $progreso = 0;
    public static $comando = null;
    protected $proceso;
    protected $usuario;

    public static function setProporcion()
    {
        if (self::$cantidad <= 1000) self::$proporcion = 10;
        if (self::$cantidad > 1000 and self::$cantidad <= 10000) self::$proporcion = 100;
        if (self::$cantidad > 10000 and self::$cantidad <= 100000) self::$proporcion = 1000;
        if (self::$cantidad > 100000) self::$proporcion = 10000;
        return self::$proporcion;
    }

    public function getComando()
    {
        return self::$comando;
    }

    public function getProceso()
    {
        $this->proceso = (self::$comando) ? self::$comando->proceso : false;
        return $this->proceso;
    }

    public function salvar()
    {
        if (is_null(self::$comando) == false && empty(self::$comando) == false) {
            self::$comando->save();
        }
    }

    public function setProgreso($progreso)
    {
        if (is_null(self::$comando) == false && empty(self::$comando) == false) {
            self::$comando->progreso = ($progreso >= 100) ? 100 : $progreso;
            self::$progreso = self::$comando->progreso;
            if ($progreso >= 100) self::$comando->estado = 'F';
            self::$comando->save();
        } else {
            return false;
        }
    }
}
