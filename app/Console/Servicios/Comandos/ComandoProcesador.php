<?php

namespace App\Console\Servicios\Comandos;

use App\Exceptions\DebugException;
use App\Models\Comando;
use App\Models\ComandoEstructura;

use App\Models\ComfacaComando;
use App\Models\ComfacaComandoEstructura;

interface InComando
{

    public function actualizaProgreso(int $progreso);
    public function buscarComando(?int $id, string $fragmentoLinea);
    public function cancelarComando();
}

class ComandoProcesador extends Comandos implements InComando
{

    private $idComando;

    private $procesadorComando;

    private $estructuraProcesador;

    protected $defaultComando = Comando::class;

    protected $defaultEstructura = ComandoEstructura::class;

    protected $factoryComando = [
        'Mercurio' => Comando::class,
        'SYS' => ComfacaComando::class,
        'comfacaonline' => Comando::class,
        'sisuweb' => ComfacaComando::class
    ];

    protected $factoryEstructura = [
        'Mercurio' => ComandoEstructura::class,
        'SYS' => ComfacaComandoEstructura::class,
        'comfacaonline' => ComandoEstructura::class,
        'sisuweb' => ComfacaComandoEstructura::class
    ];

    public function __construct($mediaType, $usuario)
    {
        $this->usuario = $usuario;
        if (!$mediaType) return false;
        $this->procesadorComando = $this->factoryComando["{$mediaType}"];
        $this->estructuraProcesador = $this->factoryEstructura["{$mediaType}"];
    }

    public function actualizaProgreso(int $progreso)
    {
        if (!self::$cantidad || self::$cantidad == 0) return false;
        $progreso = ($progreso / self::$cantidad) * 100;
        $this->setProgreso($progreso);
    }

    public function finalizarProceso($resultado = '')
    {
        if (is_null(self::$comando) == false && empty(self::$comando) == false) {
            if ($resultado !== '') {
                self::$comando->resultado = $resultado;
            }
            $this->setProgreso(100);
        }
    }

    public function buscarComando(?int $id, string $fragmentoLinea)
    {
        if (!$this->procesadorComando) throw new DebugException("Error el procesador no es valido para continuar", 1);

        if (!empty($id) && !is_null($id) && $id > 0) {

            $this->idComando = $id;
            self::$comando = $this->procesadorComando::where("usuario", $this->usuario)
                ->where('id', $id)
                ->get()
                ->first();
        } else {
            self::$comando = $this->procesadorComando::where("usuario", $this->usuario)
                ->where('fecha_runner', date('Y-m-d'))
                ->where('linea_comando', 'like', '%' . $fragmentoLinea . '%')
                ->get()
                ->first();

            $this->idComando = (self::$comando) ? self::$comando->id : false;
        }
    }

    public function cancelarComando()
    {
        if (is_null(self::$comando) == false && empty(self::$comando) == false) {

            self::$comando->estado = 'X';
            return $this->salvar();
        } else {
            if ($this->idComando) {
                self::$comando = $this->procesadorComando::where("usuario", $this->usuario)
                    ->where('id', $this->idComando)
                    ->get()
                    ->first();

                if (is_null(self::$comando) == false && empty(self::$comando) == false) {
                    self::$comando->estado = 'X';
                    return $this->salvar();
                }
            }
            return false;
        }
    }
}
