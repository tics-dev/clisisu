<?php

use App\Exceptions\DebugException;
use App\Console\Servicios\General\General;
use App\Services\MercurioServicios\MercurioBeneficiario;
use App\Services\MercurioServicios\MercurioConyuge;
use App\Services\MercurioServicios\MercurioEmpresa;
use App\Services\MercurioServicios\MercurioTrabajador;
use App\Services\Company\InformationCompany;
use App\Services\Poblacion\InformationBeneficiarios;
use App\Services\Poblacion\InformationConyuges;
use App\Services\Poblacion\InformationTrabajadores;
use Illuminate\Database\QueryException;
use App\Libraries\MyParams;

class ComfacaEmpresas
{
    protected $numtraccf;
    protected $usuario;
    protected $sistema;
    protected $user;
    protected $fecha;
    protected $console;

    public function __construct($console)
    {
        $this->console = $console;
        $this->fecha = date('Y-m-d');
    }

    public function informacion_empresa($argv, $user, $sistema, $enviroment)
    {
        $this->sistema = $sistema;
        $this->user = $user;
        try {
            try {
                $jd = json_decode(base64_decode($argv));
                $estados = (isset($jd->estado)) ?  ["{$jd->estado}"] : ['A', 'I', 'S', 'D'];
                $informationCompany = new InformationCompany();
                $resultado = $informationCompany->consultaEmpresa(
                    new MyParams(
                        [
                            'estado' => $estados,
                            'nit' => $jd->nit
                        ]
                    )
                );

                $this->console->info(json_encode($resultado));
                General::logClose();
            } catch (QueryException $th) {
                throw new DebugException($th->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'estado'  => 'X',
                'success' => false
            );
            General::changeNoteLog($err->getMessage());
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    public function buscar_sucursales_en_empresa($nit, $user, $sistema, $enviroment)
    {
        $this->sistema = $sistema;
        $this->user = $user;
        try {

            $informationCompany = new InformationCompany();
            $data = $informationCompany->consultaSucursales(
                new MyParams(
                    [
                        'nit' => $nit
                    ]
                )
            );
            $this->console->info(json_encode($data));
        } catch (DebugException $err) {
            $salida = array(
                "success" => false,
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array()
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    public function buscar_listas_en_empresa($nit, $user, $sistema, $enviroment)
    {
        $this->sistema = $sistema;
        $this->user = $user;
        try {

            $informationCompany = new InformationCompany();
            $data = $informationCompany->consultaListas(
                new MyParams(
                    [
                        'nit' => $nit
                    ]
                )
            );
            $this->console->info(json_encode($data));
        } catch (DebugException $err) {
            $salida = array(
                "success" => false,
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array()
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    /**
     * informacion_trabajador function
     * buscar informacion puntual del trabajador, para proceso de aprobacion
     * @param [type] $argv
     * @param [type] $user
     * @param [type] $sistema
     * @param [type] $enviroment
     * @return void
     */
    public function informacion_trabajador($cedtra, $user, $sistema, $enviroment)
    {
        $this->sistema = $sistema;
        $this->user = $user;
        try {
            try {

                $estados = ['A', 'I', 'M'];
                $informationTrabajadores = new InformationTrabajadores();
                $data = $informationTrabajadores->buscar(
                    new MyParams(
                        [
                            'cedtra' => $cedtra,
                            'estados' => $estados
                        ]
                    )
                );
                $this->console->info(json_encode($data));
                General::logClose();
            } catch (QueryException $th) {
                throw new DebugException($th->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'estado'  => 'X',
                'success' => false
            );
            General::changeNoteLog($err->getMessage());
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    public function informacion_conyuge($cedcon, $user, $sistema, $enviroment)
    {
        $this->sistema = $sistema;
        $this->user = $user;
        try {
            try {

                $informationConyuges = new InformationConyuges();
                $data = $informationConyuges->buscar(
                    new MyParams(
                        [
                            'cedcon' => $cedcon
                        ]
                    )
                );

                $this->console->info(json_encode($data));
            } catch (QueryException $th) {
                throw new DebugException($th->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'estado'  => 'X',
                'success' => false
            );
            General::changeNoteLog($err->getMessage());
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    public function informacion_beneficiario($numdoc, $user, $sistema, $enviroment)
    {
        $this->sistema = $sistema;
        $this->user = $user;
        try {
            try {
                $informationBeneficiarios = new InformationBeneficiarios();
                $data = $informationBeneficiarios->buscar(
                    new MyParams(
                        [
                            'numdoc' => $numdoc
                        ]
                    )
                );
                $this->console->info(json_encode($data));
            } catch (QueryException $th) {
                throw new DebugException($th->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'estado'  => 'X',
                'success' => false
            );
            General::changeNoteLog($err->getMessage());
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    public function actualiza_empresa_enlinea($documento, $user, $sistema, $enviroment)
    {
        $this->sistema = $sistema;
        $this->user = $user;
        try {
            try {
                $mercurio = new MercurioEmpresa();
                $mercurio->validarEstado($documento);
                $resultado = $mercurio->getResultado();

                $this->console->info(json_encode($resultado));
            } catch (QueryException $th) {
                throw new DebugException($th->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : [],
                'estado'  => 'X',
                'success' => false
            );
            General::changeNoteLog($err->getMessage());
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    public function actualiza_trabajador_enlinea($documento, $user, $sistema, $enviroment)
    {
        $this->sistema = $sistema;
        $this->user = $user;
        try {
            try {
                $mercurio = new MercurioTrabajador();
                $mercurio->validarEstado($documento);
                $resultado = $mercurio->getResultado();
                $this->console->info(json_encode($resultado));
            } catch (QueryException $th) {
                throw new DebugException($th->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : [],
                'estado'  => 'X',
                'success' => false
            );
            General::changeNoteLog($err->getMessage());
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    public function actualiza_conyuge_enlinea($documento, $user, $sistema, $enviroment)
    {
        $this->sistema = $sistema;
        $this->user = $user;
        try {
            try {
                $mercurio = new MercurioConyuge();
                $mercurio->validarEstado($documento);
                $resultado = $mercurio->getResultado();
                $this->console->info(json_encode($resultado));
            } catch (QueryException $th) {
                throw new DebugException($th->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : [],
                'estado'  => 'X',
                'success' => false
            );
            General::changeNoteLog($err->getMessage());
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    public function actualiza_beneficiario_enlinea($documento, $user, $sistema, $enviroment)
    {
        $this->sistema = $sistema;
        $this->user = $user;
        try {
            try {
                $mercurio = new MercurioBeneficiario();
                $mercurio->validarEstado($documento);
                $resultado = $mercurio->getResultado();

                $this->console->info(json_encode($resultado));
            } catch (QueryException $th) {
                throw new DebugException($th->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : [],
                'estado'  => 'X',
                'success' => false
            );
            General::changeNoteLog($err->getMessage());
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }
}
