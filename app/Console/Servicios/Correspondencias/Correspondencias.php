<?php

declare(strict_types=1);

use App\Console\Servicios\Comandos\Comandos;
use App\Console\Servicios\Correspondencias\AvisoIncumplimiento;
use App\Console\Servicios\Estadisticas\PoblacionAfiliadosCargo;
use Illuminate\Database\QueryException;
use App\Exceptions\DebugException;
use App\Models\Gener02;
use App\Models\Gener09;
use App\Models\Subsi01;
use App\Models\Subsi20;
use App\Models\Subsi15;
use App\Models\Subsi172;
use Illuminate\Support\Facades\DB;

class Correspondencias
{

    protected $usuario;
    protected $sistema;
    protected $user;
    protected $console;
    protected $fecha;

    public function __construct($console)
    {
        $this->console = $console;
        $f = new DateTime('now');
        $this->fecha = $f->format('Y-m-d');
    }

    public function reportePrimerAviso($fecha_generacion, $user, $sistema, $enviroment)
    {
        try {
            try {

                $user = "203";

                $avisoIncumplimiento = new AvisoIncumplimiento();

                $out = $avisoIncumplimiento->primerAviso($fecha_generacion, $user);

                $this->console->info(json_encode($out));
            } catch (\Exception $th) {
                throw new DebugException($th->getMessage() . ' ' . $th->getLine() . ' ' . basename($th->getFile()), 501);
            } catch (QueryException $er) {
                throw new DebugException($er->getMessage() . ' ' . $er->getLine() . ' ' . basename($er->getFile()), 501);
            }
        } catch (DebugException $err) {
            $this->console->comandoProcesador->cancelarComando();
            $salida = array(
                "success" => false,
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode()
            );
            $this->console->error(json_encode($salida));
        }
    }

    public function reporteAvisoDesafiliacion($fecha_generacion, $user, $sistema, $enviroment)
    {
        $user = "203";
        ini_set('memory_limit', '2300M');
        try {
            try {

                $avisos = DB::table("subsi172")
                    ->select(
                        'subsi172.idreg',
                        'subsi172.periodo',
                        'subsi172.nit',
                        'subsi172.email',
                        'subsi172.valor',
                        'subsi172.fecenv',
                        'subsi172.usuario',
                        'subsi02.repleg',
                        'subsi02.direccion',
                        'subsi02.codzon',
                        'subsi02.razsoc',
                        'subsi02.telefono',
                        'subsi02.telr',
                    )
                    ->join('subsi02', 'subsi02.nit', '=', "subsi172.nit")
                    ->where('subsi172.fecgen', $fecha_generacion)
                    ->where('subsi172.enviado', 'N')
                    ->get();

                $caja = Subsi01::where('codapl', 'SU')->get()->first();
                $usuario = Gener02::where('usuario', $user)->get()->first();

                $unix = strtotime($this->fecha);
                $filepath = "/var/www/html/SYS/public/temp/avisos_correspondencia_{$unix}.csv";
                $file = fopen($filepath, "w+");
                fclose($file);

                setlocale(LC_MONETARY, 'es_CO');
                foreach ($avisos as $aviso) {
                    $zona = Gener09::where("codzon", $aviso->codzon)->get()->first();
                    $ciudad = $zona->detzon;
                    $telefono = ($aviso->telefono) ? $aviso->telefono : $aviso->telr;

                    $texto = [
                        $aviso->razsoc,
                        $aviso->email,
                        $aviso->direccion,
                        'DASU-SCF-29.1',
                        $aviso->repleg,
                        $aviso->nit,
                        $aviso->razsoc,
                        $telefono,
                        $ciudad,
                        $aviso->periodo,
                        '1011',
                        strtoupper($caja->jefapo),
                        $usuario->nombre,
                        '$ ' . number_format(intval($aviso->valor), 0, ',', '.')
                    ];

                    $info = implode(";", $texto);
                    file_put_contents($filepath, $info . "\r\n", FILE_APPEND);

                    $avisoCorrespondencia = Subsi172::where('idreg', $aviso->idreg)->get()->first();
                    $avisoCorrespondencia->enviado = 'S';
                    $avisoCorrespondencia->fecenv = $this->fecha;
                    $avisoCorrespondencia->save();
                }

                $salida = [
                    "success" => true,
                    "msj" => "Proceso completado con éxito",
                    "url" => 'public/temp/' . basename($filepath),
                    "filename" => basename($filepath)
                ];

                $this->console->info(json_encode($salida));
            } catch (\Exception $th) {
                throw new DebugException($th->getMessage() . ' ' . $th->getLine() . ' ' . basename($th->getFile()), 501);
            } catch (QueryException $er) {
                throw new DebugException($er->getMessage() . ' ' . $er->getLine() . ' ' . basename($er->getFile()), 501);
            }
        } catch (DebugException $err) {
            $this->console->comandoProcesador->cancelarComando();
            $salida = array(
                "success" => false,
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode()
            );
            $this->console->error(json_encode($salida));
        }
    }
}
