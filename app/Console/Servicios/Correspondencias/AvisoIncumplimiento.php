<?php

namespace App\Console\Servicios\Correspondencias;

use Illuminate\Support\Facades\DB;
use App\Models\Gener02;
use App\Models\Gener09;
use App\Models\Subsi01;
use App\Models\Subsi02;
use App\Models\Subsi172;
use App\Models\Subsi173;
use App\Models\Subsi48;
use DateTime;

class AvisoIncumplimiento
{

    protected $fecha;
    protected $user;

    public function __construct()
    {
        ini_set('memory_limit', '2300M');
        $f = new DateTime('now');
        $this->fecha = $f->format('Y-m-d');
    }

    public function primerAviso($fecha_generacion, $user)
    {
        $this->user = $user;
        $avisos = DB::table("subsi172")
            ->select(
                'subsi172.idreg',
                'subsi172.periodo',
                'subsi172.nit',
                'subsi172.email',
                'subsi172.valor',
                'subsi172.fecenv',
                'subsi172.usuario',
                'subsi172.codsuc',
                'subsi02.repleg',
                'subsi02.direccion',
                'subsi02.codzon',
                'subsi02.razsoc',
                'subsi02.telefono',
                'subsi02.telr'
            )
            ->join('subsi02', 'subsi02.nit', '=', "subsi172.nit")
            ->where('subsi172.fecgen', $fecha_generacion)
            ->where('subsi172.enviado', 'N')
            ->get();

        $caja = Subsi01::where('codapl', 'SU')->get()->first();
        $usuario = Gener02::where('usuario', $user)->get()->first();

        $unix = strtotime($this->fecha);
        $filepath = "/var/www/html/SYS/public/temp/avisos_correspondencia_{$unix}.csv";
        $file = fopen($filepath, "w+");
        fclose($file);

        setlocale(LC_MONETARY, 'es_CO');
        foreach ($avisos as $aviso) {
            $zona = Gener09::where("codzon", $aviso->codzon)->get()->first();
            $ciudad = $zona->detzon;
            $telefono = ($aviso->telefono) ? $aviso->telefono : $aviso->telr;

            $texto = [
                $aviso->razsoc,
                $aviso->email,
                $aviso->direccion,
                'DASU-SCF-29.1',
                $aviso->repleg,
                $aviso->nit,
                $aviso->razsoc,
                $telefono,
                $ciudad,
                $aviso->periodo,
                '1011',
                strtoupper($caja->jefapo),
                $usuario->nombre,
                '$ ' . number_format(intval($aviso->valor), 0, ',', '.')
            ];

            $info = implode(";", $texto);
            file_put_contents($filepath, $info . "\r\n", FILE_APPEND);

            $avisoCorrespondencia = Subsi172::where('idreg', $aviso->idreg)->get()->first();
            $avisoCorrespondencia->enviado = 'S';
            $avisoCorrespondencia->fecenv = $this->fecha;
            $avisoCorrespondencia->save();

            $this->registrarNovedad($aviso);

            $this->suspendeEmpresa($aviso->nit);
        }
        return [
            "success" => true,
            "msj" => "Proceso completado con éxito",
            "url" => 'public/temp/' . basename($filepath),
            "filename" => basename($filepath)
        ];
    }

    public function registrarNovedad($aviso)
    {
        $sucursal = Subsi48::where('nit', $aviso->nit)
            ->where('codsuc', $aviso->codsuc)
            ->get()
            ->first();

        $subsi173 = new Subsi173();
        $subsi173->idreg = null;
        $subsi173->usuario = $this->user;
        $subsi173->fecdig = $this->fecha;
        $subsi173->nit = $aviso->nit;
        $subsi173->codsuc = $aviso->codsuc;
        $subsi173->acccob = 1;
        $subsi173->fecacc = $this->fecha;
        $subsi173->valor = 0;
        $subsi173->nota = "AVISO DE INCUMPLIMIENTO - ENVIO MASIVO DE E-MAILS";
        $subsi173->nomcon = $sucursal->nomcon;
        $subsi173->telcon = $sucursal->telefono;
        $subsi173->concob = '2';
        $subsi173->ciecas = 'N';
        $subsi173->save();
    }

    public function suspendeEmpresa($nit)
    {
        $fecha = date('Y-m-d');
        Subsi02::where("nit", $nit)->update(["estado" => 'S', "codest" => 42, "fecest" => $fecha]);
    }
}
