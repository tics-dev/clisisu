<?php
use Illuminate\Support\Facades\DB;
use App\Console\Servicios\ServicioSat\WsSat;
use App\Console\Servicios\ServicioSat\Empresa;
use App\Console\Servicios\ServicioSat\Trabajador;
use App\Exceptions\DebugException;
use App\Models\Sat20;
use App\Models\Gener08;
use App\Models\Sat01;
use App\Models\Sat02;
use App\Models\Sat03;
use App\Models\Subsi02;
use App\Models\Sat09;
use GrahamCampbell\ResultType\Success;


class Independientes extends WsSat
{
    protected $console;
    public function __construct($console)
    {
        $this->console = $console;
    }

    /**
     * emplador_nuevo function
     * Emplador nuevo independiente 
     * @param [type] $argv
     * @param string $user
     * @param string $sistema
     * @param string $enviroment
     * @return void
     */
    public function emplador_nuevo($argv, $user='1', $sistema='sisuweb', $enviroment='2')
    {
        try {
            try {  
                $this->set_enviroment($enviroment);
                $jd = json_decode(base64_decode($argv));
                $nit = $jd->numdocemp;

                $empresa = new Empresa($nit);
                $empresa->nueva_independiente();

                $empresa->salvar();
                $data = $empresa->prepara_data_sat();
                $this->principal($data, $empresa->sat->getTable());
                $attr = $empresa->sat->makeVisible('attribute')->toArray();
                $attr['numtraccf'] = $empresa->numtraccf;
                $this->resultado['data'] = $attr;
                $this->resultado['success'] = true;
                $this->console->info(json_encode($this->resultado, JSON_NUMERIC_CHECK));
            } catch(ErrorException $ex) 
            {
                throw new DebugException($ex->getMessage(), 1);
            }
        } catch (DebugException $err)
        {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555)? DebugException::item("valid") : array(),
                'estado'  => 'X'
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    /**
     * empresa_reintegro function
     *
     * @param [type] $argv
     * @param string $user
     * @param string $sistema
     * @param string $enviroment
     * @return void
     */
    public function empresa_reintegro($argv, $user='1', $sistema='sisuweb', $enviroment='2')
    {
        try {
            $this->set_enviroment($enviroment);
            $jd = json_decode(base64_decode($argv));
            $nit = $jd->numdocemp;
            
            $empresa = new Empresa($nit);
            $empresa->reintegro_independiente();

            $empresa->salvar();
            $data = $empresa->prepara_data_sat();
            $this->principal($data, $empresa->sat->getTable());
            $attr = $empresa->sat->makeVisible('attribute')->toArray();

            $attr['numtraccf'] = $empresa->numtraccf;
            $this->resultado['data'] = $attr;
            $this->resultado['success'] = true;
            $this->console->info(json_encode($this->resultado, JSON_NUMERIC_CHECK));
        } catch (DebugException $err)
        {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555)? DebugException::item("valid") : array()
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    /**
     * empresa_retiro function
     * Empresa retiro 
     * @param [type] $argv
     * @param string $user
     * @param string $sistema
     * @param string $enviroment
     * @return void
     */
    public function empresa_retiro($argv, $user='1', $sistema='sisuweb', $enviroment='2')
    {
        try {
            $this->set_enviroment($enviroment);
            $jd = json_decode(base64_decode($argv));
            $nit = $jd->numdocemp;

            $empresa = new Empresa($nit);
            $empresa->retiro();

            $empresa->salvar();
            $data = $empresa->prepara_data_sat();
            $this->principal($data, $empresa->sat->getTable());
            $this->console->info(json_encode($this->resultado, JSON_NUMERIC_CHECK));
        } catch (DebugException $err)
        {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555)? DebugException::item("valid") : array()
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    /**
     * empresa_causa_grave function
     * Empresa causa grave
     * @param [type] $argv
     * @param string $user
     * @param string $sistema
     * @param string $enviroment
     * @return void
     */
    public function empresa_causa_grave($argv, $user='1', $sistema='sisuweb', $enviroment='2')
    {
        try {
            $this->set_enviroment($enviroment);
            $jd = json_decode(base64_decode($argv));
            $nit = $jd->numdocemp;
            $detalle = $jd->detalle;

            $empresa = new Empresa($nit);
            $empresa->causa_grave($detalle);

            $empresa->salvar();
            $data = $empresa->prepara_data_sat();
            $this->principal($data, $empresa->sat->getTable());
            $this->console->info(json_encode($this->resultado, JSON_NUMERIC_CHECK));
        } catch (DebugException $err)
        {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555)? DebugException::item("valid") : array()
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    /**
     * procesa_trabajador function
     * p7 artisan server:send ServicioSat procesa_trabajador '1110491952' 1 sisuweb
     * @param [type] $argv
     * @return void
     */
    public function procesa_trabajador($argv, $user='1', $sistema='sisuweb', $enviroment='2')
    {
        try {
            $this->set_enviroment($enviroment);
            $jd = json_decode(base64_decode($argv));
            $cedtra = $jd->cedtra; 

            $trabajador = new Trabajador($cedtra);
            $trabajador->iniciar_independientes();            
    
            $trabajador->salvar();
            $data = $trabajador->prepara_data_sat();
            $this->principal($data, $trabajador->sat->getTable());
            $this->console->info(json_encode($this->resultado, JSON_NUMERIC_CHECK));
        } catch (DebugException $err)
        {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555)? DebugException::item("valid") : array(),
                'success' => false
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    /**
     * termina_trabajador function
     * termina relacion laboral trabajador 
     * @param [type] $argv
     * @param string $user
     * @param string $sistema
     * @param string $enviroment
     * @return void
     */
    public function termina_trabajador($argv, $user='1', $sistema='sisuweb', $enviroment='2')
    {
        try {
            $this->set_enviroment($enviroment);
            $jd = json_decode(base64_decode($argv));
            $cedtra = $jd->cedtra; 

            $trabajador = new Trabajador($cedtra);
            $trabajador->terminar_independientes();            
            
            $trabajador->salvar();
            $data = $trabajador->prepara_data_sat();
            $this->principal($data, $trabajador->sat->getTable());
            $this->console->info(json_encode($this->resultado, JSON_NUMERIC_CHECK));
        } catch (DebugException $err)
        {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555)? DebugException::item("valid") : array(),
                'success' => false
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

}