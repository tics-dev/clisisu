<?php 
namespace  App\Console\Servicios\Novedades;

class AportesLog
{
    public static $file;
    public static $fileLog;

    public static function initLog($headers, $filename='')
    {
        self::$fileLog = ($filename != '')? $filename : "/var/www/html/SYS/public/temp/aportes_trabajador_".strtotime('now').".csv";
        self::$file = fopen(self::$fileLog, "w+");
        $texto_write = implode(';', $headers);
        fwrite(self::$file, strtoupper($texto_write) . "\t\n");
    }

    public static function closeLog()
    {
        fclose(self::$file);
        chmod(self::$fileLog, 0777);
    }

    public static function addLine($data)
    {
        $texto_write = implode(';', $data);
        fwrite(self::$file, strtoupper($texto_write) . "\t\n");
    }
}