<?php 
namespace  App\Console\Servicios\Novedades;

use Illuminate\Database\QueryException;
use App\Exceptions\DebugException;
use Illuminate\Support\Facades\DB;
use App\Models\Subsi160;
use App\Models\Subsi165;
use App\Models\Subsi15;
use App\Models\Subsi16;
use App\Models\Subsi168;
use App\Models\Subsi65;
use App\Models\Subsi64;
use App\Models\Subsi10;
use App\Models\Subsi12;

class AportesTrabajador
{

    protected $console;
    public static $procesados = array();
    public static $file;
    public static $fileLog;
    
    public function set_env($console)
	{
		$this->console = $console;
	}

}