<?php 
namespace  App\Console\Servicios\Novedades;

use Illuminate\Database\QueryException;
use App\Exceptions\DebugException;
use App\Models\Subsi12;
use Illuminate\Support\Facades\DB;
use App\Models\Subsi16;
use App\Models\Subsi160;
use App\Models\Subsi64;
use DateTime;

class AportesTrabajadorNoAfiliado
{

    protected $console;
    private $empresa_aportante;
    private $agente_responsable;
    public static $procesados = array();
    public static $file;
    public static $fileLog;

    public function set_env($console)
	{
		$this->console = $console;
	}
    
    public static function initLog($sistema)
    {
        AportesLog::initLog(
            ['nit','codsuc','cedtra','perapo','idreg','numero','aportante','nota','periodos','estado'],
            "/var/www/html/{$sistema}/public/temp/aportes_trabajador_".strtotime('now').".csv"
        );
    }

    /**
     * aportesTrabajadorNoAfiliado function
     * Una vez el trabajador está afiliado esta novedad se debe cerrar
     * @param [type] $novedad
     * @return void
     */
    public function principal($novedad)
    {
        $novedad = Subsi160::where('idreg', $novedad->idreg)
        ->where('estado', 'A')
        ->get()
        ->first();
        
        if(!$novedad) return false;

        //responsable por quien se debe pagar el aporte Subsi161
        $this->agente_responsable = $novedad->agente;

        //subsi162 empresa sucursal morosa solicitante
        $this->empresa_aportante = $novedad->solicitante;

        //buscamos las planillas donde se hace el pago de los aportes, por terceros
        $aporte = Subsi64::select(
            'subsi64.nit',
            'subsi64.nitcaj', 
            'subsi64.razpla', 
            'subsi64.nit', 
            'subsi64.codciu', 
            'subsi64.coddep',
            'subsi64.codsuc', 
            'subsi64.numero',
            'subsi64.perapo'
        )
        ->where("numero", $this->empresa_aportante->num)
        ->where("nit", $this->empresa_aportante->nit)
        ->where("nitcaj", '891190047')
        ->where("codsuc", $this->empresa_aportante->codsuc)
        ->get()
        ->first();

        if(!$aporte) return false;

        //se busca los trabajadores en planillas relacionadas por terceros
        $aporte_trabajador = DB::table('subsi65')
        ->select(
            'subsi64.codsuc',
            'subsi64.nit',
            'subsi65.cedtra',
            'subsi65.novret', 
            'subsi64.numero', 
            'subsi65.valnom', 
            'subsi65.valapo', 
            'subsi64.perapo'
        )
        ->join('subsi64', 'subsi64.numero', '=', 'subsi65.numero', 'INNER')
        ->where("subsi65.cedtra", $this->agente_responsable->cedtra)
        ->where("subsi64.perapo", $aporte->perapo)
        ->where("subsi64.nit",    $this->empresa_aportante->nit)
        ->where("subsi64.codsuc", $this->empresa_aportante->codsuc)
        ->get()
        ->first();

        if($aporte_trabajador)
        {
            $trayectos = Subsi16::where("cedtra", $this->agente_responsable->cedtra)
            ->where('nit', $this->empresa_aportante->nit)
            ->where('codsuc', $this->empresa_aportante->codsuc)
            ->get();

            $has_trayecto = false;
            $periodos_activo = array();

            foreach ($trayectos as $trayecto) 
            {  
                //aportes dentro del trayecto de afiliación
                //trayecto vigente
                $fecafi = new DateTime($trayecto->fecafi);
                $periodo_ini_trayecto = $fecafi->format('Ym');

                if(is_null($trayecto->fecret)) {

            
                    if($periodo_ini_trayecto <= $aporte->perapo)
                    {
                        $has_trayecto = true;
                    }
                    $fecret = new DateTime(date('Y-m-d'));
                    $periodo_fin_trayecto = $fecret->format('Ym');
                    $periodos = Subsi12::where('periodo', '>=', $periodo_ini_trayecto)->where('periodo', '<=', $periodo_fin_trayecto)->get();
                    foreach ($periodos as $periodo)
                    {
                        $periodos_activo[] = $periodo->periodo;
                    }
                    
                } else {

                    //trayecto finalizado
                    $fecret = new DateTime($trayecto->fecret);
                    $periodo_fin_trayecto = $fecret->format('Ym');

                    if($periodo_ini_trayecto <= $aporte->perapo && $periodo_fin_trayecto >= $aporte->perapo)
                    {
                        $has_trayecto = true;
                    }

                    $periodos = Subsi12::where('periodo', '>=', $periodo_ini_trayecto)->where('periodo', '<=', $periodo_fin_trayecto)->get();
                    foreach ($periodos as $periodo)
                    {
                        $periodos_activo[] = $periodo->periodo;
                    }
                }
            }

            $retiro_trabajador = $this->aportesPosteriorRetiro($aporte->perapo);
            if($retiro_trabajador)
            {
                $has_trayecto = true;
            }

            if($has_trayecto)
            {
                AportesLog::addLine(
                [
                    'nit'=> $this->empresa_aportante->nit,
                    'codsuc'=> $this->empresa_aportante->codsuc,
                    'cedtra'=> $this->agente_responsable->cedtra,
                    'perapo'=> $aporte->perapo,
                    'idreg'=> $novedad->idreg,
                    'numero'=> $aporte_trabajador->numero,
                    'aportante' => $aporte_trabajador->codsuc,
                    'nota' => '',
                    'periodos'=> implode('|', $periodos_activo),
                    'estado' => 'CERRADO'
                ]);
                return true;
            } else {
                AportesLog::addLine(
                [
                    'nit'=> $this->empresa_aportante->nit,
                    'codsuc'=> $this->empresa_aportante->codsuc,
                    'cedtra'=> $this->agente_responsable->cedtra,
                    'perapo'=> $aporte->perapo,
                    'idreg'=> $novedad->idreg,
                    'numero'=> $aporte_trabajador->numero,
                    'aportante' => $aporte_trabajador->codsuc,
                    'nota' => '',
                    'periodos'=> implode('|', $periodos_activo),
                    'estado' => 'PENDIENTE'
                ]); 
            }
            
        } else {
            return false;
        }
    }

    public static function closeLog()
    {
        AportesLog::closeLog();
    }

    public function getResultado()
    {
        return [
            "success" => true,
            "msj" => "Proceso completado con éxito",
            "url" => 'public/temp/'.AportesLog::$fileLog,
            "filename" => basename(AportesLog::$fileLog) 
        ];
    }

    /**
     * aportesPosteriorRetiro function
     *  Reporte de aportes trabajador no afiliado con novedad de retiro
     * @param integer $perapo
     * @return void
     */
    public function aportesPosteriorRetiro($perapo) {
        
        return DB::table('subsi65')
        ->select(
            'subsi64.codsuc',
            'subsi64.nit',
            'subsi65.cedtra',
            'subsi65.novret', 
            'subsi64.numero', 
            'subsi65.valnom', 
            'subsi65.valapo', 
            'subsi64.perapo'
        )
        ->join('subsi64', 'subsi64.numero', '=', 'subsi65.numero', 'INNER')
        ->where("subsi65.cedtra", $this->agente_responsable->cedtra)
        ->where("subsi64.perapo", '>=', $perapo)
        ->where("subsi64.nit", $this->empresa_aportante->nit)
        ->where("subsi64.codsuc", $this->empresa_aportante->codsuc)
        ->whereNull("subsi65.ingtra")
        ->whereNotNull("subsi65.fecret")
        ->get()
        ->first();
    }

}
