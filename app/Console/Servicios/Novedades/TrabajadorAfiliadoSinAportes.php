<?php 
namespace  App\Console\Servicios\Novedades;

use Illuminate\Database\QueryException;
use App\Exceptions\DebugException;
use App\Models\Subsi10;
use App\Models\Subsi12;
use Illuminate\Support\Facades\DB;
use App\Models\Subsi160;
use App\Models\Subsi165;
use App\Models\Subsi15;
use App\Models\Subsi16;
use App\Models\Subsi168;
use App\Models\Subsi65;
use App\Models\Subsi64;

class TrabajadorAfiliadoSinAportes
{
    
    protected $console;
    private $empresa_aportante;
    private $agente_responsable;
    public static $procesados = array();
    public static $file;
    public static $fileLog;
    private static $validaSucursal;
    
    public function set_env($console)
	{
		$this->console = $console;
	}

    public static function initLog($sistema)
    {
        AportesLog::initLog([
            "Nit",
            "Sucursal novedad",
            "Cedula trabajador",
            "Periodo aporte",
            "Novedad",
            "Numero",
            "En nomina",
            "Sucursal aportante",
            "Nota"
        ],"/var/www/html/{$sistema}/public/temp/aportes_trabajador_".strtotime('now').".csv");
    }

    public static function closeLog()
    {
        AportesLog::closeLog();
    }

    public static function setValidaSucursal($valida)
    {
        self::$validaSucursal = $valida;
    }

    public function validaAportesNomina($cedtra, $nit, $periodo)
	{
		$aporteNomina = Subsi10::where("periodo", $periodo)
        ->where("nit", $nit)
        ->where("cedtra", $cedtra)
        ->get();

        if($aporteNomina)
        {
           return $aporteNomina->first();
		} else {
            return False;
        }
	}

    public function pilaSinAportes($novedad)
    {
        //responsable por quien se debe pagar el aporte Subsi161
        $this->agente_responsable = $novedad->agente;

        //subsi162 empresa sucursal morosa solicitante
        $this->empresa_aportante = $novedad->solicitante;

        $aporte = Subsi64::select('subsi64.nit','subsi64.nitcaj', 'subsi64.razpla', 'subsi64.nit', 'subsi64.codciu', 'subsi64.coddep','subsi64.codsuc', 'subsi64.numero','subsi64.perapo')
        ->where("numero", $this->empresa_aportante->num)
        ->where("nit", $this->empresa_aportante->nit);

        if(self::$validaSucursal === "SI"){
            $aporte = $aporte->where("codsuc", $this->empresa_aportante->codsuc)->where("nitcaj", '891190047');
        }
        $aportes = $aporte->get();
        if($aportes->count() == 0) return false;

        $periodos = [];
        foreach ($aportes as $aporte) {
            $periodos[] = $aporte->perapo;
        }

        $aporte_trabajador = DB::table('subsi65')->select('subsi64.codsuc','subsi64.nit','subsi65.cedtra','subsi65.novret','subsi64.numero','subsi65.valnom','subsi65.valapo','subsi64.perapo')
        ->join('subsi64', 'subsi64.numero', '=', 'subsi65.numero')
        ->where("subsi65.cedtra", $this->agente_responsable->cedtra)
        ->whereIn("subsi64.perapo", $periodos)
        ->where("subsi64.nit",    $this->empresa_aportante->nit);

        if(self::$validaSucursal === "SI"){
            $aporte_trabajador = $aporte_trabajador->where("subsi64.codsuc", $this->empresa_aportante->codsuc)->where("subsi64.nitcaj", '891190047');
        }
        $aportesTra = $aporte_trabajador->get();
        if($aportesTra->count() > 0)
        {
            $aporte_trabajador = $aportesTra->first();
            AportesLog::addLine([
                'nit'=>$this->empresa_aportante->nit,
                'codsuc'=>$this->empresa_aportante->codsuc,
                'cedtra'=>$this->agente_responsable->cedtra,
                'perapo'=> implode('|', $periodos),
                'idreg'=> $novedad->idreg,
                'numero'=> $aporte_trabajador->numero,
                'nomina'=>'NO',
                "aportante" => $aporte_trabajador->codsuc,
                'nota' => 'Aporte Planilla'
            ]);
            return true;
        }else{
            return false;
        }
    }

    public function trabajadorNominaSinAportes($novedad)
    {
        //responsable por quien se debe pagar el aporte Subsi161
        $this->agente_responsable = $novedad->agente;

        //subsi162 empresa sucursal morosa solicitante
        $this->empresa_aportante = $novedad->solicitante;

        $aporte = Subsi64::select('subsi64.nit','subsi64.nitcaj', 'subsi64.razpla', 'subsi64.nit', 'subsi64.codciu', 'subsi64.coddep','subsi64.codsuc', 'subsi64.numero','subsi64.perapo')
        ->where("numero", $this->empresa_aportante->num)
        ->where("nit", $this->empresa_aportante->nit);

        if(self::$validaSucursal === "SI") {
            $aporte = $aporte->where("subsi64.codsuc", $this->empresa_aportante->codsuc)->where("nitcaj", '891190047');
        }
        $aportes = $aporte->get();

        if($aportes->count() == 0) return false;
        $periodos = [];
        foreach ($aportes as $aporte) {
            $periodos[] = $aporte->perapo;
        }

        $aporte_trabajador = DB::table('subsi10')->select("nit","codsuc","periodo","cedtra")
        ->whereIn("subsi10.periodo", $periodos)
        ->where("subsi10.nit",    $this->empresa_aportante->nit)
        ->where("subsi10.cedtra", $this->agente_responsable->cedtra);

        if(self::$validaSucursal === "SI"){
            $aporte_trabajador = $aporte_trabajador->where("subsi10.codsuc", $this->empresa_aportante->codsuc);
        }
        $aporte_trabajador = $aporte_trabajador->get()->first();
        if($aporte_trabajador){
            AportesLog::addLine([
                'nit' => $this->empresa_aportante->nit,
                'codsuc' => $this->empresa_aportante->codsuc,
                'cedtra' => $this->agente_responsable->cedtra,
                'perapo' => implode('|', $periodos),
                'idreg' => $novedad->idreg,
                'numero' => '0',
                'nomina' =>'SI',
                'aportante' => $aporte_trabajador->codsuc,
                'nota' => 'Aporte Nomina'
            ]);
            return true;
        }else{
            return false;
        }
    }

    public function novedadSinPlanilla($novedad)
    {
        //responsable por quien se debe pagar el aporte Subsi161
        $this->agente_responsable = $novedad->agente;

        //subsi162 empresa sucursal morosa solicitante
        $this->empresa_aportante = $novedad->solicitante;

        $aporte = Subsi64::select('subsi64.nit','subsi64.nitcaj', 'subsi64.razpla', 'subsi64.nit', 'subsi64.codciu', 'subsi64.coddep','subsi64.codsuc', 'subsi64.numero','subsi64.perapo')
        ->where("numero", $this->empresa_aportante->num)
        ->where("nit", $this->empresa_aportante->nit);

        if(self::$validaSucursal === "SI"){
            $aporte = $aporte->where("codsuc", $this->empresa_aportante->codsuc)->where("nitcaj", '891190047');
        }
        $aporte = $aporte->get()->first();
        
        if(!$aporte){
            AportesLog::addLine([
                'nit'=> $this->empresa_aportante->nit,
                'codsuc'=> $this->empresa_aportante->codsuc,
                'cedtra'=> $this->agente_responsable->cedtra,
                'perapo'=> '',
                'idreg'=> $novedad->idreg,
                'numero'=> '',
                'nomina'=>'NO',
                "aportante" => '',
                'nota' => 'No existe planilla'
            ]);
            return true;
        }
        return false;
    }

    public function validaTrayectorias($novedad)
    {
        $this->agente_responsable = $novedad->agente;

        $this->empresa_aportante = $novedad->solicitante;

        $trayecto = Subsi16::where('cedtra', $this->agente_responsable->cedtra)
        ->where("nit", $this->empresa_aportante->nit);

        if(self::$validaSucursal === "SI"){
            $trayecto = $trayecto->where("subsi16.codsuc", $this->empresa_aportante->codsuc);
        }

        if($trayecto->get()->count() == 0){
            
            $trayecto = Subsi168::where('cedtra', $this->agente_responsable->cedtra)
            ->where("nit", $this->empresa_aportante->nit);

            if(self::$validaSucursal === "SI"){
                $trayecto = $trayecto->where("subsi168.codsuc", $this->empresa_aportante->codsuc);
            }

            if($trayecto->get()->count() == 0) {
                AportesLog::addLine([
                    'nit'=> $this->empresa_aportante->nit,
                    'codsuc'=> $this->empresa_aportante->codsuc,
                    'cedtra'=> $this->agente_responsable->cedtra,
                    'perapo'=> '',
                    'idreg'=> $novedad->idreg,
                    'numero'=> '',
                    'nomina'=>'NO',
                    "aportante" => '',
                    'nota' => 'No Trayectoria'
                ]);
                return true;
            }
        }
        return false;
    }

    public function getResultado()
    {
        return [
            "success" => true,
            "msj" => "Proceso completado con éxito",
            "url" => 'public/temp/'.AportesLog::$fileLog,
            "filename" => basename(AportesLog::$fileLog) 
        ];
    }
}
