<?php

namespace  App\Console\Servicios\Novedades;

use Illuminate\Database\QueryException;
use App\Exceptions\DebugException;
use App\Models\Subsi160;
use App\Models\Subsi165;
use Illuminate\Support\Facades\DB;

class GestionNovedades
{
	protected $resultado;
	protected $console;
	protected $enviroment;

	public function set_env($console, $enviroment)
	{
		$this->console = $console;
		$this->enviroment = $enviroment;
	}

	public function cerrarNovedad04($cedtra, $usuario)
	{
		$hoy = date('Y-m-d');
		$hora = date('H:i:s');
		$novedades = DB::table('subsi160')
			->select('subsi160.idreg')
			->leftJoin('subsi161', 'subsi161.idreg', '=', 'subsi160.idreg')
			->where('cedtra', $cedtra)
			->where('subsi160.tipmot', '04')
			->where('subsi160.estado', 'A')
			->get();

		foreach ($novedades as $novedad) {

			$id = $novedad->idreg;
			$secuencia = Subsi165::where("idreg", $id)->max('sec');
			$secuencia = ($secuencia) ? $secuencia + 1 : 1;

			$nuevo = new Subsi165();
			$nuevo->idreg = $id;
			$nuevo->sec = $secuencia;
			$nuevo->fecha = $hoy;
			$nuevo->hora = $hora;
			$nuevo->usuario = $usuario;
			$nuevo->tipcon = 6;
			$nuevo->nota = 'CIERRE AUTOMATICO DE NOVEDAD 04';
			$nuevo->ciecas = 'S';
			$nuevo->save();

			Subsi160::where('idreg', $id)->update([
				"estado" => 'C',
				"fecest" => $hoy
			]);
		}
	}

	public function getResultado()
	{
		return $this->resultado;
	}

	public static function cerrarNovedadAportesTrabajador(Subsi160 $novedad, $usuario)
	{
		$hoy = date('Y-m-d');
		$max_sec = Subsi165::where("idreg", $novedad->idreg)->max('sec');
		$hora = date('H:i:s');
		$subsi165 = new Subsi165();
		$subsi165->idreg = $novedad->idreg;
		$subsi165->sec = $max_sec  + 1;
		$subsi165->fecha = $hoy;
		$subsi165->hora = $hora;
		$subsi165->usuario = $usuario;
		$subsi165->tipcon = '6';
		$subsi165->nota = 'CIERRE NOVEDAD ' . $novedad->tipmot;
		$subsi165->ciecas = 'S';
		$subsi165->save();

		$novedad->estado = 'C';
		$novedad->nota = 'CIERRE NOVEDAD ' . $novedad->tipmot;
		$novedad->fecest = $hoy;
		$novedad->save();
	}
}
