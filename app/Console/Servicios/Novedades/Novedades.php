<?php

use App\Console\Servicios\Comandos\Comandos;
use App\Console\Servicios\Novedades\AportesLog;
use App\Console\Servicios\Novedades\AportesTrabajador;
use App\Console\Servicios\Novedades\AportesTrabajadorNoAfiliado;
use App\Console\Servicios\Novedades\GestionNovedades;
use App\Console\Servicios\Novedades\TrabajadorAfiliadoSinAportes;
use App\Exceptions\DebugException;
use App\Models\Comando;
use App\Models\Subsi160;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class Novedades
{
    protected $console;
    protected $trabajadorAfiliadoSinAportes;
    protected $aportesTrabajadorNoAfiliado;
    protected $fecha;

    public function __construct($console)
    {
        ini_set('memory_limit', '2000M');
        $this->console = $console;
        $this->fecha = date('Y-m-d');
        $this->trabajadorAfiliadoSinAportes = new TrabajadorAfiliadoSinAportes();
        $this->aportesTrabajadorNoAfiliado = new AportesTrabajadorNoAfiliado();
        $this->trabajadorAfiliadoSinAportes->set_env($this->console);
        $this->aportesTrabajadorNoAfiliado->set_env($this->console);
    }

    /**
     * aportes_trabajador_noafiliado function
     * novedad 04
     * @param [type] $argv
     * @param [type] $user
     * @param [type] $sistema
     * @param [type] $env
     * @return void
     */
    public function aportes_trabajador_noafiliado($argv, $user, $sistema, $env)
    {
        try {
            $modelo = json_decode(base64_decode($argv), true);
            $novedades = Subsi160::where("subsi160.tipmot", '04')
                ->where("subsi160.estado", 'A')
                ->where("subsi160.fecha", ">=", $modelo['fechaini'])
                ->where("subsi160.fecha", "<=", $modelo['fechafin'])
                ->orderBy("subsi160.fecha", "ASC")
                ->get();

            Comandos::$cantidad = $novedades->count();
            Comandos::setProporcion();
            $fraccion = Comandos::$cantidad / Comandos::$proporcion;
            $progreso_ini_conteo = 0;
            $num = 0;

            AportesTrabajadorNoAfiliado::initLog($sistema);
            foreach ($novedades as $novedad) {
                $num++;
                $progreso_ini_conteo++;
                if ($progreso_ini_conteo >= $fraccion) {
                    $progreso_ini_conteo = 0;
                    $this->console->comandoProcesador->actualizaProgreso($num);
                }
                $has = $this->aportesTrabajadorNoAfiliado->principal($novedad);

                if ($has && $modelo['procesar'] == 'SI') {
                    GestionNovedades::cerrarNovedadAportesTrabajador($novedad, $user);
                }
            }
            AportesLog::closeLog();

            $this->console->comandoProcesador->finalizarProceso();
            $salida = $this->aportesTrabajadorNoAfiliado->getResultado();
            $this->console->info(json_encode($salida));
        } catch (DebugException $err) {
            $this->console->comandoProcesador->cancelarComando();
            $salida = array(
                "success" => false,
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode()
            );
            $this->console->error(json_encode($salida));
        }
    }

    /**
     * trabajador_afiliado_sinaportes function
     * novedad 04
     * @param [type] $argv
     * @param [type] $user
     * @param [type] $sistema
     * @param [type] $env
     * @return void
     */
    public function trabajador_afiliado_sinaportes($argv, $user, $sistema, $env)
    {
        try {
            try {
                $modelo = json_decode(base64_decode($argv), true);
                $novedades = Subsi160::where("subsi160.tipmot", '03')
                    ->where("subsi160.estado", 'A')
                    ->where("subsi160.fecha", ">=", $modelo['fechaini'])
                    ->where("subsi160.fecha", "<=", $modelo['fechafin'])
                    ->orderBy("subsi160.fecha", "ASC")
                    ->get();

                Comandos::$cantidad = $novedades->count();
                Comandos::setProporcion();
                $fraccion = Comandos::$cantidad / Comandos::$proporcion;
                $progreso_ini_conteo = 0;
                $num = 0;

                TrabajadorAfiliadoSinAportes::setValidaSucursal($modelo['valida_sucursal']);
                TrabajadorAfiliadoSinAportes::initLog($sistema);
                foreach ($novedades as $novedad) {
                    $num++;
                    $progreso_ini_conteo++;
                    if ($progreso_ini_conteo >= $fraccion) {
                        $progreso_ini_conteo = 0;
                        $this->console->comandoProcesador->actualizaProgreso($num);
                    }
                    $has = $this->trabajadorAfiliadoSinAportes->pilaSinAportes($novedad);
                    if (!$has) {
                        $has = $this->trabajadorAfiliadoSinAportes->trabajadorNominaSinAportes($novedad);
                    }
                    if ($has && $modelo['procesar'] == 'SI') {
                        GestionNovedades::cerrarNovedadAportesTrabajador($novedad, $user);
                    }
                }
                AportesLog::closeLog();

                $this->console->comandoProcesador->finalizarProceso();
                $salida = $this->trabajadorAfiliadoSinAportes->getResultado();
                $this->console->info(json_encode($salida));
            } catch (\Exception $th) {
                throw new DebugException($th->getMessage() . ' ' . $th->getLine() . ' ' . basename($th->getFile()), 501);
            } catch (QueryException $er) {
                throw new DebugException($er->getMessage() . ' ' . $er->getLine() . ' ' . basename($er->getFile()), 501);
            }
        } catch (DebugException $err) {
            $this->console->comandoProcesador->cancelarComando();
            $salida = array(
                "success" => false,
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode()
            );
            $this->console->error(json_encode($salida));
        }
    }

    /**
     * novedad_trabajador_sin_planilla function
     * novedad 03
     * @param [type] $argv
     * @param [type] $user
     * @param [type] $sistema
     * @param [type] $env
     * @return void
     */
    public function novedad_trabajador_sin_planilla($argv, $user, $sistema, $env)
    {
        try {
            try {
                $modelo = json_decode(base64_decode($argv), true);
                $novedades = Subsi160::where("subsi160.tipmot", '03')
                    ->where("subsi160.estado", 'A')
                    ->where("subsi160.fecha", ">=", $modelo['fechaini'])
                    ->where("subsi160.fecha", "<=", $modelo['fechafin'])
                    ->orderBy("subsi160.fecha", "ASC")
                    ->get();

                Comandos::$cantidad = $novedades->count();
                Comandos::setProporcion();
                $fraccion = Comandos::$cantidad / Comandos::$proporcion;
                $progreso_ini_conteo = 0;
                $num = 0;

                TrabajadorAfiliadoSinAportes::setValidaSucursal($modelo['valida_sucursal']);
                TrabajadorAfiliadoSinAportes::initLog($sistema);
                foreach ($novedades as $novedad) {
                    $num++;
                    $progreso_ini_conteo++;
                    if ($progreso_ini_conteo >= $fraccion) {
                        $progreso_ini_conteo = 0;
                        $this->console->comandoProcesador->actualizaProgreso($num);
                    }
                    $has = $this->trabajadorAfiliadoSinAportes->novedadSinPlanilla($novedad);

                    if ($has && $modelo['procesar'] == 'SI') {
                        GestionNovedades::cerrarNovedadAportesTrabajador($novedad, $user);
                    }
                }
                AportesLog::closeLog();

                $this->console->comandoProcesador->finalizarProceso();
                $salida = $this->trabajadorAfiliadoSinAportes->getResultado();
                $this->console->info(json_encode($salida));
            } catch (\Exception $th) {
                throw new DebugException($th->getMessage() . ' ' . $th->getLine() . ' ' . basename($th->getFile()), 501);
            } catch (QueryException $er) {
                throw new DebugException($er->getMessage() . ' ' . $er->getLine() . ' ' . basename($er->getFile()), 501);
            }
        } catch (DebugException $err) {
            $this->console->comandoProcesador->cancelarComando();
            $salida = array(
                "success" => false,
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode()
            );
            $this->console->error(json_encode($salida));
        }
    }

    /**
     * novedad_trabajador_trayectoria function
     * novedad 03
     * @param [type] $argv
     * @param [type] $user
     * @param [type] $sistema
     * @param [type] $env
     * @return void
     */
    public function novedad_trabajador_trayectoria($argv, $user, $sistema, $env)
    {
        try {
            try {
                $modelo = json_decode(base64_decode($argv), true);
                $novedades = Subsi160::where("subsi160.tipmot", '03')
                    ->where("subsi160.estado", 'A')
                    ->where("subsi160.fecha", ">=", $modelo['fechaini'])
                    ->where("subsi160.fecha", "<=", $modelo['fechafin'])
                    ->orderBy("subsi160.fecha", "ASC")
                    ->get();

                Comandos::$cantidad = $novedades->count();
                Comandos::setProporcion();
                $fraccion = Comandos::$cantidad / Comandos::$proporcion;
                $progreso_ini_conteo = 0;
                $num = 0;

                TrabajadorAfiliadoSinAportes::setValidaSucursal($modelo['valida_sucursal']);
                TrabajadorAfiliadoSinAportes::initLog($sistema);
                foreach ($novedades as $novedad) {
                    $num++;
                    $progreso_ini_conteo++;
                    if ($progreso_ini_conteo >= $fraccion) {
                        $progreso_ini_conteo = 0;
                        $this->console->comandoProcesador->actualizaProgreso($num);
                    }
                    $has = $this->trabajadorAfiliadoSinAportes->validaTrayectorias($novedad);
                    if ($has && $modelo['procesar'] == 'SI') {
                        GestionNovedades::cerrarNovedadAportesTrabajador($novedad, $user);
                    }
                }
                AportesLog::closeLog();

                $this->console->comandoProcesador->finalizarProceso();
                $salida = $this->trabajadorAfiliadoSinAportes->getResultado();
                $this->console->info(json_encode($salida));
            } catch (\Exception $th) {
                throw new DebugException($th->getMessage() . ' ' . $th->getLine() . ' ' . basename($th->getFile()), 501);
            } catch (QueryException $er) {
                throw new DebugException($er->getMessage() . ' ' . $er->getLine() . ' ' . basename($er->getFile()), 501);
            }
        } catch (DebugException $err) {
            $this->console->comandoProcesador->cancelarComando();
            $salida = array(
                "success" => false,
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode()
            );
            $this->console->error(json_encode($salida));
        }
    }

    public function prueba_novedades($argv, $user, $sistema, $env)
    {
        ini_set('memory_limit', '3024M');

        $sql = "SELECT 
            from subsi160 
            LEFT JOIN subsi161 ON subsi161.idreg = subsi160.idreg 
            LEFT JOIN subsi162 ON subsi162.idreg = subsi162.idreg 
            where subsi160.tipmot='04' and subsi160.estado='A' and (subsi160.fecha >= '2020-01-01' and subsi160.fecha <= '2023-03-01');
        ";

        $novedades = DB::table('subsi160')
            ->select(
                'subsi160.fecha',
                'subsi160.valcon',
                'subsi160.fecha',
                'subsi160.usuario',
                'subsi162.nit',
                'subsi161.cedtra',
                'subsi162.codsuc',
                'subsi64.perapo',
                'subsi64.tipapo'
            )
            ->join('subsi161', 'subsi161.idreg', '=', 'subsi160.idreg')
            ->join('subsi162', 'subsi162.idreg', '=', 'subsi160.idreg')
            ->join('subsi64', 'subsi64.numero', '=', 'subsi162.num', 'LEFT')
            ->where("subsi160.tipmot", '04')
            ->where("subsi160.estado", 'A')
            ->where("subsi64.nitcaj", "891190047")
            ->where("subsi160.fecha", '>=', '2022-01-01')
            ->where("subsi160.fecha", '<=', '2022-12-30')
            ->get();

        if ($novedades) {
            foreach ($novedades as $novedad) {
                $dataNovedad = [
                    'fecha' => $novedad->fecha,
                    'valcon' => intval($novedad->valcon),
                    'fecha' => $novedad->fecha,
                    'usuario' => $novedad->usuario,
                    'nit' => $novedad->nit,
                    'cedtra' => $novedad->cedtra,
                    'codsuc' => "'$novedad->codsuc'",
                    'perapo' => @$novedad->perapo,
                    'tipapo' => @$novedad->tipapo
                ];
                $this->console->info(implode(';', $dataNovedad));
            }
        }
    }
}
