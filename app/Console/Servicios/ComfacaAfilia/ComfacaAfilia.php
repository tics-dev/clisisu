<?php

use App\Exceptions\DebugException;
use App\Libraries\MyParams;
use Illuminate\Database\QueryException;
use App\Services\Afiliaciones\AfiliaBeneficiario;
use App\Services\Afiliaciones\AfiliaConyuge;
use App\Services\Afiliaciones\AfiliaEmpresa;
use App\Services\Afiliaciones\AfiliaIndependiente;
use App\Services\Afiliaciones\AfiliaTrabajador;
use App\Services\AfiliacionParametros\ParametrosBeneficiario;
use App\Services\AfiliacionParametros\ParametrosConyuge;
use App\Services\AfiliacionParametros\ParametrosEmpresa;
use App\Services\AfiliacionParametros\ParametrosIndependiente;
use App\Services\AfiliacionParametros\ParametrosPensionado;
use App\Services\AfiliacionParametros\ParametrosTrabajador;
use App\Services\ConsultasGenerales\ListarCiudadesDepartamentos;
use App\Services\Poblacion\ListarBeneficiarios;
use App\Services\Poblacion\ListarConyuges;
use App\Services\Poblacion\ListarTrabajadores;

class ComfacaAfilia
{
	protected $usuario;
	protected $sistema;
	protected $user;
	protected $afiliaTrabajador;
	protected $afiliaConyugue;
	protected $afiliaBeneficiario;
	protected $console;
	protected $fecha;
	protected $afiliaEmpresa;

	public function __construct($console)
	{
		$this->console = $console;
		$this->fecha = date('Y-m-d');
		$this->afiliaTrabajador = new AfiliaTrabajador();
		$this->afiliaConyugue = new AfiliaConyuge();
		$this->afiliaBeneficiario = new AfiliaBeneficiario();
		$this->afiliaEmpresa = new AfiliaEmpresa();
	}

	/**
	 * parametros_trabajador function
	 * Todos los parametros requeridos para hacer el registro de trabajador
	 * @param [type] $argv
	 * @param [type] $user
	 * @param [type] $sistema
	 * @param [type] $enviroment
	 * @return void
	 */
	public function parametros_trabajadores($argv, $user, $sistema, $enviroment)
	{
		$this->sistema = $sistema;
		$this->user = $user;
		try {
			$parametrosTrabajador = new ParametrosTrabajador();
			$params = $parametrosTrabajador->getParametros();
			$this->console->info(json_encode($params));
		} catch (DebugException $err) {
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode()
			);
			$this->console->error(json_encode($salida));
		}
	}

	public function listar_trabajadores($argv, $user, $sistema, $enviroment)
	{
		$this->sistema = $sistema;
		$this->user = $user;
		try {
			$jd = json_decode(base64_decode($argv));
			$nit = $jd->nit;
			$estados_trabajador = (empty($jd->estado)) ? ['A', 'T', 'I'] : ["$jd->estado"];
			$listarTrabajadores = new ListarTrabajadores();
			$data = $listarTrabajadores->listarByNit($nit, $estados_trabajador);
			$this->console->info(json_encode($data));
		} catch (DebugException $err) {
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode()
			);
			$this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
		}
	}

	public function parametros_conyuges($argv, $user, $sistema, $enviroment)
	{
		$this->sistema = $sistema;
		$this->user = $user;
		try {
			$parametrosConyuge = new ParametrosConyuge();
			$params = $parametrosConyuge->getParametros();
			$this->console->info(json_encode($params));
		} catch (DebugException $err) {
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode()
			);
			$this->console->error(json_encode($salida));
		}
	}

	public function listar_conyuges($argv, $user, $sistema, $enviroment)
	{
		$this->sistema = $sistema;
		$this->user = $user;
		try {
			$jd = json_decode(base64_decode($argv));
			$nit = $jd->nit;
			$estados = (empty($jd->estado)) ? ['A', 'I'] : ["$jd->estado"];
			$listarConyuges = new ListarConyuges();
			$data = $listarConyuges->listarByNit($nit, $estados);
			$this->console->info(json_encode($data));
		} catch (DebugException $err) {
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode()
			);
			$this->console->error(json_encode($salida));
		}
	}

	public function listar_conyuges_trabajador($cedtra, $user, $sistema, $enviroment)
	{
		$this->sistema = $sistema;
		$this->user = $user;
		try {
			$estados = ['A', 'I'];
			$listarConyuges = new ListarConyuges();
			$data = $listarConyuges->listarForTrabajador(
				new MyParams(array(
					'cedtra' => $cedtra,
					'estados' => $estados
				))
			);

			$this->console->info(json_encode($data));
		} catch (DebugException $err) {
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode()
			);
			$this->console->error(json_encode($salida));
		}
	}

	public function parametros_beneficiarios($argv, $user, $sistema, $enviroment)
	{
		$this->sistema = $sistema;
		$this->user = $user;
		try {
			$parametrosBeneficiario = new ParametrosBeneficiario();
			$params = $parametrosBeneficiario->getParametros();
			$this->console->info(json_encode($params));
		} catch (DebugException $err) {
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode()
			);
			$this->console->error(json_encode($salida));
		}
	}

	public function parametros_empresa($argv, $user, $sistema, $enviroment)
	{
		$this->sistema = $sistema;
		$this->user = $user;
		try {
			$parametrosEmpresa = new ParametrosEmpresa();
			$params = $parametrosEmpresa->getParametros();
			$this->console->info(json_encode($params));
		} catch (DebugException $err) {
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode()
			);
			$this->console->error(json_encode($salida));
		}
	}

	public function trabajador($argv, $user, $sistema, $enviroment)
	{
		$this->sistema = $sistema;
		$this->user = $user;
		try {
			$jd = json_decode(base64_decode($argv));
			$cedtra = $jd->cedtra;
			$estados_trabajador = (empty($jd->estado)) ? ['A', 'M', 'I'] : ["$jd->estado"];

			$listarTrabajadores = new ListarTrabajadores();
			$salida = $listarTrabajadores->buscarByCedtra(
				new MyParams(
					array(
						'cedtra' => $cedtra,
						'estados' => $estados_trabajador
					)
				)
			);

			$this->console->info(json_encode($salida));
		} catch (DebugException $err) {
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode()
			);
			$this->console->error(json_encode($salida));
		}
	}

	/**
	 * afilia_trabajador function
	 *  Metodo de afilicacion de trabajadores en sisuweb, recibe modelo de argumentos
	 *  Pemite crear el registro, cerrar la novedades 
	 * @param [type] $argv
	 * @param [type] $user
	 * @param [type] $sistema
	 * @param [type] $enviroment
	 * @return void
	 */
	public function afilia_trabajador($argv, $user, $sistema, $enviroment)
	{
		$this->sistema = $sistema;
		$this->user = $user;
		try {
			$modelo = json_decode(base64_decode($argv));
			$afilia = new AfiliaTrabajador();
			$afilia->procesar($modelo);
			$rqs = $afilia->getResultado();
			$this->console->info(json_encode($rqs));
		} catch (DebugException $err) {
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode(),
				'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array()
			);
			$this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
		}
	}

	public function afilia_empresa($argv, $user, $sistema, $enviroment)
	{
		$this->sistema = $sistema;
		$this->user = $user;
		try {
			$modelo = json_decode(base64_decode($argv));
			$afilia = new AfiliaEmpresa();
			$afilia->procesar($modelo);
			$result = $afilia->getResultado();
			$this->console->info(json_encode($result));
		} catch (DebugException $err) {
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode(),
				'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array()
			);
			$this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
		}
	}

	public function conyuge($argv, $user, $sistema, $enviroment)
	{
		$this->sistema = $sistema;
		$this->user = $user;
		try {
			$jd = json_decode(base64_decode($argv));
			$cedcon = $jd->cedcon;
			$estados_conyuge = (empty($jd->estado)) ? ['A', 'B', 'I'] : ["$jd->estado"];

			$listarConyuges = new ListarConyuges();
			$out = $listarConyuges->buscarByCedcon(
				new MyParams(
					array(
						'cedcon' => $cedcon,
						'estados' => $estados_conyuge
					)
				)
			);

			$this->console->info(json_encode($out));
		} catch (DebugException $err) {
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode()
			);
			$this->console->error(json_encode($salida));
		}
	}

	public function beneficiario($argv, $user, $sistema, $enviroment)
	{
		$this->sistema = $sistema;
		$this->user = $user;
		try {
			$jd = json_decode(base64_decode($argv));
			$numdoc = $jd->numdoc;
			$estados = (empty($jd->estado)) ? ['A', 'B', 'I'] : ["$jd->estado"];

			$listarBeneficiarios = new ListarBeneficiarios();
			$salida = $listarBeneficiarios->buscarByNumdoc(new MyParams(
				array(
					'numdoc' => $numdoc,
					'estados' => $estados
				)
			));
			$this->console->info(
				json_encode($salida)
			);
		} catch (DebugException $err) {
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode()
			);
			$this->console->error(json_encode($salida));
		}
	}

	/**
	 * afilia_beneficiario function
	 * @param [type] $argv
	 * @param [type] $user
	 * @param [type] $sistema
	 * @param [type] $enviroment
	 * @return void
	 */
	public function afilia_beneficiario($argv, $user, $sistema, $enviroment)
	{
		$this->sistema = $sistema;
		$this->user = $user;
		try {
			$modelo = json_decode(base64_decode($argv));
			$this->afiliaBeneficiario->procesar($modelo);
			$this->console->info(json_encode($this->afiliaBeneficiario->getResultado()));
		} catch (DebugException $err) {
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode(),
				'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array()
			);
			$this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
		}
	}

	/**
	 * afilia_conyuge function
	 * procesar la afiliación del conyuge
	 * @param [type] $argv
	 * @param [type] $user
	 * @param [type] $sistema
	 * @param [type] $enviroment
	 * @return void
	 */
	public function afilia_conyuge($argv, $user, $sistema, $enviroment)
	{
		$this->sistema = $sistema;
		$this->user = $user;
		try {
			$modelo = json_decode(base64_decode($argv));
			$this->afiliaConyugue->procesar($modelo);
			$this->console->info(json_encode($this->afiliaConyugue->getResultado()));
		} catch (DebugException $err) {
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode(),
				'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array()
			);
			$this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
		}
	}

	public function actualiza_beneficiario($argv, $user, $sistema, $enviroment)
	{
		$this->sistema = $sistema;
		$this->user = $user;
		try {
			$data = json_decode(base64_decode($argv));
			$numdoc = $data->numdoc;
			$modelo = $data->modelo;

			$this->afiliaBeneficiario->actualizarDatos($numdoc, $modelo);
			$this->console->info(json_encode($this->afiliaBeneficiario->getResultado()));
		} catch (DebugException $err) {
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode(),
				'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array()
			);
			$this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
		}
	}

	public function listar_ciudades_departamentos($argv, $user, $sistema, $enviroment)
	{
		$this->sistema = $sistema;
		$this->user = $user;
		try {
			try {
				$listarCiudadesDepartamentos = new ListarCiudadesDepartamentos();
				$params = $listarCiudadesDepartamentos->getData();
				$this->console->info(json_encode($params));
			} catch (QueryException $th) {
				throw new DebugException($th->getMessage(), 1);
			}
		} catch (DebugException $err) {
			$salida = array(
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode(),
				'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : [],
				'estado'  => 'X',
				'success' => false
			);
			$this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
		}
	}

	public function actualiza_empresa($argv, $user, $sistema, $enviroment)
	{
		$this->sistema = $sistema;
		$this->user = $user;
		try {
			$modelo = json_decode(base64_decode($argv));
			$this->afiliaEmpresa->actualizarDatos($modelo);
			$this->console->info(json_encode($this->afiliaEmpresa->getResultado()));
		} catch (DebugException $err) {
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode(),
				'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array()
			);
			$this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
		}
	}

	public function trabajador_empresa($argv, $user, $sistema, $enviroment)
	{
		$this->sistema = $sistema;
		$this->user = $user;
		try {
			$jd = json_decode(base64_decode($argv));
			$cedtra = $jd->cedtra;
			$estados_trabajador = (empty($jd->estado)) ? ['A', 'M', 'I'] : ["$jd->estado"];
			$nit = $jd->nit;

			$listarTrabajadores = new ListarTrabajadores();
			$data = $listarTrabajadores->buscarCedtraByNit(new MyParams(
				array(
					'cedtra' => $cedtra,
					'estados' => $estados_trabajador,
					'nit' => $nit,
				)
			));

			$this->console->info(json_encode($data));
		} catch (DebugException $err) {
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode()
			);
			$this->console->error(json_encode($salida));
		}
	}

	public function conyugue_trabajador_beneficiario($argv, $user, $sistema, $enviroment)
	{
		$this->sistema = $sistema;
		$this->user = $user;
		try {
			$jd = json_decode(base64_decode($argv));
			$listarBeneficiarios = new ListarBeneficiarios();
			$salida = $listarBeneficiarios->buscarBeneficiarioForCedtra(new MyParams(
				array(
					'cedtra' => $jd->cedtra,
					'documento' => $jd->documento
				)
			));

			$this->console->info(
				json_encode([
					"success" => ($salida != false),
					"data" => $salida
				])
			);
		} catch (DebugException $err) {
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode()
			);
			$this->console->error(json_encode($salida));
		}
	}

	/**
	 * parametros_pensionado function
	 * @changed [2023-12-20]
	 * @author elegroag <elegroag@ibero.edu.co>
	 * @param [type] $argv
	 * @param [type] $user
	 * @param [type] $sistema
	 * @param [type] $enviroment
	 * @return void
	 */
	public function parametros_pensionado($argv, $user, $sistema, $enviroment)
	{
		$this->sistema = $sistema;
		$this->user = $user;
		try {
			$parametrosPensionado = new ParametrosPensionado();
			$params = $parametrosPensionado->getParametros();
			$this->console->info(json_encode($params));
		} catch (DebugException $err) {
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode()
			);
			$this->console->error(json_encode($salida));
		}
	}

	/**
	 * parametros_independiente function
	 * @changed [2023-12-28]
	 * @author elegroag <elegroag@ibero.edu.co>
	 * @param [type] $argv
	 * @param [type] $user
	 * @param [type] $sistema
	 * @param [type] $enviroment
	 * @return void
	 */
	public function parametros_independiente($argv, $user, $sistema, $enviroment)
	{
		$this->sistema = $sistema;
		$this->user = $user;
		try {
			$parametrosIndependiente = new ParametrosIndependiente();
			$params = $parametrosIndependiente->getParametros();
			$this->console->info(json_encode($params));
		} catch (DebugException $err) {
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode()
			);
			$this->console->error(json_encode($salida));
		}
	}

	public function afilia_independiente($argv, $user, $sistema, $enviroment)
	{
		$this->sistema = $sistema;
		$this->user = $user;
		try {
			$modelo = json_decode(base64_decode($argv));
			$afilia = new AfiliaIndependiente();
			$afilia->procesar($modelo);
			$result = $afilia->getResultado();
			$this->console->info(json_encode($result));
		} catch (DebugException $err) {
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode(),
				'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array()
			);
			$this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
		}
	}
}
