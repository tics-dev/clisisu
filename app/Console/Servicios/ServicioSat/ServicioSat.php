<?php

use App\Services\ServicioSat\RestService\RestServiceFactoryImpl;
use App\Services\ServicioSat\Tareas;
use Illuminate\Support\Facades\DB;
use App\Exceptions\DebugException;
use App\Exceptions\GlozaException;
use App\Libraries\MyParams;
use App\Models\Gener18;
use App\Models\Sat02;
use App\Models\Sat03;
use App\Models\Sat06;
use App\Models\Sat08;
use App\Models\Sat09;
use App\Models\Sat10;
use App\Models\Sat13;
use App\Models\Sat15;
use App\Models\Sat20;
use App\Models\Subsi02;
use App\Models\Subsi15;
use App\Services\ServicioSat\Empresa;
use App\Services\ServicioSat\GlosaServices;
use App\Services\ServicioSat\PagoAportes;
use App\Services\ServicioSat\ReprocesarGlosas;
use App\Services\ServicioSat\Trabajador;
use Illuminate\Database\QueryException;

class ServicioSat
{
    protected $console;
    public function __construct($console)
    {
        $this->console = $console;
    }

    public function empresa_nueva($argv, $user = '1', $sistema = 'sisuweb', $enviroment = '2')
    {
        try {
            try {
                $jd = json_decode(base64_decode($argv));
                $empresa = new Empresa($jd->numdocemp);
                $empresa->nueva();
                $empresa->salvar();
                $data = $empresa->prepara_data_sat();

                $restServiceFactoryImpl = new RestServiceFactoryImpl();
                RestServiceFactoryImpl::setData($data);
                $rqs = $restServiceFactoryImpl->afiliacionPrimeraVezService();

                if ($rqs['codigo']) {
                    Sat02::where('numtraccf', $empresa->numtraccf)->update([
                        'codigo' =>  $rqs['codigo'],
                        'mensaje' => $rqs['mensaje']
                    ]);
                }
                $attr = $empresa->sat->makeVisible('attribute')->toArray();
                $attr['numtraccf'] = $empresa->numtraccf;

                $rqs['data'] = $attr;
                $rqs['modelo'] = $data;
                $rqs['success'] = true;

                $this->console->info(json_encode($rqs, JSON_NUMERIC_CHECK));
            } catch (QueryException $ex) {
                throw new DebugException($ex->getMessage(), 501);
            } catch (GlozaException $ge) {
                throw new DebugException($ge->getMessage(), 501);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'estado'  => 'X',
                'success' => false
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    public function empresa_reintegro($argv, $user = '1', $sistema = 'sisuweb', $enviroment = '2')
    {
        try {
            try {
                $jd = json_decode(base64_decode($argv));
                $empresa = new Empresa($jd->numdocemp);
                $empresa->reintegro();
                $empresa->salvar();
                $data = $empresa->prepara_data_sat();

                $restServiceFactoryImpl = new RestServiceFactoryImpl();
                RestServiceFactoryImpl::setData($data);
                $rqs = $restServiceFactoryImpl->afiliacionNoPrimeraVezService();

                if ($rqs['codigo']) {
                    Sat03::where('numtraccf', $empresa->numtraccf)->update([
                        'codigo' =>  $rqs['codigo'],
                        'mensaje' => $rqs['mensaje']
                    ]);
                }

                $attr = $empresa->sat->makeVisible('attribute')->toArray();
                $attr['numtraccf'] = $empresa->numtraccf;

                $rqs['data'] = $attr;
                $rqs['modelo'] = $data;
                $rqs['success'] = true;

                $this->console->info(json_encode($rqs, JSON_NUMERIC_CHECK));
            } catch (QueryException $ex) {
                throw new DebugException($ex->getMessage(), 1);
            } catch (GlozaException $ge) {
                throw new DebugException($ge->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'success' => false
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    public function empresa_retiro($argv, $user = '1', $sistema = 'sisuweb', $enviroment = '2')
    {
        try {
            try {
                $jd = json_decode(base64_decode($argv));
                $empresa = new Empresa($jd->numdocemp);
                $empresa->retiro();
                $empresa->salvar();
                $data = $empresa->prepara_data_sat();

                $restServiceFactoryImpl = new RestServiceFactoryImpl();
                RestServiceFactoryImpl::setData($data);
                $rqs = $restServiceFactoryImpl->desafiliacionService();

                if ($rqs['codigo']) {
                    Sat06::where('numtraccf', $empresa->numtraccf)->update([
                        'codigo' =>  $rqs['codigo'],
                        'mensaje' => $rqs['mensaje']
                    ]);
                }

                $attr = $empresa->sat->makeVisible('attribute')->toArray();
                $attr['numtraccf'] = $empresa->numtraccf;
                $rqs['data'] = $attr;
                $rqs['modelo'] = $data;
                $rqs['success'] = true;

                $this->console->info(json_encode($rqs, JSON_NUMERIC_CHECK));
            } catch (QueryException $ex) {
                throw new DebugException($ex->getMessage(), 1);
            } catch (GlozaException $ge) {
                throw new DebugException($ge->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'success' => false
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    public function empresa_causa_grave($argv, $user = '1', $sistema = 'sisuweb', $enviroment = '2')
    {
        try {
            try {
                $jd = json_decode(base64_decode($argv));
                $numdocemp = $jd->numdocemp;
                $detalle = $jd->detalle;

                $empresa = new Empresa($numdocemp);
                $empresa->causa_grave($detalle);
                $empresa->salvar();
                $data = $empresa->prepara_data_sat();

                $restServiceFactoryImpl = new RestServiceFactoryImpl();
                RestServiceFactoryImpl::setData($data);
                $rqs = $restServiceFactoryImpl->perdidaAfiliacionCausaGraveService();

                if ($rqs['codigo']) {
                    Sat08::where('numtraccf', $empresa->numtraccf)->update([
                        'codigo' =>  $rqs['codigo'],
                        'mensaje' => $rqs['mensaje']
                    ]);
                }

                $attr = $empresa->sat->makeVisible('attribute')->toArray();
                $attr['numtraccf'] = $empresa->numtraccf;
                $rqs['data'] = $attr;
                $rqs['modelo'] = $data;
                $rqs['success'] = true;

                $this->console->info(json_encode($rqs, JSON_NUMERIC_CHECK));
            } catch (QueryException $ex) {
                throw new DebugException($ex->getMessage(), 1);
            } catch (GlozaException $ge) {
                throw new DebugException($ge->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'success' => false
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    /**
     * procesa_trabajador function
     * Inicia relacion laboral trabajador empleador
     * p7 artisan server:send ServicioSat procesa_trabajador '1110491952' 1 sisuweb
     * @param [type] $argv
     * @return void
     */
    public function procesa_trabajador($argv, $user = '1', $sistema = 'sisuweb', $enviroment = '2')
    {
        try {
            try {
                $jd = json_decode(base64_decode($argv));
                $trabajador = new Trabajador($jd->numdoctra);
                $trabajador->iniciar();
                $trabajador->salvar();
                $data = $trabajador->prepara_data_sat();

                $restServiceFactoryImpl = new RestServiceFactoryImpl();
                RestServiceFactoryImpl::setData($data);
                $rqs = $restServiceFactoryImpl->inicioRelacionLaboralService();

                if ($rqs['codigo']) {
                    Sat09::where('numtraccf', $trabajador->numtraccf)->update([
                        'codigo' =>  $rqs['codigo'],
                        'mensaje' => $rqs['mensaje']
                    ]);
                }

                $attr = $trabajador->sat->makeVisible('attribute')->toArray();
                $attr['numtraccf'] = $trabajador->numtraccf;
                $rqs['data'] = $attr;
                $rqs['modelo'] = $data;
                $rqs['success'] = true;

                $this->console->info(json_encode($rqs, JSON_NUMERIC_CHECK));
            } catch (QueryException $ex) {
                throw new DebugException($ex->getMessage(), 1);
            } catch (GlozaException $ge) {
                throw new DebugException($ge->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'success' => false
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    /**
     * termina_trabajador function
     * termina relacion laboral trabajador 
     * @param [type] $argv
     * @param string $user
     * @param string $sistema
     * @param string $enviroment
     * @return void
     */
    public function termina_trabajador($argv, $user = '1', $sistema = 'sisuweb', $enviroment = '2')
    {
        try {
            try {
                $jd = json_decode(base64_decode($argv));
                $trabajador = new Trabajador($jd->cedtra);
                $trabajador->terminar();
                $trabajador->salvar();
                $data = $trabajador->prepara_data_sat();

                $restServiceFactoryImpl = new RestServiceFactoryImpl();
                RestServiceFactoryImpl::setData($data);
                $rqs = $restServiceFactoryImpl->terminacionRelacionLaboralService();

                if ($rqs['codigo']) {
                    Sat10::where('numtraccf', $trabajador->numtraccf)->update([
                        'codigo' =>  $rqs['codigo'],
                        'mensaje' => $rqs['mensaje']
                    ]);
                }

                $attr = $trabajador->sat->makeVisible('attribute')->toArray();
                $attr['numtraccf'] = $trabajador->numtraccf;
                $rqs['data'] = $attr;
                $rqs['modelo'] = $data;
                $rqs['success'] = true;

                $this->console->info(json_encode($rqs, JSON_NUMERIC_CHECK));
            } catch (QueryException $ex) {
                throw new DebugException($ex->getMessage(), 1);
            } catch (GlozaException $ge) {
                throw new DebugException($ge->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'success' => false
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    public function get_modelo($argv, $user = '1', $sistema = 'sisuweb', $enviroment = '2')
    {
        try {
            try {
                $jd = json_decode(base64_decode($argv));
                $numtraccf = $jd->numtraccf;
                $tablaSat = $jd->tabla;

                $sat = DB::table($tablaSat)
                    ->select("{$tablaSat}.*")
                    ->join('sat20', 'sat20.numtraccf', '=', "{$tablaSat}.numtraccf")
                    ->where("{$tablaSat}.numtraccf", $numtraccf)
                    ->where("sat20.persiste", '0')
                    ->get()
                    ->first();

                if (!$sat) {
                    $salida = [
                        'success' => false,
                        'data' => [],
                        'mensaje' => "Error el registro no se encuentra {$tablaSat}."
                    ];
                } else {
                    $sat20 = Sat20::where("numtraccf", $numtraccf)->get()->first();
                    $sat->sat20 = $sat20;

                    $salida = [
                        'success' => true,
                        'data' => $sat
                    ];
                }
                $this->console->info(json_encode($salida, JSON_NUMERIC_CHECK));
            } catch (QueryException $ex) {
                throw new DebugException($ex->getMessage(), 1);
            } catch (GlozaException $ge) {
                throw new DebugException($ge->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'success' => false
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    public function get_empresa($nit, $user = '1', $sistema = 'sisuweb', $enviroment = '2')
    {
        try {
            try {
                $subsi02 = Subsi02::where("nit", $nit)->get()->first();
                $this->console->info(json_encode(
                    [
                        'success' => true,
                        'data' => $subsi02
                    ],
                    JSON_NUMERIC_CHECK
                ));
            } catch (QueryException $ex) {
                throw new DebugException($ex->getMessage(), 1);
            } catch (GlozaException $ge) {
                throw new DebugException($ge->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'success' => false
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    public function modelo_reprocesar($argv, $user, $sistema = 'sisuweb', $enviroment = '2')
    {
        try {
            try {
                $jd = json_decode(base64_decode($argv));
                $reprocesarGlosas = new ReprocesarGlosas(
                    new MyParams(
                        array(
                            'post' => $jd->post,
                            'tabla' => $jd->tabla,
                        )
                    )
                );
                $rqs = $reprocesarGlosas->procesar(new Tareas());
                $this->console->info(json_encode($rqs, JSON_NUMERIC_CHECK));
            } catch (QueryException $ex) {
                throw new DebugException($ex->getMessage(), 1);
            } catch (GlozaException $ge) {
                throw new DebugException($ge->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'success' => false
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    public function gloza_delete($argv, $user = '1', $sistema = 'sisuweb', $enviroment = '2')
    {
        try {
            try {
                $jd = json_decode(base64_decode($argv));
                $glosaServices = new GlosaServices(
                    new MyParams(
                        array(
                            'numtraccf' => $jd->numtraccf,
                            'tabla' => $jd->tabla
                        )
                    )
                );
                $rqs = $glosaServices->glozaBorrar();
                $this->console->info(json_encode($rqs, JSON_NUMERIC_CHECK));
            } catch (QueryException $ex) {
                throw new DebugException($ex->getMessage(), 1);
            } catch (GlozaException $ge) {
                throw new DebugException($ge->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'success' => false
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    /**
     * actualiza_persistencia function
     * Para definir que ya se proceso con exito un gloza se debe dejar el parametro persiste en 1. para que estás no se vuelvan a procesar. 
     * @param [type] $argv
     * @param string $user
     * @param string $sistema
     * @param string $enviroment
     * @return void
     */
    public function actualiza_persistencia($argv, $user = '1', $sistema = 'sisuweb', $enviroment = '2')
    {
        try {
            try {
                $jd = json_decode(base64_decode($argv));
                $glosaServices = new GlosaServices(
                    new MyParams(
                        array(
                            'numtraccf' => $jd->numtraccf,
                            'tabla' => $jd->tabla,
                            'numdocemp' => $jd->numdocemp,
                            'numdoctra' => $jd->numdoctra
                        )
                    )
                );
                $rqs = $glosaServices->actualizaPersistencia();

                $this->console->info(json_encode($rqs, JSON_NUMERIC_CHECK));
            } catch (QueryException $ex) {
                throw new DebugException($ex->getMessage(), 1);
            } catch (GlozaException $ge) {
                throw new DebugException($ge->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'success' => false
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    public function get_trabajador($cedtra, $user = '1', $sistema = 'sisuweb', $enviroment = '2')
    {
        try {
            try {
                $subsi15 = Subsi15::where("cedtra", $cedtra)->get()->first();
                $rqs = [
                    'success' => true,
                    'data' => $subsi15
                ];
                $this->console->info(json_encode($rqs, JSON_NUMERIC_CHECK));
            } catch (QueryException $ex) {
                throw new DebugException($ex->getMessage(), 1);
            } catch (GlozaException $ge) {
                throw new DebugException($ge->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'success' => false
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    public function estado_pago_aportes($argv, $user = '1', $sistema = 'sisuweb', $enviroment = '2')
    {
        try {
            try {
                $jd = json_decode(base64_decode($argv));
                $nit = $jd->nit;
                $estpag = $jd->estpag;

                $aportes = new PagoAportes($nit);
                $aportes->estado($estpag);
                $aportes->salvar();
                $data = $aportes->prepara_data_sat();

                $restServiceFactoryImpl = new RestServiceFactoryImpl();
                RestServiceFactoryImpl::setData($data);
                $rqs = $restServiceFactoryImpl->estadoPagoEmpleadorService();

                if ($rqs['codigo']) {
                    Sat15::where('numtraccf', $aportes->numtraccf)->update([
                        'codigo' =>  $rqs['codigo'],
                        'mensaje' => $rqs['mensaje']
                    ]);
                }

                $attr = $aportes->sat->makeVisible('attribute')->toArray();
                $attr['numtraccf'] = $aportes->numtraccf;
                $rqs['data'] = $attr;
                $rqs['modelo'] = $data;
                $rqs['success'] = true;
                $rqs['result'] = $aportes->result();

                $this->console->info(json_encode($rqs, JSON_NUMERIC_CHECK));
            } catch (QueryException $ex) {
                throw new DebugException($ex->getMessage(), 1);
            } catch (GlozaException $ge) {
                throw new DebugException($ge->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'success' => false
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    public function consulta_empresa_empleados($nit, $user = '1', $sistema = 'sisuweb', $enviroment = '2')
    {
        try {
            try {
                $empresa = Subsi02::where("nit", $nit)->get()->first();
                $tipo_documento = Gener18::where("coddoc", $empresa->coddoc)->get()->first();
                $data = [
                    "TipoDocumentoEmpleador" => $tipo_documento->codrua,
                    "NumeroDocumentoEmpleador" => $empresa->nit,
                    "SerialSat" => '0'
                ];
                $restServiceFactoryImpl = new RestServiceFactoryImpl();
                RestServiceFactoryImpl::setData($data);
                $rqs = $restServiceFactoryImpl->consultarEmpresaYEmpleados();
                $this->console->info(json_encode($rqs, JSON_NUMERIC_CHECK));
            } catch (QueryException $ex) {
                throw new DebugException($ex->getMessage(), 1);
            } catch (GlozaException $ge) {
                throw new DebugException($ge->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'success' => false
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    public function consulta_empresa_afiliaciones($nit, $user = '1', $sistema = 'sisuweb', $enviroment = '2')
    {
        try {
            try {
                $empresa = Subsi02::where("nit", $nit)->get()->first();
                $tipo_documento = Gener18::where("coddoc", $empresa->coddoc)->get()->first();
                $data = [
                    "TipoDocumentoEmpleador" => $tipo_documento->codrua,
                    "NumeroDocumentoEmpleador" => $empresa->nit,
                    "SerialSat" => '0'
                ];
                $restServiceFactoryImpl = new RestServiceFactoryImpl();
                RestServiceFactoryImpl::setData($data);
                $rqs = $restServiceFactoryImpl->consultarEmpresaYAfiliaciones();
                $this->console->info(json_encode($rqs, JSON_NUMERIC_CHECK));
            } catch (QueryException $ex) {
                throw new DebugException($ex->getMessage(), 1);
            } catch (GlozaException $ge) {
                throw new DebugException($ge->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'success' => false
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    /**
     * trabajador_cambio_salario function
     *  p7 artisan server:send ServicioSat trabajador_cambios_salario 'data_json_base64' 1 sisuweb 1
     * @param [type] $argv
     * @param string $user
     * @param string $sistema
     * @param string $enviroment
     * @return void
     */
    public function trabajador_cambio_salario($argv, $user = '1', $sistema = 'sisuweb', $enviroment = '2')
    {
        try {
            try {
                $jd = json_decode(base64_decode($argv));
                $cedtra = $jd->cedtra;
                $salario = $jd->salario;

                $trabajador = new Trabajador($cedtra);
                $trabajador->cambio_salario($salario);
                $trabajador->salvar();
                $data = $trabajador->prepara_data_sat();

                $restServiceFactoryImpl = new RestServiceFactoryImpl();
                RestServiceFactoryImpl::setData($data);
                $rqs = $restServiceFactoryImpl->cambioSalarialService();

                if ($rqs['codigo']) {
                    Sat13::where('numtraccf', $trabajador->numtraccf)->update([
                        'codigo' =>  $rqs['codigo'],
                        'mensaje' => $rqs['mensaje']
                    ]);
                }

                $attr = $trabajador->sat->makeVisible('attribute')->toArray();
                $attr['numtraccf'] = $trabajador->numtraccf;
                $rqs['data'] = $attr;
                $rqs['modelo'] = $data;
                $rqs['success'] = true;

                $this->console->info(json_encode($rqs, JSON_NUMERIC_CHECK));
            } catch (QueryException $ex) {
                throw new DebugException($ex->getMessage(), 1);
            } catch (GlozaException $ge) {
                throw new DebugException($ge->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'success' => false
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }
}
