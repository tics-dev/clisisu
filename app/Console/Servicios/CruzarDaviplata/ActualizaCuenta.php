<?php 
declare(strict_types=1);
namespace App\Console\Servicios\CruzarDaviplata;

use App\Exceptions\DebugException;
use App\Models\Subsi15;
use App\Models\CuentasDaviplata;
use App\Models\Subsi20;

class ActualizaCuenta
{
    protected $ruta_archivo;
    public static $progreso;
    public static $proceso;
    public static $proporcion;
    public static $cantidad;
    public static $fileLog;
    public static $archivo;
    private static $file;
    public static $errors = [];

    public static function Inicializa(string $sistema)
    {
        self::$fileLog = "/var/www/html/".$sistema."/public/temp/update_cuentas_daviplata_".strtotime('now').".csv";
        self::$file = fopen(self::$fileLog, "w+");
        $texto_write = implode(';', array(
            "Cedula",
            "Cuenta",
            "Tipo afiliado",
            "Se hizo cambio",
            "Es Nueva"
        ));
        fwrite(self::$file, strtoupper($texto_write) . "\r\n");
    }

    public function procesarCuenta(int $cedula, int $cuenta)
    {
        $tipo ='';
        $has = false;
        $nueva = 'NO';

        $query = Subsi15::where('cedtra', $cedula)
        ->whereIn('tippag', ['D','T','E','G','B','C'])
        ->get();

        if($query->count() > 0)
        {
            $trabajador = $query->first();
            $trabajador->tippag = 'D';
            $trabajador->tipcue = 'A';
            $trabajador->numcue = $cuenta;
            $trabajador->codban = '51';                        
            $trabajador->codcue = '';
            $trabajador->codgru = '';
            $trabajador->ofides = '';

            if(!$trabajador->save()) {
                self::$errors[]= $trabajador->cedtra;
            } else {
                $has= true;
                $tipo="TRABAJADOR";
            }
        }

        $query = Subsi20::where('cedcon', $cedula)
        ->whereIn('tippag', ['D','T','E','G','B','C'])
        ->get();

        if($query->count() > 0)
        {
            $conyugue = $query->first();
            $conyugue->tippag = 'D';
            $conyugue->tipcue = 'A';
            $conyugue->numcue = $cuenta;
            $conyugue->codban = '51';
            $conyugue->codcue = '';
            $conyugue->codgru = '';
            $conyugue->ofides = '';

            if(!$conyugue->save()) {
                self::$errors[] = $conyugue->cedcon;
            } else {
                $has = true;
                $tipo.=":CONYUGUE";
            }
        }
        
        $cantidad = CuentasDaviplata::where("cedula", $cedula)
        ->get()
        ->count();
        
        if($cantidad == 0)
        {
            $cuentaDaviplata = CuentasDaviplata::create([
                "cuenta" => $cuenta,
                "cedula" => $cedula,
                "estado" => "A",
                "createAt" => date('Y-m-d H:i:s'),
                "updateAt" => date('Y-m-d H:i:s')
            ]);
            $cuentaDaviplata->save();
            $nueva='SI';

        }else{
            //se debe crear una nueva y cambiar el estado de la viejas
            $daviplataCuentas = CuentasDaviplata::where("cedula", $cedula)
            ->where("cuenta", "!=", $cuenta)
            ->get();

            if($daviplataCuentas->count() > 0)
            {
                foreach ($daviplataCuentas as $cuentaDaviplata)
                {
                    $cuentaDaviplata->estado ='I';
                    $cuentaDaviplata->updateAt = date('Y-m-d H:i:s');
                    $cuentaDaviplata->save();
                }
            }
            
            //si no existe se crea
            $hasCuenta = CuentasDaviplata::where("cedula", $cedula)->where("cuenta", $cuenta)->get()->count();
            if($hasCuenta == 0)
            {
                $id = CuentasDaviplata::max('id') + 1;
                $cuentaDaviplata = CuentasDaviplata::create([
                    'id' => $id,
                    'cuenta' => $cuenta,
                    'cedula' => $cedula,
                    'estado' => "A",
                    'createAt' => date('Y-m-d H:i:s'),
                    'updateAt' => date('Y-m-d H:i:s')
                ]);
                $cuentaDaviplata->save();
                $nueva='SI';
            }
        }

        $texto_write = implode(';', 
        array(
            $cedula,
            $cuenta,
            $tipo,
            ($has)? "SI":"NO",
            $nueva
        ));
        fwrite(self::$file, strtoupper($texto_write) . "\r\n");
    }

    public static function CloseLog()
    {
        chmod(self::$fileLog, 0777);
        fclose(self::$file);
    }

    public function getResultado()
    {
        return [
            "success"=> true,
            "msj"=> "Proceso completado con éxito",
            "url" => self::$fileLog,
            "filename" => basename(self::$fileLog),
            "errors" => self::$errors
        ];
    }

}   