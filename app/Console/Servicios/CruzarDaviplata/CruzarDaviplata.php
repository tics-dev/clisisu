<?php

declare(strict_types=1);
require_once base_path('app/Libraries/PHPExcel.php');
require_once base_path('app/Libraries/PHPExcel/IOFactory.php');
require_once base_path('app/Libraries/PHPExcel/Shared/Date.php');

use App\Console\Servicios\Comandos\Comandos;
use App\Console\Servicios\CruzarDaviplata\ActualizaCuenta;
use App\Console\Servicios\CruzarDaviplata\DaviplataNoCertificado;
use App\Exceptions\DebugException;
use App\Models\Subsi20;
use App\Models\Subsi15;
use App\Models\PrescripcionCuota;
use Illuminate\Database\QueryException;

class CruzarDaviplata
{

    protected $usuario;
    protected $sistema;
    protected $user;
    protected $console;
    protected $actualizaCuenta;
    protected $fecha;

    public function __construct($console)
    {
        $this->console = $console;
        $f = new DateTime('now');
        $this->fecha = $f->format('Y-m-d');
        $this->actualizaCuenta = new ActualizaCuenta();
    }

    public function cruza_daviplata_pago_prescripcion($argv, $user, $sistema, $enviroment)
    {
        ini_set('memory_limit', '1300M');
        $jd = json_decode(base64_decode($argv));
        $periodo = $jd->periodo;
        $file = $jd->file;
        $tipo = $jd->tipo;
        try {
            try {
                $archivo = "/var/www/html/clisisu/storage/files/" . $file;
                $inputFileType = PHPExcel_IOFactory::identify($archivo);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($archivo);
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                $num = 0;

                $errors = array("Novedad de Error En Pago Por Daviplata Prescripción Saldos||", "Tipo|Cedula|Cuenta");
                for ($row = 2; $row <= $highestRow; $row++) {
                    $num++;
                    $cedula = intval($sheet->getCell("A{$row}")->getValue());
                    $cuenta = intval($sheet->getCell("B{$row}")->getValue());

                    $responsable = Subsi15::where('cedtra', $cedula)
                        ->get()
                        ->first();

                    if (!$responsable) {
                        $responsable = Subsi20::where('cedcon', $cedula)
                            ->get()
                            ->first();
                    }

                    if ($responsable) {
                        $prescripcion = PrescripcionCuota::where("periodo", $periodo)
                            ->where("tipo", $tipo)
                            ->where("estado", "AD")
                            ->where("cedres", $cedula)
                            ->get()
                            ->first();

                        if ($prescripcion) {
                            PrescripcionCuota::where("periodo", $periodo)
                                ->where("tipo", $tipo)
                                ->where("estado", "AD")
                                ->where("cedres", $cedula)
                                ->update([
                                    "estado" => "PD",
                                    "cuenta_redime" => $cuenta
                                ]);
                            $this->console->info($prescripcion->cedres . '|' . $prescripcion->valor_descontar . '|' . $prescripcion->saldo_actual . '|' . $prescripcion->periodo);
                        } else {
                            $errors[] = "2|{$cedula}|{$cuenta}";
                            $this->console->info("501 {$cedula} {$cuenta}");
                        }
                    } else {
                        $this->console->info("502 {$cedula} {$cuenta}");
                        $errors[] = "1|{$cedula}|{$cuenta}";
                    }
                }
                $file1 = base_path("storage/files/") . "prescripcion_errors_pagos_daviplata_" . date('Ymd') . ".csv";
                file_put_contents($file1, implode("\n", $errors));
            } catch (\ErrorException $th) {
                $this->console->info($th->getMessage());
            }
        } catch (DebugException $err) {
            $this->console->info($err->getMessage());
        }
    }


    /**
     * actualiza_cuentas_daviplata function
     * Actualizar las cuenta de daviplata al certificarlas con davivienda
     * @param [type] $argv
     * @param [type] $user
     * @param [type] $sistema
     * @param [type] $enviroment
     * @return void
     */
    public function actualiza_cuentas_daviplata($argv, $user, $sistema, $enviroment)
    {
        try {
            try {
                $modelo = json_decode(base64_decode($argv), true);
                $archivo = base_path("storage/files/daviplata/" . $modelo['file']);
                if (!file_exists($archivo)) {
                    throw new DebugException("Error el archivo es requerido para procesar", 501);
                }
                $inputFileType = PHPExcel_IOFactory::identify($archivo);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($archivo);
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $sheet->getHighestColumn();

                Comandos::$cantidad = $highestRow;
                Comandos::setProporcion();
                $fraccion = Comandos::$cantidad / Comandos::$proporcion;
                $progreso_ini_conteo = 0;


                ActualizaCuenta::Inicializa($sistema);
                ActualizaCuenta::$errors = [];
                $num = 0;
                for ($row = 2; $row <= $highestRow; $row++) {
                    $num++;
                    $progreso_ini_conteo++;
                    if ($progreso_ini_conteo >= $fraccion) {
                        $progreso_ini_conteo = 0;
                        $this->console->comandoProcesador->actualizaProgreso($num);
                    }
                    $cedula = intval($sheet->getCell("A{$row}")->getValue());
                    if (!$cedula) continue;
                    $cuenta = intval($sheet->getCell("B{$row}")->getValue());
                    $this->actualizaCuenta->procesarCuenta($cedula, $cuenta);
                }

                ActualizaCuenta::CloseLog();
                $resultado = json_encode($this->actualizaCuenta->getResultado());
                $this->console->comandoProcesador->finalizarProceso($resultado);
                $this->console->info($resultado);
            } catch (\Exception $th) {
                throw new DebugException($th->getMessage() . ' ' . $th->getLine() . ' ' . basename($th->getFile()), 501);
            } catch (QueryException $er) {
                throw new DebugException($er->getMessage() . ' ' . $er->getLine() . ' ' . basename($er->getFile()), 501);
            }
        } catch (DebugException $err) {
            $this->console->comandoProcesador->cancelarComando();
            $salida = array(
                "success" => false,
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode()
            );
            $this->console->error(json_encode($salida));
        }
    }


    /**
     * cuentas_daviplata_no_certificadas function
     * Procesar cuentas no certificadas pasar a tarjetas
     * @param [type] $argv
     * @param [type] $user
     * @param [type] $sistema
     * @param [type] $enviroment
     * @return void
     */
    public function cuentas_daviplata_no_certificadas($argv, $user, $sistema, $enviroment)
    {
        try {
            try {
                $modelo = json_decode(base64_decode($argv), true);
                $archivo = base_path("storage/files/daviplata/" . $modelo['file']);
                if (!file_exists($archivo)) {
                    throw new DebugException("Error el archivo es requerido para procesar", 501);
                }
                $inputFileType = PHPExcel_IOFactory::identify($archivo);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($archivo);
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $sheet->getHighestColumn();

                Comandos::$cantidad = $highestRow;
                Comandos::setProporcion();
                $fraccion = Comandos::$cantidad / Comandos::$proporcion;
                $progreso_ini_conteo = 0;

                $num = 0;
                for ($row = 2; $row <= $highestRow; $row++) {
                    $num++;
                    $progreso_ini_conteo++;
                    if ($progreso_ini_conteo >= $fraccion) {
                        $progreso_ini_conteo = 0;
                        $this->console->comandoProcesador->actualizaProgreso($num);
                    }
                    $cedula = intval($sheet->getCell("A{$row}")->getValue());
                    if (!$cedula) continue;
                    $cuenta = intval($sheet->getCell("B{$row}")->getValue());

                    $daviplataNoCertificado = new DaviplataNoCertificado();
                    $daviplataNoCertificado->procesarCuenta($cedula, $cuenta);
                }

                $this->console->comandoProcesador->finalizarProceso("Proceso 100%");

                $this->console->info(json_encode([
                    "success" => true,
                    "msj" => "Proceso completado"
                ]));
            } catch (\Exception $th) {
                throw new DebugException($th->getMessage() . ' ' . $th->getLine() . ' ' . basename($th->getFile()), 501);
            } catch (QueryException $er) {
                throw new DebugException($er->getMessage() . ' ' . $er->getLine() . ' ' . basename($er->getFile()), 501);
            }
        } catch (DebugException $err) {
            $this->console->comandoProcesador->cancelarComando();
            $salida = array(
                "success" => false,
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode()
            );
            $this->console->error(json_encode($salida));
        }
    }
}
