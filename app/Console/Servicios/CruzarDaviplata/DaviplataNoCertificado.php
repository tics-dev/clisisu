<?php

declare(strict_types=1);

namespace App\Console\Servicios\CruzarDaviplata;

use App\Exceptions\DebugException;
use App\Models\CuentasDaviplata;
use App\Models\Subsi15;
use App\Models\Subsi20;

class DaviplataNoCertificado
{

    public static $errors = [];

    public function procesarCuenta(int $cedula, int $cuenta)
    {
        $query = Subsi15::where('cedtra', $cedula)->where('numcue', $cuenta)->where('tippag', 'D')->get();
        if ($query->count() > 0) {
            $trabajador = $query->first();
            $trabajador->tippag = 'T';
            $trabajador->tipcue = '';
            $trabajador->numcue = '';
            $trabajador->codban = '';
            $trabajador->codcue = '';
            $trabajador->codgru = '';
            $trabajador->ofides = '';

            if (!$trabajador->save()) {
                self::$errors[] = $trabajador->cedtra;
            } else {
                $cuentaDaviplata = new CuentasDaviplata();
                $cuentaDaviplata->id = CuentasDaviplata::max('id') + 1;
                $cuentaDaviplata->cuenta = $cuenta;
                $cuentaDaviplata->cedula = $cedula;
                $cuentaDaviplata->estado = "I";
                $cuentaDaviplata->createAt = date('Y-m-d H:i:s');
                $cuentaDaviplata->updateAt = date('Y-m-d H:i:s');
                $cuentaDaviplata->save();
            }
        }

        $query = Subsi20::where('cedcon', $cedula)->where('numcue', $cuenta)->where('tippag', 'D')->get();
        if ($query->count() > 0) {
            $conyugue = $query->first();
            $conyugue->tippag = 'T';
            $conyugue->tipcue = '';
            $conyugue->numcue = '';
            $conyugue->codban = '';
            $conyugue->codcue = '';
            $conyugue->codgru = '';
            $conyugue->ofides = '';

            if (!$conyugue->save()) {
                self::$errors[] = $conyugue->cedcon;
            } else {
                $cuentaDaviplata = new CuentasDaviplata();
                $cuentaDaviplata->id = CuentasDaviplata::max('id') + 1;
                $cuentaDaviplata->cuenta = $cuenta;
                $cuentaDaviplata->cedula = $cedula;
                $cuentaDaviplata->estado = "I";
                $cuentaDaviplata->createAt = date('Y-m-d H:i:s');
                $cuentaDaviplata->updateAt = date('Y-m-d H:i:s');
                $cuentaDaviplata->save();
            }
        }
    }


    public function getResultado()
    {
        return [
            "success" => true,
            "msj" => "Proceso completado con éxito"
        ];
    }
}
