<?php
require_once base_path('app/Libraries/PHPExcel.php');
require_once base_path('app/Libraries/PHPExcel/IOFactory.php');
require_once base_path('app/Libraries/PHPExcel/Shared/Date.php');

use Illuminate\Support\Facades\DB;
use App\Exceptions\DebugException;
use Illuminate\Database\QueryException;
use App\Console\Servicios\General\General;
use App\Libraries\MyParams;
use App\Services\AfiliacionMasivos\CargarBeneficiarios;
use App\Services\AfiliacionMasivos\CargarConyuges;
use App\Services\AfiliacionMasivos\CargarConyugues;
use App\Services\AfiliacionMasivos\CargarTrabajadores;

class CargueMasivos
{

    protected $fecha;
    protected $usuario;
    protected $sistema;
    protected $user;

    protected $console;
    public function __construct($console)
    {
        ini_set('memory_limit', '1300M');
        $this->console = $console;
        $this->fecha = date('Y-m-d');
    }

    /**
     * procesar_trabajadores function
     *  Cargue masivo de trabajadores al sistema de mercurio comfaca online
     * @param [type] $argv
     * @param [type] $user
     * @param [type] $sistema
     * @param [type] $enviroment
     * @return void
     */
    public function procesar_trabajadores($argv, $user, $sistema, $enviroment)
    {
        try {
            try {
                $jd = json_decode(base64_decode($argv));
                $cargue = new CargarTrabajadores(new MyParams(
                    array(
                        'filepath' =>  $jd->filepath,
                        'documento' => $jd->documento,
                        'nit' => $jd->nit,
                        'codsuc' => $jd->codsuc
                    )
                ));

                $cargue->procesarDocumento($jd->filepath);
                $salida = $cargue->getResultado();

                $this->console->info(json_encode($salida, JSON_NUMERIC_CHECK));
            } catch (QueryException $th) {
                throw new DebugException($th->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'success' => false
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    /**
     * procesar_conyuges function
     *  Cargue masivo de conyuges al sistema de mercurio comfaca online
     * @param [type] $argv
     * @param [type] $user
     * @param [type] $sistema
     * @param [type] $enviroment
     * @return void
     */
    public function procesar_conyuges($argv, $user, $sistema, $enviroment)
    {
        try {
            try {
                $jd = json_decode(base64_decode($argv));
                $cargue = new CargarConyuges(new MyParams(
                    array(
                        'filepath' =>  $jd->filepath,
                        'documento' => $jd->documento,
                        'nit' => $jd->nit
                    )
                ));
                $cargue->procesarDocumento($jd->filepath);
                $this->console->info(json_encode($cargue->getResultado(), JSON_NUMERIC_CHECK));
            } catch (QueryException $th) {
                throw new DebugException($th->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'success' => false
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    /**
     * procesar_beneficiarios function
     *  Cargue masivo de beneficiarios al sistema de mercurio comfaca online
     * @param [type] $argv
     * @param [type] $user
     * @param [type] $sistema
     * @param [type] $enviroment
     * @return void
     */
    public function procesar_beneficiarios($argv, $user, $sistema, $enviroment)
    {
        try {
            try {
                $jd = json_decode(base64_decode($argv));
                $cargue = new CargarBeneficiarios(new MyParams(
                    array(
                        'filepath' =>  $jd->filepath,
                        'documento' => $jd->documento,
                        'nit' => $jd->nit
                    )
                ));

                $cargue->procesarDocumento($jd->filepath);
                $salida = $cargue->getResultado();
                $this->console->info(json_encode($salida, JSON_NUMERIC_CHECK));
            } catch (QueryException $th) {
                throw new DebugException($th->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'success' => false
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }
}
