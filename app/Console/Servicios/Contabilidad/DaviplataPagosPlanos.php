<?php

declare(strict_types=1);

namespace App\Console\Servicios\Contabilidad;

use App\Exceptions\DebugException;
use App\Models\CuotaComprobante;
use App\Models\CuotaNoCobrada;
use App\Models\Subsi15;
use App\Models\Subsi18;
use App\Models\Subsi19;
use App\Models\Subsi20;
use App\Models\Subsi22;
use Symfony\Component\Console\Logger\ConsoleLogger;

class DaviplataPagosPlanos
{

    protected $console;
    public static $adicionados = 0;
    public static $tercerosError = array();
    public static $errors = array();
    public static $padres = array();
    public static $responsableNoDisponible = array();
    public static $rechazoFormaPago = array();
    public static $nodebitaControlInterno = array();
    public static $internoSaldos = array();
    public static $conceptoPago = '6'; //6 = daviplata 
    public static $banco = 51; //davivienda
    public static $debitar = False; // procesar retiro de control interno
    protected $feccon;

    public function set_env($console, $feccon)
    {
        $this->console = $console;
        $this->feccon = $feccon;
    }

    public static function initLog($sistema)
    {
        ContableLog::initLog(
            [
                'marca',
                'cedula',
                'detalle',
                'cuenta_banco',
                'fecha_contable',
                'movimiento',
                'valor',
                'centro_costos',
                'documento',
                'comprobante'
            ],
            "/var/www/html/{$sistema}/public/temp/girochesub_" . strtotime('now') . ".csv"
        );
    }

    public function principal(CuotaComprobante $cuotaComprobante)
    {
        $cuotaNocobrada = CuotaNoCobrada::where('id', $cuotaComprobante->nocobrada)->get()->first();
        if (!$cuotaNocobrada) {
            throw new DebugException("Error no tiene cuota no cobrada principal $cuotaComprobante->nocobrada", 405);
        }
        $centro_costos = '1502505';
        $cuenta_banco = "112030102005"; //NO 111030101005

        $cedula = intval($cuotaComprobante->cedula);
        if (!$cedula || $cedula == '') {
            self::$tercerosError[] = $cedula;
            return false;
        }

        $valorDebitar = intval($cuotaComprobante->valor_detalle);
        $detalle = substr(ContableLog::sanetizar($cuotaNocobrada->detalle), 6);
        $documento = $cuotaComprobante->documento;
        $comprobante = $cuotaComprobante->comprobante;
        $concepto_detalle = $cuotaNocobrada->detalle;

        //valida terceros, para hacer los pagos 
        $responsable = Subsi20::where("cedcon", $cedula)->where('tippag', 'D')->get()->first();
        if (!$responsable) {
            $responsable = Subsi15::where("cedtra", $cedula)->where('tippag', 'D')->get()->first();
        }

        if (!$responsable) {
            $responsable = Subsi19::where("cedres", $cedula)->where('tippag', 'D')->get()->first();
        }

        if (!$responsable) {
            $responsable = Subsi18::where("cedres", $cedula)->where('tippag', 'D')->get()->first();
        }

        $beneficiarioPadre = Subsi22::where("documento", $cedula)->where("parent", 3)->get()->first();
        if ($beneficiarioPadre) {
            self::$padres[] = $cedula;
        }

        if (!$responsable) {
            self::$responsableNoDisponible[] = $cedula;
            return false;
        }

        if ($documento == 'ILCM') {
            if (strtoupper(trim($concepto_detalle)) == 'SALDO TARJETAS') {

                //saldos en tarjeta y saldos por pagar
                $cuenta_debita = "2302050606";
                $concepto = ['1', '3'];
            } else {

                //pendientes definir forma de pago
                $cuenta_debita = "2302050605";
                $concepto = ['2']; //pendiente definir forma de pago
            }

            $this->addLine(
                $cedula,
                $detalle,
                $cuenta_banco,
                $this->feccon,
                $valorDebitar,
                $centro_costos,
                $documento,
                $comprobante,
                $cuenta_debita
            );

            if (self::$debitar == True) {
                $debitarControlInterno = new DebitarControlInterno();
                $debitarControlInterno->set_env($this->console);
                $res = $debitarControlInterno->pagoSaldosPendientesDefinirFormaPago($cedula, $valorDebitar, $cuotaNocobrada->cuenta_abonar, $concepto, self::$conceptoPago, self::$banco);
                if ($res == FALSE) {
                    self::$nodebitaControlInterno[] = $cedula;
                }
                $cuotaComprobante->estado = 'D';
                $cuotaComprobante->save();
            }
        } else {

            $cuenta_debita = "230210";
            $this->addLine(
                $cedula,
                $detalle,
                $cuenta_banco,
                $this->feccon,
                $valorDebitar,
                $centro_costos,
                $documento,
                $comprobante,
                $cuenta_debita
            );

            $cuotaComprobante->estado = 'D';
            $cuotaComprobante->save();
        }
    }

    private function addLine($cedula, $detalle, $cuenta_banco, $feccon, $valorDebitar, $centro_costos, $documento, $comprobante, $cuenta_debita)
    {
        $data_credita = array(
            "EG8",
            $cedula,
            "ABONO " . $detalle,
            $cuenta_banco,
            $feccon,
            'C',
            $valorDebitar,
            $centro_costos,
            $documento,
            $comprobante
        );
        ContableLog::addLine($data_credita);

        $data_debita = array(
            "EG8",
            $cedula,
            "DEBITO " . $detalle,
            $cuenta_debita,
            $feccon,
            'D',
            $valorDebitar,
            $centro_costos,
            $documento,
            $comprobante
        );
        ContableLog::addLine($data_debita);
        self::$adicionados++;
    }

    public static function closeLog()
    {
        ContableLog::closeLog();
    }

    public function getResultado()
    {
        return [
            "success" => true,
            "msj" => "Proceso completado con éxito",
            "url" => 'public/temp/' . basename(ContableLog::$fileLog),
            "filename" => basename(ContableLog::$fileLog),
            "errors" => self::$errors,
            "padres" => self::$padres,
            "rechazoFormaPago" => self::$rechazoFormaPago,
            "responsableNoDisponible" => self::$responsableNoDisponible,
            "tercerosError" => self::$tercerosError,
            "nodebitaControlInterno" => self::$nodebitaControlInterno,
            "adicionados" => self::$adicionados
        ];
    }
}
