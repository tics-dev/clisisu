<?php

declare(strict_types=1);

namespace App\Console\Servicios\Contabilidad;

use App\Exceptions\DebugException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use App\Console\Servicios\Comandos\Comandos;
use App\Models\CuotaComprobante;
use App\Models\CuotaNoCobrada;
use App\Models\Subsi15;
use App\Models\Subsi18;
use App\Models\Subsi19;
use App\Models\Subsi20;
use App\Models\Subsi22;

class GiradosNoCobrados
{

    public static $responsableNoDisponible = array();
    public static $rechazoFormaPago = array();
    public static $erroNoCobrada = array();
    public static $intentoDuplicado = array();
    private $console;
    private $tipo_pago;

    public function __construct($console)
    {
        $this->console = $console;
    }

    public function setTipoPago($tippag)
    {
        $this->tipo_pago = $tippag;
    }

    public function main($linea)
    {
        if (!$this->tipo_pago) {

            throw new DebugException("Error el tipo de pago a procesar no es valido", 501);
        }
        $cedula =  $linea['cedula'];
        $cuotaNoCobrada = CuotaNoCobrada::where('cedula', $cedula)
            ->where('fecha', $linea['fecha_detalle'])
            ->where('detalle', '=', $linea['detalle'])
            ->get()
            ->first();

        if (!$cuotaNoCobrada) {
            self::$erroNoCobrada[] = $cedula;
            $this->console->error($cedula);
            return false;
        }

        $responsable = Subsi20::where("cedcon", $cedula)->get()->first();
        if (!$responsable) {
            $responsable = Subsi15::where("cedtra", $cedula)->get()->first();
        }

        if (!$responsable) {
            $responsable = Subsi19::where("cedres", $cedula)->get()->first();
        }

        if (!$responsable) {
            $responsable = Subsi18::where("cedres", $cedula)->get()->first();
        }

        if (!$responsable) {
            self::$responsableNoDisponible[] = $cedula;
            return false;
        }

        $cuotaComprobante = CuotaComprobante::where('cedula', $cedula)
            ->where('fecha_detalle', $cuotaNoCobrada->fecha)
            ->where('valor_detalle', $linea['valor_detalle'])
            ->where('documento', $linea['documento'])
            ->where('comprobante', $linea['comprobante'])
            ->get()
            ->first();

        if ($cuotaComprobante) {
            //ya se proceso el cargue
            self::$intentoDuplicado[] = $cedula;
            $cuotaNoCobrada->banco = $responsable->codban;
            $cuotaNoCobrada->save();
            $this->console->error($cedula . ' BANCO <<' . $responsable->codban . '>>');
            return false;
        } else {
            $cuotaComprobante = new CuotaComprobante();
            $cuotaComprobante->nocobrada = $cuotaNoCobrada->id;
            $cuotaComprobante->cedula = $cuotaNoCobrada->cedula;
            $cuotaComprobante->documento = $linea['documento'];
            $cuotaComprobante->comprobante = $linea['comprobante'];
            $cuotaComprobante->valor_detalle = $linea['valor_detalle'];
            $cuotaComprobante->estado = $linea['estado'];
            $cuotaComprobante->fecha_detalle = $linea['fecha_detalle'];
        }

        if ($responsable->tippag == $this->tipo_pago) {

            $cuotaComprobante->estado = 'C';
            $cuotaComprobante->save();

            $cuotaNoCobrada->cuenta_abonar = $responsable->numcue;
            $cuotaNoCobrada->banco = $responsable->codban;
            $cuotaNoCobrada->estado = 'C';
            $cuotaNoCobrada->save();
        } else {

            $cuotaNoCobrada->estado = 'P';
            $cuotaNoCobrada->save();

            $cuotaComprobante->estado = 'P';
            $cuotaComprobante->save();
            self::$rechazoFormaPago[] = $cedula;
        }

        $this->console->info($cedula . ' <<' . $cuotaNoCobrada->estado . '>>');
    }

    public static function getResultado()
    {
        return [
            "success" => true,
            "msj" => "Proceso completado con éxito",
            "rechazoFormaPago" => self::$rechazoFormaPago,
            "responsableNoDisponible" => self::$responsableNoDisponible,
            "erroNoCobrada" => self::$erroNoCobrada,
            "intentoDuplicado" => self::$intentoDuplicado
        ];
    }
}
