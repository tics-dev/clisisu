<?php
declare(strict_types=1);
namespace App\Console\Servicios\Contabilidad;

use App\Exceptions\DebugException;
use Illuminate\Support\Facades\DB;
use App\Models\InternoAbono;
use App\Models\InternoDebito;
use App\Models\InternoSaldo;

class ControlInternoPlano
{

    protected $console;
    protected $actualizaCuenta;
    protected $fecha;

    public function set_env($console)
	{
		$this->console = $console;
	}

    public static function initLog($sistema)
    {
        ContableLog::initLog(
            [
                'cedula',
                'detalle',
                'cuenta_banco',
                'feccon',
                'valorDebitar',
                'centro_costos',
                'marca',
                'comprobante',
                'cuenta_debita'
            ],
            "/var/www/html/{$sistema}/public/temp/girochesub_".strtotime('now').".csv"
        );
    }

    public function principal($sheet, $row)
    {
        $centro_costos = '1502505';
        $feccon = '12/05/2022';
        $cuenta_banco = "112030102005";

        $cedula = intval($sheet->getCell("A{$row}")->getValue());
        if (!$cedula) return false;

        $valor = intval($sheet->getCell("B{$row}")->getValue());
        $detalle = substr($sheet->getCell("C{$row}")->getValue(), 6);

        $internoDebito = InternoDebito::where("cedula", $cedula)->where("valor_debito", $valor)->get()->first();
        
        //abonos pendientes por redimir
        $abonoMigracion = InternoAbono::where("cedula", $cedula)->where('concepto', 1)->where('estado', 'P')->get();
        $abonoGiro = InternoAbono::where("cedula", $cedula)->where('concepto', 2)->get();

        $valorAbonoMigracion = 0;
        if($abonoMigracion->count() > 0)
        {
            $abono_migracion = $abonoMigracion->first();
            $valorAbonoMigracion += $abono_migracion->valor_abono;
        }

        $valorAbonoGirado = 0;
        if($abonoGiro->count() > 0)
        {   
            foreach ($abonoGiro as $abono){
                $valorAbonoGirado += $abono->valor_abono;
            }
        }

        if($internoDebito->valor_debito  ==  ($valorAbonoGirado + $valorAbonoMigracion))
        {
            if($valorAbonoGirado > 0)
            {
                $cuenta_debita = "2302050605";
                $marca = 'ILCM';
                $comprobante = '2'; 
                $valorDebitar = $valorAbonoGirado;

                $this->addLine($cedula, $detalle, $cuenta_banco, $feccon, $valorDebitar, $centro_costos, $marca, $comprobante, $cuenta_debita);
            }

            if($valorAbonoMigracion > 0)
            {
                $cuenta_debita = "23020502";
                $marca = 'EG8';
                $comprobante = '';
                $valorDebitar = $valorAbonoMigracion;

                $this->addLine($cedula, $detalle, $cuenta_banco, $feccon, $valorDebitar, $centro_costos, $marca, $comprobante, $cuenta_debita);

                if($abono_migracion)
                {
                    $abono_migracion->procesado = 'S';
                    $abono_migracion->save();
                }
            }
        } else {
            if($internoDebito->valor_debito  == $valorAbonoGirado)
            {
                $cuenta_debita = "2302050605";
                $marca = 'ILCM';
                $comprobante = '2'; 
                $valorDebitar = $valorAbonoGirado;

                $this->addLine($cedula, $detalle, $cuenta_banco, $feccon, $valorDebitar, $centro_costos, $marca, $comprobante, $cuenta_debita);

            } elseif ($internoDebito->valor_debito  == $valorAbonoMigracion){

                $cuenta_debita = "23020502";
                $marca = 'EG8';
                $comprobante = '';
                $valorDebitar = $valorAbonoMigracion;

                $this->addLine($cedula, $detalle, $cuenta_banco, $feccon, $valorDebitar, $centro_costos, $marca, $comprobante, $cuenta_debita);

            } else {
                
                $cuenta_debita = "23020502";
                $marca = 'EG8';
                $comprobante = '';
                $valorDebitar = $internoDebito->valor_debito;
                $this->addLine($cedula, $detalle, $cuenta_banco, $feccon, $valorDebitar, $centro_costos, $marca, $comprobante, $cuenta_debita);
            }
        }   
    }

    private function addLine($cedula, $detalle, $cuenta_banco, $feccon, $valorDebitar, $centro_costos, $marca, $comprobante, $cuenta_debita)
    {
        $data_credita = array(
            "EG8",
            $cedula,
            "ABONO ".$detalle,
            $cuenta_banco,
            $feccon,
            'C',
            $valorDebitar,
            $centro_costos,
            $marca,
            $comprobante
        );
        ContableLog::addLine($data_credita);
        
        $data_debita = array(
            "EG8",
            $cedula,
            "DEBITO ". $detalle,
            $cuenta_debita,
            $feccon,
            'D',
            $valorDebitar,
            $centro_costos,
            $marca,
            $comprobante
        );
        ContableLog::addLine($data_debita);
    }

    public static function closeLog()
    {
        ContableLog::closeLog();
    }

    public function getResultado()
    {
        return [
            "success" => true,
            "msj" => "Proceso completado con éxito",
            "url" => 'public/temp/'.ContableLog::$fileLog,
            "filename" => basename(ContableLog::$fileLog) 
        ];
    }
}