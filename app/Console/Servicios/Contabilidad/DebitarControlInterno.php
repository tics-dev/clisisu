<?php

declare(strict_types=1);

namespace App\Console\Servicios\Contabilidad;

use App\Exceptions\DebugException;
use App\Models\InternoAbono;
use App\Models\InternoDebito;
use App\Models\InternoSaldo;

class DebitarControlInterno
{

    protected $console;
    public function set_env($console)
    {
        $this->console = $console;
    }

    /**
     * pagoSaldosPendientesDefinirFormaPgo function
     * @param [type] $cedula
     * @param [type] $valor
     * @param [type] $cuenta
     * @param string $concepto
     * @return void
     */
    public function pagoSaldosPendientesDefinirFormaPago($cedula, $valor, $cuenta, $concepto, $conceptoPago, $banco)
    {

        //estado diferente a redimido
        $internoAbono = InternoAbono::where("cedula", $cedula)->whereIn('concepto', $concepto)->where('estado', '!=', 'R')->get();
        if ($internoAbono) {

            $internoSaldo = InternoSaldo::where("cedula", $cedula)->where('estado', '!=', 'L')->get()->first();
            if ($internoSaldo) {

                if ($internoSaldo->valor_acumulado == 0) {

                    foreach ($internoAbono as $interno) {
                        $interno->estado = 'R';
                        $interno->procesado = 'S';
                        $interno->save();
                    }
                    return false;
                } elseif ($internoSaldo->valor_acumulado > 0) {

                    $internoDebito = new InternoDebito();
                    $internoDebito->usuario = 200;
                    $internoDebito->valor_debito = $valor;
                    $internoDebito->fecha = date('Y-m-d H:i:s');
                    $internoDebito->concepto = $conceptoPago; //6 = pago a daviplata
                    $internoDebito->estado = 'P';
                    $internoDebito->procesado = 'S';
                    $internoDebito->interno_saldo = $internoSaldo->id;
                    $internoDebito->cedula = $cedula;
                    $internoDebito->numero_cuenta = $cuenta;
                    $internoDebito->banco = ($conceptoPago == '6') ? '51' : $banco;
                    $internoDebito->tipo_pago = ($conceptoPago == '6') ? 'D' : 'A';

                    $internoDebito->save();

                    $internoSaldo->valor_acumulado = $internoSaldo->valor_acumulado - $valor;
                    $internoSaldo->fecha_ultima = date('Y-m-d');
                    if ($internoSaldo->valor_acumulado == 0) {
                        $internoSaldo->estado = 'L';
                    }
                    $internoSaldo->save();

                    foreach ($internoAbono as $interno) {
                        $interno->estado = 'R';
                        $interno->procesado = 'S';
                        $interno->save();
                    }
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
