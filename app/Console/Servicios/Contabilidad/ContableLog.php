<?php 
namespace  App\Console\Servicios\Contabilidad;

class ContableLog
{
    public static $file;
    public static $fileLog;

    public static function initLog($headers, $filename='')
    {
        self::$fileLog = ($filename != '')? $filename : "/var/www/html/SYS/public/temp/pagos_interfaz_".strtotime('now').".csv";
        self::$file = fopen(self::$fileLog, "w+");
        $texto_write = implode('|', $headers);
        fwrite(self::$file, strtoupper($texto_write) . "\r\n");
        fclose(self::$file);
        chmod(self::$fileLog, 0777);
    }

    public static function closeLog()
    {
        chmod(self::$fileLog, 0777);
    }

    public static function addLine($data)
    {
        $texto_write = implode('|', $data);
        file_put_contents(self::$fileLog, strtoupper($texto_write) . "\r\n", FILE_APPEND);   
    }

    public static function sanetizar($string)
    {
        $string = str_replace("NÃ¯Â¿Â½","N", $string);
        $string = str_replace("ALPÃ‰S","ALPES",$string);
        $string = str_replace("URBANIZACIÃ“N", "URBANIZACION", $string);
        $string = str_replace("NÃ‚Âº", "NU", $string);
        $string = str_replace("CARRERA", "CR", $string);
        $string = str_replace(array('Ã±',"Ã`","Ã‘",'ÑOB','?','Ñ'),'N',$string);
        $string = str_replace("NÂ°","NU", $string);
        $string = str_replace("N°","NU", $string);
        $string = str_replace("NIÃAA½O","NINO", $string);
        $string = str_replace("\t"," ", $string);
        $string = str_replace("\n"," ", $string);
        
        $string = str_replace(
        array("¨", "º", "-", "~","·",",","$", "%", "&", "/","°","*","|",
        "(", ")", "?", "'", "¡",
        "¿", "[", "^", "<code>", "]",
        "+", "}", "{", "¨", "´",
        ">", "< ", ";", ",", ":", "Ã","Â","¿"),' ', $string);
        return $string;
    }
}