<?php

declare(strict_types=1);

namespace App\Console\Servicios\Contabilidad;

use App\Models\CuotaNoCobrada;
use Illuminate\Support\Facades\DB;

class CruceFormasPago
{

    protected $console;

    public function set_env($console)
    {
        $this->console = $console;
    }

    public function principal($fecha)
    {

        DB::table("tmp_cruce_daviplata")->truncate();

        $cuotaNoCobradas = CuotaNoCobrada::where('fecha', '>=', $fecha)->get();
        foreach ($cuotaNoCobradas as $cuotaNoCobrada) {

            $this->buscarConyuge($cuotaNoCobrada);
            $this->buscarTrabajador($cuotaNoCobrada);

            $query = DB::table('tmp_cruce_daviplata')->where('cedula', $cuotaNoCobrada->cedula)->get();
            $responsable = ($query) ? collect($query)->first() : false;

            if (!$responsable) {
                $this->console->error($cuotaNoCobrada->cedula);
            } else {
                $this->console->info($cuotaNoCobrada->cedula);
            }
        }
    }

    function buscarConyuge($cuotaNoCobrada)
    {
        $responsable = false;
        $resultQuery = DB::select(
            "SELECT
                subsi20.cedcon as 'cedula', 
                subsi20.codban as 'banco', 
                subsi20.prinom, 
                subsi20.segnom, 
                subsi20.priape, 
                subsi20.segape, 
                subsi15.nit, 
                subsi20.tippag, 
                subsi20.telefono, 
                subsi20.tippag,
                subsi20.numcue, 
                subsi48.detalle as 'empresa' 
                FROM subsi20 
                INNER JOIN subsi21 ON subsi20.cedcon = subsi21.cedcon 
                LEFT JOIN subsi15 ON subsi15.cedtra = subsi21.cedtra 
                LEFT JOIN subsi48 ON subsi48.nit = subsi15.nit and subsi48.codsuc = subsi15.codsuc 
                WHERE subsi20.tippag = :tippag and subsi20.cedcon=:cedcon",
            [
                'cedcon' => $cuotaNoCobrada->cedula,
                'tippag' => 'A'
            ]
        );

        if ($resultQuery) {
            $responsable = collect($resultQuery)->first();
            if (($responsable->banco) && ($responsable->numcue)) {
                DB::insert(
                    'INSERT INTO tmp_cruce_daviplata (cedula, nit, empresa, telefono, nombre, tipo, forma_pago, cuenta) values (?,?,?,?,?,?,?,?)',
                    [
                        $responsable->cedula,
                        $responsable->nit,
                        $responsable->empresa,
                        ($responsable->telefono < 3000000000) ? '' : $responsable->telefono,
                        "{$responsable->prinom} {$responsable->segnom} {$responsable->priape} {$responsable->segape}",
                        'conyuge',
                        ($responsable->tippag == 'A') ?  'Abono' : 'Otra',
                        ($responsable->tippag == 'A') ?  $responsable->numcue : ''
                    ]
                );

                $cuotaNoCobrada->cuenta_abonar = ($responsable->banco) ? $responsable->numcue : null;
                $cuotaNoCobrada->banco = ($responsable->numcue) ? $responsable->banco : null;
                $cuotaNoCobrada->save();
            }
        }
        return $responsable;
    }

    function buscarTrabajador($cuotaNoCobrada)
    {

        $responsable = false;
        $resultQuery = DB::select(
            "SELECT
            subsi15.cedtra as 'cedula', 
            subsi15.codban as 'banco',
            subsi15.prinom, 
            subsi15.segnom, 
            subsi15.priape, 
            subsi15.segape, 
            subsi15.nit, 
            subsi15.tippag, 
            subsi15.numcue,
            subsi15.telefono, 
            subsi48.detalle as 'empresa' 
            FROM subsi15 
            LEFT JOIN subsi48 ON subsi48.nit = subsi15.nit and subsi48.codsuc = subsi15.codsuc 
            WHERE subsi15.tippag = :tippag 
            AND subsi15.cedtra = :cedtra  
            AND subsi15.cedtra NOT IN(SELECT DISTINCT cedula FROM tmp_cruce_daviplata)",
            [
                'cedtra' => $cuotaNoCobrada->cedula,
                'tippag' => 'A'
            ]
        );

        if ($resultQuery) {
            $responsable = collect($resultQuery)->first();
            if (($responsable->banco) && ($responsable->numcue)) {
                DB::insert(
                    'INSERT INTO tmp_cruce_daviplata (cedula, nit, empresa, telefono, nombre, tipo, forma_pago, cuenta) values (?,?,?,?,?,?,?,?)',
                    [
                        $responsable->cedula,
                        $responsable->nit,
                        $responsable->empresa,
                        ($responsable->telefono < 3000000000) ? '' : $responsable->telefono,
                        "{$responsable->prinom} {$responsable->segnom} {$responsable->priape} {$responsable->segape}",
                        'trabajador',
                        ($responsable->tippag == 'A') ?  'Abono' : 'Otra',
                        ($responsable->tippag == 'A') ?  $responsable->numcue : ''
                    ]
                );

                $cuotaNoCobrada->cuenta_abonar = ($responsable->banco) ? $responsable->numcue : null;
                $cuotaNoCobrada->banco = ($responsable->numcue) ? $responsable->banco : null;
                $cuotaNoCobrada->save();
            }
        }

        return $responsable;
    }
}
