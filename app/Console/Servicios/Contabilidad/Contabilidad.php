<?php

declare(strict_types=1);
require_once base_path('app/Libraries/PHPExcel.php');
require_once base_path('app/Libraries/PHPExcel/IOFactory.php');
require_once base_path('app/Libraries/PHPExcel/Shared/Date.php');

use App\Exceptions\DebugException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use App\Console\Servicios\Comandos\Comandos;
use App\Console\Servicios\Contabilidad\AbonoCuentaPlanos;
use App\Console\Servicios\Contabilidad\ControlInternoPlano;
use App\Console\Servicios\Contabilidad\CruceFormasPago;
use App\Console\Servicios\Contabilidad\DaviplataPagosPlanos;
use App\Console\Servicios\Contabilidad\GiradosNoCobrados;
use App\Models\CuotaComprobante;
use App\Models\CuotaNoCobrada;
use App\Models\Subsi15;
use App\Models\Subsi18;
use App\Models\Subsi19;
use App\Models\Subsi20;

class Contabilidad
{

    protected $usuario;
    protected $sistema;
    protected $user;
    protected $console;
    protected $actualizaCuenta;
    protected $fecha;

    private static $file;

    public function __construct($console)
    {
        ini_set('memory_limit', '2300M');
        $this->console = $console;
    }

    public function reprocesarInterfazControlInterno($argv, $user, $sistema, $enviroment)
    {
        $modelo = json_decode(base64_decode($argv), true);
        try {
            try {
                $archivo = "/var/www/html/clisisu/storage/files/tesoreria/" . $modelo['file'];
                if (!file_exists($archivo)) {
                    throw new DebugException("Error el archivo es requerido para procesar", 501);
                }

                $inputFileType = PHPExcel_IOFactory::identify($archivo);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($archivo);
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $sheet->getHighestColumn();

                Comandos::$cantidad = $highestRow;
                Comandos::setProporcion();
                $fraccion = Comandos::$cantidad / Comandos::$proporcion;
                $progreso_ini_conteo = 0;
                $num = 0;

                ControlInternoPlano::initLog($sistema);
                $controlInternoPlano = new ControlInternoPlano();
                $controlInternoPlano->set_env($this->console, '16/02/2023');

                for ($row = 2; $row <= $highestRow; $row++) {
                    $num++;
                    $progreso_ini_conteo++;
                    if ($progreso_ini_conteo >= $fraccion) {
                        $progreso_ini_conteo = 0;
                        $this->console->comandoProcesador->actualizaProgreso($num);
                    }
                    $controlInternoPlano->principal($sheet, $row);
                }

                ControlInternoPlano::closeLog();

                $salida = $controlInternoPlano->getResultado();
                $this->console->comandoProcesador->finalizarProceso();
                $this->console->info($salida);
            } catch (\Exception $th) {
                throw new DebugException($th->getMessage() . ' ' . $th->getLine() . ' ' . basename($th->getFile()), 501);
            } catch (QueryException $er) {
                throw new DebugException($er->getMessage() . ' ' . $er->getLine() . ' ' . basename($er->getFile()), 501);
            }
        } catch (DebugException $err) {
            fclose(self::$file);
            $this->console->comandoProcesador->cancelarComando();
            $salida = array(
                "success" => false,
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode()
            );
            $this->console->error(json_encode($salida));
        }
    }

    public function depurarAbonosRedimidos($argv, $user, $sistema, $enviroment)
    {
        try {
            try {
                $cedulas = array();
                $query = DB::table('interno_saldos')->where("valor_acumulado", 0)->get();
                foreach ($query as $saldo) {
                    $cedulas[] = $saldo->cedula;
                }

                $query = DB::table('interno_abonos')
                    ->where("concepto", '1')
                    ->whereIn("cedula", $cedulas)
                    ->where("estado", 'P');

                if ($query->count() > 0) {
                    foreach ($query->get() as $abono) {
                        $this->console->info($abono->cedula);
                        /*
                        $internoAbono =  InternoAbono::where('id', $abono->id)->get()->first();
                        $internoAbono->estado = 'R';
                        $internoAbono->procesado = 'S';
                        $internoAbono->save();
                        */
                    }
                }
            } catch (\Exception $th) {
                throw new DebugException($th->getMessage() . ' ' . $th->getLine() . ' ' . basename($th->getFile()), 501);
            } catch (QueryException $er) {
                throw new DebugException($er->getMessage() . ' ' . $er->getLine() . ' ' . basename($er->getFile()), 501);
            }
        } catch (DebugException $err) {
            $salida = array(
                "success" => false,
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode()
            );
            $this->console->error(json_encode($salida));
        }
    }

    /**
     * interfaceDaviplataPlano function
     * procesar interface de pagos usando archivo plano de cargue, para pagos por daviplata, 
     * procesa pagos de Saldos Girados No Cobrados cuenta 230210, parametros archivo excel:
     * A => Cedula
     * B => Valor Debitar
     * C => Detalle
     * D => Marcacion
     * E => Comprobante
     * fecha_contable (dia/mes/año)
     * @param [type] $argv
     * @param [type] $user
     * @param [type] $sistema
     * @param [type] $enviroment
     * @return void
     */
    public function interfaceDaviplataPlano($argv, $user, $sistema, $enviroment)
    {
        try {
            try {
                $modelo = json_decode(base64_decode($argv));
                $fecha = $modelo->fecha;

                $cuotasComprobantes = CuotaComprobante::where('estado', 'C')->where('fecha_detalle', $fecha)->get();
                Comandos::$cantidad = $cuotasComprobantes->count();
                Comandos::setProporcion();
                $fraccion = Comandos::$cantidad / Comandos::$proporcion;
                $progreso_ini_conteo = 0;
                $num = 0;

                DaviplataPagosPlanos::initLog($sistema);
                DaviplataPagosPlanos::$debitar = ($modelo->procesar == 'S') ? True : False;

                $daviplataPagosPlanos = new DaviplataPagosPlanos();

                $daviplataPagosPlanos->set_env($this->console, $fecha);

                foreach ($cuotasComprobantes as $cuotaComprobante) {
                    $num++;
                    $progreso_ini_conteo++;
                    if ($progreso_ini_conteo >= $fraccion) {
                        $progreso_ini_conteo = 0;
                        $this->console->comandoProcesador->actualizaProgreso($num);
                    }
                    $daviplataPagosPlanos->principal($cuotaComprobante);
                }

                DaviplataPagosPlanos::closeLog();

                $salida = $daviplataPagosPlanos->getResultado();

                $this->console->comandoProcesador->finalizarProceso();

                $this->console->info(json_encode($salida));
            } catch (\Exception $th) {
                throw new DebugException($th->getMessage() . ' ' . $th->getLine() . ' ' . basename($th->getFile()), 501);
            } catch (QueryException $er) {
                throw new DebugException($er->getMessage() . ' ' . $er->getLine() . ' ' . basename($er->getFile()), 501);
            }
        } catch (DebugException $err) {
            $this->console->comandoProcesador->cancelarComando();
            $salida = array(
                "success" => false,
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode()
            );
            $this->console->error(json_encode($salida));
        }
    }

    /**
     * interfaceAbonosPlano function
     * procesar interface de pagos usando archivo plano de cargue, para pagos por daviplata, 
     * procesa pagos de Saldos Girados No Cobrados cuenta 230210, parametros archivo excel:
     * A => Cedula
     * B => Valor Debitar
     * C => Detalle
     * D => Marcacion
     * E => Comprobante
     * fecha_contable (dia/mes/año)
     * @param [type] $argv
     * @param [type] $user
     * @param [type] $sistema
     * @param [type] $enviroment
     * @return void
     */
    public function interfaceAbonosPlano($argv, $user, $sistema, $enviroment)
    {
        try {
            try {
                $modelo = json_decode(base64_decode($argv));
                $fecha = $modelo->fecha;

                $cuotasComprobantes = CuotaComprobante::where('estado', 'C')->where('fecha_detalle', $fecha)->get();

                Comandos::$cantidad = $cuotasComprobantes->count();
                Comandos::setProporcion();
                $fraccion = Comandos::$cantidad / Comandos::$proporcion;
                $progreso_ini_conteo = 0;
                $num = 0;

                AbonoCuentaPlanos::initLog($sistema);
                AbonoCuentaPlanos::$debitar = ($modelo->procesar == 'S') ? True : False;

                $abonoCuentaPlanos = new AbonoCuentaPlanos();
                $abonoCuentaPlanos->set_env($this->console, $fecha);

                foreach ($cuotasComprobantes as $cuotaComprobante) {
                    $num++;
                    $progreso_ini_conteo++;
                    if ($progreso_ini_conteo >= $fraccion) {
                        $progreso_ini_conteo = 0;
                        $this->console->comandoProcesador->actualizaProgreso($num);
                    }
                    $abonoCuentaPlanos->principal($cuotaComprobante);
                }

                AbonoCuentaPlanos::closeLog();

                $salida = $abonoCuentaPlanos->getResultado();
                $this->console->comandoProcesador->finalizarProceso();
                $this->console->info(json_encode($salida));
            } catch (\Exception $th) {
                throw new DebugException($th->getMessage() . ' ' . $th->getLine() . ' ' . basename($th->getFile()), 501);
            } catch (QueryException $er) {
                throw new DebugException($er->getMessage() . ' ' . $er->getLine() . ' ' . basename($er->getFile()), 501);
            }
        } catch (DebugException $err) {
            $this->console->comandoProcesador->cancelarComando();
            $salida = array(
                "success" => false,
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode()
            );
            $this->console->error(json_encode($salida));
        }
    }

    /**
     * loadGiradosNoCobrados function
     * girados_no_cobrados_cargue_2023_03_28.xlsx
     * Cargue de archivo excel para crear cada uno de los comprobantes de informaweb que cruza con las cuotas no cobradas
     * tipo pago, indica por donde se desea procesar los pagos, para buscar las cuentas respectivas
     * @param [type] $file
     * @param [type] $user
     * @param [type] $sistema
     * @param [type] $enviroment
     * @return void
     */
    public function loadGiradosNoCobrados($file, $user, $sistema, $enviroment)
    {
        try {
            try {
                $archivo = "/var/www/html/clisisu/storage/files/" . $file;
                $inputFileType = PHPExcel_IOFactory::identify($archivo);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($archivo);
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                $num = 0;

                for ($row = 2; $row <= $highestRow; $row++) {
                    $num++;
                    $cedula = intval($sheet->getCell("A{$row}")->getValue());
                    if ($cedula == '' || is_null($cedula)) continue;

                    $nombre = trim($sheet->getCell("B{$row}")->getValue());
                    $documento = $sheet->getCell("C{$row}")->getValue();
                    $comprobante = $sheet->getCell("D{$row}")->getValue();
                    $valor_detalle  = $sheet->getCell("E{$row}")->getValue();
                    $estado = trim($sheet->getCell("F{$row}")->getValue());
                    $fecha_detalle = date(
                        'Y-m-d',
                        PHPExcel_Shared_Date::ExcelToPHP($sheet->getCell("G{$row}")->getValue() + 1)
                    );
                    $detalle = trim($sheet->getCell("H{$row}")->getValue());

                    $linea = [
                        'cedula' => $cedula,
                        'nombre' => $nombre,
                        'documento' => $documento,
                        'comprobante' => $comprobante,
                        'valor_detalle' => $valor_detalle,
                        'estado' => $estado,
                        'fecha_detalle' => $fecha_detalle,
                        'detalle' => $detalle
                    ];

                    $giradosNoCobrados = new GiradosNoCobrados($this->console);

                    //A = tipo pago abono a cuenta 
                    //D = tipo pago daviplata
                    $giradosNoCobrados->setTipoPago('A');
                    $giradosNoCobrados->main($linea);
                }

                $salida = GiradosNoCobrados::getResultado();
                $this->console->info(json_encode($salida));
            } catch (\Exception $th) {
                throw new DebugException($th->getMessage() . ' ' . $th->getLine() . ' ' . basename($th->getFile()), 501);
            } catch (QueryException $er) {
                throw new DebugException($er->getMessage() . ' ' . $er->getLine() . ' ' . basename($er->getFile()), 501);
            }
        } catch (DebugException $err) {
            $this->console->comandoProcesador->cancelarComando();
            $salida = array(
                "success" => false,
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode()
            );
            $this->console->error(json_encode($salida));
        }
    }

    /**
     * loadNoCobradas function
     * Cargue de archivo excel para registrar las cuotas de informaweb, que se van a procesar para pago
     * con valores consolidados por terceros
     * @param [type] $file
     * @param [type] $user
     * @param [type] $sistema
     * @param [type] $enviroment
     * @return void
     */
    public function loadNoCobradas($file, $user, $sistema, $enviroment)
    {
        try {
            try {
                $archivo = "/var/www/html/clisisu/storage/files/informa/" . $file;
                $inputFileType = PHPExcel_IOFactory::identify($archivo);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($archivo);
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                $num = 0;

                for ($row = 2; $row <= $highestRow; $row++) {
                    $num++;
                    $cedula = intval($sheet->getCell("A{$row}")->getValue());
                    if ($cedula == '' || is_null($cedula)) continue;
                    $nombre = trim($sheet->getCell("B{$row}")->getValue());
                    $detalle = trim($sheet->getCell("C{$row}")->getValue());
                    $valor  = $sheet->getCell("D{$row}")->getValue();
                    $estado = trim($sheet->getCell("E{$row}")->getValue());
                    $fecha = date(
                        'Y-m-d',
                        PHPExcel_Shared_Date::ExcelToPHP($sheet->getCell("F{$row}")->getValue() + 1)
                    );
                    $cantidad = CuotaNoCobrada::where('cedula', $cedula)->where('fecha', $fecha)->count();
                    if ($cantidad  == 0) {
                        $cuotaNoCobrada = new CuotaNoCobrada();
                        $cuotaNoCobrada->cedula = $cedula;
                        $cuotaNoCobrada->nombre = $nombre;
                        $cuotaNoCobrada->cuenta_abonar = null;
                        $cuotaNoCobrada->banco = null;
                        $cuotaNoCobrada->valor = $valor;
                        $cuotaNoCobrada->estado = $estado;
                        $cuotaNoCobrada->fecha = $fecha;
                        $cuotaNoCobrada->detalle = $detalle;
                        $cuotaNoCobrada->save();
                        $this->console->info($cedula . '|' . $valor) . '|';
                    } else {
                        $this->console->error($cedula . '|' . $valor) . '|';
                    }
                }
            } catch (\Exception $th) {
                throw new DebugException($th->getMessage() . ' ' . $th->getLine() . ' ' . basename($th->getFile()), 501);
            } catch (QueryException $er) {
                throw new DebugException($er->getMessage() . ' ' . $er->getLine() . ' ' . basename($er->getFile()), 501);
            }
        } catch (DebugException $err) {
            $this->console->comandoProcesador->cancelarComando();
            $salida = array(
                "success" => false,
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode()
            );
            $this->console->error(json_encode($salida));
        }
    }

    /**
     * cruceFormasPagoNoCobradas function 
     * Se hace el cruce de cuotas_nocobrdas con las formas de pago para certificar las cuentas de daviplata
     * @return void
     */
    public function cruceFormasPagoNoCobradas($fecha, $user, $sistema, $enviroment)
    {
        try {
            try {
                $cruceFormasPago = new CruceFormasPago();
                $cruceFormasPago->set_env($this->console);
                $cruceFormasPago->principal($fecha);
            } catch (\Exception $th) {
                throw new DebugException($th->getMessage() . ' ' . $th->getLine() . ' ' . basename($th->getFile()), 501);
            } catch (QueryException $er) {
                throw new DebugException($er->getMessage() . ' ' . $er->getLine() . ' ' . basename($er->getFile()), 501);
            }
        } catch (DebugException $err) {
            $this->console->comandoProcesador->cancelarComando();
            $salida = array(
                "success" => false,
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode()
            );
            $this->console->error(json_encode($salida));
        }
    }

    /**
     * cruceDepurarPagosInternosPendientes function
     * Saldos Pednientes Definir Forma de Pago
     * Comprobar la fecha si aplica para el proceso o se requiera segmentar por conceptos
     * se encarga de hacer la depuración, de los registros de nocobrados que ya fueron pagos por control interno
     * @param [type] $fecha
     * @param [type] $user
     * @param [type] $sistema
     * @param [type] $enviroment
     * @return void
     */
    public function cruceDepurarPagosInternosPendientes($fecha, $user, $sistema, $enviroment)
    {
        try {
            try {
                DB::table('cuotas_nocobradas')
                    ->where('fecha', '=', $fecha)
                    ->where('valor', 0)
                    ->update(['estado' => 'X']);

                $sql = "SELECT 
                    cn.cedula, 
                    cn.nombre, 
                    cn.valor as valor_informa, 
                    cn.fecha, 
                    cn.detalle,
                    sum(ia.valor_abono) as valor_debitado, 
                    ia.estado as 'estado_interno', 
                    (SELECT sum(ia2.valor_abono) 
                        FROM interno_abonos ia2 
                        WHERE ia2.cedula = cn.cedula and ia2.concepto='2' and ia2.estado='P') as valor_pendiente
                    FROM cuotas_nocobradas cn 
                    INNER join interno_abonos ia ON ia.cedula = cn.cedula   
                    WHERE cn.estado='P' and cn.fecha = :fecha and ia.concepto='2' and ia.estado='R' 
                    GROUP by cn.cedula";

                $resultQuery = DB::select($sql, ['fecha' => $fecha]);

                foreach ($resultQuery as $nocobrado) {

                    if (is_null($nocobrado->valor_pendiente)) {

                        $cuotasNoCobrada = CuotaNoCobrada::where('cedula', $nocobrado->cedula)
                            ->where('fecha', '=', $fecha)
                            ->where('estado', 'P')
                            ->get()
                            ->first();

                        if ($cuotasNoCobrada) {
                            $cuotasNoCobrada->estado = 'X';
                            $cuotasNoCobrada->save();
                        }
                    } else {
                        if ($nocobrado->valor_informa != $nocobrado->valor_pendiente) {
                            $this->console->error("Validar saldo: " . $nocobrado->cedula);
                        }
                    }
                }
            } catch (\Exception $th) {
                throw new DebugException($th->getMessage() . ' ' . $th->getLine() . ' ' . basename($th->getFile()), 501);
            } catch (QueryException $er) {
                throw new DebugException($er->getMessage() . ' ' . $er->getLine() . ' ' . basename($er->getFile()), 501);
            }
        } catch (DebugException $err) {
            $this->console->comandoProcesador->cancelarComando();
            $salida = array(
                "success" => false,
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode()
            );
            $this->console->error(json_encode($salida));
        }
    }

    /**
     * cruceDepurarPagosInternosTarjetas function
     * De Saldos en Tarjetas y Por Pagar
     * Comprobar la fecha si aplica para el proceso o se requiera segmentar por conceptos
     * se encarga de hacer la depuración, de los registros de nocobrados que ya fueron pagos por control interno
     * @param [type] $fecha
     * @param [type] $user
     * @param [type] $sistema
     * @param [type] $enviroment
     * @return void
     */
    public function cruceDepurarPagosInternosTarjetas($fecha, $user, $sistema, $enviroment)
    {
        try {
            try {

                DB::table('cuotas_nocobradas')
                    ->where('fecha', '=', $fecha)
                    ->where('valor', 0)
                    ->update(['estado' => 'X']);

                $sql = "SELECT 
                    cn.cedula, 
                    cn.nombre, 
                    cn.valor as valor_informa, 
                    cn.fecha, 
                    cn.detalle,
                    sum(ia.valor_abono) as valor_debitado, 
                    ia.estado as 'estado_interno', 
                    (SELECT sum(ia2.valor_abono) 
                        FROM interno_abonos ia2 
                        WHERE ia2.cedula = cn.cedula and ia2.concepto IN(1,3) and ia2.estado='P') as valor_pendiente 
                    FROM cuotas_nocobradas cn 
                    INNER join interno_abonos ia ON ia.cedula = cn.cedula   
                    WHERE cn.estado='P' and cn.fecha = :fecha and ia.concepto IN(1,3) and ia.estado='R' 
                    GROUP by cn.cedula";

                $resultQuery = DB::select($sql, ['fecha' => $fecha]);

                foreach ($resultQuery as $nocobrado) {

                    if (is_null($nocobrado->valor_pendiente)) {

                        $cuotasNoCobrada = CuotaNoCobrada::where('cedula', $nocobrado->cedula)
                            ->where('fecha', '=', $fecha)
                            ->where('estado', 'P')
                            ->get()
                            ->first();

                        if ($cuotasNoCobrada) {
                            $cuotasNoCobrada->estado = 'X';
                            $cuotasNoCobrada->save();
                        }
                    } else {
                        if ($nocobrado->valor_informa != $nocobrado->valor_pendiente) {
                            $this->console->error("Validar saldo: " . $nocobrado->cedula);
                        }
                    }
                }
            } catch (\Exception $th) {
                throw new DebugException($th->getMessage() . ' ' . $th->getLine() . ' ' . basename($th->getFile()), 501);
            } catch (QueryException $er) {
                throw new DebugException($er->getMessage() . ' ' . $er->getLine() . ' ' . basename($er->getFile()), 501);
            }
        } catch (DebugException $err) {
            $this->console->comandoProcesador->cancelarComando();
            $salida = array(
                "success" => false,
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode()
            );
            $this->console->error(json_encode($salida));
        }
    }
}
