<?php

declare(strict_types=1);

use App\Console\Servicios\GiroSubsidio\GiroActual;
use Illuminate\Support\Facades\DB;
use App\Exceptions\DebugException;
use App\Models\Subsi02;
use App\Models\Subsi12;
use App\Models\Subsi14;
use App\Models\Subsi15;
use App\Models\Subsi17;
use App\Models\Subsi20;
use App\Services\ProcesadorCuotas;
use Illuminate\Database\QueryException;
use Elibyy\TCPDF\Facades\TCPDF;

class GiroSubsidio
{
	protected $usuario;
	protected $fecha;
	protected $console;
	protected $sistema;
	protected $enviroment;

	public function __construct($console)
	{
		ini_set('memory_limit', '2000M');
		$this->console = $console;
		$this->fecha = date('Y-m-d');
	}

	public function reporteGiroEstadistica($argv, $user, $sistema, $enviroment)
	{
	}

	public function buscarGiroActualCuotasBeneficiarios($argv, $user, $sistema, $enviroment)
	{
		try {
			$giroActual = Subsi14::select(
				'cedtra',
				'cedres',
				'cedcon',
				'codben',
				'periodo',
				DB::raw('SUM(valor) as valor_cuota'),
				DB::raw('COUNT(numcuo) as numero_cuotas')
			)
				->where('parent', 1)
				->groupBy('codben', 'periodo')
				->get();

			$data = [
				'Cedula trabajador',
				'Cedula conyuge',
				'Valor cuota',
				'Número cuotas',
				'Número salarios',
				'Periodo',
				'Codben',
				'Salario Tra',
				'Salario Con',
				'Derecho a cuota'
			];
			$this->console->info(implode(';', $data));

			foreach ($giroActual as $giroCuota) {
				$trabajador = Subsi15::where('cedtra', $giroCuota->cedtra)->get()->first();
				$conyuges = Subsi20::select("subsi20.*")
					->join('subsi21', 'subsi21.cedcon', 'subsi20.cedcon')
					->where('subsi21.cedtra', $trabajador->cedtra)
					->where('subsi20.cedcon', $giroCuota->cedcon)
					->where('subsi21.comper', 'S')
					->get()
					->first();

				$salario_conyuge = 0;
				$salario_trabajador = 0;
				$periodoConfig = Subsi12::where('periodo', $giroCuota->periodo)->get()->first();

				$aporte = $this->aportePlanilla($giroCuota->nit, $giroCuota->cedtra);
				if ($aporte) {
					$salarios = $aporte->salario;
					$salario_trabajador = $aporte->salario;
				} else {
					$salarioMoldel = Subsi17::select('subsi17.*')->where('cedtra', $giroCuota->cedtra)->orderBy('fecha', 'desc')->first();
					$salario_trabajador = $salarioMoldel->salario;
					$salarios = $salarioMoldel->salario;
				}

				if ($conyuges) {
					$conyugeTrabajador = Subsi15::where('cedtra', $giroCuota->cedcon)->where('estado', 'A')->get()->first();
					if ($conyugeTrabajador) {
						$aporte = $this->aportePlanilla($conyugeTrabajador->nit, $conyugeTrabajador->cedtra);
						if ($aporte) {
							$salario_conyuge = $aporte->salario;
							$salarios += $aporte->salario;
						} else {
							$salarioMoldel = Subsi17::select('subsi17.*')->where('cedtra', $conyugeTrabajador->cedtra)->orderBy('fecha', 'desc')->first();
							$salario_conyuge = $salarioMoldel->salario;
							$salarios += $salarioMoldel->salario;
						}
					}
				}

				$numSalarios = $salarios / $periodoConfig->salmin;
				$data = array();
				if ($numSalarios >= 4 && $numSalarios <= 6) {
					$data = [
						$giroCuota->cedtra,
						$giroCuota->cedcon,
						$giroCuota->valor_cuota,
						$giroCuota->numero_cuotas,
						$numSalarios,
						$giroCuota->periodo,
						$giroCuota->codben,
						$salario_trabajador . ';' . $salario_conyuge,
						'1'
					];
				}

				if ($numSalarios > 6) {
					$data = [
						$giroCuota->cedtra,
						$giroCuota->cedcon,
						$giroCuota->valor_cuota,
						$giroCuota->numero_cuotas,
						$numSalarios,
						$giroCuota->periodo,
						$giroCuota->codben,
						$salario_trabajador . ';' . $salario_conyuge,
						'0'
					];
				}
				if (count($data) > 0) {
					$this->console->info(implode(';', $data));
				}
			}
		} catch (DebugException $err) {
			$this->console->comandoProcesador->cancelarComando();
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode()
			);
			$this->console->error(json_encode($salida));
		}
	}

	public function aportePlanilla($nit, $cedula)
	{
		$aporte = DB::table('subsi65')
			->select('subsi65.*', 'subsi65.salbas as salario')
			->join('subsi64', 'subsi64.numero', '=', 'subsi65.numero')
			->where("subsi65.cedtra", $cedula)
			->where("subsi64.nit", $nit)
			->orderBy("subsi64.perapo", 'desc')
			->get()
			->first();
		return $aporte;
	}

	public function procesarGiro($argv, $user, $sistema, $enviroment)
	{
		$procesador = new ProcesadorCuotas();
		$empresa = new Subsi02(); // crear una instancia de la clase Empresa con los datos necesarios
		$trabajadores = $empresa->trabajadores();
		$procesador->procesarCuotas($trabajadores, $empresa, '202304');
	}
}
