<?php

namespace App\Console\Servicios\Tesoreria;

use Illuminate\Support\Facades\DB;
use App\Exceptions\DebugException;
use Illuminate\Database\QueryException;
use DateTime;
use App\Models\Asopa05;
use App\Models\Asopa07;
use App\Models\Asopa03;
use App\Models\Asopa04;
use App\Models\Subsi15;
use App\Models\Subsi20;

class NovedadMonetaria implements INovedades
{
    protected $novedad;
    protected $tarjeta;
    protected $cedula;
    protected $valor;
    protected $motivo;
    protected $coddoc;
    protected $fecha;
    protected $tipo_documento;

    public static $nit = "8911900472";
    public static $idseg = "636877";
    public static $subtipo = "101";

    public static $file;
    public static $file_fecha;
    public static $codsup = "013";
    public static $consecutivo;
    public static $cantidad = 0;
    public static $monto = 0;
    public static $path;
    public static $file_novedad = "";

    public static function Inicializa(string $fecha, int $consecutivo)
    {
        self::$consecutivo = $consecutivo;
        $date = new DateTime($fecha);
        self::$file_fecha = $date->format('Ymd');
        $cons_nombre = array(
            'C',
            self::$codsup,
            substr(self::$file_fecha, 2, 8),
            str_pad(self::$consecutivo, 3, "0", STR_PAD_LEFT),
            '.txt'
        );
        self::$file_novedad = implode("", $cons_nombre);
        if (file_exists(self::$path . self::$file_novedad)) {
            self::$file_novedad = str_replace('.txt', '-' . date("Hi") . ".txt", self::$file_novedad);
        }
        self::$file = fopen(self::$path . self::$file_novedad, "a+");
    }

    /**
     * debita function
     * debita = 1
     * estado = P procesado
     * @param string $tarjeta
     * @return string
     */
    public function debita(): bool
    {
        $tipnov = 1;
        $this->novedad = new Asopa05();
        $this->novedad->numtar = $this->tarjeta;
        $this->novedad->cedres = $this->cedula;
        $this->novedad->valor = $this->valor;
        $this->novedad->fecha = $this->fecha;
        $this->novedad->tipnov = $tipnov;
        $this->novedad->estado = 'P';
        $this->novedad->save();

        $tsaldos = Asopa07::where("numtar", $this->tarjeta)->get()->first();
        Asopa07::where('numtar', $this->tarjeta)
        ->update([
            "saldo" => ($tsaldos->saldo - $this->valor),
            "fecha" => $this->fecha
        ]);

        self::$cantidad++;
        self::$monto += $this->valor;

        $numtar = str_pad(self::$idseg . str_pad($this->tarjeta, 10, "0", STR_PAD_LEFT), 19);
        $date = new DateTime($this->fecha);
        $salida = array(
            str_pad(self::$nit, 15, "0", STR_PAD_LEFT),
            $date->format('Ymd'),
            str_pad(self::$consecutivo, 5, "0", STR_PAD_LEFT),
            str_pad($this->tipo_documento, 2, " ", STR_PAD_LEFT),
            str_pad($this->cedula, 15, "0", STR_PAD_LEFT),
            str_pad($numtar, 19),
            str_pad($numtar, 19),
            $tipnov,
            str_pad($this->valor.'00', 17, "0", STR_PAD_LEFT),
            "01",
            str_pad(" ", 29),
            "\n"
        );
        fwrite(self::$file, implode("", $salida));
        return true;
    }

    public function abona(): bool
    {
        return true;
    }

    public function getNovedad(): Asopa05
    {
        return $this->novedad;
    }

    public function __construct(int $cedula, int $tarjeta, int $valor, string $motivo, string $fecha, $coddoc='')
    {
        $this->tarjeta = $tarjeta;
        $this->cedula = $cedula;
        $this->valor = $valor;
        $this->motivo = $motivo;
        $this->fecha = $fecha;
        $this->coddoc = $coddoc;
        $this->comprobar_datos();
    }

    public function comprobar_saldos(): bool
    {
        $tsaldos = Asopa07::where("numtar", $this->tarjeta)->get()->first();
        if ($tsaldos == false) {
            return false;
        }
        if ($tsaldos->saldo == 0 || $tsaldos->saldo < $this->valor) {
            return false;
        }
        return true;
    }

    public function comprobar_datos(): bool
    {
        //buscra en trazabiliad de las tarjetas asopagos
        $ttarjeta = Asopa04::where('numtar', $this->tarjeta)->get()->first();
        if ($ttarjeta == false) {
            $this->tipo_documento = 2;
            return true;
        }

        $cedres = $ttarjeta->cedres;
        $trabajador = Subsi15::where('cedtra', $cedres)->get()->first();
        $conyugue = Subsi20::where('cedcon', $cedres)->get()->first();
        
        if ($trabajador) {
            $this->coddoc = $trabajador->coddoc;
        } elseif ($conyugue) {
            $this->coddoc = $conyugue->coddoc;
        } else {
            if(empty($this->coddoc)){

                $this->tipo_documento = 2;
                return true;
            }
        }
        switch ($this->coddoc) {
            case '1': //cedula
                $this->tipo_documento = 2;
                break;
            case '2':
                $this->tipo_documento = 4;
                break;
            case '3':
                $this->tipo_documento = 1;
                break;
            case '4':
                $this->tipo_documento = 3;
                break;
            case '5':
                $this->tipo_documento = 9;
                break;
            case '6':
                $this->tipo_documento = 9;
                break;
            case '7':
                $this->tipo_documento = 5;
                break;
            default:
                $this->tipo_documento = 2;
            break;
        }
        return true;
    }

    public static function Consolidar(): string
    {
        $footer = array(
            str_pad(self::$nit, 15, "0", STR_PAD_LEFT),
            self::$file_fecha,
            str_pad(self::$consecutivo, 5, "0", STR_PAD_LEFT),
            str_pad(self::$cantidad + 1, 6, "0", STR_PAD_LEFT),
            str_pad(" ", 34),
            str_pad(self::$monto . "00", 19, "00", STR_PAD_LEFT),
            str_pad(strrev(self::$monto . "00"), 19, "0", STR_PAD_RIGHT),
            str_pad(" ", 13),
            self::$idseg,
            self::$subtipo,
            "T",
            str_pad(" ", 3),
            "\n"
        );
        fwrite(self::$file, implode("", $footer));
        fclose(self::$file);

        return self::$path . '' . self::$file_novedad;
    }
}
