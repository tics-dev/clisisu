<?php 
declare(strict_types=1);
namespace App\Console\Servicios\Tesoreria;

use App\Models\Asopa03;
use App\Models\Subsi15;
use App\Models\Subsi168;
use App\Models\Subsi20;
use DateTime;

class BloqueoTarjetas
{
    public static $procesados = array();
    public static $file;
    public static $file_novedad;
    public static $consecutivo;
    public static $cantidad = 0;
    public static $file_fecha;
    public static $path;
    public static $nit = "8911900472";
    public static $idseg = "636877";
    public static $subtipo = "101";
    public static $codsup = "013";
    protected $tipoDocumento;
    
    public static function Inicializa(string $sistema, string $fecha, int $consecutivo)
    {
        self::$path = "/var/www/html/{$sistema}/public/temp/";
        self::$consecutivo = $consecutivo;
        $date = new DateTime($fecha);
        self::$file_fecha = $date->format('Ymd');
        
        $cons_nombre = array(
            'A01',
            self::$codsup,
            substr(self::$file_fecha, 2, 8),
            str_pad("".self::$consecutivo, 3, "0", STR_PAD_LEFT),
            '.txt'
        );
        self::$file_novedad = implode('', $cons_nombre);
        if (file_exists(self::$path . self::$file_novedad)) {
            self::$file_novedad = str_replace('.txt', '-' . date("Hi") . ".txt", self::$file_novedad);
        }
        self::$file = fopen(self::$path . self::$file_novedad, "a+");
    }

    public static function addLine(Asopa03 $novedad, $responsable)
    {
        $texto_write = implode('', array(
            $novedad->tipnov,
            str_pad("0", 14, "0", STR_PAD_LEFT),
            "636877",
            str_pad("{$novedad->numtar}", 10, "0", STR_PAD_LEFT),
            str_pad("N", 4, " ", STR_PAD_LEFT),
            $responsable['tipo_documento'],
            str_pad("{$novedad->cedres}",15,"0",STR_PAD_LEFT),
            substr(str_pad($responsable['priape'],15),0,15),
            substr(str_pad($responsable['segape'],15),0,15),
            substr(str_pad($responsable['prinom'],15),0,15),
            substr(str_pad($responsable['segnom'],15),0,15),
            $responsable['nomcor'],
            $responsable['nomrea'],
            str_pad($responsable['fecnac'],8),
            $responsable['sexo'],
            $responsable['estciv'],
            str_pad(substr($responsable['direccion'],0,40), 40),
            str_pad(substr($responsable['direccion'],0,40), 40),
            $responsable['dep'],
            $responsable['ciu'],
            "999999",
            str_pad(substr($responsable['direccion'],0,40), 40),
            str_pad(substr($responsable['direccion'],0,40), 40),
            $responsable['dep'],
            $responsable['ciu'],
            "999999",
            "00000",
            str_pad(substr($responsable['telefono'],0,14), 14),
            str_pad(substr($responsable['telefono'],0,14), 14),
            "00000",
            "000000000000000",
            "00000000",
            "000",
            "000",
            "001",
            "000000",
            " ",
            str_pad(" ",4),
            "0000000000",
            str_pad("0",19),
            "000",
            "209",
            str_pad($novedad->codblo, 2),
            str_pad($novedad->motblo, 2),
            "01",
            str_pad(" ",36)
        ));
        fwrite(self::$file, strtoupper($texto_write)."\r\n");
        self::$cantidad++;
    }

    public function procesarBloqueo($novedad)
    {
        $rps = Subsi20::where('cedcon', $novedad->cedres)->get()->first();
        if(!$rps)
        {
            $rps = Subsi15::where('cedtra', $novedad->cedres)->get()->first();
            if(!$rps)
            {
                $rps = Subsi168::where('cedtra', $novedad->cedres)->get()->first();
            }
        }
        if(!$rps){
            return false;
        }

        $responsable = collect($rps->toArray())->map(function($item){
            return (string) $this->sanetizar($item);
        });

        if($this->tipoDocumento){
            $responsable['coddoc'] = $this->tipoDocumento; 
        }
        switch ($responsable['coddoc']) {
            case '1': //cedula
                $responsable['tipo_documento'] = 2;
                break;
            case '2':
                $responsable['tipo_documento'] = 4;
                break;
            case '3':
                $responsable['tipo_documento'] = 1;
                break;
            case '4':
                $responsable['tipo_documento'] = 3;
                break;
            case '5':
                $responsable['tipo_documento'] = 9;
                break;
            case '6':
                $responsable['tipo_documento'] = 9;
                break;
            case '7':
                $responsable['tipo_documento'] = 5;
                break;
            default:
                $responsable['tipo_documento'] = 2;
            break;
        }

        switch ($responsable['estciv'])
        {
            case '1':
                $responsable['estciv'] = 1;
                break;
            case '2':
                $responsable['estciv'] = 5;
                break;
            case '3':
                $responsable['estciv'] = 3;
                break;
            case '4':
                $responsable['estciv'] = 4;
                break;
            case '5':
                $responsable['estciv'] = 2;
                break;
            case '6':
                $responsable['estciv'] = 2;
            break;
        }

        $responsable['fecnac'] = ($responsable['fecnac'])? str_replace(" ",'', "".$responsable['fecnac']) : "";

        $nomcor = substr($responsable['prinom'].' '.$responsable['priape'], 0, 20);
        $responsable['nomcor'] = str_pad($nomcor, 20);
        
        $nomrea = substr($responsable['prinom'].' '.substr(''.$responsable['segnom'],0,1).' '.$responsable['priape'].' '.substr(''.$responsable['segape'],0,1), 0, 26);
        $responsable['nomrea'] = str_pad($nomrea,26);
        
        $responsable['dep'] = substr($responsable['codzon'], 0, 2);
        $responsable['ciu'] = $responsable['codzon'];

        $this->addLine($novedad, $responsable);
        return true;
    }

    public function getResultado()
    {   
        return [
            "success" => true,
            "msj" => "Proceso completado con éxito",
            "url" => self::$path.''.self::$file_novedad,
            "filename" => self::$file_novedad 
        ];
    }

    public static function Consolidar(): string
    {
        $cantidad = self::$cantidad;
        $footer = array(
            str_pad("".self::$nit, 15, "0", STR_PAD_LEFT),
            self::$file_fecha,
            str_pad("".self::$consecutivo, 5, "0", STR_PAD_LEFT),
            str_pad("{$cantidad}", 6, "0", STR_PAD_LEFT),
            str_pad("00", 2),
            str_pad(" ", 427),
            self::$idseg,
            self::$subtipo,
            "T",
            str_pad(" ", 3)
        );
        fwrite(self::$file, implode('', $footer)."\r\n");
        fclose(self::$file);
        chmod(self::$path . '' . self::$file_novedad, 0777);
        return self::$path . '' . self::$file_novedad;
    }

    public function sanetizar($string)
    {
        $string = str_replace("NÃ¯Â¿Â½","N", $string);
        $string = str_replace("ALPÃ‰S","ALPES",$string);
        $string = str_replace("URBANIZACIÃ“N", "URBANIZACION", $string);
        $string = str_replace("NÃ‚Âº", "NU", $string);
        $string = str_replace("CARRERA", "CR", $string);
        $string = str_replace(array('Ã±',"Ã`","Ã‘",'ÑOB','?','Ñ'),'N',$string);
        $string = str_replace("NÂ°","NU", $string);
        $string = str_replace("N°","NU", $string);
        $string = str_replace("NIÃAA½O","NINO", $string);
        $string = str_replace("\t"," ", $string);
        $string = str_replace("\n"," ", $string);
        
        $string = str_replace(
			array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
			array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
			$string
		);
		$string = str_replace(
			array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
			array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
			$string
		);
		$string = str_replace(
			array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
			array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
			$string
		);
		$string = str_replace(
			array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
			array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
			$string
		);
		$string = str_replace(
			array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
			array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
			$string
		);
		$string = str_replace(
			array('ñ', 'Ñ', 'ç', 'Ç'),
			array('n', 'N', 'c', 'C',),
			$string
		);
        $string = str_replace(
        array("¨", "º", "-", "~","·",",","$", "%", "&", "/","°","*",
        "(", ")", "?", "'", "¡",
        "¿", "[", "^", "<code>", "]",
        "+", "}", "{", "¨", "´",
        ">", "< ", ";", ",", ":", "Ã","Â","¿"),' ', $string);
        return $string;
    }

    public function setTipoDocumento($tipoDocumento)
	{
		$this->tipoDocumento = $tipoDocumento;
	}
}