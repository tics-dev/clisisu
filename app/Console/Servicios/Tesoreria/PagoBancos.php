<?php

declare(strict_types=1);

namespace App\Console\Servicios\Tesoreria;

use App\Exceptions\DebugException;
use App\Models\Banco;
use App\Models\InternoDebito;
use DateTime;

class PagoBancos
{

    public static $files = [];
    public static $path;
    public static $file_inter_bancarios;
    public static $file_caja_social;

    public static function Inicializa($sistema)
    {
        self::$file_inter_bancarios = "PagoAbonos-Interbancarios-" . date('Ym') . ".txt";
        self::$file_caja_social = "PagoAbonos-CajaSocial-" . date('Ym') . ".txt";
        self::$path = "/var/www/html/{$sistema}/public/temp/";
        if (file_exists(self::$path . '' . self::$file_inter_bancarios)) {
            unlink(self::$path . '' . self::$file_inter_bancarios);
        }

        if (file_exists(self::$path . '' . self::$file_caja_social)) {
            unlink(self::$path . '' . self::$file_caja_social);
        }
        $file = fopen(self::$path . self::$file_inter_bancarios, "a+");
        fclose($file);
        $file = fopen(self::$path . self::$file_caja_social, "a+");
        fclose($file);
    }

    public static function addInterBancarios($valor, string $cuenta, $code_banco, $cedula, $nombre)
    {
        $content = array(
            "632",
            $valor,
            "00",
            strval($cuenta),
            $code_banco,
            str_pad("{$cedula}", 15),
            str_pad("{$nombre}", 22),
            "V ",
            str_pad("", 13),
            str_pad("PAGO", 10),
            str_pad("PROVEEDORES", 30),
            str_pad("", 26),
            "."
        );

        $content_text = implode($content);
        file_put_contents(self::$path . '' . self::$file_inter_bancarios, $content_text . "\n", FILE_APPEND);
    }

    public static function addCajaSocial($valor, string $cuenta, $code_banco, $cedula, $nombre)
    {
        $content = array(
            "632",
            $valor,
            "00",
            strval($cuenta),
            $code_banco,
            str_pad("{$cedula}", 15),
            str_pad("{$nombre}", 22),
            "V ",
            str_pad("", 13),
            str_pad("PAGO", 10),
            str_pad("SUBSIDIO", 30),
            str_pad("", 26),
            "."
        );

        $content_text = implode($content);
        file_put_contents(self::$path . '' . self::$file_caja_social, $content_text . "\n", FILE_APPEND);
    }

    public function procesarLineaExcel($cedula, $fullname, $valor, $banco, $cuenta)
    {
        if (!is_numeric($cedula) || is_null($cedula)) {
            throw new DebugException("Error cedula del responsable {$cedula}", 501);
        }

        if (!is_numeric($valor) || is_null($valor)) {
            throw new DebugException("Error valor de pago {$valor}", 501);
        }

        if ($fullname == '' || is_null($fullname)) {
            throw new DebugException("Error nombre del responsable {$fullname}", 501);
        }

        if ($banco == '' || is_null($banco)) {
            throw new DebugException("Error banco del responsable {$banco}", 501);
        }

        $fullname = trim($this->sanetizar($fullname));
        $nombre = substr($fullname, 0, 21);
        $valor = str_pad("{$valor}", 10, "0", STR_PAD_LEFT);
        $bancoEntity = Banco::where("codban", $banco)->get()->first();
        if (!$bancoEntity) {
            throw new DebugException("Error banco {$banco} no disponible", 501);
        }

        $code_banco = str_pad("{$bancoEntity->codban1}", 9, "0", STR_PAD_LEFT);

        $cuenta = str_replace('_', '', $cuenta);
        $cuenta = (string) str_pad("{$cuenta}", 17);

        if ($banco == 32) {
            $this->addCajaSocial($valor, $cuenta, $code_banco, $cedula, $nombre);
        } else {
            $this->addInterBancarios($valor, $cuenta, $code_banco, $cedula, $nombre);
        }
    }

    public static function Consolidar()
    {
        chmod(self::$path . '' . self::$file_caja_social, 0775);
        chmod(self::$path . '' . self::$file_inter_bancarios, 0775);
    }

    public function getResultado()
    {
        return [
            "inter_bancarios" => self::$file_inter_bancarios,
            "caja_social" => self::$file_caja_social,
            "success" => true,
            "msj" => "Proceso completado con éxito"
        ];
    }

    public function sanetizar($string)
    {
        $string = str_replace(array('Ã±', "Ã`", "Ã‘", 'ÑOB', '?', 'Ñ'), 'N', $string);
        $string = str_replace("NÂ°", "NU", $string);
        $string = str_replace("N°", "NU", $string);
        $string = str_replace("\t", " ", $string);
        $string = str_replace("\n", " ", $string);

        $string = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $string
        );
        $string = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $string
        );
        $string = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $string
        );
        $string = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $string
        );
        $string = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $string
        );
        $string = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'N', 'c', 'C',),
            $string
        );
        $string = str_replace(
            array(
                "¨", "º", "-", "~", "·", ",", "$", "%", "&", "/", "°", "*",
                "(", ")", "?", "'", "¡",
                "¿", "[", "^", "<code>", "]",
                "+", "}", "{", "¨", "´",
                ">", "< ", ";", ",", ":", "Ã", "Â", "¿"
            ),
            ' ',
            $string
        );
        return $string;
    }
}
