<?php
namespace App\Console\Servicios\Tesoreria;
use App\Models\Asopa05;

interface  INovedades {

    public function debita(): bool;
    
    public function abona(): bool;

    public function getNovedad(): Asopa05;
}