<?php

declare(strict_types=1);

require_once base_path('app/Libraries/PHPExcel.php');
require_once base_path('app/Libraries/PHPExcel/IOFactory.php');
require_once base_path('app/Libraries/PHPExcel/Shared/Date.php');

use App\Console\Servicios\Comandos\Comandos;
use App\Console\Servicios\Tesoreria\BloqueoTarjetas;
use Illuminate\Support\Facades\DB;
use App\Exceptions\DebugException;
use Illuminate\Database\QueryException;
use App\Console\Servicios\Tesoreria\NovedadMonetaria;
use App\Console\Servicios\Tesoreria\PagoBancos;
use App\Models\Asopa03;
use App\Models\PrescripcionCuota;


class Tesoreria
{
	protected $usuario;
	protected $fecha;
	protected $console;
	protected $sistema;
	protected $enviroment;
	protected $bloqueoTarjetas;
	protected $novedad_monetaria;

	public function __construct($console)
	{
		$this->console = $console;
		$f = new DateTime('now');
		$this->fecha = $f->format('Y-m-d');
		$this->bloqueoTarjetas = new BloqueoTarjetas();
	}

	/**
	 * debitaMasivoTarjetas function
	 * Metodo genera reporte para debitar saldos en tarjeta, para proceso de prescripción
	 * @param [type] $argv
	 * @param [type] $user
	 * @param [type] $sistema
	 * @param [type] $enviroment
	 * @return void
	 */
	public function debitaMasivoTarjetas($argv, $user, $sistema, $enviroment)
	{
		$this->usuario = $user;
		$this->sistema = $sistema;
		$this->enviroment = $enviroment;
		try {
			try {
				$request = json_decode(base64_decode($argv), true);
				if (!$request['file']) {
					throw new DebugException("Error el nombre del archivo a recorrer no existe", 501);
				}
				if (!$request['fecha']) {
					throw new DebugException("Error la fecha de generación del reporte es necesaria", 501);
				}
				if (!$request['consecutivo']) {
					throw new DebugException("Error el consecutivo es necesario", 501);
				}

				$archivo = storage_path('files/' . $request['file']);
				$inputFileType = PHPExcel_IOFactory::identify($archivo);
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);

				$objPHPExcel = $objReader->load($archivo);
				$sheet = $objPHPExcel->getSheet(0);
				$highestRow = $sheet->getHighestRow();
				$sheet->getHighestColumn();

				NovedadMonetaria::$path = "/var/www/html/{$sistema}/public/temp/";
				NovedadMonetaria::Inicializa($request['fecha'], intval($request['consecutivo']));

				$path = storage_path('logs/log_debita_masivo.csv');
				if (file_exists($path)) unlink($path);

				$log = fopen($path, "a+");
				fwrite($log, strtoupper("cedula;tarjeta;valor;motivo;fecha;periodo\r\n"));

				for ($row = 2; $row <= $highestRow; $row++) {
					$cedula = intval($sheet->getCell("A{$row}")->getValue());
					if (!$cedula) {
						continue;
					}
					$tarjeta = intval($sheet->getCell("B{$row}")->getValue());
					$valor = intval($sheet->getCell("C{$row}")->getValue());
					$novedad = $sheet->getCell("D{$row}")->getValue();
					$fecha = date('Y-m-d', strtotime($sheet->getCell("E{$row}")->getValue()) + 1);
					$motivo = $sheet->getCell("F{$row}")->getValue();
					$periodo = intval($sheet->getCell("G{$row}")->getValue()) + 1;

					$novedad_monetaria = new NovedadMonetaria($cedula, $tarjeta, $valor, $motivo, $fecha);
					if ($novedad_monetaria->comprobar_saldos() == FALSE) {

						PrescripcionCuota::where("tipo", 'TA')
							->where("cedres", $cedula)
							->where("periodo", $periodo)
							->update([
								"estado" => "AD"
							]);
						fwrite($log, "{$cedula};{$tarjeta};{$valor};{$motivo};{$fecha};{$periodo}\r\n");
					} else {
						if ($novedad == "D") {
							$novedad_monetaria->debita($tarjeta);
						}
						if ($novedad == "A") {
							$novedad_monetaria->abona($tarjeta);
						}
					}
				}
				fclose($log);
				$archivo_consolidado = NovedadMonetaria::Consolidar();
				$salida = array(
					'success' => true,
					'archivo_consolidado' => $archivo_consolidado,
					'msj' => "El proceso se completo OK!"
				);
				$this->console->info(json_encode($salida));
			} catch (QueryException $sql_err) {
				$msj = $sql_err->getMessage() . ' ' . basename($sql_err->getFile()) . ' ' . $sql_err->getLine();
				throw new DebugException($msj, 1);
			} catch (ErrorException $th) {
				$msj = $th->getMessage() . ' ' . basename($th->getFile()) . ' ' . $th->getLine();
				throw new DebugException($msj, 1);
			}
		} catch (DebugException $err) {
			$salida = array(
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode(),
				'success' => false
			);
			$this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
		}
	}

	/**
	 * crearNovedadesDebito function
	 * Metodo para realizar el archivo de novedades para debitar de forma masiva los saldos en Asopagos
	 * @param [type] $argv
	 * @param [type] $user
	 * @param [type] $sistema
	 * @param [type] $enviroment
	 * @return void
	 */
	public function crearNovedadesDebito($argv, $user, $sistema, $enviroment)
	{
		$this->usuario = $user;
		$this->sistema = $sistema;
		$this->enviroment = $enviroment;

		try {
			try {
				$request = json_decode(base64_decode($argv), true);
				if (!$request['file']) {
					throw new DebugException("Error el nombre del archivo a recorrer no existe", 501);
				}
				if (!$request['fecha']) {
					throw new DebugException("Error la fecha de generación del reporte es necesaria", 501);
				}
				if (!$request['consecutivo']) {
					throw new DebugException("Error el consecutivo es necesario", 501);
				}

				$archivo = storage_path('files/' . $request['file']);
				$inputFileType = PHPExcel_IOFactory::identify($archivo);
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);

				$objPHPExcel = $objReader->load($archivo);
				$sheet = $objPHPExcel->getSheet(0);
				$highestRow = $sheet->getHighestRow();
				$sheet->getHighestColumn();

				NovedadMonetaria::$path = "/var/www/html/{$sistema}/public/temp/";
				NovedadMonetaria::Inicializa(
					$request['fecha'],
					intval($request['consecutivo'])
				);

				$path = storage_path('logs/log_debita_masivo.csv');
				if (file_exists($path)) unlink($path);

				$log = fopen($path, "a+");
				fwrite($log, strtoupper("cedula;tarjeta;valor;motivo;fecha;coddoc\r\n"));

				for ($row = 2; $row <= $highestRow; $row++) {

					$cedula = intval($sheet->getCell("A{$row}")->getValue());
					if (!$cedula) continue;

					$tarjeta = intval($sheet->getCell("B{$row}")->getValue());
					$valor = intval($sheet->getCell("C{$row}")->getValue());
					$novedad = $sheet->getCell("D{$row}")->getValue();
					$fecha = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($sheet->getCell("E{$row}")->getValue() + 1));
					$motivo = $sheet->getCell("F{$row}")->getValue();
					$coddoc = $sheet->getCell("G{$row}")->getValue();

					$novedad_monetaria = new NovedadMonetaria($cedula, $tarjeta, $valor, $motivo, $fecha, $coddoc);
					if ($novedad_monetaria->comprobar_saldos() == FALSE) {

						fwrite($log, "{$cedula};{$tarjeta};{$valor};{$motivo};{$fecha};{$coddoc}\r\n");
					} else {
						if ($novedad == "D") {
							$novedad_monetaria->debita($tarjeta);
						}
					}
				}
				fclose($log);

				$archivo_consolidado = NovedadMonetaria::Consolidar();
				$salida = array(
					'success' => true,
					'archivo_consolidado' => $archivo_consolidado,
					'msj' => "El proceso se completo OK!"
				);
				$this->console->info(json_encode($salida));
			} catch (QueryException $sql_err) {
				$msj = $sql_err->getMessage() . ' ' . basename($sql_err->getFile()) . ' ' . $sql_err->getLine();
				throw new DebugException($msj, 1);
			} catch (ErrorException $th) {
				$msj = $th->getMessage() . ' ' . basename($th->getFile()) . ' ' . $th->getLine();
				throw new DebugException($msj, 1);
			}
		} catch (DebugException $err) {
			$salida = array(
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode(),
				'success' => false
			);
			$this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
		}
	}

	/**
	 * bloqueoTarjetasMasivo function
	 * asopa03
	 * @param [type] $argv
	 * @param [type] $user
	 * @param [type] $sistema
	 * @param [type] $env
	 * @return void
	 */
	public function bloqueoTarjetasMasivo($argv, $user, $sistema, $enviroment)
	{
		try {
			try {
				$modelo = json_decode(base64_decode($argv), true);
				$archivo = "/var/www/html/clisisu/storage/files/tesoreria/" . $modelo['file'];
				if (!file_exists($archivo)) {
					throw new DebugException("Error el archivo es requerido para procesar", 501);
				}
				$inputFileType = PHPExcel_IOFactory::identify($archivo);
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($archivo);
				$sheet = $objPHPExcel->getSheet(0);
				$highestRow = $sheet->getHighestRow();
				$sheet->getHighestColumn();

				Comandos::$cantidad = $highestRow;
				Comandos::setProporcion();
				$fraccion = Comandos::$cantidad / Comandos::$proporcion;
				$progreso_ini_conteo = 0;
				$num = 0;

				BloqueoTarjetas::Inicializa($sistema, $modelo['fecha'], intval($modelo['consecutivo']));

				for ($row = 2; $row <= $highestRow; $row++) {
					$num++;
					$progreso_ini_conteo++;
					if ($progreso_ini_conteo >= $fraccion) {
						$progreso_ini_conteo = 0;
						$this->console->comandoProcesador->actualizaProgreso($num);
					}

					$cedula = intval($sheet->getCell("A{$row}")->getValue());
					if (!$cedula) continue;
					$numtar = intval($sheet->getCell("B{$row}")->getValue());
					$fecha = date('Y-m-d', strtotime($sheet->getCell("C{$row}")->getValue()) + 1);
					$tipo_documento = $sheet->getCell("D{$row}")->getValue();

					$this->bloqueoTarjetas->setTipoDocumento($tipo_documento);

					$novedades = Asopa03::where('cedres', $cedula)
						->where('numtar', $numtar)
						->where('tipnov', '04')
						->get();

					if ($novedades->count() == 0) {
						$novedad = new Asopa03();
						$novedad->cedres = $cedula;
						$novedad->numtar = $numtar;
						$novedad->fecha =  $fecha;
						$novedad->tipnov = '04';
						$novedad->codblo = '01';
						$novedad->motblo = '07';
						$novedad->estado = 'A';
						if ($novedad->save()) {
							$this->bloqueoTarjetas->procesarBloqueo($novedad);
						}
					} else {

						$novedad = $novedades->last();
						if (strtotime($novedad->fecha) == strtotime($fecha)) {
							$novedad->estado = 'A';
							$novedad->codblo = '01';
							$novedad->motblo = '07';
							if ($novedad->save()) {
								$this->bloqueoTarjetas->procesarBloqueo($novedad);
							}
						}
					}
				}
				BloqueoTarjetas::Consolidar();

				$salida = json_encode($this->bloqueoTarjetas->getResultado());
				$this->console->comandoProcesador->finalizarProceso($salida);
				$this->console->info($salida);
			} catch (\Exception $th) {
				throw new DebugException($th->getMessage() . ' ' . $th->getLine() . ' ' . basename($th->getFile()), 501);
			} catch (QueryException $er) {
				throw new DebugException($er->getMessage() . ' ' . $er->getLine() . ' ' . basename($er->getFile()), 501);
			}
		} catch (DebugException $err) {
			$this->console->comandoProcesador->cancelarComando();
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode()
			);
			$this->console->error(json_encode($salida));
		}
	}


	/**
	 * procesarPagosBancos function
	 * generar los archivos planos para realizar los pagos
	 * @param [type] $argv
	 * @param [type] $user
	 * @param [type] $sistema
	 * @param [type] $enviroment
	 * @return void
	 */
	public function procesarPagosBancos($file, $user, $sistema, $enviroment)
	{
		PagoBancos::Inicializa($sistema);
		try {
			try {
				$archivo = base_path("storage/files/tesoreria/" . $file);
				if (!file_exists($archivo)) {
					throw new DebugException("Error el archivo es requerido para procesar", 501);
				}
				$inputFileType = PHPExcel_IOFactory::identify($archivo);
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($archivo);
				$sheet = $objPHPExcel->getSheet(0);
				$highestRow = $sheet->getHighestRow();
				$sheet->getHighestColumn();

				$pagoBancos = new PagoBancos();
				for ($row = 2; $row <= $highestRow; $row++) {
					$cedula = intval($sheet->getCell("A{$row}")->getValue());
					if (!$cedula) continue;
					$cuenta = $sheet->getCell("B{$row}")->getValue();
					$fullname = $sheet->getCell("C{$row}")->getValue();
					$valor = intval($sheet->getCell("D{$row}")->getValue());
					$banco = $sheet->getCell("E{$row}")->getValue();
					$pagoBancos->procesarLineaExcel($cedula, $fullname, $valor, $banco, $cuenta);
				}
				PagoBancos::Consolidar();
				$salida = json_encode($pagoBancos->getResultado());
				$this->console->info($salida);
			} catch (\Exception $th) {
				throw new DebugException($th->getMessage() . ' ' . $th->getLine() . ' ' . basename($th->getFile()), 501);
			} catch (QueryException $er) {
				throw new DebugException($er->getMessage() . ' ' . $er->getLine() . ' ' . basename($er->getFile()), 501);
			}
		} catch (DebugException $err) {
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode()
			);
			$this->console->error(json_encode($salida));
			PagoBancos::Consolidar();
		}
	}
}
