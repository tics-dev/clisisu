<?php
use App\Exceptions\DebugException;
use Illuminate\Database\QueryException;

class SendEmails
{
    protected $console;
    public function __construct($console)
    {
        $this->console = $console;
        $this->fecha = date('Y-m-d');
    }

    public function prueba_correo($argv, $user, $sistema, $enviroment)
    {
        $transport = (new Swift_SmtpTransport('smtp.gmail.org', 25))
        ->setUsername('your username')
        ->setPassword('your password');

        $mailer = new Swift_Mailer($transport);

        $message = (new Swift_Message('Wonderful Subject'))
        ->setFrom(['john@doe.com' => 'John Doe'])
        ->setTo(['receiver@domain.org', 'other@domain.org' => 'A name'])
        ->setBody('Here is the message itself');

        $result = $mailer->send($message);
        $this->console->info($result);
    }
}