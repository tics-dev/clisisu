<?php

use App\Exceptions\DebugException;
use App\Services\AportesServicios\MoraEmpresa;
use Illuminate\Support\Facades\DB;

class AportesEmpresas
{
    protected $console;

    public function __construct($console)
    {
        ini_set('memory_limit', '2000M');
        $this->console = $console;
    }

    public function buscarAportesEmpresa($nit, $user, $sistema, $env)
    {
        try {
            $aportes = DB::table('subsi65')
                ->select(
                    'subsi64.fecrec',
                    'subsi64.fecsis',
                    'subsi64.codsuc',
                    'subsi64.nit',
                    'subsi65.cedtra',
                    'subsi65.novret',
                    'subsi64.numero',
                    'subsi65.valnom',
                    'subsi65.valapo',
                    'subsi64.perapo'
                )
                ->join('subsi64', 'subsi64.numero', '=', 'subsi65.numero')
                ->where("subsi64.nit", $nit)
                ->get();

            $this->console->info(json_encode([
                'success' => true,
                'data' => $aportes->toArray()
            ]));
        } catch (DebugException $err) {
            $this->console->comandoProcesador->cancelarComando();
            $salida = array(
                "success" => false,
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode()
            );
            $this->console->error(json_encode($salida));
        }
    }

    public function buscarMoraEmpresa($argv, $user, $sistema, $env)
    {
        try {
            $exp = explode('|', $argv);
            $nit = $exp[0];
            $codsuc = $exp[1];
            $aportes = new MoraEmpresa($nit, $codsuc);
            $aportes->set_env($this->console);
            $this->console->info(json_encode($aportes->buscarMora()));
        } catch (DebugException $err) {
            $this->console->comandoProcesador->cancelarComando();
            $salida = array(
                "success" => false,
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode()
            );
            $this->console->error(json_encode($salida));
        }
    }
}
