<?php

use App\Exceptions\DebugException;
use App\Models\Subsi24;
use App\Models\Subsi26;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class Certificados
{
    protected $usuario;
    protected $sistema;
    protected $user;
    protected $console;
    protected $fecha;

    public function __construct($console)
    {
        $this->console = $console;
        $this->fecha = date('Y-m-d');
    }

    /**
     * buscarCertificadosBeneficiario function
     *  reporte de certificado presentados por trabajador, para comfaca en línea  
     * @param [type] $cedtra
     * @param int    $user
     * @param string $sistema
     * @param string $enviroment
     * @return void
     */
    public function buscarCertificadosBeneficiario($cedtra, $user=1, $sistema='SYS', $enviroment='1')
    {
        try {
            $beneficiarios = DB::table('subsi23')
            ->select(
                'parent',
                'captra',
                'priape',
                'segape',
                'prinom',
                'segnom',
                'subsi22.codben',
                'subsi22.documento'
            )
            ->leftJoin('subsi22', 'subsi22.codben','=','subsi23.codben')
            ->where('cedtra', $cedtra)
            ->get();

            $result = array();
            foreach ($beneficiarios as $beneficiario) 
            {
                $codcer = array();
                $cetificados = Subsi24::where('parent', $beneficiario->parent)
                ->where('captra', $beneficiario->captra)
                ->get();
                
                foreach ($cetificados as $cetificado) 
                {
                    $codcer["{$cetificado->codcer}"] = $cetificado->detalle;
                }

                $presentaCertificado = Subsi26::where('codben', $beneficiario->codben)->max('fecent');
                $fecent = ($presentaCertificado)? $presentaCertificado->fecent:'';
                
                if ($fecent == "") $fecent = "No ha presentado";
                
                $result[] = array(
                    'codben' => trim($beneficiario->codben),
                    'nombre' => trim($beneficiario->priape)." ".trim($beneficiario->segape)." ".trim($beneficiario->prinom)." ".trim($beneficiario->segnom),
                    'codcer' => $codcer,
                    'ultfec' => $fecent
                );
            }

            $this->console->info(json_encode([
                "success" => true,
                "data" => $result
            ]));
            
        } catch (DebugException $err) {
            $salida = array(
                "success" => false,
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode()
            );
            $this->console->error(json_encode($salida));
        }
    }

    /**
     * buscarCertificadoEstudioSubsidio function
     * reporte de afiliados beneficiarios   
     * trabajador activos 
     * ultimo cargado a partir de la fecha 1 sept 2022, 
     * Certificados estudio,  
     * hijos e hijastro hermanos  
     * mayores de 12 años
     * @param [type] $cedtra
     * @param int    $user
     * @param string $sistema
     * @param string $enviroment
     * @return void
     */
    public function buscarCertificadoEstudioSubsidio($fecha, $user=1, $sistema='SYS', $enviroment='1')
    {
        try {

            $filepath = "/var/www/html/".$sistema."/public/temp/certificados_estudio_hijos_".strtotime('now').".csv";
            $file = fopen($filepath, "w+");
            $texto_write = implode(';', array(
                "codben",
                "nombre",
                "fecha",
                "codcer",
                "parent",
                "cedtra"
            ));
            fwrite($file, strtoupper($texto_write) . "\r\n");
            fclose($file);

            $beneficiarios = DB::table('subsi23')
            ->select(
                'subsi22.parent',
                'subsi22.captra',
                'subsi22.priape',
                'subsi22.segape',
                'subsi22.prinom',
                'subsi22.segnom',
                'subsi22.codben',
                'subsi22.documento',
                'subsi22.parent',
                'subsi23.cedtra'
            )
            ->join('subsi22', 'subsi22.codben','=','subsi23.codben')
            ->join('subsi15', 'subsi15.cedtra','=','subsi23.cedtra')
            ->where('subsi15.estado', 'A')
            ->where('subsi22.estado', 'A')
            ->whereIn('subsi22.parent', ['1','2'])
            ->get();

            
            foreach ($beneficiarios as $beneficiario) 
            {
                $codcer='01';
                $presentaCertificado = Subsi26::where('codben', $beneficiario->codben)->where('codcer', $codcer)->where('fecent', '>=', $fecha);
                if($presentaCertificado->count() > 0)
                {
                    $presentaCertificado = $presentaCertificado->get()->last();
                    $data = array(
                        'codben' => trim($beneficiario->codben),
                        'nombre' => trim($beneficiario->priape)." ".trim($beneficiario->segape)." ".trim($beneficiario->prinom)." ".trim($beneficiario->segnom),
                        'fecha' => $presentaCertificado->fecent,
                        'codcer' => $presentaCertificado->codcer,
                        'parent' => $presentaCertificado->parent,
                        'cedtra' => $beneficiario->cedtra
                    );   
                    file_put_contents($filepath, implode(";", $data)."\r\n", FILE_APPEND);
                }
            }

            $this->console->info($filepath);

        } catch (DebugException $err) {
            $salida = array(
                "success" => false,
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode()
            );
            $this->console->error(json_encode($salida));
        }
    }
}
