<?php

declare(strict_types=1);
require_once base_path('app/Libraries/PHPExcel.php');
require_once base_path('app/Libraries/PHPExcel/IOFactory.php');
require_once base_path('app/Libraries/PHPExcel/Shared/Date.php');

use App\Console\Servicios\Comandos\Comandos;
use App\Console\Servicios\SaldoTarjeta\DebitoControlInterno;
use App\Console\Servicios\SaldoTarjeta\TrasladoSaldoInterno;
use App\Exceptions\DebugException;
use App\Models\Asopa07;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use App\Models\Gener09;
use App\Models\InternoAbono;
use App\Models\InternoDebito;
use App\Models\InternoSaldo;
use App\Models\Subsi15;
use App\Models\Subsi20;
use App\Models\Subsi21;

class SaldoTarjeta
{
    protected $console;
    public function __construct($console)
    {
        ini_set('memory_limit', '2000M');
        $this->console = $console;
    }

    public function procesarSaldos($argv = '', $user = '', $sistem = '', $env = '')
    {
        $sql = "SELECT sum(saldo) as tsaldo, numtar FROM asopa07 where saldo > 0 group by numtar";
        $rqsaldos = DB::select($sql);

        $collection_saldos = array();
        $collection_saldos[0] = array();

        foreach ($rqsaldos as $i => $rsaldo) {

            $res = DB::table('asopa04')
                ->where('numtar', $rsaldo->numtar)
                ->where('estado', 'A')
                ->groupBy("numtar", "cedres")
                ->get()
                ->first();

            if ($res) {

                if (!isset($collection_saldos["{$res->cedres}"])) {
                    $collection_saldos["{$res->cedres}"] = 0;
                }
                $collection_saldos["{$res->cedres}"] += $rsaldo->tsaldo;
            } else {
                $collection_saldos[0][] = ["numtar" => "{$rsaldo->numtar}", "saldo" => $rsaldo->tsaldo];
            }
        }
        $res = null;
        $rqsaldos = null;
        $sql = null;
        $rsaldo = null;

        $data = array(
            'cedula',
            'saldo',
            'nombre',
            'nit',
            'estado',
            'codezona',
            'zona',
            'telefono',
            'cuenta',
            'tipo_pago',
            'banco',
            'coddoc'
        );
        $this->console->info(implode(';', $data));

        foreach ($collection_saldos as $key => $valor) {

            if ($key == 0) continue;

            $responsable = Subsi20::where('cedcon', $key)->get()->first();
            if (!$responsable) {
                $responsable = Subsi15::where('cedtra', $key)->get()->first();
            } else {

                $relation = Subsi21::where('cedcon', $responsable->cedcon)->get()->first();
                if ($relation) {
                    $trabajador = Subsi15::where('cedtra', $relation->cedtra)->get()->first();
                    if ($trabajador) {
                        $responsable->nit = $trabajador->nit;
                        $responsable->estado = $trabajador->estado;
                    } else {
                        $responsable->nit = '';
                    }
                } else {
                    $responsable->nit = '';
                }
            }

            if ($responsable) {

                $zonas = Gener09::where('codzon', $responsable->codzon)->get()->first();
                $data = array(
                    $key,
                    $valor,
                    $responsable->prinom . ' ' . $responsable->segnom . ' ' . $responsable->priape . '' . $responsable->segape,
                    $responsable->nit,
                    $responsable->estado,
                    $responsable->codzon,
                    ($zonas) ? $zonas->detzon : '',
                    $responsable->telefono,
                    $responsable->numcue,
                    $responsable->tippag,
                    $responsable->codban,
                    $responsable->coddoc
                );
            } else {
                $data = array(
                    $key,
                    $valor,
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    ''
                );
            }
            $this->console->info(implode(';', $data));
            $responsable = null;
            $data = null;
        }

        $data = array(
            'NUMTAR',
            'SALDO',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        );
        $this->console->info(implode(';', $data));

        if (isset($collection_saldos[0])) {

            foreach ($collection_saldos[0] as $key => $valor) {

                $data = array(
                    $valor['numtar'],
                    $valor['saldo'],
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    ''
                );
                $this->console->info(implode(';', $data));
            }
        }
    }

    public function numtarSaldos($argv = '', $user = '', $sistem = '', $env = '')
    {
        $path = "/var/www/html/SYS/public/temp/";
        $file_novedad = "reporte_saldos_tarjetas.csv";
        $file = fopen($path . '' . $file_novedad, "w+");

        $sql = "SELECT sum(saldo) as tsaldo, numtar FROM asopa07 GROUP BY numtar";
        $rqsaldos = DB::select($sql);

        $data = array(
            'numtar',
            'cedres',
            'saldo',
            'estado',
            'custodia'
        );
        fwrite($file, implode(';',  $data) . "\r\n");

        $tarjetas_problema = array();
        foreach ($rqsaldos as $i => $rsaldo) {
            if ($rsaldo->tsaldo == 0) continue;
            //valida la novedades por trazabilidad de asopagos
            $res = DB::table('asopa04')->where("numtar", $rsaldo->numtar)
                ->groupBy("numtar", "cedres")
                ->get()
                ->first();

            if ($res) {
                $data = array(
                    $res->numtar,
                    $res->cedres,
                    $rsaldo->tsaldo,
                    $res->estado,
                    "asopa04"
                );
                fwrite($file, implode(';',  $data) . "\r\n");
            } else {

                $novedad_solicitud = DB::table('asopa03')->where("numtar", $rsaldo->numtar)
                    ->groupBy("numtar", "cedres")
                    ->get();

                if ($novedad_solicitud->count() > 0) {
                    $res = $novedad_solicitud->first();
                    $data = array(
                        $res->numtar,
                        $res->cedres,
                        $rsaldo->tsaldo,
                        $res->estado,
                        "asopa03"
                    );
                    fwrite($file, implode(';',  $data) . "\r\n");
                } else {
                    $tarjetas_problema[] = array(
                        $rsaldo->numtar,
                        $rsaldo->tsaldo,
                        'NU'
                    );
                }
            }
        }

        $this->console->info("TARJETAS NULAS;;;");
        $data = array(
            'numtar',
            'saldo',
            'estado'
        );
        fwrite($file, implode(';',  $data) . "\r\n");
        foreach ($tarjetas_problema as $data) {
            fwrite($file, implode(';',  $data) . "\r\n");
        }
        fclose($file);
        chmod($path . '' . $file_novedad, 0777);
        $this->console->info($path . '' . $file_novedad);
    }

    public function numtarPendientesBloquear($argv = '', $user = '', $sistem = '', $env = '')
    {
        $sql = "SELECT asopa04.numtar, asopa04.cedres, asopa07.saldo 
        FROM asopa04 
        LEFT JOIN asopa07 ON asopa07.numtar = asopa04.numtar 
        WHERE asopa07.saldo > 0 GROUP BY numtar";

        $tarjetas = DB::select($sql);

        $data = array(
            'numtar',
            'cedres',
            'estado',
            'fecha',
            'bloqueo'
        );
        $this->console->info(implode(';', $data));

        foreach ($tarjetas as $i => $tarjeta) {
            //valida la novedades por trazabilidad de asopagos
            $res = DB::table('asopa03')->where("numtar", $tarjeta->numtar)
                ->where("tipnov", "04")
                ->where("estado", "P")
                ->get();

            if ($res->count() > 0) {
                $novedad = $res->first();
                $data = array(
                    $novedad->numtar,
                    $novedad->cedres,
                    $novedad->estado,
                    $novedad->fecha,
                    "BLOQUEADA"
                );

                $this->console->info(implode(';', $data));
            } else {
                $data = array(
                    $tarjeta->numtar,
                    $tarjeta->cedres,
                    '',
                    '',
                    "ACTIVA"
                );
                $this->console->info(implode(';', $data));
            }
        }
    }

    public function numtarPendientesSinSaldoBloquear($argv = '', $user = '', $sistem = '', $env = '')
    {
        $sql = "SELECT asopa04.numtar, asopa04.cedres FROM asopa04 WHERE asopa04.numtar > 0 GROUP BY 1, 2";

        $tarjetas = DB::select($sql);

        $data = array(
            'numtar',
            'cedres',
            'estado',
            'fecha',
            'bloqueo',
            'saldo',
            'coddoc'
        );
        $this->console->info(implode(';', $data));

        foreach ($tarjetas as $i => $tarjeta) {
            //valida la novedades por trazabilidad de asopagos
            $res = DB::table('asopa03')->where("numtar", $tarjeta->numtar)
                ->where("tipnov", "04")
                ->where("estado", "P")
                ->get();

            if ($res->count() > 0) {
                $novedad = $res->first();
                $data = array(
                    $novedad->numtar,
                    $novedad->cedres,
                    $novedad->estado,
                    $novedad->fecha,
                    "BLOQUEADA",
                    0
                );
                $this->console->info(implode(';', $data));
            } else {
                $res = DB::table('asopa07')->where("numtar", $tarjeta->numtar)
                    ->where("saldo", ">", 0)
                    ->get();

                if ($res->count() > 0) {
                    $tarjeta_saldos = $res->first();
                    $responsable =  DB::table('subsi15')->where("cedtra", $tarjeta->cedres)->get();

                    if ($responsable->count() == 0) {

                        $responsable = DB::table('subsi20')->where("cedcon", $tarjeta->cedres)->get();

                        if ($responsable->count() == 0) {
                            $responsable = DB::table('subsi22')->where("documento", $tarjeta->cedres)->get();
                        }
                    }
                    $responsable = $responsable->first();
                    $data = array(
                        $tarjeta->numtar,
                        $tarjeta->cedres,
                        '',
                        '',
                        "ACTIVA",
                        $tarjeta_saldos->saldo,
                        $responsable->coddoc
                    );
                    $this->console->info(implode(';', $data));
                }
            }
        }
    }

    public function migrationSaldos($argv = '', $user = '', $sistem = '', $env = '')
    {
        try {
            try {
                $modelo = json_decode(base64_decode($argv), true);
                $archivo = "/var/www/html/clisisu/storage/files/tesoreria/" . $modelo['file'];
                if (!file_exists($archivo)) {
                    throw new DebugException("Error el archivo es requerido para procesar", 501);
                }
                $inputFileType = PHPExcel_IOFactory::identify($archivo);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($archivo);
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $sheet->getHighestColumn();

                Comandos::$cantidad = $highestRow;
                Comandos::setProporcion();
                $fraccion = Comandos::$cantidad / Comandos::$proporcion;
                $progreso_ini_conteo = 0;
                $num = 0;

                $path = "/var/www/html/SYS/public/temp/";
                $file_novedad = "reporte_asopagos_sisuweb.csv";
                $file = fopen($path . '' . $file_novedad, "w+");

                fwrite($file, implode(';', array(
                    "cedula",
                    "tippag",
                    "numcue",
                    "saldo",
                    "diferencia"
                )) . "\r\n");

                $trabajadores = array();
                for ($row = 2; $row <= $highestRow; $row++) {
                    $num++;
                    $progreso_ini_conteo++;
                    if ($progreso_ini_conteo >= $fraccion) {
                        $progreso_ini_conteo = 0;
                        $this->console->comandoProcesador->actualizaProgreso($num);
                    }
                    $cedula = intval($sheet->getCell("A{$row}")->getValue());
                    if (!$cedula) continue;
                    $saldo_responsable = intval($sheet->getCell("B{$row}")->getValue());

                    $tarjetas_activas = DB::table('asopa04')->where("cedres", $cedula)
                        ->groupBy("cedres", "numtar")
                        ->get();


                    if (!isset($trabajadores["$cedula"])) $trabajadores["$cedula"] = ["saldo" => 0, "tippag" => "", "numcue" => 0];

                    if ($tarjetas_activas->count() > 0) {
                        foreach ($tarjetas_activas as $tarjeta) {
                            $saldo = Asopa07::where("numtar", $tarjeta->numtar)->where("saldo", ">", 0)->sum("saldo");
                            $trabajadores["$cedula"]["saldo"] += $saldo;
                        }
                    } else {
                        //buscar en asopa03, novedades de solicitud de tarjeta
                        $tarjetas_pedidas = DB::table('asopa03')->where("cedres", $cedula)
                            ->groupBy("cedres", "numtar")
                            ->get();

                        if ($tarjetas_pedidas->count() > 0) {
                            foreach ($tarjetas_pedidas as $tarjeta) {
                                $saldo = Asopa07::where("numtar", $tarjeta->numtar)->where("saldo", ">", 0)->sum("saldo");
                                $trabajadores["$cedula"]["saldo"] += $saldo;
                            }
                        }
                    }

                    //buscar medios de pago
                    $sql = "SELECT
                    subsi20.cedcon as 'cedula',
                    CONCAT_WS(' ',subsi20.prinom, subsi20.segnom, subsi20.priape, subsi20.segape) as nombre,
                    subsi15.nit,
                    subsi20.email,
                    subsi20.tippag,
                    subsi20.numcue,
                    IF(subsi20.telefono < 3000000000, '', subsi20.telefono) as 'telefono'
                    FROM subsi20
                    INNER JOIN subsi21 ON subsi20.cedcon = subsi21.cedcon
                    INNER JOIN subsi15 ON subsi15.cedtra = subsi21.cedtra
                    WHERE 
                    subsi20.cedcon=?";
                    $sisu_conyuge = DB::select($sql, [$cedula]);

                    $sql = "SELECT 
                    cedtra as 'cedula',
                    CONCAT_WS(' ', prinom, segnom, priape, segape) as nombre,
                    subsi15.nit,
                    subsi15.email,
                    subsi15.tippag,
                    subsi15.numcue,
                    IF(subsi15.telefono < 3000000000, '', subsi15.telefono) as 'telefono'
                    FROM subsi15
                    WHERE subsi15.cedtra=?";
                    $sisu_trabajador = DB::select($sql, [$cedula]);

                    $_conyuge = false;
                    $tippag_conyuge = false;
                    if ($sisu_conyuge) {
                        $_conyuge = collect($sisu_conyuge)->first();
                        $tippag_conyuge = $_conyuge->tippag;
                    }

                    $_trabajador = false;
                    $tippag_trabajador = false;
                    if ($sisu_trabajador) {
                        $_trabajador = collect($sisu_trabajador)->first();
                        $tippag_trabajador = $_trabajador->tippag;
                    }

                    if ($tippag_trabajador == "D" || $tippag_conyuge == "D") {
                        $trabajadores["$cedula"]["tippag"] = ($tippag_trabajador == "D") ? $_trabajador->tippag : $_conyuge->tippag;
                        $trabajadores["$cedula"]["numcue"] = ($tippag_trabajador == "D") ? $_trabajador->numcue : $_conyuge->numcue;
                    } else {
                        if ($tippag_trabajador == "A" || $tippag_conyuge == "A") {

                            $trabajadores["$cedula"]["tippag"] = ($tippag_trabajador == "A") ? $_trabajador->tippag : $_conyuge->tippag;
                            $trabajadores["$cedula"]["numcue"] = ($tippag_trabajador == "A") ? $_trabajador->numcue : $_conyuge->numcue;
                        } else {
                            //todo lo demas formas de pago
                            $trabajadores["$cedula"]["tippag"] = ($_conyuge) ? $_conyuge->tippag : (($_trabajador) ? $_trabajador->tippag : 'NA');
                            $trabajadores["$cedula"]["numcue"] = 0;
                        }
                    }
                    if ($saldo_responsable == $trabajadores["$cedula"]["saldo"]) {
                        fwrite($file, implode(';', array(
                            $cedula,
                            $trabajadores["$cedula"]['tippag'],
                            $trabajadores["$cedula"]['numcue'],
                            $trabajadores["$cedula"]['saldo'],
                            0
                        )) . "\r\n");
                    } else {
                        fwrite($file, implode(';', array(
                            $cedula,
                            $trabajadores["$cedula"]['tippag'],
                            $trabajadores["$cedula"]['numcue'],
                            $trabajadores["$cedula"]['saldo'],
                            intval($saldo_responsable) - intval($trabajadores["$cedula"]['saldo'])
                        )) . "\r\n");
                    }
                }

                fclose($file);
                chmod($path . '' . $file_novedad, 0777);
                $this->console->comandoProcesador->finalizarProceso("");
                $this->console->info($path . '' . $file_novedad);
            } catch (\Exception $th) {
                throw new DebugException($th->getMessage() . ' ' . $th->getLine() . ' ' . basename($th->getFile()), 501);
            } catch (QueryException $er) {
                throw new DebugException($er->getMessage() . ' ' . $er->getLine() . ' ' . basename($er->getFile()), 501);
            }
        } catch (DebugException $err) {
            $this->console->comandoProcesador->cancelarComando();
            $salida = array(
                "success" => false,
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode()
            );
            $this->console->error(json_encode($salida));
        }
    }

    public function traspasoSaldos($argv = '', $user = '', $sistem = '', $env = '')
    {
        try {
            try {
                $modelo = json_decode(base64_decode($argv), true);
                $archivo = "/var/www/html/clisisu/storage/files/tesoreria/" . $modelo['file'];
                if (!file_exists($archivo)) {
                    throw new DebugException("Error el archivo es requerido para procesar", 501);
                }
                $inputFileType = PHPExcel_IOFactory::identify($archivo);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($archivo);
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $sheet->getHighestColumn();

                Comandos::$cantidad = $highestRow;
                Comandos::setProporcion();
                $fraccion = Comandos::$cantidad / Comandos::$proporcion;
                $progreso_ini_conteo = 0;
                $num = 0;

                $path = "/var/www/html/SYS/public/temp/";
                $file_novedad = "traspaso_control_interno_sisuweb.csv";
                $file = fopen($path . '' . $file_novedad, "w+");

                fwrite($file, implode(';', array(
                    "cedula",
                    "saldo"
                )) . "\r\n");

                for ($row = 2; $row <= $highestRow; $row++) {
                    $num++;
                    $progreso_ini_conteo++;
                    if ($progreso_ini_conteo >= $fraccion) {
                        $progreso_ini_conteo = 0;
                        $this->console->comandoProcesador->actualizaProgreso($num);
                    }
                    $cedula = intval($sheet->getCell("A{$row}")->getValue());
                    if (!$cedula) continue;
                    $saldo_responsable = intval($sheet->getCell("B{$row}")->getValue());
                    $forma_pago = $sheet->getCell("C{$row}")->getValue();

                    //abono o por daviplata se omite
                    //if($forma_pago == 'A' || $forma_pago == 'D') continue;

                    $internoSaldo = new InternoSaldo();
                    $internoSaldo->cedula = $cedula;
                    $internoSaldo->fecha_inicial = date('Y-m-d');
                    $internoSaldo->fecha_ultima = date('Y-m-d');
                    $internoSaldo->estado = "P";
                    $internoSaldo->valor_inicial = $saldo_responsable;
                    $internoSaldo->valor_acumulado = $saldo_responsable;

                    $internoAbono = new InternoAbono();
                    $internoAbono->usuario = $user;
                    $internoAbono->valor_abono = $saldo_responsable;

                    $trasladoSaldoInterno = new TrasladoSaldoInterno($this->console);
                    $trasladoSaldoInterno->setInternoSaldo($internoSaldo);
                    $trasladoSaldoInterno->setInternoAbono($internoAbono);
                    $trasladoSaldoInterno->procesar($cedula);

                    fwrite($file, implode(';', array(
                        $cedula,
                        $saldo_responsable
                    )) . "\r\n");
                }

                fclose($file);
                chmod($path . '' . $file_novedad, 0777);
                $this->console->comandoProcesador->finalizarProceso("");
                $this->console->info($path . '' . $file_novedad);
            } catch (\Exception $th) {
                throw new DebugException($th->getMessage() . ' ' . $th->getLine() . ' ' . basename($th->getFile()), 501);
            } catch (QueryException $er) {
                throw new DebugException($er->getMessage() . ' ' . $er->getLine() . ' ' . basename($er->getFile()), 501);
            }
        } catch (DebugException $err) {
            $this->console->comandoProcesador->cancelarComando();
            $salida = array(
                "success" => false,
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode()
            );
            $this->console->error(json_encode($salida));
        }
    }

    public function prescribirSaldos($argv = '', $user = '', $sistem = '', $env = '')
    {
        DB::beginTransaction();
        try {
            try {
                $modelo = json_decode(base64_decode($argv), true);
                $archivo = "/var/www/html/clisisu/storage/files/tesoreria/" . $modelo['file'];
                if (!file_exists($archivo)) {
                    throw new DebugException("Error el archivo es requerido para procesar", 501);
                }
                $inputFileType = PHPExcel_IOFactory::identify($archivo);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($archivo);
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $sheet->getHighestColumn();

                Comandos::$cantidad = $highestRow;
                Comandos::setProporcion();
                $fraccion = Comandos::$cantidad / Comandos::$proporcion;
                $progreso_ini_conteo = 0;
                $num = 0;

                $path = "/var/www/html/SYS/public/temp/";
                $file_novedad = "psubsidio.txt";
                $file = fopen($path . '' . $file_novedad, "w+");

                $fecha = date('Y/m/d');
                $cuenta_debita = "23020502";
                $cuenta_credita = "421205";

                for ($row = 2; $row <= $highestRow; $row++) {
                    $num++;
                    $progreso_ini_conteo++;
                    if ($progreso_ini_conteo >= $fraccion) {
                        $progreso_ini_conteo = 0;
                        $this->console->comandoProcesador->actualizaProgreso($num);
                    }
                    $cedula = intval($sheet->getCell("A{$row}")->getValue());
                    if (!$cedula) continue;
                    $saldo_debitar = intval($sheet->getCell("B{$row}")->getValue());

                    $internoSaldo = InternoSaldo::where("cedula", $cedula)->get()->first();
                    $internoDebito = new InternoDebito();
                    $internoDebito->usuario = $user;
                    $internoDebito->valor_debito = $saldo_debitar;
                    $internoDebito->fecha = date('Y-m-d H:i:s');
                    $internoDebito->concepto = 8;
                    $internoDebito->usuario = $user;
                    $internoDebito->estado = 'P';
                    $internoDebito->procesado = 'S';
                    $internoDebito->interno_saldo = $internoSaldo->id;
                    $internoDebito->cedula = $cedula;
                    $internoDebito->save();

                    $internoSaldo->valor_acumulado = $internoSaldo->valor_acumulado - $saldo_debitar;
                    $internoSaldo->fecha_ultima = date('Y-m-d');
                    if ($internoSaldo->valor_acumulado == 0) {
                        $internoSaldo->estado = 'L';
                    }
                    $internoSaldo->save();
                    $prescripcion_credita = array(
                        $cedula,
                        $cuenta_credita,
                        $fecha,
                        $saldo_debitar,
                        "C"
                    );

                    $prescripcion_debita = array(
                        $cedula,
                        $cuenta_debita,
                        $fecha,
                        $saldo_debitar,
                        "D"
                    );

                    $texto_write = implode('|', $prescripcion_credita);
                    fwrite($file, $texto_write . "\n");

                    $texto_write = implode('|', $prescripcion_debita);
                    fwrite($file, $texto_write . "\n");
                }

                DB::commit();
                fclose($file);
                chmod($path . '' . $file_novedad, 0777);
                $this->console->comandoProcesador->finalizarProceso($path . '' . $file_novedad);
                $this->console->info($path . '' . $file_novedad);
            } catch (\Exception $th) {
                throw new DebugException($th->getMessage() . ' ' . $th->getLine() . ' ' . basename($th->getFile()), 501);
            } catch (QueryException $er) {
                throw new DebugException($er->getMessage() . ' ' . $er->getLine() . ' ' . basename($er->getFile()), 501);
            }
        } catch (DebugException $err) {
            DB::rollBack();
            $this->console->comandoProcesador->cancelarComando();
            $salida = array(
                "success" => false,
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode()
            );
            $this->console->error(json_encode($salida));
        }
    }

    public function pagoDaviplataSaldos($argv = '', $user = '', $sistem = '', $env = '')
    {
        DB::beginTransaction();
        try {
            try {
                $modelo = json_decode(base64_decode($argv), true);
                $archivo = "/var/www/html/clisisu/storage/files/tesoreria/" . $modelo['file'];
                if (!file_exists($archivo)) {
                    throw new DebugException("Error el archivo es requerido para procesar", 501);
                }
                $inputFileType = PHPExcel_IOFactory::identify($archivo);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($archivo);
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $sheet->getHighestColumn();

                Comandos::$cantidad = $highestRow;
                Comandos::setProporcion();
                $fraccion = Comandos::$cantidad / Comandos::$proporcion;
                $progreso_ini_conteo = 0;
                $num = 0;

                $path = "/var/www/html/SYS/public/temp/";
                $file_novedad = "giroabosub1.txt";
                $file = fopen($path . '' . $file_novedad, "w+");

                $fecha = date('m/d/Y');
                $cuenta_debita = "23020503";
                $cuenta_credita = "11200519";
                $nit = 891190047;

                for ($row = 2; $row <= $highestRow; $row++) {
                    $num++;
                    $progreso_ini_conteo++;
                    if ($progreso_ini_conteo >= $fraccion) {
                        $progreso_ini_conteo = 0;
                        $this->console->comandoProcesador->actualizaProgreso($num);
                    }
                    $cedula = intval($sheet->getCell("A{$row}")->getValue());
                    if (!$cedula) continue;
                    $saldo_debitar = intval($sheet->getCell("B{$row}")->getValue());
                    $cuenta_daviplata = intval($sheet->getCell("C{$row}")->getValue());

                    $internoSaldo = InternoSaldo::where("cedula", $cedula)->get()->first();
                    $internoDebito = new InternoDebito();
                    $internoDebito->usuario = $user;
                    $internoDebito->valor_debito = $saldo_debitar;
                    $internoDebito->fecha = date('Y-m-d H:i:s');
                    $internoDebito->concepto = 6; //pago a daviplata del tercero
                    $internoDebito->usuario = $user;
                    $internoDebito->estado = 'P';
                    $internoDebito->procesado = 'S';
                    $internoDebito->interno_saldo = $internoSaldo->id;
                    $internoDebito->cedula = $cedula;
                    $internoDebito->save();

                    $internoSaldo->valor_acumulado = $internoSaldo->valor_acumulado - $saldo_debitar;
                    $internoSaldo->fecha_ultima = date('Y-m-d');
                    if ($internoSaldo->valor_acumulado == 0) {
                        $internoSaldo->estado = 'L';
                    }
                    $internoSaldo->save();
                    $interfaz_credita = array(
                        $nit,
                        "ABONO ",
                        "IPFP PAGO DAVIPLATA TERCERO " . $cedula,
                        $cuenta_daviplata,
                        $cuenta_credita,
                        $fecha,
                        "C",
                        $saldo_debitar,
                        ""
                    );
                    $texto_write = implode('|', $interfaz_credita);
                    fwrite($file, $texto_write . "\n");
                }

                $interfaz_debita = array(
                    $nit,
                    "DEBITO ",
                    " TOTAL DEBITO",
                    $cuenta_debita,
                    $cuenta_debita,
                    $fecha,
                    "D",
                    $saldo_debitar,
                    ""
                );
                $texto_write = implode('|', $interfaz_debita);
                fwrite($file, $texto_write . "\n");

                DB::commit();
                fclose($file);
                chmod($path . '' . $file_novedad, 0777);
                $this->console->comandoProcesador->finalizarProceso($path . '' . $file_novedad);
                $this->console->info($path . '' . $file_novedad);
            } catch (\Exception $th) {
                throw new DebugException($th->getMessage() . ' ' . $th->getLine() . ' ' . basename($th->getFile()), 501);
            } catch (QueryException $er) {
                throw new DebugException($er->getMessage() . ' ' . $er->getLine() . ' ' . basename($er->getFile()), 501);
            }
        } catch (DebugException $err) {
            DB::rollBack();
            $this->console->comandoProcesador->cancelarComando();
            $salida = array(
                "success" => false,
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode()
            );
            $this->console->error(json_encode($salida));
        }
    }
}
