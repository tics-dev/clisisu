<?php 
namespace App\Console\Servicios\SaldoTarjeta;

use App\Exceptions\DebugException;
use Illuminate\Support\Facades\DB;
use App\Models\InternoAbono;
use App\Models\InternoSaldo;

class TrasladoSaldoInterno implements InSaldoTarjeta
{
    protected $console;
    public function __construct($console)
    {
        ini_set('memory_limit', '2000M');
        $this->console = $console;
    }

    private $internoSaldo; 

    private $internoAbono; 

    public function setInternoSaldo(InternoSaldo $interno){
        $this->internoSaldo = $interno;
    }

    public function getInternoSaldo() {
        return $this->internoSaldo;
    }

    public function setInternoAbono(InternoAbono $abono){
        $this->internoAbono = $abono;
    }

    public function getInternoAbono() {
        return $this->internoAbono;
    }

    public function procesar($cedula)
    {
        if(intval($this->internoSaldo->cedula) != intval($cedula)) throw new DebugException("Error Processing Request", 1);

        $interno = InternoSaldo::where("cedula", $this->internoSaldo->cedula)->get();
        if($interno->count() > 0){
            $this->internoSaldo = $interno->first();
        }else{
            $this->internoSaldo->save();
        }
        $this->internoAbono->cedula = $cedula;
        $this->internoAbono->interno_saldo = $this->internoSaldo->id;
        $this->internoAbono->fecha = date('Y-m-d H:i:s');
        $this->internoAbono->concepto = 1; //traslado de asopagos a control interno
        $this->internoAbono->estado= "A";
        $this->internoAbono->procesado= "N";
        $this->internoAbono->save();
        $this->internoAbono = null;
    }

    public function getResultado()
    {
        return [];
    }
}
