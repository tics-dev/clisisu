<?php
namespace App\Console\Servicios\SaldoTarjeta;

interface InSaldoTarjeta {

    public function procesar($cedtra);
    public function getResultado();

}