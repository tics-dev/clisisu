<?php 
declare(strict_types=1);
namespace App\Console\Servicios\SaldoTarjeta;

use Illuminate\Support\Facades\DB;
use App\Models\Asopa03;
use App\Models\Asopa04;
use App\Models\Asopa07;
use App\Models\InternoAbono;
use App\Models\InternoSaldo;
use DateTime;

class DebitoControlInterno implements InSaldotarjeta
{
    
    protected $console;
    public function __construct($console)
    {
        ini_set('memory_limit', '2000M');
        $this->console = $console;
    }

    public function procesar($cedtra = "")
    {
        $internoSaldo = new InternoSaldo();
        $internoSaldo->cedula = "";
        $internoSaldo->fecha_inicial = "";
        $internoSaldo->fecha_ultima = "";
        $internoSaldo->valor_inicial = "";
        $internoSaldo->valor_acumulado = "";
        $internoSaldo->estado = "";
        $internoSaldo->save();
        $id = $internoSaldo->id;

        $internoAbono  = new InternoAbono();
        $internoAbono->cedula= $internoSaldo->cedula;
        $internoAbono->interno_saldo = $id;
        $internoAbono->fecha = date('Y-m-d H:i:s');
        $internoAbono->valor_abono ="";
        $internoAbono->concepto ="";
        $internoAbono->usuario ="";
        $internoAbono->estado= "";
        $internoAbono->procesado= "";
        $internoAbono->save();
    }

    public function getResultado(): array
    {
        return [

        ];
    }


}
