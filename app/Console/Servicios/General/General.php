<?php
namespace  App\Console\Servicios\General;
use Illuminate\Support\Facades\DB;
use App\Models\Gener02;
use App\Models\Mercurio20;
use App\Models\Mercurio08;
use App\Models\Mercurio04;
use App\Models\Mercurio05;
use App\Models\Subsi12;
use App\Models\Subsi30;

class General
{
    public static $usuario; 
    public static $logger;
    public static $sistema; 
    static $log_file;
    

    public static function setUser($usuario)
    {
        self::$usuario = Gener02::where("usuario", $usuario)
        ->get()
        ->first();
        return self::$usuario;
    }

    public static function createLog($documento, $coddoc, $accion, $nota='', $tipo='')
    {
        self::$logger = new Mercurio20;
        self::$logger->tipo = $tipo;
        self::$logger->coddoc = $coddoc;
        self::$logger->documento = $documento;
        self::$logger->ip = '172.0.0.1';
        self::$logger->fecha = date('Y-m-d');
        self::$logger->hora = date('H:i:s');
        self::$logger->accion = $accion;
        self::$logger->nota = substr($nota, 0, 300);
        self::$logger->save();
    }

    public static function changeNoteLog($nota)
    {
        if(self::$logger)
        {
            self::$logger->nota = substr($nota, 0, 300);
            self::$logger->save();
        }
    }

    public static function logClose()
    {
        if(self::$logger)
        {
            self::$logger->nota = substr(self::$logger->nota."\n"."Proceso exitoso.", 0, 300);
            self::$logger->save();
        }
    }

    public static function setModel(&$modelo, $data)
    {
        foreach ($data as $clave => $valor){
            $modelo->$clave = $valor;
        }
        return $modelo;
    }

    public static function createLogFile()
    {
        if(self::$log_file)
        {
            return self::$log_file;
        }else{
            $user = self::$usuario->usuario;
            $log_file_path = base_path("storage/logs/").$user.".log"; 
            if(!file_exists($log_file_path))
            {
                self::$log_file = fopen("{$log_file_path}", "w+");
            }else{
                self::$log_file = fopen("{$log_file_path}", "a+");
            }
        }
    }

    public static function addLogFile($texto_write)
    {
        if(self::$log_file)
        {
            fwrite(self::$log_file, $texto_write."\n");
        }
    }

    public static function closeLogFile()
    {
        if(self::$log_file){
            fclose(self::$log_file);
        }
        self::$log_file = null;
    }

    public static function asignarUsuario($tipopc, $codciu)
    {
        $mercurio05 = Mercurio05::where("codciu", $codciu)->get()->first();
        if(!$mercurio05){
            $mercurio04 = Mercurio04::where("principal", 'S')->get()->first();
            $codofi = $mercurio04->codofi;
        }else{
            $codofi = $mercurio05->codofi;
        }
        $mercurio08 = Mercurio08::where("codofi", $codofi)
        ->where("tipopc", $tipopc)
        ->where("orden",1)
        ->get()
        ->first();
        
        if(!$mercurio08){
        
            $usuario = Mercurio08::where("codofi",$codofi)
            ->where("tipopc",$tipopc)
            ->min("usuario");
        }else{
            $usuario = $mercurio08->usuario;
        }
        if($usuario == "") return "";

        $usuario_orden = Mercurio08::where("codofi", $codofi)
        ->where("tipopc",$tipopc)
        ->where("usuario",'>', $usuario)
        ->min("usuario");
        
        Mercurio08::where("codofi", $codofi)
        ->where("tipopc",$tipopc)
        ->update(["orden"=> 0]);

        Mercurio08::where("codofi", $codofi)
        ->where("tipopc",$tipopc)
        ->where("usuario",$usuario_orden)
        ->update(["orden"=> 1]);
        
        return $usuario;
    }

    public static function calcula_categoria($salario, $periodo = '')
    {
        if(empty($periodo)){

            $mperiodo = Subsi12::where("girado",'N')->min('periodo');
            if(!$mperiodo){
                return [
                        "success"=>false,
                        "msg"=>"No hay periodo no girardo vigente"
                    ];
            }
        }else{
            $mperiodo = Subsi12::where("periodo", $periodo)->get()->first();
            if(!$mperiodo){
                return [
                    "success"=>false,
                    "msg"=>"No hay periodo vigente {$periodo}"
                ];
            }
        }

        if($mperiodo->salmin == 0)
        {
            return [
                "success"=>false,
                "msg"=>"El periodo {$periodo} no tiene definido el salario minimo."
            ];
        }
        $cansal = $salario / $mperiodo->salmin;
        $categoria='C';
        $categorias = DB::table('subsi30')->orderBy('cansal','desc')->get();
        foreach ($categorias as $ai => $category) {
            if($category->cansal <= $cansal) {
                $categoria = $category->codcat;
                break; 
            }
        }

       return [
            "success" => true,
            "valor" => $categoria 
        ];
    }

}