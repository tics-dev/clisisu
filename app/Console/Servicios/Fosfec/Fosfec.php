<?php
require_once base_path('app/Libraries/PHPExcel.php');
require_once base_path('app/Libraries/PHPExcel/IOFactory.php');
require_once base_path('app/Libraries/PHPExcel/Shared/Date.php');

use Illuminate\Support\Facades\DB;
use App\Exceptions\DebugException;
use App\Console\Servicios\General\General;
use Illuminate\Database\QueryException;
use App\Console\Servicios\Fosfec\ModificaAfiliacion;
use App\Console\Servicios\Fosfec\NovedadRetiros;

class Fosfec
{
    protected $usuario;
    protected $console;

    public function __construct($console)
    {
        $this->console = $console;
    }

    public function cruzar_activos_novedad_retiro($argv, $user, $sistema, $enviroment)
    {
        try {
            try {
                $filepath = base64_decode($argv);
                $this->usuario = $user;
                $novedad = new NovedadRetiros();
                $novedad->procesar($filepath, $sistema, $this->console);
                $resultado = $novedad->getResultado();
                $this->console->info(json_encode($resultado, JSON_NUMERIC_CHECK));
            } catch (QueryException $th) {
                throw new DebugException($th->getMessage(), 1);
            } catch (ErrorException $er) {
                throw new DebugException($er->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'estado'  => 'X',
                'success' => false
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    public function modificar_afiliacion_compensacion_familiar($argv, $user, $sistema, $enviroment)
    {
        try {
            try {
                $this->usuario = $user;
                $novedad = new ModificaAfiliacion();
                $novedad->procesar($sistema, $this->console);
                $resultado = $novedad->getResultado();
                $this->console->info(json_encode($resultado, JSON_NUMERIC_CHECK));
            } catch (QueryException $th) {
                throw new DebugException($th->getMessage(), 1);
            } catch (ErrorException $er) {
                throw new DebugException($er->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'estado'  => 'X',
                'success' => false
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }
}
