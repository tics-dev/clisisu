<?php
namespace  App\Console\Servicios\Fosfec;
use App\Exceptions\DebugException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use App\Models\Subsi15;
use App\Models\Subsi16;
use App\Models\Subsi20;

class ModificaAfiliacion {

    protected $resultado;

    public function procesar($sistema, $console)
    {
        ini_set('memory_limit','1300M');
        $num=0;
        $errors = [];
        $_date = date('Ymd');
        $name = "CNCCCF13{$_date}";
        $filepath = "/var/www/html/{$sistema}/public/temp/{$name}";
        $file = fopen("{$filepath}", "w+");

        $trabajadores = Subsi15::where("estado", 'A')
        ->get();

        $cantidad=0;
        foreach ($trabajadores as $trabajador)
        {
            $num++;
            $ultima_fecha_afiliacion = Subsi16::where('cedtra', $trabajador->cedtra)
            ->whereNull('fecret')
            ->max('fecafi');
            
            if($ultima_fecha_afiliacion)
            {
                $cantidad++;
                $codigo_documento = $trabajador->get_gener18;
                $codrua = ($codigo_documento->codrua == 'PEP')? 'PE': $codigo_documento->codrua;
                $content = array(
                    '2',
                    "CCF13",
                    $codrua,
                    $trabajador->cedtra,
                    $trabajador->priape,
                    $trabajador->segape,
                    $trabajador->prinom,
                    $trabajador->segnom,
                    "C03",
                    $ultima_fecha_afiliacion,
                    "1",
                    "1",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    ""
                );
                $texto_write = implode(',', $content);
                fwrite($file, $texto_write."\n");
                $console->info("ACTIVO {$trabajador->cedula} FechaAfiliacion: {$ultima_fecha_afiliacion}");
            }
        }
        fclose($file);
        $header = array(
            "1",
            "CCF13",
            date('Y-m-d'),
            date('Y-m-d'),
            $cantidad,
            "{$name}",
            ""
        );
        $path = "/var/www/html/clisisu/storage/files/";
        $texto_write = implode(',', $header);
        file_put_contents($path.''.$name.'.txt', $texto_write."\n");
        
        $content = file_get_contents($filepath);
        file_put_contents($path.''.$name.'.txt', $content, FILE_APPEND);

        file_put_contents($path.''.$name.'_errors.txt', json_encode($errors));
        $this->resultado = $name.'.txt';
    }

    public function getResultado()
    {
        return $this->resultado;
    }

}