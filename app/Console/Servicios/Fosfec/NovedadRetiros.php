<?php

namespace  App\Console\Servicios\Fosfec;

use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Shared_Date;

use App\Exceptions\DebugException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use App\Models\Subsi15;
use App\Models\Subsi20;

class NovedadRetiros
{

    protected $resultado;

    public function procesar($archivo, $sistema, $console)
    {
        $archivo = "/var/www/html/clisisu/storage/files/{$archivo}";
        $inputFileType = PHPExcel_IOFactory::identify($archivo);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($archivo);
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        $num = 0;
        $errors = [];
        $_date = date('Ymd');
        $name = "CNCCCF13{$_date}";
        $filepath = "/var/www/html/{$sistema}/public/temp/{$name}";
        $file = fopen("{$filepath}", "w+");

        $cantidad = 0;
        for ($row = 2; $row <= $highestRow; $row++) {
            $has = 0;
            $num++;
            $cedula = intval($sheet->getCell("A{$row}")->getValue());
            $fecha_afiliacion = trim($sheet->getCell("B{$row}")->getValue());
            $fecha_retiro = false;

            $trabajador = Subsi15::where('cedtra', $cedula)
                ->get()
                ->first();
            if ($trabajador) {
                $trayectoria = $trabajador->getTrayectoria()->toArray();
                if ($trayectoria) {
                    foreach ($trayectoria as $ai => $trayecto) {
                        if ($fecha_afiliacion == $trayecto->fecafi && strlen($trayecto->fecret) > 0) {
                            //si posee fecha de retiro y fecha de afiliacion se debe reportar 
                            $fecha_retiro = $trayecto->fecret;
                            $has++;
                            break;
                        }
                    }
                }
                if ($has) {
                    $cantidad++;
                    $codigo_documento = $trabajador->get_gener18;
                    $codrua = ($codigo_documento->codrua == 'PEP') ? 'PE' : $codigo_documento->codrua;
                    $content = array(
                        '2',
                        "CCF13",
                        $codrua,
                        $cedula,
                        $trabajador->priape,
                        $trabajador->segape,
                        $trabajador->prinom,
                        $trabajador->segnom,
                        "C04",
                        $fecha_afiliacion,
                        $fecha_retiro,
                        "",
                        "1",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""
                    );
                    $texto_write = implode(',', $content);
                    fwrite($file, $texto_write . "\n");
                    $console->info("INACTIVO {$cedula} FechaAfiliacion: {$fecha_afiliacion} FechaRetiro: {$fecha_retiro}");
                }
            } else {
                $errors[] = array("cedula" => $cedula, "fecha_afiliacion" => $fecha_afiliacion, "error" => "No dispone de información");
            }
        }
        fclose($file);
        $header = array(
            "1",
            "CCF13",
            date('Y-m-d'),
            date('Y-m-d'),
            $cantidad,
            "{$name}",
            ""
        );
        $path = "/var/www/html/clisisu/storage/files/";
        $texto_write = implode(',', $header);
        file_put_contents($path . '' . $name . '.txt', $texto_write . "\n");

        $content = file_get_contents($filepath);
        file_put_contents($path . '' . $name . '.txt', $content, FILE_APPEND);

        file_put_contents($path . '' . $name . '_errors.txt', json_encode($errors));
        $this->resultado = $name . '.txt';
    }

    public function getResultado()
    {
        return $this->resultado;
    }
}
