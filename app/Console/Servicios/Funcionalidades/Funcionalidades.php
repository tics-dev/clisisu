<?php
use Illuminate\Support\Facades\DB;
use App\Exceptions\DebugException;
use App\Console\Servicios\ServicioSat\TareasMercurio;
use App\Console\Servicios\ServicioSat\Tareas;
use App\Console\Servicios\Funcionalidades\ProcesarSolicitudes;
use App\Console\Servicios\General\General;
use App\Models\Sat20;
use App\Models\Sat16;
use App\Models\Sat17;
use App\Models\Sat18;
use App\Models\Gener18;
use Illuminate\Database\QueryException;

class Funcionalidades
{
    protected $numtraccf;
    protected $usuario;
    protected $subsi02;
    protected $fecha;
    protected $campos;
    protected $rules;
    public $sat;

    protected $console;
    public function __construct($console)
    {
        $this->console = $console;
        $this->fecha = date('Y-m-d');
    }

    /**
     * consulta_detalle function
     * Se consulta el detalle de la solicitod en el servicio dispuesto por el ministerio
     * @param [type] $argv
     * @param string $user
     * @param string $sistema
     * @param string $enviroment
     * @return void
     */
    public function consulta_detalle($argv, $user='1', $sistema='sisuweb', $enviroment='2')
    {
        try {
            try {
                $jd = json_decode(base64_decode($argv));
                General::setUser($user);
                //se busca en estado para consulta
                $nota = Sat18::where("id", $jd->id)
                ->where("numero_transaccion", $jd->numero_transaccion)
                ->get()
                ->first();
   
                if(!$nota) {
                    throw new DebugException("Lo datos de la solicitud no correponden al id:{$jd->id} number: {$jd->numero_transaccion}", 501);
                }
                if($nota->estado == 'C') {
                    throw new DebugException("Lo datos de la solicitud ya fueron consultados.", 501);
                }
                if($nota->estado == 'X') {
                    throw new DebugException("El sat reporto error en el llamado de la solicitud de consulta", 501);
                }
                $procesar = new ProcesarSolicitudes();
                $procesar->set_env($this->console, $enviroment, true);
                
                switch ($nota->codigo_novedad)
                {
                    case '1':
                    case '2':
                        $procesar->solicitud_afiliacion_primeray_segundavez($jd);
                        break;
                    case '3':
                        $procesar->solicitud_desafiliacion_auna_ccf($jd);
                        break;
                    case '4':
                        $procesar->solicitud_inicio_relacion_laboral($jd);
                        break;
                    case '5':
                        $procesar->terminacion_relacion_laboral($jd);
                        break;
                    case '6':
                        $procesar->suspension_temporal_delcontrato_detrabajo($jd);
                        break;
                    case '7':
                        $procesar->licencias_remuneradas_yno_remuneradas($jd);
                        break;
                    case '8':
                        $procesar->modificacion_de_salario($jd);
                        break;
                    case '9':
                        $procesar->retiro_definitivo_delempleador_alssf($jd);
                        break;
                }

                $resultado = $procesar->getResultado();
                $this->console->info(json_encode($resultado, JSON_NUMERIC_CHECK));
                General::logClose();
            } catch (QueryException $th)
            {
                throw new DebugException($th->getMessage(), 1);
            }
        } catch (DebugException $err)
        {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555)? DebugException::item("valid") : array(),
                'estado'  => 'X',
                'success' => false
            );
            General::changeNoteLog($err->getMessage());
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    public function listar_notificaciones_sat($argv, $user='1', $sistema='sisuweb', $enviroment='2')
    {
        try {
            $jd = json_decode(base64_decode($argv));
            $total_records = Sat18::where('estado', $jd->estado)
            ->count();
            
            $total_pages = ceil($total_records / $jd->limit);
            $start_from = ($jd->page-1) * $jd->limit;

            $notificaciones = Sat18::where('estado', $jd->estado)
            ->limit($jd->limit)
            ->offset($start_from)
            ->orderBy("fecha_vigencia", 'ASC')
            ->get();

            
            $data = array(
                "data"          => $notificaciones->toArray(),
                "total_records" => $total_records,
                "total_pages"   => $total_pages,
                "start_from"    => $start_from,
                "success" => true
            );
            $this->console->info(json_encode($data, JSON_NUMERIC_CHECK));
        } catch (DebugException $err)
        {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => array(),
                'estado'  => 'X'
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    public function respuesta_notificaciones($argv, $user='1', $sistema='sisuweb', $enviroment='2')
    {
        try {
            try {
                $this->usuario = $user;
                $jd = json_decode(base64_decode($argv));
                $tareas = new Tareas($this->console);
                $tareas->set_enviroment($enviroment);
                $nota = Sat18::where("numero_transaccion", $jd->numero_transaccion)
                ->get()
                ->first();

                if(!$nota) {
                    throw new DebugException("El número de transacción no es valido", 501);
                }
                if($nota->estado == 'F') {
                    throw new DebugException("Lo datos de la solicitud ya fueron consultados.", 501);
                }
                if($nota->estado == 'X') {
                    throw new DebugException("La solicitud reporto error en la consulta previa.", 501);
                }
                switch ($nota->codigo_novedad)
                {
                    case '1':
                    case '2':
                        $tareas->respuesta_afiliacion_ccf_primera_vez($jd, $this->usuario);
                        break;
                    case '3':
                        break;
                    case '4':
                        break;
                    case '5':
                        break;
                    case '6':
                        break;
                    case '7':
                        break;
                    case '8':
                        break;
                    case '9':
                        break;
                }
                if($tareas->resultado['success'] == TRUE){
                    Sat18::where("numero_transaccion", $jd->numero_transaccion)->update(["estado" => 'F']);
                }else{
                    Sat18::where("numero_transaccion", $jd->numero_transaccion)->update(["estado" => 'C']);
                }
                $this->console->info(json_encode($tareas->resultado, JSON_NUMERIC_CHECK));
                General::logClose();
            } catch (QueryException $th)
            {
                throw new DebugException($th->getMessage(), 1);
            }
        } catch (DebugException $err)
        {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555)? DebugException::item("valid") : array(),
                'estado'  => 'X',
                'success' => false
            );
            General::changeNoteLog($err->getMessage());
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    public function createSat20($tiptra)
    {
        $sat20 = new Sat20();
        $sat20->numtraccf = $this->numtraccf;
        $sat20->fecha     = $this->fecha;
        $sat20->hora      = date('H:i:s');
        $sat20->usuario   = $this->usuario;
        $sat20->tiptra    = $tiptra;
        $sat20->gestion   = 'C';
        $sat20->estado    = 'P';
        $sat20->save();
    }

    public function prepara_data_sat()
    {
        $data = array();
        $attr =  $this->sat->makeVisible('attribute')->toArray();
        foreach ($this->campos as $key => $value)
        {
            $data[$value] = (isset($attr["$key"]))? $attr["$key"] : '';
        }
        $data['NumeroRadicadoSolicitud'] = $this->numtraccf;
        return $data;
    }

}