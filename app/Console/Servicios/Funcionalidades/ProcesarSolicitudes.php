<?php
namespace  App\Console\Servicios\Funcionalidades;

use Illuminate\Database\QueryException;
use App\Exceptions\DebugException;
use Illuminate\Support\Facades\DB;
use App\Console\Servicios\ServicioSat\TareasMercurio;
use App\Console\Servicios\ServicioSat\Tareas;
use App\Models\Sat16;
use App\Models\Sat18;
use App\Models\Mercurio30;

class ProcesarSolicitudes {

    protected $resultado;
    protected $codsuc;
    protected $console;
    protected $enviroment;
    protected $debugger = false;

    public function set_env($console, $enviroment, $sat= true)
    {
        $this->console = $console;
        $this->enviroment = $enviroment;
        $this->enabled_sat = $sat;
    }

    public function solicitud_afiliacion_primeray_segundavez($jd)
    {
        try {
            //registra solicitud de afiliacion primera y segunda vez
            $solicitud = Sat16::where('numero_radicado', $jd->numero_transaccion)
                ->get()
                ->first();

            if (!$solicitud) {
                $solicitud = new Sat16;
                $solicitud->numero_radicado = $jd->numero_transaccion;
                $solicitud->fecha = date('Y-m-d H:i:s');
            } else {
                if ($solicitud->estado == 'X')
                {
                    Sat18::where("id", $jd->id)
                    ->where("numero_transaccion", $jd->numero_transaccion)
                    ->update(["estado" => 'C']);
                    throw new DebugException("Los datos ya se consultaron previamente, el sat reporto código de error para la presente solicitud.", 202);
                }
            }

            $solicitud->estado = 'P'; //pendiente de consultar
            $solicitud->save();
            if($this->debugger == true)
            {
                $result = $this->seeds_debugger_empresa_sat($jd->numero_transaccion);
            }else{
                $tareas = new Tareas($this->console);
                $tareas->set_enviroment($this->enviroment);
                $result = $tareas->alternativo(
                    [
                        "NumeroTransaccion" =>  $jd->numero_transaccion
                    ],
                    "sat16",
                    $jd->numero_transaccion
                );
            }
            $detalle = json_decode($result);
            $tareas_mercurio = new TareasMercurio($this->console);
            $tareas_mercurio->guardar_recepcion_sat($result, $jd->numero_transaccion);
            $data = array();
            $mensaje = "Proceso pendiente de ejecutar.";
            $success = false;

            if (!isset($detalle->Message) && isset($detalle->NumeroDocumentoEmpleador)) 
            {
                //verfica que este registrado tambien en la mercurio30
                $mercurio30 = Mercurio30::where('nit', $detalle->NumeroDocumentoEmpleador)
                    ->where("tipdoc", $detalle->TipoDocumentoEmpleador)
                    ->get()
                    ->count();
                if ($mercurio30 > 0) {
                    throw new DebugException("Los datos ya se consultaron previamente, no se requiere de más acciones.", 202);
                } else {
                    DB::beginTransaction();
                    $detalle->clave = $jd->clave;
                    $detalle->hash  = $jd->hash;
                    $data = $tareas_mercurio->mercurio_afiliacion_empresa_modelo_sat($detalle, $jd->numero_transaccion);
                    $mensaje = "Proceso completado con éxito";
                    Sat18::where("id", $jd->id)
                        ->where("numero_transaccion", $jd->numero_transaccion)
                        ->update(["estado" => 'C']);
                    $success = true;
                    $codigo = 200;
                    DB::commit();
                }
            } elseif (isset($detalle->codigo) && isset($detalle->mensaje)) 
            {
                //error reportado por el servicio de consulta con gloza
                Sat18::where("id", $jd->id)
                    ->where("numero_transaccion", $jd->numero_transaccion)
                    ->update(["estado" => 'X']);
                Sat16::where("numero_radicado", $jd->numero_transaccion)->update(["estado" => 'X']);
                throw new DebugException($detalle->mensaje, 402);
            } else {
                //error reportado 500 por conexion
                Sat18::where("id", $jd->id)
                    ->where("numero_transaccion", $jd->numero_transaccion)
                    ->update(["estado" => 'X']);

                Sat16::where("numero_radicado", $jd->numero_transaccion)->update(["estado" => 'X']);
                throw new DebugException($detalle->Message, 403);
            }
            $data['response_sat_detalle'] = $detalle;
            $this->resultado = array(
                "success" => $success,
                "mensaje" => $mensaje,
                "data" => $data,
                "codigo" => $codigo
            );
        } catch (QueryException $ex) {
            DB::rollBack();
            throw new DebugException($ex->getMessage() . ' ' . $ex->getLine() . ' ' . basename($ex->getFile()), 1);
        }
    }

    public function solicitud_desafiliacion_auna_ccf($jd)
    {  
    }

    public function solicitud_inicio_relacion_laboral($jd)
    {
    }

    public function terminacion_relacion_laboral($jd)
    {
    }

    public function suspension_temporal_delcontrato_detrabajo($jd)
    {
    }

    public function licencias_remuneradas_yno_remuneradas($jd)
    {
    }

    public function modificacion_de_salario($jd)
    {
    }

    public function retiro_definitivo_delempleador_alssf($jd)
    {
    }

    public function getResultado()
    {
        return $this->resultado;
    }

    public function seeds_debugger_empresa_sat($numero_transaccion)
    {
        return json_encode(
            array(
            "NumeroRadicadoSolicitud"=> "",
            "NumeroTransaccion"=> "{$numero_transaccion}",
            "TipoPersona"=> "N",
            "NaturalezaJuridicaEmpleador"=> "N",
            "TipoDocumentoEmpleador"=> "CC",
            "NombreRazonSocial"=> "PRUEBA EMPRESA",
            "NumeroDocumentoEmpleador"=> "1110491953",
            "SerialSAT"=> "0",
            "PrimerNombreEmpleador"=> "Prueba",
            "SegundoNombreEmpleador"=> "Prueba",
            "PrimerApellidoEmpleador"=> "Prueba",
            "SegundoApellidoEmpleador"=> "Prueba",
            "FechaSolicitud"=> "2022-03-10",
            "PerdidaAfiliacionCausaGrave"=> "",
            "FechaEfectivaAfiliacion"=> "2022-03-10",
            "RazonSocial"=> "Prueba Razon",
            "NumeroMatriculaMercantil"=> "0001",
            "Departamento"=> "18",
            "Municipio"=> "1",
            "DireccionContacto"=> "Prueba Direccion",
            "NumeroTelefono"=> "0",
            "CorreoElectronico"=> "",
            "TipoDocumentoRepresentante"=> "NI",
            "NumeroDocumentoRepresentante"=> "CC",
            "NumeroDocumentoRepresentanteLegal"=> "1110491953",
            "PrimerNombreRepresentanteLegal"=> "Prueba",
            "SegundoNombreRepresentanteLegal"=> "Prueba",
            "PrimerApellidoRepresentanteLegal"=> "Prueba",
            "SegundoApellidoRepresentanteLegal"=> "Prueba",
            "AutorizacionManejoDatos"=> "S",
            "AutorizacionNotificaciones"=> "S",
            "ManifestacionNoAfiliacionPrevia"=> "",
            "TelefonoContacto"=> "3100000",
            "CorreoElectronicoContacto"=> "sistemas@comfaca.com",
            "TipoDocumentoRepresentanteLegal"=> "CC"
        ));
    }

}