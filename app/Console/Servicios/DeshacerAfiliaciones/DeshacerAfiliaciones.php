<?php

use App\Console\Servicios\Comandos\Comandos;
use App\Exceptions\DebugException;
use App\Models\Comando;
use App\Models\Subsi02;
use App\Models\Subsi15;
use App\Models\Trazabilidad;
use App\Services\DeshacerAfiliaciones\DeshaceEmpresa;
use App\Services\DeshacerAfiliaciones\DeshaceTrabajador;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class DeshacerAfiliaciones
{
    protected $console;

    public function __construct($console)
    {
        ini_set('memory_limit', '2000M');
        $this->console = $console;
    }

    /**
     * buscar los datos de empresa y trayectorias.
     * valida que tenga más de una trayectoria. 
     * para más de una trayectoria omite el borrado de la empresa
     * para una sola trayecto, se valida la fecha de retiro fecret, si esta definido y la fecha de afiliacion es = fecafi del trayecto se debe borrar al igual que el trayecto
     */
    public function deshacerAprobacionEmpresa($argv, $user, $sistema, $env)
    {
        try {
            $modelo = json_decode(base64_decode($argv));
            $deshaceEmpresa = new DeshaceEmpresa($modelo->nit, $modelo->tipo_documento);
            $deshaceEmpresa->procesar();

            $trasa = new Trazabilidad();
            $trasa->create(
                [
                    "id" => Trazabilidad::max('id') + 1,
                    "usuario" => $user,
                    "detalle_evento" => substr("Deshacer aprobación empresa {$modelo->nit}, {$modelo->nota}", 0, 224),
                    "modulo" =>  "Comfaca en línea, Aprobación De Empresas",
                    "medio" => 'Gestión subsidio',
                    "estado" => "Evento generado",
                    "hora" => date('H:i:s'),
                    "create_at" => date('Y-m-d H:i:s')
                ]
            );

            $this->console->info(json_encode([
                'success' => true,
                'data' => $deshaceEmpresa->getResultado()
            ]));
        } catch (DebugException $err) {
            $this->console->comandoProcesador->cancelarComando();
            $salida = array(
                "success" => false,
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode()
            );
            $this->console->error(json_encode($salida));
        }
    }



    public function deshacerAprobacionTrabajador($argv, $user, $sistema, $env)
    {
        try {
            $modelo = json_decode(base64_decode($argv));
            $deshaceTrabajador = new DeshaceTrabajador($modelo->nit, $modelo->tipo_documento, $modelo->cedtra);
            $deshaceTrabajador->procesar();
            $trasa = new Trazabilidad();
            $trasa->create(
                [
                    "id" => Trazabilidad::max('id') + 1,
                    "usuario" => $user,
                    "detalle_evento" => substr("Deshacer aprobación trabajador {$modelo->cedtra}, {$modelo->nota}", 0, 224),
                    "modulo" =>  "Comfaca en línea, aprobación de trabajador",
                    "medio" => 'Gestión subsidio',
                    "estado" => "Evento generado",
                    "hora" => date('H:i:s'),
                    "create_at" => date('Y-m-d H:i:s')
                ]
            );

            $data = $deshaceTrabajador->getResultado();

            $this->console->info(json_encode([
                'success' => true,
                'data' => $data
            ]));
            // dd($data);
        } catch (DebugException $err) {
            $this->console->comandoProcesador->cancelarComando();
            $salida = array(
                "success" => false,
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode()
            );
            $this->console->error(json_encode($salida));
        }
    }
}
