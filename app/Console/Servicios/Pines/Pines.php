<?php
declare(strict_types=1);
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Exceptions\DebugException;
use App\Models\Subsi15;
use App\Models\Subsi20;
use App\Models\Subsi22;
use App\Models\Subsi23;
use App\Models\PinAfiliado;

class Pines
{
	protected $fecha;
	protected $console;

	public function __construct($console)
	{
		ini_set('memory_limit', '2000M');
		$this->console = $console;
		$f = new DateTime('now');
		$this->fecha = $f->format('Y-m-d');
	}

    public function listarPines($argv, $user, $sistema, $enviroment)
	{
        try {
		} catch (DebugException $err)
		{
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode()
			);
			$this->console->error(json_encode($salida));
		} 
    }

	public function pinesTrabajador($cedtra, $user, $sistema, $enviroment)
	{
        try {
			$pinesAfiliado = PinAfiliado::where('cedtra', $cedtra)->where('estado', 'A')->get();
			$salida = [
				'success' => 'true',
				'data' => $pinesAfiliado->toArray()
			];
			$this->console->info(json_encode($salida));
		} catch (DebugException $err)
		{
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode()
			);
			$this->console->error(json_encode($salida));
		}
    }
}