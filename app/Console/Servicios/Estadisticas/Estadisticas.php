<?php

declare(strict_types=1);
require_once base_path('app/Libraries/PHPExcel.php');
require_once base_path('app/Libraries/PHPExcel/IOFactory.php');
require_once base_path('app/Libraries/PHPExcel/Shared/Date.php');

use App\Console\Servicios\Comandos\Comandos;
use App\Console\Servicios\Estadisticas\PoblacionAfiliadosCargo;
use Illuminate\Database\QueryException;
use App\Exceptions\DebugException;
use App\Models\Subsi20;
use App\Models\Subsi15;
use Illuminate\Support\Facades\DB;

class Estadisticas
{

    protected $usuario;
    protected $sistema;
    protected $user;
    protected $console;
    protected $poblacionAfiliadosCargo;
    protected $fecha;

    public function __construct($console)
    {
        $this->console = $console;
        $f = new DateTime('now');
        $this->fecha = $f->format('Y-m-d');
    }

    public function proceso_poblacion_afiliados_cargo($argv, $user, $sistema, $enviroment)
    {
        ini_set('memory_limit', '1300M');
        try {
            try {
                $modelo = json_decode(base64_decode($argv), true);
                $archivo = "/var/www/html/clisisu/storage/files/estadistica/" . $modelo['file'];
                if (!file_exists($archivo)) {
                    throw new DebugException("Error el archivo es requerido para procesar", 501);
                }
                $inputFileType = PHPExcel_IOFactory::identify($archivo);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($archivo);
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $sheet->getHighestColumn();

                Comandos::$cantidad = $highestRow;
                Comandos::setProporcion();
                $fraccion = Comandos::$cantidad / Comandos::$proporcion;
                $progreso_ini_conteo = 0;


                PoblacionAfiliadosCargo::$periodo = $modelo['periodo'];
                $num = 0;
                for ($row = 2; $row <= $highestRow; $row++) {
                    $poblacionAfiliadosCargo = new PoblacionAfiliadosCargo();
                    $num++;
                    $progreso_ini_conteo++;
                    if ($progreso_ini_conteo >= $fraccion) {
                        $progreso_ini_conteo = 0;
                        $this->console->comandoProcesador->actualizaProgreso($num);
                    }
                    $nit = intval($sheet->getCell("A{$row}")->getValue());
                    if (!$nit) continue;
                    $cedtra = intval($sheet->getCell("B{$row}")->getValue());
                    $code = intval($sheet->getCell("C{$row}")->getValue());
                    $poblacionAfiliadosCargo->validaDerechoAgro($nit, $cedtra, "0{$code}");
                }

                $resultado = json_encode(PoblacionAfiliadosCargo::getResultado());
                $this->console->comandoProcesador->finalizarProceso($resultado);
                $this->console->info($resultado);
            } catch (\Exception $th) {
                throw new DebugException($th->getMessage() . ' ' . $th->getLine() . ' ' . basename($th->getFile()), 501);
            } catch (QueryException $er) {
                throw new DebugException($er->getMessage() . ' ' . $er->getLine() . ' ' . basename($er->getFile()), 501);
            }
        } catch (DebugException $err) {
            $this->console->comandoProcesador->cancelarComando();
            $salida = array(
                "success" => false,
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode()
            );
            $this->console->error(json_encode($salida));
        }
    }

    public function reporteDian($argv, $user, $sistema, $enviroment)
    {
        ini_set('memory_limit', '1300M');
        try {
            try {

                $sql = "SELECT nit, cedtra, fecafi, fecret, COUNT(*) as numafi 
                FROM subsi16 
                WHERE nit IN(901385327,901425554,901441181,901417414,901171422,901279059,901426432,901429148,901158501,901442068,901354748,901137049,901352029,901394570,
                901236476,901372267,901320240,901436971,901248941,901182682,901392205,901228678,901227269,901142857,901246662,901253779,901254084,901257825,901262006,
                901262007,901262069,901263377,901264044,901320240,901335339,901251766,906135484,901426874,901282917,901263377,901137165,901326422,901134190,901240746,
                901357483,901183961,901194823,901335339,901214536,901164682,901230003,901229229,901151712,901439293,901368242,901124804,901392209,901425075,901198575,
                901386452,901373659,901282594,901310523,901150929,901148927,901171828,901179399,901210482,901220875,901160159,901148280,901166779,901093888,901142344,
                901152252,901356582,40782832,901243220,901202600,901228791,901130786,901066970,901183265,901425017,901182900) 
                GROUP BY nit, cedtra ORDER BY nit";

                $texto = [
                    'Nit',
                    'Perapo',
                    'Cedtra',
                    'Tipo documento',
                    'Nombre',
                    'Numero',
                    'Fecha planilla',
                    'Fecha pago',
                    'Dias cotizados',
                    'Valapo',
                    'Valnom',
                    'Salbas'
                ];
                $this->console->info(implode(";", $texto));

                $query = DB::select($sql);
                foreach ($query as $row) {
                    $planillas = DB::table("subsi64")
                        ->select(
                            'subsi64.numrad',
                            'subsi64.nit',
                            'subsi64.fecsis',
                            'subsi64.fecrec',
                            'subsi65.diatra',
                            'subsi65.cedtra',
                            'subsi64.perapo',
                            'subsi65.prinom',
                            'subsi65.priape',
                            'subsi65.segape',
                            'subsi65.segnom',
                            'subsi65.valapo',
                            'subsi65.valnom',
                            'subsi65.salbas',
                            'subsi65.coddoc'
                        )
                        ->join("subsi65", "subsi65.numero", "=", "subsi64.numero")
                        ->where('subsi64.nit', $row->nit)
                        ->where('subsi65.cedtra', $row->cedtra)
                        ->get();

                    foreach ($planillas as $planilla) {
                        $texto = [
                            $planilla->nit,
                            $planilla->perapo,
                            $planilla->cedtra,
                            $planilla->coddoc,
                            $planilla->priape . ' ' . $planilla->segape . ' ' . $planilla->prinom . ' ' . $planilla->segnom,
                            $planilla->numrad,
                            $planilla->fecsis,
                            $planilla->fecrec,
                            $planilla->diatra,
                            intval($planilla->valapo),
                            intval($planilla->valnom),
                            intval($planilla->salbas)
                        ];
                        $this->console->info(implode(";", $texto));
                    }
                }

                $this->console->info("OK procesado");
            } catch (\Exception $th) {
                throw new DebugException($th->getMessage() . ' ' . $th->getLine() . ' ' . basename($th->getFile()), 501);
            } catch (QueryException $er) {
                throw new DebugException($er->getMessage() . ' ' . $er->getLine() . ' ' . basename($er->getFile()), 501);
            }
        } catch (DebugException $err) {
            $this->console->comandoProcesador->cancelarComando();
            $salida = array(
                "success" => false,
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode()
            );
            $this->console->error(json_encode($salida));
        }
    }
}
