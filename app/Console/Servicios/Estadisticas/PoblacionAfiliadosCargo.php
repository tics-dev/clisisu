<?php 
declare(strict_types=1);
namespace App\Console\Servicios\Estadisticas;

use App\Exceptions\DebugException;
use App\Models\Subsi15;
use App\Models\Subsi16;
use App\Models\Subsi20;
use App\Models\Subsi48;
use App\Models\Subsi02;
use App\Models\Subsi03;
use App\Models\Subsi04;
use App\Models\Subsi09;
use App\Models\Xml87;
use Estadisticas;

class PoblacionAfiliadosCargo
{

    public static $periodo;
    public static $trabajadores_agro;

    /**
     * validaDerechoAgro function
     * 01 normal        
     * 02 discapacidad
     * 03 sector agro
     * 04 discapacidad, sector del agro
     * 05 extraordinario muerte trabajador o beneficiario
     * 06 no tiene derecho
     * 07 extraordinario muerte trabajador o benefiario, sector agro
     * 08 extraordinario muerte trabajador o benefiario, discapacidad
     * 09 extraordinario muerte trabajador o benefiario, discapacidad, sector del agro
     * @param integer $nit
     * @param integer $cedtra
     * @param string $code
     * @return void
     */
    public function validaDerechoAgro(int $nit, int $cedtra, string $code)
    {
        $estadistica = Xml87::where('nit', $nit)
        ->where('cedtra', $cedtra)
        ->where('tipcuo', $code)
        ->where('periodo', self::$periodo)
        ->get()
        ->first();
        
        if(!$estadistica) return false;
        $giro = Subsi09::where('pergir', self::$periodo)
        ->where('nit', $nit)
        ->where('cedtra', $cedtra)
        ->get()
        ->first(); 
        
        if(!$giro) return false;
        $sucursal = Subsi48::where('nit', $nit)->where('codsuc', $giro->codsuc)->get()->first();

        $actividad = Subsi04::where('codact', $sucursal->codact)->get()->first();

        if($actividad->esagro == 1){
            
            //no tiene derecho
            if( $estadistica->tipcuo == '03' || 
            $estadistica->tipcuo == '04' || 
            $estadistica->tipcuo == '07' || 
            $estadistica->tipcuo == '09'){   
                $trabajadores_agro[] = $cedtra; 
            } else {

                if($estadistica->tipcuo != '06')
                {
                    $estadistica->tipcuo = '03';
                    $estadistica->save(); 
                    $trabajadores_agro[] = $cedtra;     
                }
            }
        } else {
            switch ($estadistica->tipcuo) {
                case '03':
                    //solo normal
                    $estadistica->tipcuo = '01';
                break;
                case '04':
                    //solo discapacidad
                    $estadistica->tipcuo = '02';
                break;
                case '07':
                    //solo muerte extraordinario 
                    $estadistica->tipcuo = '05';
                break;
                case '09':
                    //solo muerte extraordinario 
                    $estadistica->tipcuo = '08';
                break;
            }
            $estadistica->save();  
        }
    }


    public static function getResultado()
    {
        $file = "/var/www/html/SYS/public/temp/log_estadistica.json";
        file_put_contents($file, json_encode(self::$trabajadores_agro));
        return [
            "success"=> true,
            "msj"=> "Proceso completado con éxito",
            "url"=> $file,
            "filename"=> basename($file)
        ];
    }

}