<?php
require_once base_path('app/Libraries/PHPExcel.php');
require_once base_path('app/Libraries/PHPExcel/IOFactory.php');
require_once base_path('app/Libraries/PHPExcel/Shared/Date.php');

use App\Console\Servicios\Comandos\Comandos;
use Illuminate\Support\Facades\DB;
use App\Exceptions\DebugException;
use App\Console\Servicios\ServicioSat\Tareas;
use App\Console\Servicios\ServicioSat\Empresa;
use App\Console\Servicios\ServicioSat\RestService\RestServiceFactoryImpl;
use App\Exceptions\GlozaException;
use Illuminate\Database\QueryException;
use App\Models\Sat20;
use App\Models\Sat02;
use App\Models\Sat03;
use App\Models\Sat06;
use App\Models\Sat01;
use App\Models\Sat09;
use App\Models\Sat13;
use App\Models\Subsi02;
use App\Models\Sat08;
use App\Models\Subsi15;
use App\Models\Gener08;
use App\Models\Sat10;
use App\Models\Sat15;
use App\Models\Subsi168;

class Reprocesos
{
	protected $usuario;
	protected $fecha;
	protected $console;

	public function __construct($console)
	{
		$this->console = $console;
		$this->fecha = date('Y-m-d');
	}

	/**
	 * reprocesar_empresas_nuevas function
	 * se busca reprocesar las empresas nuevas que se generp glozas de forma masiva, con respecto al estado
	 * @property commando p7 artisan server:send Reprocesos reprocesar_empresas_nuevas true 1 SYS 1
	 * @param [type] $argv
	 * @param string $user
	 * @param string $sistema
	 * @param string $enviroment
	 * @return void
	 */
	public function reprocesar_empresas_nuevas($argv, $user = '1', $sistema = 'sisuweb', $enviroment = '2')
	{
		try {
			try {
				$tareas = new Tareas($this->console);
				$tareas->set_enviroment($enviroment);

				$this->usuario 	= $user;
				$glozas = Sat20::where("tiptra", '1')->where('estado', 'G')->where('persiste', '0');

				Comandos::$cantidad = $glozas->count();
				Comandos::setProporcion();
				$fraccion = Comandos::$cantidad / Comandos::$proporcion;
				$progreso_ini_conteo = 0;

				$num = 0;
				foreach ($glozas->get() as $gloza) {
					$num++;
					$progreso_ini_conteo++;
					if ($progreso_ini_conteo >= $fraccion) {
						$progreso_ini_conteo = 0;
						$this->console->comandoProcesador->actualizaProgreso($num);
					}

					$sat2 = Sat02::where("numtraccf", $gloza->numtraccf)->get()->first();
					if (!$sat2) {
						Sat20::where("numtraccf", $gloza->numtraccf)->delete();
						continue;
					}

					$params = new stdClass;
					$params->numdocemp = $sat2->numdocemp;
					$resultado = $tareas->empresa_nueva_afiliacion($params);
					Sat20::where('numtraccf', $gloza->numtraccf)->update(['persiste' => '1']);

					$this->console->info("nit: {$resultado['data']['numdocemp']} | {$resultado['data']['numtraccf']} | {$resultado['codigo']}");
				}

				$this->console->comandoProcesador->finalizarProceso();
			} catch (QueryException $sql_err) {
				$msj = $sql_err->getMessage() . ' ' . basename($sql_err->getFile()) . ' ' . $sql_err->getLine();
				throw new DebugException($msj, 1);
			} catch (GlozaException $ge) {
				throw new DebugException($ge->getMessage(), 1);
			}
		} catch (DebugException $err) {

			$this->console->comandoProcesador->cancelarComando();
			$salida = array(
				"Error"   => $err->getMessage(),
				'Archivo' => basename($err->getFile()),
				'Linea'   => $err->getLine(),
				'Codigo'  => $err->getCode(),
				'Data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
				'estado'  => 'X'
			);
			$this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
		}
	}

	/**
	 * depurar_empresa_novalida function
	 * Permite depurar las desafilaiciones que no poseen ninguna empresa registarda en el sistema de sisuweb
	 * tabla sat02, sat03, sat08, sat15, sat06
	 * @param [type] $argv
	 * @param string $user
	 * @param string $sistema
	 * @param string $enviroment
	 * @return void
	 */
	public function depurar_empresa_novalida($argv, $user = '1', $sistema = 'sisuweb', $enviroment = '2')
	{
		try {
			try {
				$modelo = json_decode(base64_decode($argv), true);
				$tareas = new Tareas($this->console);
				$tareas->set_enviroment($enviroment);

				$solicitudes = DB::table("sat02")
					->select("sat02.numtraccf", "sat02.numdocemp")
					->join('sat20', 'sat20.numtraccf', '=', "sat02.numtraccf")
					->whereIn("sat20.estado", $modelo['estado'])
					->get();

				foreach ($solicitudes as $solicitud) {
					$has = Subsi02::where('nit', $solicitud->numdocemp)->get()->first();
					if (!$has) {
						$params = (object) array(
							"numtraccf" => $solicitud->numtraccf,
							"numdocemp" => $solicitud->numdocemp,
							"tabla"     => 'sat02'
						);
						$tareas->gloza_delete($params);
					}
				}

				$solicitudes = DB::table("sat03")
					->select("sat03.numtraccf", "sat03.numdocemp")
					->join('sat20', 'sat20.numtraccf', '=', "sat03.numtraccf")
					->whereIn("sat20.estado", $modelo['estado'])
					->get();

				foreach ($solicitudes as $solicitud) {
					$has = Subsi02::where('nit', $solicitud->numdocemp)->get()->first();
					if (!$has) {
						$params = (object) array(
							"numtraccf" => $solicitud->numtraccf,
							"numdocemp" => $solicitud->numdocemp,
							"tabla"     => 'sat03'
						);
						$tareas->gloza_delete($params);
					}
				}

				$salida = [
					'success' => true,
					'msj' => "Proceso completado con éxito"
				];

				$this->console->comandoProcesador->finalizarProceso(json_encode($salida));
				$this->console->info(json_encode($salida, JSON_NUMERIC_CHECK));
			} catch (QueryException $sql_err) {
				$msj = $sql_err->getMessage() . ' ' . basename($sql_err->getFile()) . ' ' . $sql_err->getLine();
				throw new DebugException($msj, 1);
			} catch (GlozaException $ge) {
				throw new DebugException($ge->getMessage(), 1);
			}
		} catch (DebugException $err) {

			$this->console->comandoProcesador->cancelarComando();
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode(),
				'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
				'estado'  => 'X'
			);
			$this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
		}
	}

	/**
	 * depurar_trabajadores_novalidos function
	 * Permite depurar los trabajadores que no estan registrados en el sistema de sisuweb
	 * tabla sat09
	 * @param [type] $argv
	 * @param string $tabla
	 * @param string $sistema
	 * @param string $enviroment
	 * @return void
	 */
	public function depurar_trabajadores_novalidos($argv, $user = '1', $sistema = 'sisuweb', $enviroment = '2')
	{
		try {
			try {
				$modelo = json_decode(base64_decode($argv), true);
				$tareas = new Tareas($this->console);
				$tareas->set_enviroment($enviroment);

				$solicitudes = DB::table('sat09')
					->select("sat09.numtraccf", "sat09.numdocemp", "sat09.numdoctra")
					->join('sat20', 'sat20.numtraccf', '=', "sat09.numtraccf")
					->whereIn("sat20.estado", $modelo['estado'])
					->get();

				foreach ($solicitudes as $solicitud) {
					$has = Subsi15::where('cedtra', $solicitud->numdoctra)->where('estado', 'A')->get()->first();
					if (!$has) {
						$has = Subsi168::where('cedtra', $solicitud->numdoctra)->where('estado', 'A')->get()->first();
						if (!$has) {
							$params = (object) array(
								"numtraccf" => $solicitud->numtraccf,
								"tabla" => 'sat09'
							);
							$tareas->gloza_delete($params);
						}
					}
				}

				$salida = [
					'success' => true,
					'msj' => "Proceso completado con éxito"
				];

				$this->console->comandoProcesador->finalizarProceso(json_encode($salida));
				$this->console->info(json_encode($salida, JSON_NUMERIC_CHECK));
			} catch (QueryException $sql_err) {
				$msj = $sql_err->getMessage() . ' ' . basename($sql_err->getFile()) . ' ' . $sql_err->getLine();
				throw new DebugException($msj, 1);
			} catch (GlozaException $ge) {
				throw new DebugException($ge->getMessage(), 1);
			}
		} catch (DebugException $err) {

			$this->console->comandoProcesador->cancelarComando();
			$salida = array(
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode(),
				'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
				'estado'  => 'X'
			);
			$this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
		}
	}

	public function depurar_glozas_masivo_empresas($argv, $user = '1', $sistema = 'sisuweb', $enviroment = '2')
	{
		try {
			try {
				$tareas = new Tareas($this->console);
				$tareas->set_enviroment($enviroment);

				$query = DB::table('sat02')
					->select('sat02.numtraccf', 'sat02.numdocemp', 'sat20.estado', 'subsi02.nit', 'sat20.persiste', 'sat20.estado', 'subsi02.razsoc', 'sat02.tipper', "subsi02.repleg")
					->join('sat20', 'sat20.numtraccf', '=', 'sat02.numtraccf')
					->join('subsi02', 'subsi02.nit', '=', 'sat02.numdocemp')
					->where("sat20.persiste", '0')
					->whereIn("sat20.estado", ['X', 'P'])
					->get();
				$pendientes = array();

				foreach ($query as $sat02) {
					$modelo = Sat02::where('numtraccf', $sat02->numtraccf)
						->where('numdocemp', $sat02->numdocemp)
						->get()
						->first();

					$sat02->tipper = (intval($sat02->numdocemp) > 799999999 && $sat02->tipper == 'N') ? 'J' : $sat02->tipper;
					$modelo->matmer = "";
					$modelo->fecsol = $modelo->fecafi;
					$modelo->coddep = '18';
					$ciu = str_pad($modelo->codmun, 3, "0", STR_PAD_LEFT);
					$codciu = "18{$ciu}";

					$ciudad = Gener08::where("codciu", $codciu)->get()->first();
					if ($ciudad) {
						$modelo->codmun = $modelo->codmun;
					} else {
						$modelo->codmun = '001';
					}
					$modelo->telefono = (strlen($modelo->telefono) == 0) ? '0' : $modelo->telefono;

					$dt = $tareas->set_nombre($sat02->repleg);

					$modelo->tipemp = "";
					if ($sat02->tipper == 'N') {
						$modelo->tipdocrep = "";
						$modelo->razsoc = '';
						$modelo->numdocrep = '';
						$modelo->tipdocemp = ($modelo->tipdocemp == 'CC') ? $modelo->tipdocemp : "CC";
						$modelo->prinom = ($modelo->prinom == '') ? $dt[0] : $modelo->prinom;
						$modelo->segnom = ($modelo->segnom == '') ? $dt[1] : $modelo->segnom;
						$modelo->priape = ($modelo->priape == '') ? $dt[2] : $modelo->priape;
						$modelo->segape = ($modelo->segape == '') ? $dt[3] : $modelo->segape;
					} else {
						$modelo->razsoc = ($modelo->razsoc == '') ? $sat02->razsoc : $modelo->razsoc;
						$modelo->tipdocrep = ($modelo->tipdocrep == '') ? "CC" : $modelo->tipdocrep;
						$modelo->tipdocemp = ($modelo->tipdocemp == 'CC' || $modelo->tipdocemp == '') ? "NI" : $modelo->tipdocemp;
						$modelo->prinom2 = ($modelo->prinom2 == '') ? $dt[0] : $modelo->prinom2;
						$modelo->segnom2 = ($modelo->segnom2 == '') ? $dt[1] : $modelo->segnom2;
						$modelo->priape2 = ($modelo->priape2 == '') ? $dt[2] : $modelo->priape2;
						$modelo->segape2 = ($modelo->segape2 == '') ? $dt[3] : $modelo->segape2;
					}

					if (!$modelo->save()) {
						throw new DebugException("Error al guardar modelo sat02 {$sat02->numtraccf}", 1);
					}
					$data = (object) $modelo->makeVisible('attribute')->toArray();
					$data->numtraccf = $sat02->numtraccf;

					$params = new stdClass;
					$params->post = $data;
					$params->tabla = 'sat02';
					$rqs = $tareas->glozas_reprocesar($params);


					if ($rqs['estado'] == 'U' || $rqs['estado'] == 'O') {
						$params = (object) array(
							"numdocemp" => $sat02->numdocemp,
							"numtraccf" => $sat02->numtraccf,
							"tabla"     => "sat02",
							"tiptra"    => '1'
						);
						$rqs = $tareas->glozas_persistencia($params);
					} else {
						$pendientes[] = [
							"numdocemp" => $sat02->numdocemp,
							"numtraccf" => $sat02->numtraccf
						];
					}
					$this->console->info('--' . $modelo->numtraccf . '|' . $modelo->numdocemp . "|--\n");
				}
				$salida = [
					'success' => true,
					'data' => [
						"pendientes" => $pendientes
					]
				];

				$this->console->info(json_encode($salida));
			} catch (QueryException $sql_err) {
				$msj = $sql_err->getMessage() . ' ' . basename($sql_err->getFile()) . ' ' . $sql_err->getLine();
				throw new DebugException($msj, 1);
			} catch (GlozaException $ge) {
				throw new DebugException($ge->getMessage(), 1);
			}
		} catch (DebugException $err) {

			$salida = array(
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'Llnea'   => $err->getLine(),
				'codigo'  => $err->getCode(),
				'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
				'estado'  => 'X'
			);
			$this->console->comandoProcesador->cancelarComando();
			$this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
		}
	}

	/**
	 * Undocumented function
	 * @param [type] $argv
	 * @param string $user
	 * @param string $sistema
	 * @param string $enviroment
	 * @return void
	 */
	public function trabajadores_inicio_relacion($argv, $user = '1', $sistema = 'sisuweb', $enviroment = '2')
	{
		try {
			try {
				$tareas = new Tareas($this->console);
				$tareas->set_enviroment($enviroment);
				// $modelo = json_decode(base64_decode($argv), true);

				$query = DB::table('sat09')
					->select('sat09.numtraccf', 'sat09.numdocemp', 'sat20.estado', 'subsi15.cedtra', 'sat20.persiste', 'subsi15.nit', 'sat09.numdoctra')
					->join('sat20', 'sat20.numtraccf', '=', 'sat09.numtraccf')
					->leftJoin('subsi15', 'subsi15.cedtra', '=', 'sat09.numdoctra')
					->where("sat20.persiste", '0')
					->whereIn("sat20.estado", ['G', 'P', 'X']);

				Comandos::$cantidad = $query->count();
				Comandos::setProporcion();
				$fraccion = Comandos::$cantidad / Comandos::$proporcion;
				$progreso_ini_conteo = 0;

				$num = 0;
				$pendientes = array();
				foreach ($query->get() as $sat09) {
					$num++;
					$progreso_ini_conteo++;
					if ($progreso_ini_conteo >= $fraccion) {
						$progreso_ini_conteo = 0;
						$this->console->comandoProcesador->actualizaProgreso($num);
					}

					$modelo = Sat09::where('numtraccf', $sat09->numtraccf)
						->where('numdoctra', $sat09->numdoctra)
						->get()
						->first();

					$sat20 = Sat20::where('numtraccf', $sat09->numtraccf)->get()->first();
					if ($sat20->persiste != 0) continue;

					$modelo->fecnac = null;
					$modelo->coddep = '18';
					$ciu = str_pad($modelo->codmun, 3, "0", STR_PAD_LEFT);
					$codciu = "18{$ciu}";

					$ciudad = Gener08::where("codciu", $codciu)->get()->first();
					if ($ciudad) {
						// $modelo->codmun = strval(substr($ciudad->codciu, 2, 3));
					} else {
						$modelo->codmun = '001';
					}

					if (!$modelo->save()) {
						throw new DebugException("Error al guardar modelo sat09 {$sat09->numtraccf}", 1);
					}

					$param_trabajador              = new stdClass;
					$param_trabajador->numtraccf   = $modelo->numtraccf;
					$param_trabajador->post        = $modelo;
					$param_trabajador->tabla       = 'sat09';
					$resultado = $tareas->glozas_reprocesar($param_trabajador);

					$has = 0;
					if ($resultado['codigo'] == 'GN11') {
						$tabla = 'sat02';
						$empresa = Sat02::where("numdocemp", $modelo->numdocemp)->get()->first();
						if (!$empresa) {
							$tabla = 'sat03';
							$empresa = Sat03::where("numdocemp", $modelo->numdocemp)->get()->first();
						}
						$param_empresa = new stdClass;
						if ($empresa) {
							$param_empresa->numtraccf    = $empresa->numtraccf;
							$param_empresa->numdocemp    = $modelo->numdocemp;
							$param_empresa->post         = $empresa;
							$param_empresa->tabla        = $tabla;
							$tareas->glozas_reprocesar($param_empresa);
						} else {
							$param_empresa->numdocemp = $modelo->numdocemp;
							$tareas->empresa_nueva_afiliacion($param_empresa);
						}
						$tareas->glozas_reprocesar($param_trabajador);
					} else {
						$has++;
					}

					if ($has) {
						if ($resultado['estado'] == 'U' || $resultado['estado'] == 'O') {
							$params_persistencia               = new stdClass;
							$params_persistencia->numdoctra    = $sat09->numdoctra;
							$params_persistencia->numtraccf    = $modelo->numtraccf;
							$params_persistencia->tabla        = "sat09";
							$params_persistencia->tiptra       = '5';
							$tareas->glozas_persistencia($params_persistencia);
						} else {
							$pendientes[] = [
								"numdoctra" => $sat09->numdoctra,
								"numtraccf" => $modelo->numtraccf
							];
						}
					}
				}

				$salida = [
					'success' => true,
					'msj' => "Proceso completado con éxito",
					'data' => [
						"pendientes" => $pendientes
					]
				];

				$this->console->comandoProcesador->finalizarProceso();
				$this->console->info(json_encode($salida));
			} catch (QueryException $sql_err) {
				$msj = $sql_err->getMessage() . ' ' . basename($sql_err->getFile()) . ' ' . $sql_err->getLine();
				throw new DebugException($msj, 1);
			} catch (GlozaException $ge) {
				throw new DebugException($ge->getMessage(), 1);
			}
		} catch (DebugException $err) {

			$this->console->comandoProcesador->cancelarComando();
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode(),
				'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
				'estado'  => 'X'
			);
			$this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
		}
	}

	public function trabajadores_terminacion_laboral($argv, $user = '1', $sistema = 'sisuweb', $enviroment = '2')
	{
		try {
			try {

				// $obj = json_decode(base64_decode($argv), true);
				$tareas = new Tareas($this->console);
				$tareas->set_enviroment($enviroment);

				$query = DB::table('sat10')
					->select('sat10.numtraccf', 'sat10.numdocemp', 'sat20.estado', 'subsi15.cedtra', 'sat20.persiste', 'subsi15.nit', 'sat10.numdoctra')
					->join('sat20', 'sat20.numtraccf', '=', 'sat10.numtraccf')
					->leftJoin('subsi15', 'subsi15.cedtra', '=', 'sat10.numdoctra')
					->where("sat20.persiste", '0')
					->whereIn("sat20.estado", ['G'])
					->get();

				$pendientes = array();
				foreach ($query as $sat10) {

					$modelo = Sat10::where('numtraccf', $sat10->numtraccf)
						->where('numdoctra', $sat10->numdoctra)
						->get()
						->first();

					$data = (object) $modelo->makeVisible('attribute')->toArray();
					$data->numtraccf = $sat10->numtraccf;

					$params = new stdClass;
					$params->post = $data;
					$params->tabla = 'sat10';
					$rqs = $tareas->glozas_reprocesar($params);

					if ($rqs['estado'] == 'U' || $rqs['estado'] == 'O') {
						$params = (object) array(
							"numdoctra" => $sat10->numdoctra,
							"numtraccf" => $sat10->numtraccf,
							"tabla"     => "sat10",
							"tiptra"    => '5'
						);
						$tareas->glozas_persistencia($params);
					} else {
						$pendientes[] = [
							"numdoctra" => $sat10->numdoctra,
							"numtraccf" => $sat10->numtraccf
						];
					}
				}

				$salida = [
					'success' => true,
					'msj' => "Proceso trabajadores terminación laboral completado con éxito",
					'data' => [
						"pendientes" => $pendientes
					]
				];

				$this->console->info(json_encode($salida));
			} catch (QueryException $sql_err) {
				$msj = $sql_err->getMessage() . ' ' . basename($sql_err->getFile()) . ' ' . $sql_err->getLine();
				throw new DebugException($msj, 1);
			} catch (GlozaException $ge) {
				throw new DebugException($ge->getMessage(), 1);
			}
		} catch (DebugException $err) {

			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'Llnea'   => $err->getLine(),
				'codigo'  => $err->getCode(),
				'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
				'estado'  => 'X'
			);
			$this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
		}
	}

	public function trabajador_cambio_salario($argv, $user = '1', $sistema = 'sisuweb', $enviroment = '2')
	{
		try {
			try {
				$tareas = new Tareas($this->console);
				$tareas->set_enviroment($enviroment);

				$query = DB::table('sat13')
					->select('sat13.numtraccf', 'sat13.numdocemp', 'sat20.estado', 'subsi15.cedtra', 'sat20.persiste', 'subsi15.nit', 'sat13.numdoctra')
					->join('sat20', 'sat20.numtraccf', '=', 'sat13.numtraccf')
					->join('subsi15', 'subsi15.cedtra', '=', 'sat13.numdoctra')
					->where("sat20.persiste", '0')
					->where("sat20.estado", ['P', 'X', ''])
					->get();

				$pendientes = array();
				foreach ($query as $sat13) {
					$modelo = Sat13::where('numtraccf', $sat13->numtraccf)
						->where('numdoctra', $sat13->numdoctra)
						->get()
						->first();

					$data = (object) $modelo->makeVisible('attribute')->toArray();
					$data->numtraccf = $sat13->numtraccf;

					$params = new stdClass;
					$params->post = $data;
					$params->tabla = 'sat13';

					$rqs = $tareas->glozas_reprocesar($params);
					if ($rqs['estado'] == 'U' || $rqs['estado'] == 'O') {
						$params = new stdClass;
						$params->numdoctra = $sat13->numdoctra;
						$params->numtraccf = $sat13->numtraccf;
						$params->tabla  = "sat13";
						$params->tiptra = '17';
						$tareas->glozas_persistencia($params);
					} else {
						$pendientes[] = [
							"numdoctra" => $sat13->numdoctra,
							"numtraccf" => $sat13->numtraccf
						];
					}
					break;
				}
				$salida = [
					'success' => true,
					'data' => [
						"pendientes" => $pendientes
					]
				];

				$this->console->comandoProcesador->finalizarProceso(json_encode($salida));
				$this->console->info(json_encode($salida));
			} catch (QueryException $sql_err) {
				$msj = $sql_err->getMessage() . ' ' . basename($sql_err->getFile()) . ' ' . $sql_err->getLine();
				throw new DebugException($msj, 1);
			} catch (GlozaException $ge) {
				throw new DebugException($ge->getMessage(), 1);
			}
		} catch (DebugException $err) {

			$this->console->comandoProcesador->cancelarComando();
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'Llnea'   => $err->getLine(),
				'codigo'  => $err->getCode(),
				'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
				'estado'  => 'X'
			);
			$this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
		}
	}

	/**
	 * replocesar_empresas_desafiliaciones function
	 * procesar las desafiliaciones de las empresas
	 * @param [type] $argv
	 * @param string $user
	 * @param string $sistema
	 * @param string $enviroment
	 * @return void
	 */
	public function reprocesar_empresas_desafiliaciones($argv, $user = '1', $sistema = 'sisuweb', $enviroment = '2')
	{
		try {
			try {
				$tareas = new Tareas($this->console);
				$tareas->set_enviroment($enviroment);

				$query = DB::table('sat06')
					->select('sat06.numtraccf', 'sat06.numdocemp', 'sat20.estado', 'subsi02.nit', 'sat20.persiste')
					->join('sat20', 'sat20.numtraccf', '=', 'sat06.numtraccf')
					->join('subsi02', 'subsi02.nit', '=', 'sat06.numdocemp')
					->where("sat20.persiste", '0')
					->where("sat20.estado", ['P', 'X'])
					->get();

				foreach ($query as $sat06) {
					$modelo = Sat06::where('numtraccf', $sat06->numtraccf)
						->where('numdoctra', $sat06->numdoctra)
						->get()
						->first();

					$param_desafilia = new stdClass;
					$param_desafilia->numtraccf = $modelo->numtraccf;
					$param_desafilia->post = $modelo;
					$param_desafilia->tabla = 'sat06';
					$resultado = $tareas->glozas_reprocesar($param_desafilia);

					$this->console->info('--' . $modelo->numtraccf . '|' . $modelo->numdocemp . "--\n");
				}
				$salida = [
					'success' => true,
					'data' => $resultado
				];
				$this->console->comandoProcesador->finalizarProceso(json_encode($salida));
				$this->console->info(json_encode($salida));
			} catch (QueryException $sql_err) {
				$msj = $sql_err->getMessage() . ' ' . basename($sql_err->getFile()) . ' ' . $sql_err->getLine();
				throw new DebugException($msj, 1);
			} catch (GlozaException $ge) {
				throw new DebugException($ge->getMessage(), 1);
			}
		} catch (DebugException $err) {

			$this->console->comandoProcesador->cancelarComando();
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'Llnea'   => $err->getLine(),
				'codigo'  => $err->getCode(),
				'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
				'estado'  => 'X'
			);
			$this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
		}
	}

	public function empresas_reintegros($argv, $user = '1', $sistema = 'sisuweb', $enviroment = '2')
	{
		try {
			try {
				// $modelo = json_decode(base64_decode($argv), true);
				$tareas = new Tareas($this->console);
				$tareas->set_enviroment($enviroment);

				$query = DB::table('sat03')
					->select('sat03.numtraccf', 'sat20.estado', 'sat20.persiste')
					->join('sat20', 'sat20.numtraccf', '=', 'sat03.numtraccf')
					->where("sat20.persiste", '0')
					->whereIn("sat20.estado", 'G')
					->get();

				$pendientes = array();
				foreach ($query as $gloza) {

					$sat3 = Sat03::where("numtraccf", $gloza->numtraccf)->get()->first();
					$param_empresa = new stdClass;
					$param_empresa->post    = (object) $sat3->makeVisible('attribute')->toArray();
					$param_empresa->tabla   = 'sat03';
					$param_empresa->tiptra  = '2';

					$resultado = $tareas->glozas_reprocesar($param_empresa);
					if ($resultado['estado'] == 'U' || $resultado['estado'] == 'O') {
					} else {
						$pendientes[] = [
							"numdocemp" => $sat3->numdocemp,
							"numtraccf" => $sat3->numtraccf
						];
					}
				}
				$salida = [
					'success' => true,
					'msj' => "Proceso completado con éxito",
					'data' => [
						"pendientes" => $pendientes
					]
				];

				$this->console->comandoProcesador->finalizarProceso(json_encode($salida));
				$this->console->info(json_encode($salida, JSON_NUMERIC_CHECK));
			} catch (QueryException $sql_err) {
				$msj = $sql_err->getMessage() . ' ' . basename($sql_err->getFile()) . ' ' . $sql_err->getLine();
				throw new DebugException($msj, 1);
			} catch (GlozaException $ge) {
				throw new DebugException($ge->getMessage(), 1);
			}
		} catch (DebugException $err) {

			$this->console->comandoProcesador->cancelarComando();
			$salida = array(
				"Error"   => $err->getMessage(),
				'Archivo' => basename($err->getFile()),
				'Linea'   => $err->getLine(),
				'Codigo'  => $err->getCode(),
				'Data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
				'estado'  => 'X'
			);
			$this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
		}
	}


	public function edita_empresas_nombres_representante($file, $user, $sistema, $enviroment)
	{
		try {
			try {
				$archivo = "/var/www/html/clisisu/storage/files/reportes/" . $file;
				if (!file_exists($archivo)) {
					throw new DebugException("Error el archivo es requerido para procesar", 501);
				}
				$inputFileType = PHPExcel_IOFactory::identify($archivo);
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($archivo);
				$sheet = $objPHPExcel->getSheet(0);
				$highestRow = $sheet->getHighestRow();
				$sheet->getHighestColumn();

				Comandos::$cantidad = $highestRow;
				Comandos::setProporcion();
				$fraccion = Comandos::$cantidad / Comandos::$proporcion;
				$progreso_ini_conteo = 0;

				$num = 0;
				for ($row = 2; $row <= $highestRow; $row++) {
					$num++;
					$progreso_ini_conteo++;
					if ($progreso_ini_conteo >= $fraccion) {
						$progreso_ini_conteo = 0;
						$this->console->comandoProcesador->actualizaProgreso($num);
					}
					$nit = intval($sheet->getCell("A{$row}")->getValue());
					if (!$nit) continue;
					$coddoc = intval($sheet->getCell("B{$row}")->getValue());
					$repleg = $sheet->getCell("C{$row}")->getValue();

					$empresa = Subsi02::where("nit", $nit)->get()->first();
					if (!$empresa) continue;
					//if(strlen($empresa->prinomrepleg) > 0 || strlen($empresa->prinom) > 0) continue;
					if (strlen($empresa->email) == 0) continue;

					$exp = explode(" ", trim($repleg));
					if ($coddoc == '3') {
						if ($empresa->prinomrepleg != 0) {
							continue;
						}
						switch (count($exp)) {
							case 6:
							case 7:
							case 8:
								$empresa->prinomrepleg = @$exp[0];
								$empresa->segnomrepleg = @$exp[1];
								$empresa->priaperepleg = @$exp[2];
								$empresa->segaperepleg = @$exp[3] . ' ' . @$exp[4] . ' ' . @$exp[5];
								break;
							case 5:
								$empresa->prinomrepleg = @$exp[0];
								$empresa->segnomrepleg = @$exp[1];
								$empresa->priaperepleg = @$exp[2];
								$empresa->segaperepleg = @$exp[3] . ' ' . @$exp[4];
								break;
							case 4:
								$empresa->prinomrepleg = @$exp[0];
								$empresa->segnomrepleg = @$exp[1];
								$empresa->priaperepleg = @$exp[2];
								$empresa->segaperepleg = @$exp[3];
								break;
							case 3:
								$empresa->prinomrepleg = @$exp[0];
								$empresa->priaperepleg = @$exp[1] . ' ' . @$exp[2];
								break;
							case 2:
								$empresa->prinomrepleg = @$exp[0];
								$empresa->priaperepleg = @$exp[1];
								break;
							case 1:
								$empresa->prinomrepleg = @$exp[0];
								$empresa->priaperepleg = @$exp[0];
								break;
							default:
								break;
						}
					} else {
						if ($empresa->prinom != 0) {
							continue;
						}
						switch (count($exp)) {
							case 6:
							case 7:
							case 8:
								$empresa->prinom = @$exp[0];
								$empresa->segnom = @$exp[1];
								$empresa->priape = @$exp[2];
								$empresa->segape = @$exp[3] . ' ' . @$exp[4] . ' ' . @$exp[5];
								break;
							case 5:
								$empresa->prinom = @$exp[0];
								$empresa->segnom = @$exp[1];
								$empresa->priape = @$exp[2];
								$empresa->segape = @$exp[3] . ' ' . @$exp[4];
								break;
							case 4:
								$empresa->prinom = @$exp[0];
								$empresa->segnom = @$exp[1];
								$empresa->priape = @$exp[2];
								$empresa->segape = @$exp[3];
								break;
							case 3:
								$empresa->prinom = @$exp[0];
								$empresa->priape = @$exp[1] . ' ' . @$exp[2];
								break;
							case 2:
								$empresa->prinom = @$exp[0];
								$empresa->priape = @$exp[1];
								break;
							case 1:
								$empresa->prinom = @$exp[0];
								$empresa->priape = @$exp[0];
								break;
							default:
								break;
						}
					}

					$validator = $empresa->isValid($empresa->reglas_creacion());
					if ($validator === true) {
						if (!$empresa->save()) {
							throw new DebugException("Error al guardar la empresa nit", 501);
						}
					} else {
						DebugException::add("valid", $validator);
						DebugException::add("nit", $nit);
						throw new DebugException("Error en validation de la empresa", 555);
					}

					$empresaSat = new Empresa($nit);
					if (Sat02::where("numdocemp", $nit)->get()->count() == 0) {
						$empresaSat->nueva();
						$empresaSat->salvar();
						$restServiceFactoryImpl = new RestServiceFactoryImpl();
						$data = $empresaSat->prepara_data_sat();
						RestServiceFactoryImpl::setData($data);
						$rqs = $restServiceFactoryImpl->afiliacionPrimeraVezService();
					} else {
						$empresaSat->reintegro();
						$empresaSat->salvar();
						$restServiceFactoryImpl = new RestServiceFactoryImpl();
						$data = $empresaSat->prepara_data_sat();
						RestServiceFactoryImpl::setData($data);
						$rqs = $restServiceFactoryImpl->afiliacionNoPrimeraVezService();
					}
					$this->console->info(json_encode($rqs, JSON_NUMERIC_CHECK));
					sleep(2);
				}

				$this->console->comandoProcesador->finalizarProceso();
				$this->console->info('OK');
			} catch (\Exception $th) {
				throw new DebugException($th->getMessage() . ' ' . $th->getLine() . ' ' . basename($th->getFile()), 501);
			} catch (QueryException $er) {
				throw new DebugException($er->getMessage() . ' ' . $er->getLine() . ' ' . basename($er->getFile()), 501);
			} catch (GlozaException $ge) {
				throw new DebugException($ge->getMessage(), 1);
			}
		} catch (DebugException $err) {
			$this->console->comandoProcesador->cancelarComando();
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode(),
				'data'    => ($err->getCode() === 501) ? DebugException::item("valid") . ' >> ' . DebugException::item("nit") : array(),
			);
			$this->console->error(json_encode($salida));
		}
	}


	public function enviar_empresas_pendientes($procesar, $user, $sistema, $enviroment)
	{
		try {
			try {
				$sql = "SELECT nit FROM subsi02 
					WHERE estado IN('A','S','D') and nit not in(select numdocemp from sat02) and nit not in(select numdocemp from sat03);";
				$query = collect(DB::select($sql));
				Comandos::$cantidad = $query->count();
				Comandos::setProporcion();
				$fraccion = Comandos::$cantidad / Comandos::$proporcion;
				$progreso_ini_conteo = 0;
				$num = 0;

				foreach ($query as $row) {
					$num++;
					$progreso_ini_conteo++;
					if ($progreso_ini_conteo >= $fraccion) {
						$progreso_ini_conteo = 0;
						$this->console->comandoProcesador->actualizaProgreso($num);
					}
					$empresa = Subsi02::where("nit", $row->nit)->get()->first();
					if (!$empresa) continue;
					if (strlen($empresa->email) == 0) continue;

					$exp = explode(" ", trim($empresa->repleg));
					if ((strlen($empresa->prinomrepleg) == 0 || strlen($empresa->priaperepleg) == 0) && $empresa->tipper == 'J') {
						switch (count($exp)) {
							case 6:
							case 7:
							case 8:
								$empresa->prinomrepleg = @$exp[0];
								$empresa->segnomrepleg = @$exp[1];
								$empresa->priaperepleg = @$exp[2];
								$empresa->segaperepleg = @$exp[3] . ' ' . @$exp[4] . ' ' . @$exp[5];
								break;
							case 5:
								$empresa->prinomrepleg = @$exp[0];
								$empresa->segnomrepleg = @$exp[1];
								$empresa->priaperepleg = @$exp[2];
								$empresa->segaperepleg = @$exp[3] . ' ' . @$exp[4];
								break;
							case 4:
								$empresa->prinomrepleg = @$exp[0];
								$empresa->segnomrepleg = @$exp[1];
								$empresa->priaperepleg = @$exp[2];
								$empresa->segaperepleg = @$exp[3];
								break;
							case 3:
								$empresa->prinomrepleg = @$exp[0];
								$empresa->priaperepleg = @$exp[1] . ' ' . @$exp[2];
								break;
							case 2:
								$empresa->prinomrepleg = @$exp[0];
								$empresa->priaperepleg = @$exp[1];
								break;
							case 1:
								$empresa->prinomrepleg = @$exp[0];
								$empresa->priaperepleg = @$exp[0];
								break;
							default:
								break;
						}
					}

					if ((strlen($empresa->prinom) == 0 || strlen($empresa->priape) == 0) && $empresa->tipper == 'N') {
						switch (count($exp)) {
							case 6:
							case 7:
							case 8:
								$empresa->prinom = @$exp[0];
								$empresa->segnom = @$exp[1];
								$empresa->priape = @$exp[2];
								$empresa->segape = @$exp[3] . ' ' . @$exp[4] . ' ' . @$exp[5];
								break;
							case 5:
								$empresa->prinom = @$exp[0];
								$empresa->segnom = @$exp[1];
								$empresa->priape = @$exp[2];
								$empresa->segape = @$exp[3] . ' ' . @$exp[4];
								break;
							case 4:
								$empresa->prinom = @$exp[0];
								$empresa->segnom = @$exp[1];
								$empresa->priape = @$exp[2];
								$empresa->segape = @$exp[3];
								break;
							case 3:
								$empresa->prinom = @$exp[0];
								$empresa->priape = @$exp[1] . ' ' . @$exp[2];
								break;
							case 2:
								$empresa->prinom = @$exp[0];
								$empresa->priape = @$exp[1];
								break;
							case 1:
								$empresa->prinom = @$exp[0];
								$empresa->priape = @$exp[0];
								break;
							default:
								break;
						}
					}

					$validator = $empresa->isValid($empresa->reglas_creacion());
					if ($validator === true) {
						if (!$empresa->save()) {
							throw new DebugException("Error al guardar la empresa nit", 501);
						}
					} else {
						DebugException::add("valid", $validator);
						DebugException::add("nit", $empresa->nit);
						throw new DebugException("Error en validation de la empresa", 555);
					}

					$empresaSat = new Empresa($empresa->nit);
					if (Sat02::where("numdocemp", $empresa->nit)->get()->count() == 0) {
						$empresaSat->nueva();
						$empresaSat->salvar();
						$restServiceFactoryImpl = new RestServiceFactoryImpl();
						$data = $empresaSat->prepara_data_sat();
						RestServiceFactoryImpl::setData($data);
						$rqs = $restServiceFactoryImpl->afiliacionPrimeraVezService();
					} else {
						$empresaSat->reintegro();
						$empresaSat->salvar();
						$restServiceFactoryImpl = new RestServiceFactoryImpl();
						$data = $empresaSat->prepara_data_sat();
						RestServiceFactoryImpl::setData($data);
						$rqs = $restServiceFactoryImpl->afiliacionNoPrimeraVezService();
					}
					$rqs['nit'] = $empresa->nit;
					$this->console->info(json_encode($rqs, JSON_NUMERIC_CHECK));
					sleep(2);
				}

				$this->console->comandoProcesador->finalizarProceso();
				$this->console->info('OK');
			} catch (\Exception $th) {
				throw new DebugException($th->getMessage() . ' ' . $th->getLine() . ' ' . basename($th->getFile()), 501);
			} catch (QueryException $er) {
				throw new DebugException($er->getMessage() . ' ' . $er->getLine() . ' ' . basename($er->getFile()), 501);
			} catch (GlozaException $ge) {
				throw new DebugException($ge->getMessage(), 1);
			}
		} catch (DebugException $err) {
			$this->console->comandoProcesador->cancelarComando();
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode(),
				'data'    => ($err->getCode() === 501) ? DebugException::item("valid") . ' >> ' . DebugException::item("nit") : array(),
			);
			$this->console->error(json_encode($salida));
		}
	}


	/**
	 * reprocesar_empresas_reintegros function
	 * @property commando p7 artisan server:send Reprocesos reprocesar_empresas_nuevas true 1 SYS 1
	 * @param [type] $argv
	 * @param string $user
	 * @param string $sistema
	 * @param string $enviroment
	 * @return void
	 */
	public function reprocesar_empresas_reintegros($argv, $user = '1', $sistema = 'sisuweb', $enviroment = '2')
	{
		try {
			try {
				$tareas = new Tareas($this->console);
				$tareas->set_enviroment($enviroment);

				$this->usuario 	= $user;
				$glozas = Sat20::where("tiptra", '2')->where('estado', 'G')->where('persiste', '0');

				Comandos::$cantidad = $glozas->count();
				Comandos::setProporcion();
				$fraccion = Comandos::$cantidad / Comandos::$proporcion;
				$progreso_ini_conteo = 0;

				$num = 0;
				foreach ($glozas->get() as $gloza) {
					$num++;
					$progreso_ini_conteo++;
					if ($progreso_ini_conteo >= $fraccion) {
						$progreso_ini_conteo = 0;
						$this->console->comandoProcesador->actualizaProgreso($num);
					}

					$sat3 = Sat03::where("numtraccf", $gloza->numtraccf)->get()->first();
					if (!$sat3) {
						Sat20::where("numtraccf", $gloza->numtraccf)->delete();
						continue;
					}

					$params = new stdClass;
					$params->numdocemp = $sat3->numdocemp;
					$resultado = $tareas->empresa_segunda_afiliacion($params);
					Sat20::where('numtraccf', $gloza->numtraccf)->update(['persiste' => '1']);

					if ($resultado['codigo'] == 'GN09') {
						Sat20::where('numtraccf', $resultado['data']['numtraccf'])->update(['persiste' => '1']);
						$resultado = $tareas->empresa_nueva_afiliacion($params);
					}
					$this->console->info("nit: {$resultado['data']['numdocemp']} | {$resultado['data']['numtraccf']} | {$resultado['codigo']}");
				}

				$this->console->comandoProcesador->finalizarProceso();
			} catch (QueryException $sql_err) {
				$msj = $sql_err->getMessage() . ' ' . basename($sql_err->getFile()) . ' ' . $sql_err->getLine();
				throw new DebugException($msj, 1);
			} catch (GlozaException $ge) {
				throw new DebugException($ge->getMessage(), 1);
			}
		} catch (DebugException $err) {

			$this->console->comandoProcesador->cancelarComando();
			$salida = array(
				"Error"   => $err->getMessage(),
				'Archivo' => basename($err->getFile()),
				'Linea'   => $err->getLine(),
				'Codigo'  => $err->getCode(),
				'Data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
				'estado'  => 'X'
			);
			$this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
		}
	}

	/**
	 * reprocesar_empresa_aportes function
	 * @property commando p7 artisan server:send Reprocesos reprocesar_empresa_aportes true 1 SYS 1
	 * @param [type] $argv
	 * @param string $user
	 * @param string $sistema
	 * @param string $enviroment
	 * @return void
	 */
	public function reprocesar_empresa_aportes($argv, $user = '1', $sistema = 'sisuweb', $enviroment = '2')
	{
		try {
			try {
				$tareas = new Tareas($this->console);
				$tareas->set_enviroment($enviroment);

				$this->usuario 	= $user;
				$glozas = Sat20::where("tiptra", '7')->where('estado', 'P')->where('persiste', '0');

				Comandos::$cantidad = $glozas->count();
				Comandos::setProporcion();
				$fraccion = Comandos::$cantidad / Comandos::$proporcion;
				$progreso_ini_conteo = 0;

				$num = 0;
				foreach ($glozas->get() as $gloza) {
					$num++;
					$progreso_ini_conteo++;
					if ($progreso_ini_conteo >= $fraccion) {
						$progreso_ini_conteo = 0;
						$this->console->comandoProcesador->actualizaProgreso($num);
					}

					$sat = Sat15::where("numtraccf", $gloza->numtraccf)->get()->first();
					if (!$sat) {
						Sat20::where("numtraccf", $gloza->numtraccf)->where("tiptra", '7')->delete();
						continue;
					}

					$resultado = $tareas->estado_pago_aportes($sat->numdocemp, $sat->estpag);
					Sat20::where('numtraccf', $gloza->numtraccf)->update(['persiste' => '1']);

					$this->console->info("nit: {$resultado['data']['numdocemp']} | {$resultado['data']['numtraccf']} | {$resultado['codigo']}");
				}

				$this->console->comandoProcesador->finalizarProceso();
			} catch (QueryException $sql_err) {
				$msj = $sql_err->getMessage() . ' ' . basename($sql_err->getFile()) . ' ' . $sql_err->getLine();
				throw new DebugException($msj, 1);
			} catch (GlozaException $ge) {
				throw new DebugException($ge->getMessage(), 1);
			}
		} catch (DebugException $err) {

			$this->console->comandoProcesador->cancelarComando();
			$salida = array(
				"Error"   => $err->getMessage(),
				'Archivo' => basename($err->getFile()),
				'Linea'   => $err->getLine(),
				'Codigo'  => $err->getCode(),
				'Data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
				'estado'  => 'X'
			);
			$this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
		}
	}
}
