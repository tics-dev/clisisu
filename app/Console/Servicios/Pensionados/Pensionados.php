<?php
use Illuminate\Support\Facades\DB;
use App\Console\Servicios\ServicioSat\WsSat;
use App\Console\Servicios\ServicioSat\Empresa;
use App\Console\Servicios\ServicioSat\Trabajador;
use App\Exceptions\DebugException;
use App\Models\Sat20;
use App\Models\Gener08;
use App\Models\Sat01;
use App\Models\Sat02;
use App\Models\Sat03;
use App\Models\Subsi02;
use App\Models\Sat09;
use GrahamCampbell\ResultType\Success;


class Pensionados extends WsSat
{
    protected $console;
    public function __construct($console)
    {
        $this->console = $console;
    }

    /**
     * procesa_trabajador function
     * p7 artisan server:send Pensionados procesa_trabajador '1110491952' 1 sisuweb
     * @param [type] $argv
     * @return void
     */
    public function procesa_trabajador($argv, $user='1', $sistema='sisuweb', $enviroment='2')
    {
        try {
            $this->set_enviroment($enviroment);
            $param = explode(';', $argv);
            $cedtra = $param[0]; 
            $trabajador = new Trabajador($cedtra);

            $trabajador->iniciar_pensionados();            
            
            $trabajador->salvar();
            $data = $trabajador->prepara_data_sat();
            $this->principal($data, $trabajador->sat->getTable());
            $this->console->info(json_encode($this->resultado, JSON_NUMERIC_CHECK));
        } catch (DebugException $err)
        {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555)? DebugException::item("valid") : array(),
                'success' => false
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    /**
     * termina_trabajador function
     * termina relacion laboral trabajador pensionado 
     * @param [type] $argv
     * @param string $user
     * @param string $sistema
     * @param string $enviroment
     * @return void
     */
    public function termina_trabajador($argv, $user='1', $sistema='sisuweb', $enviroment='2')
    {
        try {
            $this->set_enviroment($enviroment);
            $param = explode(';', $argv);
            $cedtra = $param[0]; 
            $trabajador = new Trabajador($cedtra);

            $trabajador->terminar_pensionados();            
            
            $trabajador->salvar();
            $data = $trabajador->prepara_data_sat();
            $this->principal($data, $trabajador->sat->getTable());
            $this->console->info(json_encode($this->resultado, JSON_NUMERIC_CHECK));
        } catch (DebugException $err)
        {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555)? DebugException::item("valid") : array(),
                'success' => false
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }
}