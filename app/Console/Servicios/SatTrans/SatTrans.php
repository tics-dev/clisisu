<?php
use App\Console\Servicios\ServicioSat\Tareas;
use Illuminate\Support\Facades\DB;
use App\Exceptions\DebugException;
use GrahamCampbell\ResultType\Success;
use Illuminate\Database\QueryException;

use App\Models\Sat01;
use App\Models\Sat02;
use App\Models\Sat03;
use App\Models\Sat06;
use App\Models\Sat09;
use App\Models\Sat10;
use App\Models\Sat13;
use App\Models\Sat18;

class SatTrans
{
    protected $console;
    public function __construct($console)
    {
        $this->console = $console;
    }

    public function guardar_solicitud($argv, $user='1', $sistema='sisuweb', $enviroment='2')
    {
        try {
            try{  
                $jd = json_decode(base64_decode($argv));
                $notificacion = Sat18::where("numero_transaccion", $jd->numero_transaccion)->get()->first();
                if($notificacion) {
                    throw new DebugException("Error el número de transacción ya fue reportado previamente.", 501);
                }
                $notificacion = new Sat18();
                $notificacion->numero_transaccion = $jd->numero_transaccion;  
                $notificacion->codigo_novedad   = $jd->codigo_novedad;
                $notificacion->fecha_creacion   = $jd->fecha_creacion;
                $notificacion->fecha_vigencia   = $jd->fecha_vigencia;
                $notificacion->estado_flujo     = $jd->Estado_Fujo;
                $notificacion->url              = $jd->url;
                $notificacion->estado           = 'A';
                $notificacion->save();

                $rqs = [
                    "success"=> true,
                    "msj"=> "Notificación guardada con éxito",
                    "data"=> $notificacion->toArray()
                ];
                $this->console->info(json_encode($rqs, JSON_NUMERIC_CHECK));
            } catch (QueryException $sql_err)
            {
                $msj = $sql_err->getMessage().' '.basename($sql_err->getFile()).' '.$sql_err->getLine();
                throw new DebugException($msj, 1);
            }
        } catch (DebugException $err)
        {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555)? DebugException::item("valid") : array(),
                'estado'  => 'X',
                'success' => false
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    
}
