<?php

use App\Exceptions\DebugException;
use Illuminate\Database\QueryException;
use App\Models\Gener02;
use App\Models\Gener21;

class Usuarios
{
    protected $fecha;
    protected $numtraccf;
    protected $usuario;
    protected $rules;
    public $sat;

    protected $console;
    public function __construct($console)
    {
        $this->console = $console;
        $this->fecha = date('Y-m-d');
    }

    public function trae_usuario($usuario, $user = '1', $sistema = 'sisuweb', $enviroment = '2')
    {
        try {
            try {
                $usuario = Gener02::where("usuario", $usuario)
                    ->whereIn("estado", ['A', 'B'])
                    ->get()
                    ->first();

                $rol = Gener21::where("tipfun", $usuario->tipfun)
                    ->get()
                    ->first();

                if (!$rol) {
                    throw new DebugException("Error, el rol no esta configurado de forma correcta en sisuweb.", 405);
                } else {
                    $usuario->tipfun_detalle = $rol->detalle;
                    $resultado = [
                        'code'    => 200,
                        'success' => true,
                        'data'    => $usuario
                    ];
                    $this->console->info(json_encode($resultado, JSON_NUMERIC_CHECK));
                }
            } catch (QueryException $th) {
                throw new Exception($th->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'estado'  => 'X',
                'success' => false
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    public function confirma_politica($usuario, $user = '1', $sistema = 'sisuweb', $enviroment = '2')
    {
        try {
            try {
                $usuario = Gener02::where("usuario", $usuario)
                    ->whereIn("estado", ['A', 'B'])
                    ->get()
                    ->first();
                $usuario->confirma_politica = '1';
                $usuario->save();
                $resultado = [
                    'code'    => 200,
                    'success' => true,
                    'data'    => $usuario
                ];
                $this->console->info(json_encode($resultado, JSON_NUMERIC_CHECK));
            } catch (QueryException $th) {
                throw new Exception($th->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'estado'  => 'X',
                'success' => false
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    public function resetear_intentos($usuario, $user = '1', $sistema = 'sisuweb', $enviroment = '2')
    {
        try {
            try {
                $update_at = date('Y-m-d H:i:s');
                $usuario = Gener02::where("usuario", $usuario)
                    ->whereIn("estado", ['A', 'B'])
                    ->get()
                    ->first();

                $usuario->confirma_politica = '1';
                $usuario->estado = 'A';
                $usuario->intentos = '0';
                $usuario->update_at = $update_at;
                $usuario->save();

                $resultado = [
                    'code'    => 200,
                    'success' => true,
                    'data'    => $usuario
                ];
                $this->console->info(json_encode($resultado, JSON_NUMERIC_CHECK));
            } catch (QueryException $th) {
                throw new Exception($th->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'estado'  => 'X',
                'success' => false
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    public function cargar_intentos($usuario, $user = '1', $sistema = 'sisuweb', $enviroment = '2')
    {
        try {
            try {
                $update_at = date('Y-m-d H:i:s');
                $usuario = Gener02::where("usuario", $usuario)
                    ->whereIn("estado", ['A', 'B'])
                    ->get()
                    ->first();

                $msj = "";
                if ($usuario) {
                    $intentos = $usuario->intentos + 1;
                    if ($intentos >= 3) {
                        $usuario->confirma_politica = '1';
                        $usuario->estado = 'B';
                        $usuario->intentos = '3';
                        $usuario->update_at = $update_at;
                        $usuario->save();
                        $msj = "El usuario se ha bloqueado, por fallar en la autenticación con más de 3 intentos.";
                    } else {
                        $msj = "Intento de ingreso fallido reportado.";
                        $usuario->intentos = $intentos;
                        $usuario->update_at = $update_at;
                        $usuario->save();
                    }
                }
                $resultado = [
                    'success' => true,
                    'code'  => 200,
                    'data'  => $usuario,
                    'msj'   => $msj
                ];
                $this->console->info(json_encode($resultado, JSON_NUMERIC_CHECK));
            } catch (QueryException $th) {
                throw new Exception($th->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'estado'  => 'X',
                'success' => false
            );
            $this->console->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }
}
