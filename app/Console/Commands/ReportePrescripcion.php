<?php
namespace App\Console\Commands;

use Illuminate\Database\DebugException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use App\Models\PrescripcionCuota;

use Illuminate\Console\Command;

class ReportePrescripcion extends Command 
{
    protected $signature = 'reporte_prescripcion:send {periodo} {criterio} {sistema} {comando=0}';
    protected $description = 'Reporte de la prescripcion saldos por pagar';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->procesar();
    }

    /**
     * procesar function
     * permite procesar reporte antes de prescribir
     * @return void
     */
    public function procesar()
    {
        $periodo = (int) $this->argument('periodo');  
        $criterio = (int) $this->argument('criterio');
        $sistema = (string) $this->argument('sistema');
        $tipo = "PP";
        $fecha_prescripcion = substr($periodo, 0, 4)."-".substr($periodo, 4, 2)."-01";

        $archivo = "reporte_prescripcion_{$periodo}_{$criterio}_{$tipo}.csv";
        $filepath = "/var/www/html/{$sistema}/public/temp/".$archivo;
        $file = fopen($filepath, "w+");

        try {   
            $prescripciones = PrescripcionCuota::where("periodo", $periodo)
            ->where("estado", "AD")
            ->where("tipo", $tipo);

            switch ($criterio)
            {
                case '1':
                    $prescripciones = $prescripciones->where("valor_descontar", "<", 5000)
                    ->orderByDesc("valor_descontar")
                    ->get();
                    break;
                case '2':
                    $prescripciones = $prescripciones->where("valor_descontar", "=>", 5000)
                    ->where("valor_descontar", "<=", 30000)
                    ->orderByDesc("valor_descontar")
                    ->get();
                    break;
                case '3':
                    $prescripciones = $prescripciones->where("valor_descontar", ">", 30000)
                    ->where("valor_descontar", "<=", 100000)
                    ->orderByDesc("valor_descontar")
                    ->get();
                    break;
                case '4':
                    $prescripciones = $prescripciones->where("valor_descontar", ">", 100000)
                    ->orderByDesc("valor_descontar")
                    ->get();
                    break;
                case '5':
                    $prescripciones = $prescripciones->where("valor_descontar", ">", 0)
                    ->orderByDesc("valor_descontar")
                    ->get();
                    break;
                default: 
                    throw new \Exception("Error no debe continuar si no se pasa rango.", 1);
                break;
            }
            
            if(!$prescripciones)
            {
                $this->error('Display error: La prescripcón no está disponible para el periodo '.$periodo);
                return false;
            }
            $alista = array();
            $t=0;
            $i=0;
    
            $header = array(
                "Corte",
                "Valor",
                "Cedula",
                "Corte",
                "Valor",
                "Cedula",
                "Corte",
                "Valor",
                "Cedula"
            );
            $texto_write = implode(';', $header);
            fwrite($file, $texto_write."\n"); 
            
            foreach ($prescripciones as $row => $prescripcion) 
            {                
                $sql = "SELECT cedres, sum(saldo) as saldo_actual 
                FROM saldos 
                WHERE estado='A' AND cedres='{$prescripcion->cedres}' GROUP BY 1";
                $rqsaldos = DB::select($sql);

                if(!$rqsaldos)
                {
                    $this->info('Display: saldos null '.$prescripcion->cedres);
                    continue;
                }
                $saldos = collect($rqsaldos)->first();
                if($saldos->saldo_actual == 0)
                {
                    $this->info('Display: saldo cero '.$prescripcion->cedres);
                    continue;
                }

                $sql = "SELECT cedres, sum(saldo) as 'valor_prescribir' 
                FROM saldos 
                WHERE fecha < '{$fecha_prescripcion}' AND estado='A' AND cedres='{$prescripcion->cedres}' 
                GROUP BY 1";
                $rqs2 = collect(DB::select($sql))->first();
                $valor_prescribir = (isset($rqs2->valor_prescribir))? intval($rqs2->valor_prescribir): 0;

                if($valor_prescribir == 0) continue;

                $i++;
                $alista[$i] = ($prescripcion->periodo - 1).";".$valor_prescribir.";".$prescripcion->cedres;
                if($i == 3)
                {
                    $texto_write = implode(';', $alista);
                    fwrite($file, $texto_write."\n");
                    $alista = array();
                    $i=0;
                }
                $t++;
            }
            if(count($alista) > 0)
            {
                $texto_write = implode(';', $alista);
                fwrite($file, $texto_write."\n");
            }
            fclose($file);
            $salida = array(200, $t, 'Proceso terminado con éxito', $filepath);
        } catch (\Throwable $err)
        {
            fclose($file);
            $salida = array($err->getCode(), 0, $err->getMessage(), 0);
        }
        $this->info(implode('|', $salida));
    }

}