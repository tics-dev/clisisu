<?php
namespace App\Console\Commands;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use App\Exceptions\DebugException;
use Illuminate\Database\QueryException;
use Illuminate\Console\Command;

class ReporteDobleAfiliacion extends Command
{

    protected $signature = 'afiliados_dobles:send {parent} {estado} {comando=0}';
    protected $description = 'Comando de reporte de afiliados doble vinculación';
    protected $ruta_archivo;
    protected $archivo;

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->principal();
    }

    public function principal()
    {
        ini_set('memory_limit', '2000M');
        try {
            $parent = "".$this->argument('parent');
            $estado = "".$this->argument('estado');

            $data = DB::table('subsi22')
            ->selectRaw('subsi22.documento, COUNT(*) as has ')
            ->join('subsi23', 'subsi23.codben', '=', 'subsi22.codben')
            ->join('subsi15', 'subsi15.cedtra', '=', 'subsi23.cedtra')
            ->where('subsi22.parent', $parent)
            ->where('subsi15.estado', $estado)
            ->groupBy('subsi22.documento')
            ->havingRaw('has > ?', [1])
            ->get();

            $listado = [];
            foreach ($data as $beneficiario)
            {
                $listado[] = $beneficiario->documento;
            }

            $file_novedad = "reporte_padres_doble_afiliacion_".date('Ymd').".csv";
            $archivo = storage_path('files/reportes/' . $file_novedad);

            if(file_exists($archivo)) unlink($archivo);

            $file = fopen($archivo, "a+");
            fwrite($file, strtoupper("Documento;Cedula Trabajador;Nombre;Estado trabajador;Giro;Codigo Beneficiario\r\n"));

            $data = DB::table('subsi22')
            ->selectRaw("
                subsi22.documento,  
                subsi23.cedtra, 
                CONCAT_WS(' ',subsi22.prinom, subsi22.segnom, subsi22.priape, subsi22.segape) as 'nombre_beneficiario',
                subsi15.estado as 'estado_trabajador', 
                subsi22.giro as 'recibe_giro', 
                subsi22.codben"
            )
            ->join('subsi23', 'subsi23.codben', '=', 'subsi22.codben')
            ->join('subsi15', 'subsi15.cedtra', '=', 'subsi23.cedtra')
            ->where('subsi22.parent', $parent)
            ->where('subsi15.estado', $estado)
            ->whereIn("subsi22.documento", $listado)
            ->orderBy('subsi22.documento', 'ASC')
            ->get();

            foreach ($data as $beneficiario) {
                $texto_escribir = array(
                    $beneficiario->documento,
                    $beneficiario->cedtra,
                    $beneficiario->nombre_beneficiario,
                    $beneficiario->estado_trabajador,
                    $beneficiario->recibe_giro,
                    $beneficiario->codben
                );
                fwrite($file, implode(";", $texto_escribir)."\r\n");
            }
            fclose($file);
            $salida = array(
                'success' => true, 
                'msj'=> "El reporte se proceso de forma correcta",
                'filename'=> basename($file_novedad),
                'path'=> storage_path('files/reportes')
            );
        } catch (QueryException $erro){

            throw new DebugException($erro->getMessage(), 202);
        
        } catch (DebugException $err) {
            $salida = array(
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode(),
				'success' => false
			);
        }
        $this->info(json_encode($salida, JSON_NUMERIC_CHECK));
    }

}   