<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Console\Servicios\Comandos\ComandoProcesador;

class ProcesoComandos extends Command
{
    protected $signature = 'server:send {clase} {metodo} {params=0} {user=1} {sistema=SYS} {env=1} {comando=0}';

    protected $description = 'Comando de servicios';
    protected $command;

    public $comandoProcesador;

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->command = $this->argument('comando');
        /**
         * sistema default SYS
         */
        $this->comandoProcesador = new ComandoProcesador($this->argument('sistema'), $this->argument('user'));

        if (is_null($this->command) == False && $this->command > 0) {
            $this->comandoProcesador->buscarComando($this->command, '');
        } else {
            $this->comandoProcesador->buscarComando(
                false,
                $this->argument('clase') . ' ' . $this->argument('metodo') . ' ' . $this->argument('params')
            );
        }

        $clase = $this->argument('clase');
        require_once base_path("app/Console/Servicios/{$clase}/{$clase}.php");
        $servicio = new $clase($this);
        $argumentos = array(
            $this->argument('params'),
            $this->argument('user'),
            $this->argument('sistema'),
            $this->argument('env')
        );
        call_user_func_array(array($servicio, $this->argument('metodo')), $argumentos);
    }
}
