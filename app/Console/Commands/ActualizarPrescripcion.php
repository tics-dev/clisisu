<?php
namespace App\Console\Commands;

use App\Exceptions\DebugException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use App\Models\PrescripcionCuota;


use Illuminate\Console\Command;

class ActualizarPrescripcion extends Command
{

    protected $signature = 'actualizar_prescripcion:send {periodo} {cedtra} {estado} {todo} {comando=0}';
    protected $description = 'Comando para actualizar la prescripcion';
    protected $ruta_archivo;
    protected $archivo;
    public static $procesados = array();

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->actualizar();
    }

    /**
     * actualizar function
     * permite actualizar las prescripciones de cuotas monetarios tipo (TA) de tarjetas
     * @return void
     */
    public function actualizar()
    {
        $periodo = "".$this->argument('periodo');
        $cedtra = "".$this->argument('cedtra');
        $estado = "".$this->argument('estado');
        $todo = $this->argument('todo');

        try {
            
            if($cedtra == '1'){
                $prescripciones = PrescripcionCuota::where("periodo", $periodo)
                ->where("estado", $estado)
                ->where("tipo", "TA")
                ->get();
            }else{
                $prescripciones = PrescripcionCuota::where("periodo", $periodo)
                ->where("tipo", "TA")
                ->where("estado", $estado)
                ->where("cedres", $cedtra)
                ->get();
            }
            
            if(!$prescripciones)
            {
                throw new DebugException('La prescripcón no está disponible para el periodo '.$periodo, 502);                
            }
            
            $i=0;
            foreach ($prescripciones as $row => $prescripcion) 
            {
                //si ya se encuentra procesada no debe hacer nada con la prescripcion
                if($prescripcion->estado == "PR" || $prescripcion->estado == "DP") continue;

                $fecha_prescripcion = substr($prescripcion->periodo, 0, 4)."-".substr($prescripcion->periodo, 4, 2)."-01";

                $sql ="SELECT asopa03.cedres, IF(count(asopa03.numtar) > 1, asopa07.saldo, sum(asopa07.saldo)) as saldo_actual 
                FROM asopa03 
                LEFT JOIN asopa07 ON asopa07.numtar = asopa03.numtar 
                WHERE asopa03.cedres='{$prescripcion->cedres}' GROUP BY 1";

                $rqs = collect(DB::select($sql))->first();

                $saldo_calculado = intval($prescripcion->valor_prescribir + $prescripcion->valor_noprescribe - $prescripcion->redimido);
                
                //si el saldo actual de asopagos es diferente al saldo calculado desde la ultima prescripcion
                //se actualiza el valor redimido  
                if($todo == true || !($rqs->saldo_actual == $saldo_calculado))
                {
                    $sql = "SELECT asopa07.numtar, asopa07.saldo
                    FROM asopa03 
                    LEFT JOIN asopa07 ON asopa07.numtar=asopa03.numtar
                    WHERE asopa03.cedres='{$prescripcion->cedres}' AND asopa03.numtar > 0";
                    $rqs1 = DB::select($sql);

                    $valor_redimido=0;
                    $valor_prescribir=0;
                    $valor_noprescribe=0;
                    foreach ($rqs1 as $aj => $as07) 
                    {
                        $sql= "SELECT sum(asopa06.valor) as 'valor_prescribir' 
                        FROM asopa06 
                        WHERE asopa06.numtar='{$as07->numtar}' and asopa06.fecha < '{$fecha_prescripcion}' AND asopa06.descri LIKE '%ABONO%'";
                        $rqs2 = collect(DB::select($sql))->first();
                        $valor_prescribir+= ($rqs2->valor_prescribir)? intval($rqs2->valor_prescribir): 0;

                        $sql= "SELECT sum(asopa06.valor) as 'valor_noprescribe' 
                        FROM asopa06 
                        WHERE asopa06.numtar='{$as07->numtar}' and asopa06.fecha >= '{$fecha_prescripcion}' AND asopa06.descri LIKE '%ABONO%'";
                        $rqs2 = collect(DB::select($sql))->first();
                        $valor_noprescribe+= ($rqs2->valor_noprescribe)? intval($rqs2->valor_noprescribe): 0;

                        $sql= "SELECT sum(asopa06.valor) as 'valor_redimido' 
                        FROM asopa06 
                        WHERE asopa06.numtar='{$as07->numtar}' AND asopa06.descri NOT LIKE '%ABONO%'";                    
                        $rqs2 = collect(DB::select($sql))->first();
                        $valor_redimido+= ($rqs2->valor_redimido)? intval($rqs2->valor_redimido): 0;
                    }
                    $valor_descontar = $valor_prescribir - $valor_redimido; 
                    $saldo_calculado = intval($valor_prescribir + $valor_noprescribe - $valor_redimido);

        
                    if($valor_descontar <= 0 && $rqs->saldo_actual == $saldo_calculado)
                    {
                        PrescripcionCuota::where("id", $prescripcion->id)
                        ->update([
                            "saldo_actual" => $rqs->saldo_actual, 
                            "fecha_redime" => date('Y-m-d'),
                            "estado" => "RD"
                        ]);
                        self::$procesados[$prescripcion->cedres]="RD";
                    } else {
                        $estado = ($rqs->saldo_actual != $saldo_calculado)? "PE" : $prescripcion->estado;
                        PrescripcionCuota::where("id", $prescripcion->id)
                        ->update([
                            "saldo_actual" => $rqs->saldo_actual, 
                            "estado" => $estado, 
                            "redimido"=> $valor_redimido,
                            "valor_prescribir"=> $valor_prescribir,
                            "valor_noprescribe"=> $valor_noprescribe,
                            "valor_descontar"=> ($valor_descontar > 0)? $valor_descontar : 0  
                        ]);
                        self::$procesados[$prescripcion->cedres]= $estado;
                    }
                }else{
                    if($prescripcion->valor_descontar == 0)
                    {
                        PrescripcionCuota::where("id", $prescripcion->id)
                        ->update([
                            "saldo_actual" => $rqs->saldo_actual, 
                            "estado" => "RD"
                        ]);
                        $this->info("RD|".$prescripcion->cedres);
                    }else{
                        PrescripcionCuota::where("id", $prescripcion->id)
                        ->update([
                            "estado" => $prescripcion->estado, 
                            "saldo_actual" => $rqs->saldo_actual
                        ]);
                        self::$procesados[$prescripcion->cedres]= $prescripcion->estado;
                    }
                }
                $i++;
            }
            $salida = [
                "data" => self::$procesados,
                "success" => true,
                "msj" => "El proceso se ha completado con éxito"
            ];
            $this->info(json_encode($salida));
        } catch (DebugException $err)
		{
			$salida = array(
				"success" => false,
				"error"   => $err->getMessage(),
				'archivo' => basename($err->getFile()),
				'linea'   => $err->getLine(),
				'codigo'  => $err->getCode()
			);
			$this->error(json_encode($salida));
		}
    }

}   