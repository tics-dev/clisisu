<?php
namespace App\Console\Commands;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use App\Models\PrescripcionCuota;

use Illuminate\Console\Command;

class ActualizarPresPorPagar extends Command 
{
    protected $signature = 'actualizar_prescripcion_porpagar:send {periodo} {cedres} {estado} {todo} {comando=0}';
    protected $description = 'Comando para actualizar la prescripcion por pagar';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->actualizar();
        $this->info('Display lista: Proceso terminado con éxito');
    }

    /**
     * actualizar function
     * permite actualizar las prescripciones de saldos por pagar
     * @return void
     */
    public function actualizar()
    {
        $periodo = $this->argument('periodo');  
        $cedres = $this->argument('cedres');  
        $estado = "".$this->argument('estado');
        $todo = $this->argument('todo');
        
        if($cedres == '1')
        {
            $prescripciones = PrescripcionCuota::where("periodo", $periodo)
            ->where("tipo", "PP")
            ->get();
        }else{
            $prescripciones = PrescripcionCuota::where("periodo", $periodo)
            ->where("tipo", "PP")
            ->where("cedres", $cedres)
            ->get();
        }
        
        if(!$prescripciones)
        {
            $this->error('Display error: La prescripcón no está disponible para el periodo '.$periodo);
            return false;
        }
        
        foreach ($prescripciones as $row => $prescripcion) 
        {
            //si ya se encuentra procesada no debe hacer nada con la prescripcion
            if($prescripcion->estado == "PR" || $prescripcion->estado == "RD" || $prescripcion->estado == "DP") continue;
            $fecha_prescripcion = substr($prescripcion->periodo, 0, 4)."-".substr($prescripcion->periodo, 4, 2)."-01";
            
            $sql = "SELECT cedres, sum(saldo) as saldo_actual 
            FROM saldos 
            WHERE estado='A' AND cedres='{$prescripcion->cedres}' GROUP BY 1";
            $rqsaldos = DB::select($sql);

            if(!$rqsaldos)
            {
                $prescripcion->estado = "RD";
                $prescripcion->save();
                $this->info('Display: saldos null '.$prescripcion->cedres);
                continue;
            }
            $saldos = collect($rqsaldos)->first();
            if($saldos->saldo_actual == 0)
            {
                $prescripcion->estado = "RD";
                $prescripcion->save();
                $this->info('Display: saldo cero '.$prescripcion->cedres);
                continue;
            }

            $valor_prescribir = 0;
            $valor_noprescribe = 0;

            $sql = "SELECT cedres, sum(saldo) as 'valor_prescribir' 
            FROM saldos 
            WHERE fecha < '{$fecha_prescripcion}' AND estado='A' AND cedres='{$prescripcion->cedres}' 
            GROUP BY 1";
            $rqs2 = collect(DB::select($sql))->first();
            $valor_prescribir+= (isset($rqs2->valor_prescribir))? intval($rqs2->valor_prescribir): 0;

            $sql = "SELECT cedres, sum(saldo) as 'valor_noprescribe' 
            FROM saldos 
            WHERE fecha >= '{$fecha_prescripcion}' AND estado='A' AND cedres='{$prescripcion->cedres}'  
            GROUP BY 1";
            $rqs3 = collect(DB::select($sql))->first();
            $valor_noprescribe+= (isset($rqs3->valor_noprescribe))? intval($rqs3->valor_noprescribe): 0;
            
            if($valor_prescribir == 0)
            {
                $prescripcion->estado = "RD";
                $prescripcion->save();
                $this->info('Display: recalcular saldo cero'.$prescripcion->cedres);
                continue;
            }

            if($valor_prescribir != $prescripcion->valor_prescribir){
                $this->info('Display: saldos diferentes '.$prescripcion->cedres);
            }

            $prescripcion->valor_prescribir = $valor_prescribir;
            $prescripcion->valor_noprescribe = $valor_noprescribe;
            $prescripcion->redimido = 0;
            $prescripcion->valor_descontar = $prescripcion->valor_prescribir;
            $prescripcion->saldo_actual = $saldos->saldo_actual;
            $prescripcion->save();
        }
    }

    /**
     * actualizar function
     * permite actualizar las prescripciones de saldos por pagar
     * @return void
     */
    public function actualizar_daviplata()
    {
        $periodo = $this->argument('periodo');  
        $prescripciones = PrescripcionCuota::where("periodo", $periodo)
        ->where("tipo", "PP")
        ->get();
        
        if(!$prescripciones)
        {
            $this->error('Display error: La prescripcón no está disponible para el periodo '.$periodo);
            return false;
        }
        
        foreach ($prescripciones as $row => $prescripcion) 
        {
            //si ya se encuentra procesada no debe hacer nada con la prescripcion
            if($prescripcion->estado == "DP")
            {
                $fecha_prescripcion = substr($prescripcion->periodo, 0, 4)."-".substr($prescripcion->periodo, 4, 2)."-01";
            
                $sql = "SELECT cedres, sum(saldo) as saldo_actual 
                FROM saldos 
                WHERE estado='A' AND cedres='{$prescripcion->cedres}' GROUP BY 1";
                $rqsaldos = DB::select($sql);

                if(!$rqsaldos)
                {
                    $prescripcion->estado = "RD";
                    $prescripcion->save();
                    $this->info('Display: saldos null '.$prescripcion->cedres);
                    continue;
                }
                $saldos = collect($rqsaldos)->first();
                if($saldos->saldo_actual == 0)
                {
                    $prescripcion->estado = "RD";
                    $prescripcion->save();
                    $this->info('Display: saldo cero '.$prescripcion->cedres);
                    continue;
                }

                $valor_prescribir = 0;
                $valor_noprescribe = 0;

                $sql = "SELECT cedres, sum(saldo) as 'valor_prescribir' 
                FROM saldos 
                WHERE fecha < '{$fecha_prescripcion}' AND estado='A' AND cedres='{$prescripcion->cedres}' 
                GROUP BY 1";
                $rqs2 = collect(DB::select($sql))->first();
                $valor_prescribir+= (isset($rqs2->valor_prescribir))? intval($rqs2->valor_prescribir): 0;

                $sql = "SELECT cedres, sum(saldo) as 'valor_noprescribe' 
                FROM saldos 
                WHERE fecha >= '{$fecha_prescripcion}' AND estado='A' AND cedres='{$prescripcion->cedres}'  
                GROUP BY 1";
                $rqs3 = collect(DB::select($sql))->first();
                $valor_noprescribe+= (isset($rqs3->valor_noprescribe))? intval($rqs3->valor_noprescribe): 0;
                
                if($valor_prescribir == 0)
                {
                    $prescripcion->estado = "RD";
                    $prescripcion->save();
                    $this->info('Display: recalcular saldo cero'.$prescripcion->cedres);
                    continue;
                }

                if($valor_prescribir != $prescripcion->valor_prescribir)
                {
                    $this->info('Display: saldos diferentes '.$prescripcion->cedres);
                }

                $prescripcion->valor_prescribir = $valor_prescribir;
                $prescripcion->valor_noprescribe = $valor_noprescribe;
                $prescripcion->redimido = 0;
                $prescripcion->valor_descontar = $prescripcion->valor_prescribir;
                $prescripcion->saldo_actual = $saldos->saldo_actual;
                $prescripcion->save();
            }
        }
    }    

}