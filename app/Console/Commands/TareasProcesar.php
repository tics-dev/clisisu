<?php
namespace App\Console\Commands;

use App\Exceptions\DebugException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use App\Models\ComfacaComando;
use App\Models\Comando;
use App\Models\ComfacaComandoEstructura;
use App\Models\ComandoEstructura;
use Illuminate\Console\Command;
use PhpParser\ErrorHandler\Throwing;

class TareasProcesar extends Command
{

    protected $signature = 'tareas:send {sistema}';
    protected $description = 'Comando de prescripcion';

    private $listaComandos;
    private $procesador;
    private $linea_comando;
    private $proceso;
    private $numero_multitareas = 10;
    private $default = 'SYS';
    private static $factory;
    private $timeStop = 180; // detener por 3 minutos

    private $factories = [
		'SYS' => [
            'comando' => ComfacaComando::class,
            'estructura' => ComfacaComandoEstructura::class
        ],
		'Mercurio' => [
            'comando' => Comando::class,
            'estructura' => ComandoEstructura::class

        ]
	];

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        try {
            $this->make();
            $this->validaTareasPrevias();
            $this->consultarComandos();
            $this->procesarComandos();

        } catch (\Throwable $th) {
            $this->error("\n--- " . $th->getMessage());    
        }
        $this->info("\n--- Proceso terminado con éxito");
    }

    public function make()
	{
		self::$factory = $this->factories[$this->argument('sistema')] ?? $this->default;
	}

    public function consultarComandos()
    {
        $estructura = array();
        $comandoEstructura = self::$factory['estructura']::where("asyncro", '1')->get();
        foreach ($comandoEstructura as $estr) {
            $estructura[] = $estr->id;
        }
        $this->listaComandos = self::$factory['comando']::where("estado", 'P')->whereIn('estructura', $estructura)->get();
    }

    public function procesarComandos()
    {
        foreach ($this->listaComandos as $itemComando)
        {
            $unixComando = strtotime($itemComando->fecha_runner.' '.$itemComando->hora_runner);
            $unixActual  = strtotime('now');
            if($unixComando > $unixActual) continue;
            $itemComando->estado = 'E';
            $itemComando->save();
            $estructura = self::$factory['estructura']::where("id", $itemComando->estructura)->get()->first();
            $this->procesador =  $estructura->procesador;
            $this->linea_comando = $itemComando->linea_comando;
            $this->runnerComando($itemComando);
        }
    }

    /**
     * runnerComando function
     * correr el comando por consola en segundo plano de ejecución
     * @param [type] $itemComando
     * @return void
     */
    public function runnerComando($itemComando)
    {
        $comando = "{$this->procesador} {$this->linea_comando}";
        if($this->validaRunnerPrevio($comando))
        {
            //agregar comodin & proceso en segundo plano
            $proceso = shell_exec("{$comando} > /dev/null 2>&1 & echo $!");
            //buscra el codigo del proceso
            $itemComando->proceso = trim($proceso);
            $itemComando->save();
            $this->info($proceso);
        }
    }

    /**
     * validaTareasPrevias function
     * Busca que no se esten ejecutando tareas de server, para que no se reprocese la misma información.
     * @return void
    */
    public function validaTareasPrevias()
    {
        $flag = True;
        do {
            $lineas = array();
            exec("ps aux | grep 'p7 artisan tareas:send'", $lineas);
            $has = 0;
            foreach ($lineas as $line) {
                if(strpos($line, 'grep') === false) $has++;
            }
            // ver más de un mismo proceso corriendo
            if($has >= $this->numero_multitareas){
                sleep($this->timeStop);
            } else {
                // no hay nada corriendo en el momento puede continuar
                $flag = False;
            }
        } while ($flag == True);
        return true;
    }

    /**
     * validaRunnerPrevio function
     * valida que el mismo comando no este corriendo al mismo tiempo
     * @return void
     */
    public function validaRunnerPrevio($_comando)
    {
        $lineas = array();
        exec("ps aux | grep '{$_comando}'", $lineas);
        $has = 0;
        foreach ($lineas as $line) {
            if(strpos($line, 'grep') === false) $has++;
        }
        // ver más de un mismo proceso corriendo
        return ($has == 0) ? True : False;
    }

}

