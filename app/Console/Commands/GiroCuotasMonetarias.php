<?php
namespace App\Console\Commands;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use App\Models\Subsi02;
use App\Models\Subsi73;
use App\Models\Subsi48;
use App\Models\Subsi09;
use App\Models\Subsi20;
use App\Models\Subsi15;
use App\Models\Subsi18;
use App\Models\Subsi19;


class GiroCuotasMonetarias extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'giro_cuotas_monetarias:send {periodo} {comando=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->buscar_responsables_cuota_daviplata();
        $this->info('Display lista: Proceso terminado con éxito');
    }

    public function buscar_sucursal()
	{
		$empresa = Subsi02::where("nit",'=','891190047')->get()->first();
        $sucursales = $empresa->getSucursales();
		foreach ($sucursales as $sucursal) 
		{
			$this->info('Display :'.$sucursal->detalle);
		}
	}

	public function buscar_empresa()
	{
		$empresa =  Subsi48::where("nit",'=','891190047')->where("codsuc",'=','001')->get()->first()->empresa;
		$this->info('Display sucursal:'.$empresa->razsoc);
	}

	public function buscar_lista()
	{
		$empresa =  Subsi73::where("nit",'=','891190047')->where("codlis",'=','001')->get()->first()->empresa;
		$this->info('Display lista:'.$empresa->razsoc);
	}

    public function buscar_responsables_cuota_daviplata()
    {
        ini_set('memory_limit','1300M');
        $periodo = "".$this->argument('periodo');

        $filepath = base_path("storage/files/")."responsables_reciben_cuota.csv";
        $file = fopen("{$filepath}", "w+");

        $historicos = DB::table('subsi09')
        ->select('periodo','pergir','tippag','cedres','pago','numcue')
        ->where("periodo",'=',$periodo)
        ->where('pergir','=',$periodo)
        ->where('tippag','=','D')
        ->get();

        $texto_write = implode(',', array(
            "Cedula responsable", 
            "Número cuenta"
        ));
        fwrite($file, $texto_write."\n");
        
        foreach ($historicos as $ai => $historico)
        {
            switch ($historico->pago)
            {
                case 'C':
                    $responsable = Subsi20::where('cedcon', $historico->cedres)
                    ->where('tippag','=','D')
                    ->get()
                    ->first();
                    break;
                case 'T':
                    $responsable = Subsi15::where('cedtra', $historico->cedres)
                    ->where('tippag','=','D')
                    ->get()
                    ->first();
                    break;
                case 'N':
                    $responsable = Subsi18::where('cedtra', $historico->cedres)
                    ->where('tippag','=','D')
                    ->get()
                    ->first();
                    if(!$responsable)
                    {
                        $responsable = Subsi15::where('cedtra', $historico->cedres)
                        ->where('tippag','=','D')
                        ->get()
                        ->first();
                    }
                    if(!$responsable)
                    {
                        $responsable = Subsi20::where('cedcon', $historico->cedres)
                        ->where('tippag','=','D')
                        ->get()
                        ->first();
                    }
                    break;
                case 'D':
                    $responsable = Subsi19::where('cedtra', $historico->cedres)
                    ->where('tippag','=','D')
                    ->get()
                    ->first();
                    if(!$responsable)
                    {
                        $responsable = Subsi20::where('cedcon', $historico->cedres)
                        ->where('tippag','=','D')
                        ->get()
                        ->first();
                    }
                    if(!$responsable)
                    {
                        $responsable = Subsi15::where('cedtra', $historico->cedres)
                        ->where('tippag','=','D')
                        ->get()
                        ->first();
                    }
                    break;
                default:
                    $responsable = false;
                break;
            }
            if($responsable)
            {
                $texto_write = implode(',', array(
                    $historico->cedres,
                    $responsable->numcue
                ));
                fwrite($file, $texto_write."\n");
                $this->info($texto_write);
            }
        }
        fclose($file);
    }
}
