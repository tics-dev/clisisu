<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Exceptions\DebugException;
use Illuminate\Database\QueryException;
use App\Models\Subsi02;
use App\Models\Subsi15;
use App\Models\Subsi20;
use App\Models\Subsi21;
use App\Models\Subsi22;
use App\Models\Subsi23;

class ResponsablesGiro extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'responsables_giro:send {sistema} {empresa} {comando=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Responsables de recibir la cuota monetaria';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $empresa = $this->argument('empresa');
        if($empresa > 0)
        {
            $this->empresa_puntual($empresa);
        }else{
            $this->principal();
        }
        
    }

    public function principal()
	{
        ini_set('memory_limit', '2000M');
        $sistema = $this->argument('sistema'); 
        $filepath = "/var/www/html/".$sistema."/public/temp/reporte_responsables_de_recibir_cuota_".date('y-m-d')."_.csv";
        $file = fopen("{$filepath}", "w+");
        $texto_write = [
            "Nit",
            "Nombre trabajador",
            "Cedula trabajador",
            "Nombre responsable",
            "Cedula responsable",
            "Nombre conyugue",
            "Cedula conyugue",
            "Parentesco con trabajador",
            "Telefonos",
            "Email"
        ];
        $texto_write = implode(';', $texto_write);
        fwrite($file, strtoupper($texto_write)."\t\n");

		try {
            try {
                $trabajadores = Subsi15::where("estado",'A')
                ->get();

                $responsables = [];
                foreach ($trabajadores as $trabajador)
                {
                    $relaciones = Subsi23::where("cedtra", $trabajador->cedtra)->get();

                    $has = 0;
                    $parentesco = '';
                    if($trabajador->giro == 'S')
                    {
                        if(!in_array($trabajador->cedtra, $responsables))
                        {
                            if($relaciones)
                            {
                                foreach ($relaciones as $relacion)
                                {
                                    //posee beneficiarios padres
                                    $beneficiarios_padres_hermanos = Subsi22::where("codben", $relacion->codben)
                                    ->where('giro', 'S')
                                    ->whereIn('parent', ['2', '3'])
                                    ->get()
                                    ->first();
                                    if($beneficiarios_padres_hermanos)
                                    {
                                        $has++;
                                        $parentesco = ($beneficiarios_padres_hermanos->parent == '3')? 'Padres' : "Hermanos";
                                        break;
                                    }
                                }
                            }
                            if($has)
                            {
                                $responsables[]=$trabajador->cedtra; 
                                //agregar el trabajador
                                $texto = [
                                    $trabajador->nit,
                                    $trabajador->prinom.' '.$trabajador->segnom.' '.$trabajador->priape.' '.$trabajador->segape,
                                    $trabajador->cedtra,
                                    $trabajador->prinom.' '.$trabajador->segnom.' '.$trabajador->priape.' '.$trabajador->segape,
                                    $trabajador->cedtra,
                                    '0',
                                    '0',
                                    $parentesco,
                                    $trabajador->telefono,
                                    $trabajador->email
                                ];
                                $texto_write = implode(';', $texto);
                                fwrite($file, strtoupper($texto_write)."\t\n");
                            }
                        }
                    }

                    //posee beneficiarios hijos
                    //si el responsable de recibir el giro por los hijos es el conyugue
                    $conyugues = Subsi21::where("cedtra", $trabajador->cedtra)
                    ->get();
                    
                    if($conyugues)
                    {
                        foreach ($conyugues as $conyugue_relation)
                        {
                            if(!in_array($conyugue_relation->cedcon, $responsables))
                            {
                                $conyugue = Subsi20::where("cedcon", $conyugue_relation->cedcon)
                                ->get()
                                ->first();
                                
                                $parentesco = '';
                                $has = 0;
                                if($relaciones)
                                {
                                    foreach ($relaciones as $relacion)
                                    {
                                        if($relacion->cedcon == $conyugue->cedcon)
                                        {
                                            //posee beneficiarios hijos recibe el conyugue
                                            $beneficiarios_hijos = Subsi22::where("codben", $relacion->codben)
                                            ->where('giro', 'S')
                                            ->where('parent', '1')
                                            ->get()
                                            ->first();
                                            if($beneficiarios_hijos)
                                            {
                                                $parentesco = $beneficiarios_hijos->parent;
                                                $has++;
                                                break;
                                            }
                                        }
                                    }
                                }
                                if($has){
                                    $responsables[]=$conyugue->cedcon;
                                    //agregar la conyugue
                                    $texto_write = [
                                        $trabajador->nit,
                                        $trabajador->prinom.' '.$trabajador->segnom.' '.$trabajador->priape.' '.$trabajador->segape,
                                        $trabajador->cedtra,
                                        $conyugue->prinom.' '.$trabajador->segnom.' '.$trabajador->priape.' '.$trabajador->segape,
                                        $conyugue->cedcon,
                                        $conyugue->prinom.' '.$trabajador->segnom.' '.$trabajador->priape.' '.$trabajador->segape,
                                        $conyugue->cedcon,
                                        'Hijos',
                                        $conyugue->telefono,
                                        $conyugue->email
                                    ];
                                    $texto_write = implode(';', $texto_write);
                                    fwrite($file, strtoupper($texto_write)."\t\n");
                                }
                            }
                        }
                    }else{
                        //si el responsable de recibir el giro por los hijos es el trabajador
                        if(!in_array($trabajador->cedtra, $responsables))
                        {
                            if($has == 0)
                            {
                                $has == 0;
                                $parentesco = '';
                                if($trabajador->giro == 'S')
                                {
                                    foreach ($relaciones as $relacion)
                                    {
                                        if($relacion->cedtra == $trabajador->cedtra)
                                        {
                                            //posee beneficiarios hijos recibe el trabajador
                                            $beneficiarios_hijos = Subsi22::where("codben", $relacion->codben)
                                            ->where('giro', 'S')
                                            ->where('parent', '1')
                                            ->get()
                                            ->count();
                                            if($beneficiarios_hijos > 0)
                                            {
                                                $parentesco = $beneficiarios_hijos->parent;
                                                $has++;
                                                break;
                                            }
                                        }
                                    }
                                }
                                if($has)
                                {
                                    //responsable trabajador
                                    $responsables[] = $trabajador->cedtra;
                                    $texto_write = [
                                        $trabajador->nit,
                                        $trabajador->prinom.' '.$trabajador->segnom.' '.$trabajador->priape.' '.$trabajador->segape,
                                        $trabajador->cedtra,
                                        $trabajador->prinom.' '.$trabajador->segnom.' '.$trabajador->priape.' '.$trabajador->segape,
                                        $trabajador->cedtra,
                                        '0',
                                        '0',
                                        "Hijos",
                                        $trabajador->telefono,
                                        $trabajador->email
                                    ];
                                    $texto_write = implode(';', $texto_write);
                                    fwrite($file, strtoupper($texto_write)."\t\n");
                                }
                            }
                        }
                    }
                }

                fclose($file);
                $this->info("OK finalizado el proceso");
            } catch (QueryException $sql_err)
            {
                $msj = $sql_err->getMessage().' '.basename($sql_err->getFile()).' '.$sql_err->getLine();
                throw new DebugException($msj, 1);
            }
        } catch (DebugException $err)
        {
            fclose($file);
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555)? DebugException::item("valid") : array(),
                'estado'  => 'X'
            );
            $this->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
	}


    public function empresa_puntual($nit)
    {
        ini_set('memory_limit', '2000M');
        $sistema = $this->argument('sistema'); 
        $filepath = "/var/www/html/".$sistema."/public/temp/reporte_responsables_de_recibir_cuota_empresa".date('y-m-d')."_.csv";
        $file = fopen("{$filepath}", "w+");
        $texto_write = [
            "Nit",
            "Nombre trabajador",
            "Cedula trabajador",
            "Nombre responsable",
            "Cedula responsable",
            "Nombre conyugue",
            "Cedula conyugue",
            "Parentesco con trabajador",
            "Telefonos",
            "Email"
        ];
        $texto_write = implode(';', $texto_write);
        fwrite($file, strtoupper($texto_write)."\t\n");

		try {
            try {
                $trabajadores = Subsi15::where("estado",'A')
                ->where("nit", $nit)
                ->get();

                $responsables = [];
                foreach ($trabajadores as $trabajador)
                {
                    $relaciones = Subsi23::where("cedtra", $trabajador->cedtra)->get();

                    $has = 0;
                    $parentesco = '';
                    if($trabajador->giro == 'S')
                    {
                        if(!in_array($trabajador->cedtra, $responsables))
                        {
                            if($relaciones)
                            {
                                foreach ($relaciones as $relacion)
                                {
                                    //posee beneficiarios padres
                                    $beneficiarios_padres_hermanos = Subsi22::where("codben", $relacion->codben)
                                    ->where('giro', 'S')
                                    ->whereIn('parent', ['2', '3'])
                                    ->get()
                                    ->first();
                                    if($beneficiarios_padres_hermanos)
                                    {
                                        $has++;
                                        $parentesco = ($beneficiarios_padres_hermanos->parent == '3')? 'Padres' : "Hermanos";
                                        break;
                                    }
                                }
                            }
                            if($has)
                            {
                                $responsables[]=$trabajador->cedtra; 
                                //agregar el trabajador
                                $texto = [
                                    $trabajador->nit,
                                    $trabajador->prinom.' '.$trabajador->segnom.' '.$trabajador->priape.' '.$trabajador->segape,
                                    $trabajador->cedtra,
                                    $trabajador->prinom.' '.$trabajador->segnom.' '.$trabajador->priape.' '.$trabajador->segape,
                                    $trabajador->cedtra,
                                    '0',
                                    '0',
                                    $parentesco,
                                    $trabajador->telefono,
                                    $trabajador->email
                                ];
                                $texto_write = implode(';', $texto);
                                fwrite($file, strtoupper($texto_write)."\t\n");
                            }
                        }
                    }

                    //posee beneficiarios hijos
                    //si el responsable de recibir el giro por los hijos es el conyugue
                    $conyugues = Subsi21::where("cedtra", $trabajador->cedtra)
                    ->get();
                    
                    if($conyugues)
                    {
                        foreach ($conyugues as $conyugue_relation)
                        {
                            if(!in_array($conyugue_relation->cedcon, $responsables))
                            {
                                $conyugue = Subsi20::where("cedcon", $conyugue_relation->cedcon)
                                ->get()
                                ->first();
                                
                                $parentesco = '';
                                $has = 0;
                                if($relaciones)
                                {
                                    foreach ($relaciones as $relacion)
                                    {
                                        if($relacion->cedcon == $conyugue->cedcon)
                                        {
                                            //posee beneficiarios hijos recibe el conyugue
                                            $beneficiarios_hijos = Subsi22::where("codben", $relacion->codben)
                                            ->where('giro', 'S')
                                            ->where('parent', '1')
                                            ->get()
                                            ->first();
                                            if($beneficiarios_hijos)
                                            {
                                                $parentesco = $beneficiarios_hijos->parent;
                                                $has++;
                                                break;
                                            }
                                        }
                                    }
                                }
                                if($has){
                                    $responsables[]=$conyugue->cedcon;
                                    //agregar la conyugue
                                    $texto_write = [
                                        $trabajador->nit,
                                        $trabajador->prinom.' '.$trabajador->segnom.' '.$trabajador->priape.' '.$trabajador->segape,
                                        $trabajador->cedtra,
                                        $conyugue->prinom.' '.$trabajador->segnom.' '.$trabajador->priape.' '.$trabajador->segape,
                                        $conyugue->cedcon,
                                        $conyugue->prinom.' '.$trabajador->segnom.' '.$trabajador->priape.' '.$trabajador->segape,
                                        $conyugue->cedcon,
                                        'Hijos',
                                        $conyugue->telefono,
                                        $conyugue->email
                                    ];
                                    $texto_write = implode(';', $texto_write);
                                    fwrite($file, strtoupper($texto_write)."\t\n");
                                }
                            }
                        }
                    }else{
                        //si el responsable de recibir el giro por los hijos es el trabajador
                        if(!in_array($trabajador->cedtra, $responsables))
                        {
                            if($has == 0)
                            {
                                $has == 0;
                                $parentesco = '';
                                if($trabajador->giro == 'S')
                                {
                                    foreach ($relaciones as $relacion)
                                    {
                                        if($relacion->cedtra == $trabajador->cedtra)
                                        {
                                            //posee beneficiarios hijos recibe el trabajador
                                            $beneficiarios_hijos = Subsi22::where("codben", $relacion->codben)
                                            ->where('giro', 'S')
                                            ->where('parent', '1')
                                            ->get()
                                            ->count();
                                            if($beneficiarios_hijos > 0)
                                            {
                                                $parentesco = $beneficiarios_hijos->parent;
                                                $has++;
                                                break;
                                            }
                                        }
                                    }
                                }
                                if($has)
                                {
                                    //responsable trabajador
                                    $responsables[] = $trabajador->cedtra;
                                    $texto_write = [
                                        $trabajador->nit,
                                        $trabajador->prinom.' '.$trabajador->segnom.' '.$trabajador->priape.' '.$trabajador->segape,
                                        $trabajador->cedtra,
                                        $trabajador->prinom.' '.$trabajador->segnom.' '.$trabajador->priape.' '.$trabajador->segape,
                                        $trabajador->cedtra,
                                        '0',
                                        '0',
                                        "Hijos",
                                        $trabajador->telefono,
                                        $trabajador->email
                                    ];
                                    $texto_write = implode(';', $texto_write);
                                    fwrite($file, strtoupper($texto_write)."\t\n");
                                }
                            }
                        }
                    }
                }

                fclose($file);
                $this->info("OK finalizado el proceso");
            } catch (QueryException $sql_err)
            {
                $msj = $sql_err->getMessage().' '.basename($sql_err->getFile()).' '.$sql_err->getLine();
                throw new DebugException($msj, 1);
            }
        } catch (DebugException $err)
        {
            fclose($file);
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555)? DebugException::item("valid") : array(),
                'estado'  => 'X'
            );
            $this->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
	}
}
