<?php
namespace App\Console\Commands;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use App\Models\PrescripcionCuota;
use App\Models\Saldos;


use Illuminate\Console\Command;

class PrescribirPorpagar extends Command
{

    protected $signature = 'prescribir_porpagar:send {fecha_corte} {sistema} {comando=0}';
    protected $description = 'Comando de prescripcion saldos por pagar';
    protected $ruta_archivo;
    protected $archivo;

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $fecha_corte = "".$this->argument('fecha_corte');
        $fe = collect(DB::select("SELECT DATE_SUB('{$fecha_corte}', INTERVAL 3 YEAR) as fecha"))->first();
        $fecha_prescripcion = $fe->fecha;
        $periodo_prescripcion = substr($fecha_prescripcion, 0, 4)."".substr($fecha_prescripcion, 5, 2);

        if($this->prescripcion_vigente($periodo_prescripcion) == 0)
        {
            $this->principal($periodo_prescripcion, $fecha_prescripcion);
            $this->info('Display lista: Proceso terminado con éxito');
        }else{
            $this->info('Display nota: Hay una prescripción vigente en '.$periodo_prescripcion.' no se puede generar otra.');
        }
    }

    public function principal($periodo_prescripcion, $fecha_prescripcion)
    {
        $this->set_ruta_archivo("re_prescribir_porpagar_detalle_{$periodo_prescripcion}.csv");
        $this->crear_reporte([
            'Corte',
            'Cedula', 
            'Abonos_prescriben', 
            'Abonos_noprescriben', 
            'Redimido',
            'Saldo',
            'Prescripcion'
        ]);

        $sql = "SELECT cedres, sum(saldo) as saldo_actual FROM saldos WHERE estado='A' GROUP BY 1";
        $rqs = DB::select($sql);
        $i=0;
        foreach ($rqs as $ai => $row)
        {
            $cedres = $row->cedres;
            $valor_prescribir =0;
            $valor_noprescribe =0;

            $prescripcion = PrescripcionCuota::where("cedres",$cedres)
            ->where("periodo", $periodo_prescripcion)
            ->where("tipo","PP")
            ->get()
            ->first();
            
            if(!$prescripcion){
                $prescripcion = new PrescripcionCuota();
                $prescripcion->fecha_sistema = date('Y-m-d H:i:s');
                $prescripcion->numtar = '0';
                $prescripcion->saldo_actual = $row->saldo_actual;
                $prescripcion->cedres = $cedres;
                $prescripcion->periodo = $periodo_prescripcion;
                $prescripcion->estado = 'AD';
            } else {
                //si ya fue procesada la prescripcion no requiere más acciones
                if($prescripcion->estado == "PR") continue;
                $prescripcion->saldo_actual = $row->saldo_actual;
                $prescripcion->fecha_sistema = date('Y-m-d H:i:s');
            }
            $prescripcion->tipo = "PP";
            
            //todo lo menor a la fecha de corte prescribe
            $sql = "SELECT cedres, sum(saldo) as 'valor_prescribir' 
            FROM saldos 
            WHERE fecha < '{$fecha_prescripcion}' AND estado='A' AND cedres='{$row->cedres}' 
            GROUP BY 1";
            $rqs2 = collect(DB::select($sql))->first();
            $valor_prescribir+= (isset($rqs2->valor_prescribir))? intval($rqs2->valor_prescribir): 0;

            //todo lo mayor igual a la fecha de corte noprescribe
            $sql = "SELECT cedres, sum(saldo) as 'valor_noprescribe' 
            FROM saldos 
            WHERE fecha >= '{$fecha_prescripcion}' AND estado='A' AND cedres='{$row->cedres}' 
            GROUP BY 1";
            $rqs3 = collect(DB::select($sql))->first();
            $valor_noprescribe+= (isset($rqs3->valor_noprescribe))? intval($rqs3->valor_noprescribe): 0;
            
            if($valor_prescribir == 0) continue;

            $prescripcion->valor_prescribir = $valor_prescribir;
            $prescripcion->valor_noprescribe = $valor_noprescribe;
            $prescripcion->redimido = 0;
            $prescripcion->valor_descontar = $prescripcion->valor_prescribir;

            $this->escribir([
                $prescripcion->periodo,
                $prescripcion->cedres,
                $prescripcion->valor_prescribir,
                $prescripcion->valor_noprescribe,
                $prescripcion->redimido,
                $prescripcion->numtar,
                $prescripcion->saldo_actual,
                $prescripcion->valor_descontar
            ]);
            $prescripcion->save();
            $this->info('Display: cedtra::'.$cedres.' prescribe::'.$prescripcion->valor_descontar);
            $prescripcion =null;
            $i++;
        }
        $this->cerrar_reporte();
        Cache::flush();
    }

    public function set_ruta_archivo($archivo)
    {
        $sistema = $this->argument('sistema');
        $this->ruta_archivo = "/var/www/html/{$sistema}/public/temp/".$archivo;
    }

    public function crear_reporte($headers = array())
    {
        if(is_null($this->ruta_archivo)) $this->error('Display error: la ruta del archivo no está definida');
        if(is_null($this->archivo))
        {
            $this->archivo = fopen("{$this->ruta_archivo}", "w+");
            if($headers){
                $texto_write = implode(';', $headers);
                fwrite($this->archivo, $texto_write."\r\n");
            }
        }else{
            $this->ruta_archivo;
        }
    }

    public function cerrar_reporte()
    {
        fclose($this->archivo);
    }

    public function escribir($row)
    {
        foreach($row as $ai => $valor)
        {
            if(strpos($row[$ai], "\n") !== false){
                $row[$ai] = "'".$row[$ai]."'";
            }
            if(strpos($row[$ai], "\t") !== false){
                $row[$ai] = "'".$row[$ai]."'";
            }
            $row[$ai] = (empty($row[$ai]))? "\"\"": str_replace(";",',', $row[$ai]);
        }
        $values = array_values($row);
        $texto_write = implode(';', $values);
        fwrite($this->archivo, $texto_write."\r\n");
    }

    /**
     * prescripcion_vigente function
     * valida si hay una prescripcion vigente en caso tal no se puede generar una nueva 
     * @return void
     */
    public function prescripcion_vigente($periodo)
    {
        $coun = PrescripcionCuota::where("estado","<>","PR")
        ->where("tipo","PP")
        ->where("periodo", $periodo)
        ->count();
        return $coun;
    }

}   