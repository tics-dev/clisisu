<?php
namespace App\Console\Commands;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use App\Models\PrescripcionCuota;
use App\Models\Asopa03;
use App\Models\Asopa05;
use App\Models\Asopa06;
use App\Models\Asopa07;
use App\Models\Saldos;


use Illuminate\Console\Command;

class Prescribir extends Command
{

    protected $signature = 'prescribir:send {fecha_corte} {sistema} {comando=0}';
    protected $description = 'Comando de prescripcion';
    protected $ruta_archivo;
    protected $archivo;

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->principal();
        $this->info('Display lista: Proceso terminado con éxito');
    }

    public function principal()
    {
        $fecha_corte = "".$this->argument('fecha_corte');
        $fe = collect(DB::select("SELECT DATE_SUB('{$fecha_corte}', INTERVAL 3 YEAR) as fecha"))->first();
        $fecha_prescripcion = $fe->fecha;
        $periodo_prescripcion = substr($fecha_prescripcion, 0, 4)."".substr($fecha_prescripcion, 5, 2);

        $this->set_ruta_archivo("re_prescribir_detalle_{$periodo_prescripcion}.csv");
        $this->crear_reporte([
            'Corte',
            'Cedula',  
            'Abonos_prescriben', 
            'Abonos_noprescriben', 
            'Redimido',
            'Tajetas', 
            'Saldo',
            'Prescripcion'
        ]);

        $sql ="SELECT asopa03.cedres, sum(asopa07.saldo) as saldo_actual 
        FROM asopa03
        LEFT JOIN asopa07 ON asopa07.numtar = asopa03.numtar 
        WHERE asopa07.saldo > 0 AND asopa03.numtar > 0 GROUP BY 1";

        $rqs = DB::select($sql);
        $i=0;
        foreach ($rqs as $ai => $row)
        {
            $cedres = $row->cedres;
            $valor_prescribir =0;
            $valor_noprescribe =0;
            $valor_redimido =0;

            $sql = "SELECT asopa07.numtar, asopa07.saldo
            FROM asopa03 
            LEFT JOIN asopa07 ON asopa07.numtar=asopa03.numtar
            WHERE asopa03.cedres='{$cedres}' AND asopa03.numtar > 0";
            $rqs1 = DB::select($sql);
            if(!$rqs1) continue;

            $prescripcion = PrescripcionCuota::where("cedres",'=',$cedres)
            ->where("periodo",'=', $periodo_prescripcion)
            ->where("tipo",'=', "TA")
            ->get()
            ->first();
            
            if(!$prescripcion){
                $prescripcion = new PrescripcionCuota();
                $prescripcion->fecha_sistema = date('Y-m-d H:i:s');
                $prescripcion->numtar = '1';
                $prescripcion->saldo_actual = $row->saldo_actual;
                $prescripcion->cedres = $cedres;
                $prescripcion->periodo = $periodo_prescripcion;
                $prescripcion->estado = 'AD';
            }else{
                //si ya se encuentra procesado no requiere de más acciones
                if($prescripcion->estado == "PR") continue;
                $prescripcion->saldo_actual = $row->saldo_actual;
                $prescripcion->fecha_sistema = date('Y-m-d H:i:s');
            }
            $prescripcion->tipo = 'TA';

            $nt=0;
            foreach ($rqs1 as $aj => $as07) 
            {
                //valor abonados dentro de la fecha de prescripcion por movimientos de tarjeta abonos
                $sql= "SELECT sum(asopa06.valor) as 'valor_prescribir' 
                FROM asopa06 
                WHERE asopa06.numtar='{$as07->numtar}' and asopa06.fecha < '{$fecha_prescripcion}' AND asopa06.descri LIKE '%ABONO%'";                    
                $rqs2 = collect(DB::select($sql))->first();
                $valor_prescribir+= ($rqs2->valor_prescribir)? intval($rqs2->valor_prescribir): 0;
                
                //valor abonados posterior a la fecha de prescripcion
                $sql= "SELECT sum(asopa06.valor) as 'valor_noprescribe' 
                FROM asopa06 
                WHERE asopa06.numtar='{$as07->numtar}' and asopa06.fecha >= '{$fecha_prescripcion}' AND asopa06.descri LIKE '%ABONO%'";                    
                $rqs2 = collect(DB::select($sql))->first();
                $valor_noprescribe+= ($rqs2->valor_noprescribe)? intval($rqs2->valor_noprescribe): 0;

                //valor redimido por compras y cargos
                $sql= "SELECT sum(asopa06.valor) as 'valor_redimido' 
                FROM asopa06 
                WHERE asopa06.numtar='{$as07->numtar}' AND asopa06.descri NOT LIKE '%ABONO%'";                    
                $rqs2 = collect(DB::select($sql))->first();
                $valor_redimido+= ($rqs2->valor_redimido)? intval($rqs2->valor_redimido): 0;
                
                $rqs2 = null;
                $sql = null;
                $nt++;
            }
            if($valor_prescribir == 0) continue;

            $prescripcion->valor_prescribir = $valor_prescribir;
            $prescripcion->numtar = $nt;
            $prescripcion->valor_noprescribe = $valor_noprescribe;
            $prescripcion->redimido = $valor_redimido;


            if($prescripcion->redimido > $prescripcion->valor_prescribir)
            {
                $prescripcion->valor_descontar = 0;
            }else{
                if($prescripcion->redimido > 0)
                {
                    $prescripcion->valor_descontar = $prescripcion->valor_prescribir - $prescripcion->redimido; 
                }else{
                    $prescripcion->valor_descontar = $prescripcion->valor_prescribir; 
                }
                if($prescripcion->valor_descontar > 0)
                {
                    if($row->saldo_actual != ($prescripcion->valor_prescribir + $prescripcion->valor_noprescribe - $prescripcion->redimido))
                    {
                        $prescripcion->saldo_actual=0;
                        $prescripcion->estado = 'PE';
                    }
                    $this->escribir([
                       $prescripcion->periodo,
                       $prescripcion->cedres,
                       $prescripcion->valor_prescribir,
                       $prescripcion->valor_noprescribe,
                       $prescripcion->redimido,
                       $prescripcion->numtar,
                       $prescripcion->saldo_actual,
                       $prescripcion->valor_descontar
                    ]);
                    $prescripcion->save();
                }
            }
            $this->info('Display: cedtra::'.$cedres.' prescribe::'.$prescripcion->valor_descontar);
            $sql =null;
            $cedres =null;
            $prescripcion =null;
            $i++;
        }
        $this->cerrar_reporte();
        Cache::flush();
    }

    public function set_ruta_archivo($archivo)
    {
        $sistema = $this->argument('sistema');
        $this->ruta_archivo = "/var/www/html/{$sistema}/public/temp/".$archivo;
    }

    public function crear_reporte($headers = array())
    {
        if(is_null($this->ruta_archivo)) $this->error('Display error: la ruta del archivo no está definida');
        if(is_null($this->archivo))
        {
            $this->archivo = fopen("{$this->ruta_archivo}", "w+");
            if($headers){
                $texto_write = implode(';', $headers);
                fwrite($this->archivo, $texto_write."\r\n");
            }
        }else{
            $this->ruta_archivo;
        }
    }

    public function cerrar_reporte()
    {
        fclose($this->archivo);
    }

    public function escribir($row)
    {
        foreach($row as $ai => $valor)
        {
            if(strpos($row[$ai], "\n") !== false){
                $row[$ai] = "'".$row[$ai]."'";
            }
            if(strpos($row[$ai], "\t") !== false){
                $row[$ai] = "'".$row[$ai]."'";
            }
            $row[$ai] = (empty($row[$ai]))? "\"\"": str_replace(";",',', $row[$ai]);
        }
        $values = array_values($row);
        $texto_write = implode(';', $values);
        fwrite($this->archivo, $texto_write."\r\n");
    }

}   