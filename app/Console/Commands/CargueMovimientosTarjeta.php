<?php
namespace App\Console\Commands;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use App\Models\Asopa06;

use Illuminate\Console\Command;

class CargueMovimientosTarjeta extends Command
{

    #protected $signature = 'cargue_movimientos_tarjeta:send {sistema} {filename}';
    protected $signature = 'cargue_movimientos_tarjeta:send {sistema} {filename} {numtar} {comando=0}';
    protected $description = 'Comando para cargar todos los movimientos en tarjetas que aun no se han reportado';
    protected $ruta_archivo;
    protected $archivo;

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->actualizar_con_separador();
        $this->info('Display lista: Proceso terminado con éxito');
    }

    public function actualizar_con_posicion()
    {
        $sistema = "".$this->argument('sistema');
        $filename = "".$this->argument('filename');
        $filepath = "/var/www/html/{$sistema}/public/files/{$filename}";
        if(!file_exists($filepath)){
            return null;
        }
        $file = fopen($filepath, "rb");
        if(!$file){
            return null;
        }
        $ai=0;  
        while (!feof($file)) 
        {
            $line = fgets($file);
            $line = str_replace("\n", "", $line);
            $numtar = intval(substr($line, 15, 10));
            $val = intval(substr($line, 123, 18));
            $valor = $val / 100;
            $fe = trim(substr($line, 157, 9));
            $fecha = substr($fe, 0, 4).'-'.substr($fe, 4, 2).'-'.substr($fe, 6, 2);
            $descri = trim(substr($line, 97, 30));
            $codest = trim(substr($line, 329, 12));

            $movimiento = Asopa06::where("numtar", $numtar)
            ->where("fecha", $fecha)
            ->where("valor", $valor)
            ->where("codest", $codest)
            ->get();

            if(!$movimiento)
            {
                $movimiento = new Asopa06;
                $movimiento->numero = '';
                $movimiento->fecha  = $fecha;
                $movimiento->numtar = $numtar;
                $movimiento->file   = $filename;
                $movimiento->descri = $descri;
                $movimiento->valor  = $valor;
                $movimiento->codest = $codest;
                $movimiento->save();
            }
            $ai++;
        }
        fclose($file);
    }

    public function actualizar_con_separador()
    {
        $sistema = "".$this->argument('sistema');
        $filename = "".$this->argument('filename');
        $numtar = "".$this->argument('numtar');
        $filepath = "/var/www/html/{$sistema}/public/files/{$filename}";
        if(!file_exists($filepath)){
            return null;
        }
        $file = fopen($filepath, "rb");
        if(!$file){
            return null;
        }
        $ai=0;  
        $procesados = array();
        while (!feof($file)) 
        {
            $line = fgets($file);
            $line = str_replace("\t", " ", $line);
            if(strlen(trim($line)) > 0 && $ai > 0)
            {
                $fila = explode(",", $line);
                $val = explode(".", $fila[8]);
                $valor = $val[0]; 
                $fe = trim($fila[2]);
                $fecha = substr($fe, 0, 4).'-'.substr($fe, 4, 2).'-'.substr($fe, 6, 2);
                $descri = trim($fila[6]);
                $codest = 0;
                $ho = str_pad(trim($fila[5]), 6, "0", STR_PAD_LEFT); 
                $hora = substr($ho, 0, 2).':'.substr($ho, 2, 2).':'.substr($ho, 4, 2);

                $movimientos = Asopa06::where("numtar", $numtar)
                ->where("fecha", $fecha)
                ->where("valor", $valor)
                ->get();
                
                //si no esta en base de datos o si ya se proceso otro igual 
                if($movimientos->count() == 0 && $descri != 'CARGOS - NOV. MONETARIAS')
                {
                    $movimiento = new Asopa06;
                    $movimiento->fecha  = $fecha;
                    $movimiento->numtar = $numtar;
                    $movimiento->file   = $filename;
                    $movimiento->descri = $descri;
                    $movimiento->valor  = $valor;
                    $movimiento->codest = $codest;
                    $movimiento->hora   = $hora;
                    $movimiento->save();
                    $this->info('Display numtar: '.$movimiento->fecha);
                }
            }
            $ai++;
        }
        fclose($file);
    }
}