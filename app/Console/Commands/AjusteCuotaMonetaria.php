<?php
namespace App\Console\Commands;

use Illuminate\Database\QueryException;
use App\Exceptions\DebugException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use App\Models\Subsi14;
use App\Models\Subsi09;
use App\Models\Subsi15;
use App\Models\Subsi20;
use App\Models\Subsi19;
use App\Models\Subsi18;
use App\Models\Subsi12;
use App\Models\Subsi70;

/**
 * AjusteCuotaMonetaria class
 * periodo= 202201
 * valor_periodo= 38371
 * valsub=1475
 * valagro=1696
 * salario_minimo = 1000000
 * salario_tope=4000000
 */
class AjusteCuotaMonetaria extends Command
{
    protected $signature = 'ajuste_cuota_monetaria:send {periodo} {salario_tope} {salario_minimo} {valsub} {valagr} {valor_periodo} {comando=0}';
    protected $description = 'Comando para crear el ajuste de cuota monetaria';
    protected $ruta_archivo;
    protected $archivo;

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        try {
            $this->crear_periodo_ajuste();
            $this->migrar();
            $this->info('Display lista: Proceso terminado con éxito');
        } catch (DebugException $err)
        {
            $salida = array(
                "success" => false,
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555)? DebugException::item("valid") : array()
            );
            $this->error(json_encode($salida, JSON_NUMERIC_CHECK));
        }
    }

    public function migrar()
    {
        ini_set('memory_limit','1300M');
        $filepath = base_path("storage/files/")."ajuste_pago_cuota_".date('Ymd').".csv";
        $file = fopen("{$filepath}", "w+");

        DB::beginTransaction();
        try {
            $periodo = "".$this->argument('periodo');
            $pergir = $periodo;

            $historicos = Subsi09::where("periodo", $periodo)
            ->where('pergir', $pergir)
            ->get();

            $texto_write = implode(',', array(
                "TRABAJADOR",
                "CEDULA RESPONSABLE", 
                "TIPO PAGO", 
                "NUMERO CUENTA", 
                "TARJETA", 
                "PAGO",
                "VALOR",
                "CUOTAS"
            ));
            fwrite($file, $texto_write."\n");
            
            $cambios_formas_pago = array($texto_write); 
            $cambios_empresas = array($texto_write);

            foreach ($historicos as $ai => $historico)
            {
                $responsable = false;
                switch ($historico->pago)
                {
                    case 'C':
                        $responsable = Subsi20::where('cedcon', $historico->cedres)->get()->first();
                        break;
                    case 'T':
                        $responsable = Subsi15::where('cedtra', $historico->cedres)->get()->first();
                        break;
                    case 'N':
                        $responsable = DB::table('subsi18')
                        ->select('subsi18.*', 'subsi15.nit', 'subsi15.estado','subsi15.numtar')
                        ->leftJoin('subsi15', 'subsi15.cedtra','=','subsi18.cedtra')
                        ->where('subsi18.cedtra', $historico->cedtra)
                        ->where('subsi18.codben', $historico->codben)
                        ->where('subsi18.cedres', $historico->cedres)
                        ->get()->first();

                        if(!$responsable)
                        {
                            $responsable = Subsi20::where('cedcon', $historico->cedres)->get()->first();
                        }
                        if(!$responsable)
                        {
                            $responsable = Subsi15::where('cedtra', $historico->cedres)->get()->first();
                        }
                        break;
                    case 'D':
                        $responsable = DB::table('subsi19')
                        ->select('subsi19.*', 'subsi15.nit', 'subsi15.estado', 'subsi15.numtar')
                        ->leftJoin('subsi15', 'subsi15.cedtra','=','subsi19.cedtra')
                        ->where('subsi19.cedtra', $historico->cedtra)
                        ->where('subsi19.cedres', $historico->cedres)
                        ->get()->first();

                        if(!$responsable)
                        {
                            $responsable = Subsi15::where('cedtra', $historico->cedtra)->get()->first();
                        }
                        if(!$responsable)
                        {
                            $responsable = Subsi20::where('cedcon', $historico->cedres)->get()->first();
                        }
                        break;
                    default:
                        $responsable = false;
                    break;
                }
                if($responsable)
                {
                    $actual = $this->crear_forma_pago($responsable, $historico);
                    $_datos = array(
                        $historico->cedtra,
                        $historico->cedres, 
                        $historico->tippag, 
                        $historico->numcue, 
                        $historico->numtar, 
                        $historico->pago,
                        $actual->valor,
                        $actual->numcuo
                    );   
                    $texto_write = implode(',', $_datos);
                    fwrite($file, $texto_write."\n");
                    
                    if($responsable->tippag != $historico->tippag)
                    {
                        $cambios_formas_pago[] = $texto_write;
                    }
                    if($responsable->nit != $historico->nit)
                    {
                        $cambios_empresas[] = $texto_write;
                    }
                    $this->info($texto_write); 
                }
            }
            DB::commit();
            fclose($file);

            $file2 = base_path("storage/files/")."ajuste_cambios_formas_pago_".date('Ymd').".csv";
            file_put_contents($file2, implode("\n", $cambios_formas_pago));

            $file3 = base_path("storage/files/")."ajuste_cambios_empresas_".date('Ymd').".csv";
            file_put_contents($file3, implode("\n", $cambios_empresas));

        } catch(QueryException $ex) 
        {
            fclose($file);
            DB::rollBack();
            throw new DebugException($ex->getMessage().' '.$ex->getLine().' '.basename($ex->getFile()), 1);
        }
    }

    public function crear_forma_pago($responsable, $historico)
    {
        $valsub = "".$this->argument('valsub');
        $valagr = "".$this->argument('valagr');
        $valor_periodo = "".$this->argument('valor_periodo');

        $actual =  new Subsi14();
        $actual->periodo = ($historico->periodo - 1);
        $actual->pergir  = ($historico->periodo - 1);
        $actual->codben = $historico->codben;
        $actual->cedtra = $historico->cedtra;
        $actual->cedres = $historico->cedres;
        $actual->ofiafi = $historico->ofiafi;
        $actual->nit = $historico->nit;
        $actual->parent = $historico->parent;
        $actual->captra = $historico->captra;
        $actual->cedcon = $historico->cedcon;
        $actual->codcue = $historico->codcue;
        $actual->codgru = $historico->codgru;
        $actual->numcue = $historico->numcue;
        $actual->tipcue = $historico->tipcue;
        $actual->numche = $historico->numche;
        $actual->numcuo = $historico->numcuo;
        $actual->propag = $historico->propag;
        $actual->muetra = $historico->muetra;
        $actual->pagtes = $historico->pagtes;
        $actual->pago = $historico->pago;
        $actual->estche = $historico->estche;
        $actual->chenum = $historico->chenum;
        $actual->usuario = $historico->usuario;
        $actual->tipgir = $historico->tipgir;
        $actual->valcre = $historico->valcre;
        $actual->valaju = $historico->valaju;
        $actual->anulado = $historico->anulado;
        $actual->fecanu = $historico->fecanu;
        $actual->codanu = $historico->codanu;
        $actual->peranu = $historico->peranu;
        $actual->estanu = $historico->estanu;
        $actual->carnov = $historico->carnov;
        $actual->fecasi = $historico->fecasi;
        $actual->fecent = $historico->fecent;
        $actual->codarc = $historico->codarc;
        $actual->ruaf = $historico->ruaf;
        $actual->ruasub = $historico->ruasub;
        $actual->feccon = $historico->feccon;
        $actual->codsuc = "$historico->codsuc";
        $actual->codlis = "$historico->codlis";
        $actual->numtar = $historico->numtar;

        $tippag = $responsable->tippag;
        $actual->tippag = $tippag;
        $actual->tipcue = $historico->tipcue;

        switch ($tippag)
        {
            case 'A':
                $actual->codgru = NULL;
                $actual->codcue = $responsable->codcue;
                $actual->tipcue = $responsable->tipcue;
                $actual->numcue = $responsable->numcue; 
                $actual->numche = '0';
                break;
            case 'B':
                $actual->tipcue = $responsable->tipcue;
                $actual->codcue = '01';
                $actual->codgru = NULL;
                $actual->numcue = NULL;
                $actual->numche = '0';
                break;
            case 'C':
                $actual->codcue = '13';
                $actual->codgru = NULL;
                $actual->numche = '0';
                $actual->numcue = $responsable->numcue;
                break;
            case 'D':
                $actual->codcue = NULL;
                $actual->codgru = NULL;
                $actual->numche = '0';
                $actual->numcue = $responsable->numcue;
                break;
            case 'T':
                $actual->codcue = NULL;
                $actual->codgru = NULL;
                $actual->numcue = NULL;
                $actual->numche = '0';
                $actual->numtar = $responsable->numtar;
                break;
            default:
            break;
        }

        $actual->valor = (($historico->valor / $historico->numcuo) == $valor_periodo)? ($valsub * $historico->numcuo) : ($valagr * $historico->numcuo);
        $actual->save();
        return $actual;
    }

    public function crear_periodo_ajuste()
    {
        $periodo = "".$this->argument('periodo');
        $salario_tope = "".$this->argument('salario_tope');
        $salario_minimo = "".$this->argument('salario_minimo');
        $valsub = "".$this->argument('valsub');
        $valagr = "".$this->argument('valagr');

        $periodo_ajuste = $periodo - 1;
        $periodo_ajuste_temp = Subsi12::where("periodo", $periodo_ajuste)->get()->first();
        if(!$periodo_ajuste_temp)
        {
            $year = substr($periodo_ajuste, 0, 4); 
            $mes = substr($periodo_ajuste, 4, 2) + 1;
            $mes = str_pad($mes, 2, '0', STR_PAD_LEFT);
            $date1 = date("{$year}-{$mes}-01");
            $date2 = date("{$year}-{$mes}-30");
            
            $periodo_ajuste_temp = new Subsi12();
            $periodo_ajuste_temp->periodo = $periodo_ajuste;
            $periodo_ajuste_temp->fecini = $date1;
            $periodo_ajuste_temp->fecfin = $date2;
            $periodo_ajuste_temp->camley = '2';
            $periodo_ajuste_temp->cansal = '6';
            $periodo_ajuste_temp->pormor = '176';
            $periodo_ajuste_temp->fecuni = date('Y-m-d');
            $periodo_ajuste_temp->salmin = $salario_minimo;
            $periodo_ajuste_temp->saltop = $salario_tope;
            $periodo_ajuste_temp->girado = 'N';
            $periodo_ajuste_temp->save();         
        }

        $subsi70 = Subsi70::where("periodo", $periodo_ajuste)->where('ofiafi', '01')->get()->first();
        if(!$subsi70)
        {
            $subsi70 = new Subsi70();
            $subsi70->ofiafi ='01';
            $subsi70->periodo = $periodo_ajuste;
            $subsi70->valsub = $valsub;
            $subsi70->valagr = $valagr;
            $subsi70->subuni = $valsub;
            $subsi70->subagr = $valagr;
            $subsi70->save();
        }
    }

}   