<?php

namespace App\Providers;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        JsonResource::withoutWrapping();
        $this->loadRoutesFrom(base_path('routes/affiliation.php'));
        $this->loadRoutesFrom(base_path('routes/tipopago.php'));
        $this->loadRoutesFrom(base_path('routes/sat.php'));
        $this->loadRoutesFrom(base_path('routes/company.php'));
        $this->loadRoutesFrom(base_path('routes/satservice.php'));
        $this->loadRoutesFrom(base_path('routes/correspondencia.php'));
        $this->loadRoutesFrom(base_path('routes/aportes.php'));
        $this->loadRoutesFrom(base_path('routes/tesoreria.php'));
        $this->loadRoutesFrom(base_path('routes/novedades.php'));
        $this->loadRoutesFrom(base_path('routes/certificados.php'));
        $this->loadRoutesFrom(base_path('routes/usuarios.php'));
    }
}
