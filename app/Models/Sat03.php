<?php
namespace App\Models;

use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sat03 extends Model
{
    use HasFactory;

    protected $table = "sat03";
    
    public $timestamps = false;

    protected $primaryKey = "numtraccf";
    
    public $autoincrement = false;

    protected $fillable = [
        "numtraccf",
        "numtrasat",
        "tipper",
        "tipemp",
        "tipdocemp",
        "numdocemp",
        "sersat",
        "prinom",
        "segnom",
        "priape",
        "segape",
        "fecsol",
        "perafigra",
        "fecafi",
        "razsoc",
        "matmer",
        "coddep",
        "codmun",
        "direccion",
        "telefono",
        "email",
        "tipdocrep",
        "numdocrep",
        "prinom2",
        "segnom2",
        "priape2",
        "segape2",
        "codcaj",
        "pazsal",
        "fecpazsal",
        "autmandat",
        "autenvnot",
        "siafissfant",
        "resultado",
        "mensaje",
        "codigo"
    ];

    public function getCamposSat03()
    {
        return array(
            'numtraccf'   => "NumeroRadicadoSolicitud",
            'numtrasat'   => "NumeroTransaccion",
            'tipper'      => "TipoPersona",
            'tipemp'      => "NaturalezaJuridicaEmpleador",
            'tipdocemp'   => "TipoDocumentoEmpleador",
            'numdocemp'   => "NumeroDocumentoEmpleador",
            'sersat'      => "SerialSAT",
            'prinom'      => "PrimerNombreEmpleador",
            'segnom'      => "SegundoNombreEmpleador",
            'priape'      => "PrimerApellidoEmpleador",
            'segape'      => "SegundoApellidoEmpleador",
            'fecsol'      => "FechaSolicitud",
            'perafigra'   => "PerdidaAfiliacionCausaGrave",
            'fecafi'      => "FechaEfectivaAfiliacion",
            'razsoc'      => "RazonSocial",
            'matmer'      => "NumeroMatriculaMercantil",
            'coddep'      => "Departamento",
            'codmun'      => "Municipio",
            'direccion'   => "DireccionContacto",
            'telefono'    => "NumeroTelefono",
            'email'       => "CorreoElectronico",
            'tipdocrep'   => "TipoDocumentoRepresentante",
            'numdocrep'   => "NumeroDocumentoRepresentante",
            'prinom2'     => "PrimerNombreRepresentante",
            'segnom2'     => "SegundoNombreRepresentante",
            'priape2'     => "PrimerApellidoRepresentante",
            'segape2'     => "SegundoApellidoRepresentante",
            'codcaj'      => "CodigoCajaCompensacionFamiliarAnterior",
            'pazsal'      => "PazYSalvo", //TODO cambiar todos los PazySalvo por PazSalvo
            'fecpazsal'   => "FechaPazYSalvo",
            'autmandat'   => "AutorizacionManejoDatos",
            'autenvnot'   => "AutorizacionNotificaciones",
            'siafissfant' => "Manifestacion"
        );
    }

    public function isValid($rules)
    {
        $validator = Validator::make($this->attributes,  $rules, [
            'required' => 'El :attribute campo es requirido.',
            'same'     => 'El :attribute and :other must match.',
            'size'     => 'El :attribute must be exactly :size.',
            'between'  => 'El :attribute value :input is not between :min - :max.',
            'in'       => 'El :attribute must be one of the following types: :values',
            'email.required' => 'Se requiere la dirección de email!',
        ]);
        if ($validator->fails()){
            return $validator->errors();
        }
        return true;
    }
}
