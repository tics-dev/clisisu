<?php
namespace App\Models;

use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sat04 extends Model
{
    use HasFactory;

    protected $table = "sat04";
    
    public $timestamps = false;

    protected $primaryKey = "numtraccf";
    
    public $autoincrement = false;

    protected $fillable = [
        "numtraccf",
        "numtrasat",
        "tipdoctra",
        "numdoctra",
        "prinom",
        "segnom",
        "priape",
        "segape",
        "sexo",
        "fecnac",
        "email",
        "salario",
        "tipsal",
        "hortra",
        "resultado",
        "mensaje",
        "codigo",
    ];

    public function getCamposSat04()
    {
        return array(
            'numtraccf'   => "NumeroRadicadoSolicitud"
        );
    }

    public function isValid($rules)
    {
        $validator = Validator::make($this->attributes,  $rules, [
            'required' => 'El :attribute campo es requirido.',
            'same'     => 'El :attribute and :other must match.',
            'size'     => 'El :attribute must be exactly :size.',
            'between'  => 'El :attribute value :input is not between :min - :max.',
            'in'       => 'El :attribute must be one of the following types: :values',
            'email.required' => 'Se requiere la dirección de email!',
        ]);
        if ($validator->fails()){
            return $validator->errors();
        }
        return true;
    }
}
