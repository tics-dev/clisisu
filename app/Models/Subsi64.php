<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subsi64 extends Model
{
    use HasFactory;
    protected $table = "subsi64";
	public $timestamps = false;
	protected $primaryKey = "numero";

    protected $fillable = [
        "numero","nitcaj","nitver","razpla","coddoc","nitpla","digver","nit","tipapo","direccion",
        "codciu","coddep","telefono","fax","email","perapo","periodo","tippla","fecpagpa","fecrec",
        "fecsis","numplaas","numrad","forpre","codsuc","nomsuc","totemp","admtra","codope","modpla",
        "diamor","tottra","fecmat","coddepdp","ley1429","claapo","natjur","tipper","valnom","valcon",
        "valint","nota","estado","codest","tipdev","estapo","estnom","nomarc","marca","documento",
        "genrec","estcon","aplica","estint","feccon","fecapl","prescrito","fecpre","claemp",
        "fecley","depley","caley1","caley2","correo"
    ];

    public function subsi65()
	{
		return $this->belongsToMany(Subsi65::class, 'subsi64', 'numero', 'numero');
	}

}
