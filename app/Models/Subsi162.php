<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subsi162 extends Model
{
    use HasFactory;
    protected $table = "subsi162";
    public $timestamps = false;
    protected $primaryKey = "idreg";
    protected $fillable = [
        "idreg",
        "nit",
        "codsuc",
        "num"
    ];

    /**
     * novedades function
     * agente posee a muchas novedades
     * @return void
     */
    public function novedades_empresa()
	{
		return $this->belongsToMany(Subsi160::class, 'subsi162', 'idreg', 'idreg');
	}
}
