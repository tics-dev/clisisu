<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gener08 extends Model
{
    use HasFactory;
    
    protected $table = "gener08";

    public $timestamps = false;

    protected $keyType = 'string';

    protected $fillable = [
        "codciu",
        "detciu",
        "numpob"
    ];

    public function trabajador()
	{
		return $this->belongsTo(Subsi15::class, 'codciu', 'codciu');
	}
}