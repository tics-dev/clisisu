<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subsi46 extends Model
{
    use HasFactory;
    
    protected $table = "subsi46";

    public $timestamps = false;

    protected $keyType = 'string';

    protected $fillable = [
        "nivedu",
        "detalle"
    ];

    public function trabajador()
	{
		return $this->belongsTo(Subsi15::class, 'nivedu', 'nivedu');
	}
}