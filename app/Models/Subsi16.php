<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Thiagoprz\CompositeKey\HasCompositeKey;

class Subsi16 extends Model
{
    use HasCompositeKey;
    use HasFactory;

    protected $table = "subsi16";
    
    public $timestamps = false;
    
    protected $primaryKey = ["cedtra", "fecafi"];

    public $autoincrement = false;

    protected $fillable = [
        "cedtra",
        "nit",
        "codsuc",
        "codlis",
        "fecafi",
        "fecret",
        "fosfec"
    ];

    public function trabajador()
	{
		return $this->belongsTo(Subsi15::class, 'cedtra', 'cedtra');
	}
}
