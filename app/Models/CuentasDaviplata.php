<?php
namespace App\Models;

use App\Exceptions\DebugException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CuentasDaviplata extends Model
{
    use HasFactory;

    protected $table = "cuentas_daviplata";
    
    public $timestamps = false;
    
    protected $primaryKey = "id";

    const CREATED_AT = 'createAt';
    
    const UPDATED_AT = 'updateAt';

    protected $fillable = [
        'id',
        'cuenta',
        'cedula',
        'estado',
        'createAt',
        'updateAt'
    ];
}