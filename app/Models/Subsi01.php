<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subsi01 extends Model
{
    use HasFactory;
    
    protected $table = "subsi01";

    public $timestamps = false;

    protected $fillable = [
        "codapl",
        "control",
        "audtra",
        "cnt",
        "marapo",
        "codins",
        "codind",
        "codccf",
        "patharc",
        "pathcop",
        "pagret",
        "concie",
        "porcom",
        "jefapo",
        "diradm",
        "ofiexp",
        "convenio",
        "valcon",
        "fecest",
        "tipdocapo",
        "numdocapo",
        "priapeapo",
        "segapeapo",
        "prinomapo",
        "segnomapo",
        "emailapo",
        "telapo",
        "faxapo",
        "webapo",
        "genpla",
        "sofnumlic",
        "codcaj"
    ];
}