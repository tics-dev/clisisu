<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subsi09 extends Model
{
	use HasFactory;

	protected $table = "subsi09";

	public $timestamps = false;

	public $autoincrement = false;

	public $incrementing = false;

	protected $fillable = [
		"ofiafi",
		"periodo",
		"nit",
		"codsuc",
		"codlis",
		"codben",
		"parent",
		"captra",
		"cedtra",
		"cedcon",
		"cedres",
		"tippag",
		"codcue",
		"codgru",
		"numcue",
		"tipcue",
		"numche",
		"numcuo",
		"valor",
		"pergir",
		"propag",
		"muetra",
		"pagtes",
		"pago",
		"estche",
		"chenum",
		"usuario",
		"tipgir",
		"valcre",
		"valaju",
		"anulado",
		"fecanu",
		"codanu",
		"peranu",
		"estanu",
		"carnov",
		"fecasi",
		"fecent",
		"codarc",
		"ruaf",
		"ruasub",
		"feccon",
		"numtar"
	];

}