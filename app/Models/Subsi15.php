<?php

namespace App\Models;

use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subsi15 extends Model
{
    use HasFactory;

    protected $table = "subsi15";

    public $timestamps = false;

    protected $primaryKey = "cedtra";

    public $autoincrement = false;

    protected $fillable = [
        "nit",
        "codsuc",
        "codlis",
        "cedtra",
        "coddoc",
        "priape",
        "segape",
        "prinom",
        "segnom",
        "direccion",
        "codciu",
        "telefono",
        "email",
        "codzon",
        "rural",
        "agro",
        "cabhog",
        "captra",
        "tipdis",
        "horas",
        "salario",
        "tipsal",
        "fecsal",
        "sexo",
        "estciv",
        "tipcon",
        "feccon",
        "trasin",
        "vivienda",
        "nivedu",
        "tipcot",
        "vendedor",
        "empleador",
        "tippag",
        "codcue",
        "ofides",
        "codgru",
        "codban",
        "numcue",
        "tipcue",
        "feccar",
        "codcat",
        "fecnac",
        "ciunac",
        "fecpre",
        "fecafi",
        "fecsis",
        "giro",
        "codgir",
        "estado",
        "codest",
        "fecest",
        "carnet",
        "benef",
        "ruaf",
        "ruasub",
        "totcon",
        "tothij",
        "tother",
        "totpad",
        "nota",
        "numtar",
        "fecrua",
        "fosfec",
        "fecact",
        "tipjor",
        "usuario",
        "correo",
        "tottra",
        "cargo",
        "orisex",
        "dirlab",
        "ruralt",
        "ciulab",
        "pais",
        "facvul",
        "peretn",
        "fecha_giro",
        "resguardos_id",
        "pub_indigena_id"
    ];

    public function getTrayectoria()
    {
        return Subsi16::where('cedtra', $this->cedtra)->get();
    }

    public function getTrayectoriaByEmpresa($nit = '')
    {
        if ($nit == '') $nit = $this->nit;
        return Subsi16::where('cedtra', $this->cedtra)->where('nit', $nit)->get();
    }

    public function getSalarios()
    {
        return Subsi17::where('cedtra', $this->cedtra)->get();
    }

    public function get_gener18()
    {
        return $this->hasOne(Gener18::class, 'coddoc', 'coddoc');
    }

    public function reglas_creacion()
    {
        return array(
            "nit"     => "required|max:13",
            "codsuc"  => "required|max:3",
            "codlis"  => "required|max:3",
            "priape"  => "required|max:45",
            "prinom"  => "required|max:45",
            "segape"  => "max:45",
            "segnom"  => "max:45",
            "horas"   => "required",
            "coddoc"  => "required|max:2",
            "fecafi"  => "required|date",
            "fecsis"  => "required|date",
            "codciu"  => "required|max:2",
            "codzon"  => "required|max:2",
            "estado"  => "required|max:1",
            "fecsal"  => "date",
            "fecsol"  => "date"
        );
    }

    public function isValid($rules)
    {
        $validator = Validator::make($this->attributes,  $rules, [
            'required' => 'El :attribute campo es requirido.',
            'same'     => 'El :attribute and :other must match.',
            'size'     => 'El :attribute must be exactly :size.',
            'between'  => 'El :attribute valor :input no esta entre :min - :max.',
            'in'       => 'El :attribute must be one of the following types: :values',
            'email.required' => 'Se requiere la dirección de email!',
        ]);
        if ($validator->fails()) {
            return $validator->errors();
        }
        return true;
    }
}
