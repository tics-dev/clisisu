<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrescripcionCuota extends Model
{
    use HasFactory;
   
    protected $table = 'prescripcion_cuota';
   
    protected $primaryKey = "id";
   
    public $timestamps = false;
   
    protected $fillable = [
        "id",
        "cedres",
        "fecha",
        "saldo_actual",
        "periodo",
        "valor_descontar",
        "redimido",
        "valor_prescribir",
        "valor_noprescribe",
        "estado",
        "tipo",
        "resolucion",
        "cuenta_redime",
        "fecha_redime"
    ];

    public function getEstadoPreArray()
    {    
        return array(
            "PE"=>"Pendiente", //pendiente por inconsistencia en los saldos y los movimientos, para pasar a adicionado
            "AD"=>"Adicionado", //ya esta listo para procesar la prescripcion
            "PR"=>"Procesado", //prescripcion procesada
            "RD"=>'Redimido Durante Prescripción', //prescripcion redimida
            "DP"=>'Redimido Por Daviplata', //prescripcion redimida por daviplata
            "PD"=>'Pendiente Para Daviplata' //resultado de cruzar que se puede pagar por daviplata
        );
    }

    public function getEstadoPreDetalle()
    {
        switch($this->estado)
        {
            case 'PE': 
                return 'Pendiente'; 
            break;
            case 'AD': 
                return 'Adicionado'; 
            break;
            case 'PR': 
                return 'Procesado'; 
            break;
            case 'RD':
                return 'Redimido Durante Prescripción'; 
            break;
            case 'DP':
                return 'Redimido Por Daviplata';
            break;
            case 'PD':
                return 'Pendiente Para Daviplata';
            break;
        }
    }

    public function getTipoPreArray()
    {
        return array(
            "TA"=>"Saldos en tarjeta",
            "PP"=>"Saldos por pagar"
        );
    }

    public function getTipoPreDetalle()
    {
        switch($this->tipo)
        {
            case 'TA': 
                return 'Saldos en tarjeta'; 
            break;
            case 'PP': 
                return 'Saldos por pagar'; 
            break;
        }
    }
}
