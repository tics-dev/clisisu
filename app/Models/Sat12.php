<?php

namespace App\Models;
use App\Exceptions\DebugException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sat12 extends Model
{
    use HasFactory;

    protected $table = "sat12";

    public $timestamps = false;

    protected $primaryKey = "numtraccf";

    public $autoincrement = false;

    protected $fillable = [
        "numtraccf",
        "numtrasat",
        "tipdocemp",
        "numdocemp",
        "sersat",
        "fecini",
        "tipdoctra",
        "numdoctra",
        "priape",
        "prinom",
        "fecfin",
        "indnov",
        "autmandat",
        "autenvnot",
        "resultado",
        "mensaje",
        "codigo"
    ];

    public function getCamposSat12()
    {
        return array(
            'numtraccf' => "NumeroRadicadoSolicitud",
            'numtrasat' => "NumeroTransaccion",
            'tipdocemp' => "TipoDocumentoEmpleador",
            'numdocemp' => "NumeroDocumentoEmpleador",
            'sersat'    => "SerialSAT",
            'tiplin'    => "",
            'fecini'    => "",
            'fecfin'    => "",
            'tipdoctra' => "TipoDocumentoTrabajador",
            'numdoctra' => "NumeroDocumentoTrabajador",
            'priape'    => "PrimerApellidoTrabajador",
            'prinom'    => "PrimerNombreTrabajador",
            'indnov'    => "",
            'autmandat' => "AutorizacionManejoDatos",
            'autenvnot' => "AutorizacionNotificaciones"
        );
    }

    public function isValid($rules)
    {
        $validator = Validator::make($this->attributes,  $rules, [
            'required' => 'El :attribute campo es requirido.',
            'same'     => 'El :attribute and :other must match.',
            'size'     => 'El :attribute must be exactly :size.',
            'between'  => 'El :attribute value :input is not between :min - :max.',
            'in'       => 'El :attribute must be one of the following types: :values',
            'email.required' => 'Se requiere la dirección de email!',
        ]);
        if ($validator->fails()){
            return $validator->errors();
        }
        return true;
    }
}
