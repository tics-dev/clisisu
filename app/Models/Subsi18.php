<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subsi18 extends Model
{
	use HasFactory;
	
	protected $table = "subsi18";
	
	public $timestamps = false;

	public $incrementing = false;

	protected $primaryKey = array('cedtra', 'codben');

	protected $fillable = [  
		"cedtra",
		"codben",
		"cedres",
		"priape",
		"segape",
		"prinom",
		"segnom",
		"direccion",
		"telefono",
		"codzon",
		"nota",
		"tippag",
		"codcue",
		"ofides",
		"codgru",
		"codban",
		"numcue",
		"tipcue",
		"fecemb",
		"estado",
		"fecsis"
	];

}