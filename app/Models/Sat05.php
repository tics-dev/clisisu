<?php
namespace App\Models;

use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sat05 extends Model
{
    use HasFactory;

    protected $table = "sat05";
    
    public $timestamps = false;

    protected $primaryKey = "numtraccf";
    
    public $autoincrement = false;

    protected $fillable = [
        "numtraccf",
        "numtrasat",
        "tipdocemp",
        "numdocemp",
        "sersat",
        "restra",
        "fecafi",
        "fecefe",
        "motivo",
        "resultado",
        "mensaje",
        "codigo"
    ];

    public function getCamposSat05()
    {
        return array(
            'numtraccf'   => "NumeroRadicadoSolicitud"
        );
    }

    public function isValid($rules)
    {
        $validator = Validator::make($this->attributes,  $rules, [
            'required' => 'El :attribute campo es requirido.',
            'same'     => 'El :attribute and :other must match.',
            'size'     => 'El :attribute must be exactly :size.',
            'between'  => 'El :attribute value :input is not between :min - :max.',
            'in'       => 'El :attribute must be one of the following types: :values',
            'email.required' => 'Se requiere la dirección de email!',
        ]);
        if ($validator->fails()){
            return $validator->errors();
        }
        return true;
    }
}
