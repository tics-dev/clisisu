<?php

namespace App\Models;

use App\Exceptions\DebugException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Mercurio31 extends Model
{
    use HasFactory;

    protected $table = "mercurio31";

    public $timestamps = false;

    protected $primaryKey = "id";

    public $autoincrement = true;

    protected $fillable = [
        "id",
        "log",
        "nit",
        "razsoc",
        "cedtra",
        "tipdoc",
        "priape",
        "segape",
        "prinom",
        "segnom",
        "fecnac",
        "ciunac",
        "sexo",
        "orisex",
        "estciv",
        "cabhog",
        "codciu",
        "codzon",
        "direccion",
        "barrio",
        "telefono",
        "celular",
        "fax",
        "email",
        "fecsol",
        "fecing",
        "salario",
        "tipsal",
        "captra",
        "tipdis",
        "nivedu",
        "rural",
        "horas",
        "tipcon",
        "trasin",
        "vivienda",
        "tipafi",
        "profesion",
        "cargo",
        "autoriza",
        "usuario",
        "estado",
        "codest",
        "motivo",
        "fecest",
        "tipo",
        "coddoc",
        "documento",
        "facvul",
        "peretn",
        "dirlab",
        "ruralt",
        "comision",
        "tipjor",
        "tippag",
        "numcue",
        "codsuc",
        "otra_empresa",
        "fecha_giro",
        "codban"
    ];

    public function __construct()
    {
        parent::__construct();
        $this->setConnection('mercurio');
    }

    public function isValid($rules = '')
    {
        if (empty($rules)) {
            $rules = $this->getRules();
        }
        $validator = Validator::make($this->attributes,  $rules, [
            'required' => 'El :attribute campo es requirido.',
            'same'     => 'El :attribute y :other deben coincidir.',
            'size'     => 'El :attribute debe ser exactamente :size.',
            'between'  => 'El :attribute valor :input no está entre :min - :max.',
            'in'       => 'El :attribute debe ser uno de los siguientes valores: :values',
            'numeric'  => 'El :attribute el valor no es númerico',
            'email.required' => 'Se requiere la dirección de email!',
            'digits_between' => 'El :attribute el valor debe estar en los rangos predefinidos'
        ]);
        if ($validator->fails()) {
            return $validator->errors();
        }
        return true;
    }

    public function getRules()
    {
        return [
            "nit" => "required|max:15",
            "email"  => "required|email|max:100",
            "log" => "numeric|digits_between:1,11",
            "razsoc" => "required|max:100",
            "cedtra" => "required|numeric|digits_between:5,15",
            "tipdoc" => "required|numeric|digits_between:1,11",
            "priape" => "required|max:20",
            "segape" => "max:20",
            "prinom" => "required|max:20",
            "segnom" => "max:20",
            "fecnac" => "required|date",
            "ciunac" => "required|max:5",
            "sexo" => "required|max:1|in:F,I,M",
            "orisex" => "required|numeric|digits_between:1,4",
            "estciv" => "required|numeric|digits_between:1,6",
            "cabhog" => "required|max:1|in:S,N",
            "codciu" => "required|max:5",
            "codzon" => "required|max:5",
            "direccion" => "required|max:120",
            "barrio" => "required|max:48",
            "telefono" => "required|numeric|digits_between:1,13",
            "celular" => "required|numeric|digits_between:1,15",
            "fax" => "max:13",
            "fecsol" => "required|date",
            "fecing" => "required|date",
            "salario" => "required|numeric|digits_between:1,11",
            "tipsal" => "required|max:1|in:F,I,V",
            "captra" => "required|max:1|in:N,I",
            "tipdis" => "required|numeric|digits_between:0,7",
            "nivedu" => "required|numeric|digits_between:1,14",
            "rural" => "required|max:1|in:N,S",
            "horas" => "required|numeric|digits_between:1,3",
            "tipcon" => "required|max:1|in:F,I",
            "trasin" => "required|max:1|in:N,S",
            "vivienda" => "required|max:1|in:F,I,P,A,H",
            "tipafi" => "required|max:2",
            "profesion" => "max:45",
            "cargo" => "required|max:9",
            "autoriza" => "required|max:1|in:S,N",
            "usuario" => "required|numeric|digits_between:1,5",
            "estado" => "required|max:1",
            "codest" => "max:2",
            "motivo" => "max:100",
            "fecest" => "date",
            "tipo" => "required|max:2",
            "coddoc" => "required|max:2",
            "documento" => "required|numeric|digits_between:1,15",
            "facvul" => "required|numeric|digits_between:1,13",
            "peretn" => "required|numeric|digits_between:1,8",
            "dirlab" => "required|max:150",
            "ruralt" => "required|max:1|in:S,N",
            "comision" => "required|max:1|in:S,N",
            "tipjor" => "required|max:1|in:C,M,P"
        ];
    }

    public function mapperAttribute()
    {
        return array(
            "cedula" => "cedtra",
            "tipo_documento" => "tipdoc",
            "primer_apellido" => "priape",
            "segundo_apellido" => "segape",
            "primer_nombre" => "prinom",
            "segundo_nombre" => "segnom",
            "fecha_nacimiento" => "fecnac",
            "codigo_ciudad_nacimiento" => "ciunac",
            "estado_civil" => "estciv",
            "cabeza_hogar" => "cabhog",
            "codigo_ciudad_residencia" => "codciu",
            "codigo_ciudad_laboral" => "codzon",
            "fecha_ingreso" => "fecing",
            "tipo_salario" => "tipsal",
            "capacidad_trabajar" => "captra",
            "tipo_discapacidad" => "tipdis",
            "nivel_educativo" => "nivedu",
            "residencia_rural" => "rural",
            "horas_trabajar" => "horas",
            "tipo_contrato" => "tipcon",
            "sindicalizado" => "trasin",
            "tipo_afiliado" => "tipafi",
            "orientacion_sexual" => "orisex",
            "factor_vulnerabilidad" => "facvul",
            "etnica" => "peretn",
            "direccion_laboral" => "dirlab",
            "tratamiento_datos" => "autoriza",
            "tipo_jornada" => "tipjor",
            "labor_rural" => "ruralt",
            "recibe_comision" => "comision",
            "fecha_solicitud" => 'fecsol',
            "codigo_documento_empleador" => "coddoc"
        );
    }
}
