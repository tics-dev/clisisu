<?php
namespace App\Models;

use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Thiagoprz\CompositeKey\HasCompositeKey;

class Subsi21 extends Model
{
    use HasCompositeKey;
    use HasFactory;

    protected $table = "subsi21";
    
    public $timestamps = false;
    
    protected $primaryKey = ["cedcon", "cedtra"];

    public $autoincrement = false;

    protected $fillable = [
        "cedtra",
        "cedcon",
        "fecafi",
        "recsub",
        "fecsis",
        "comper",
        "ruaf",
        "numrua",
        "fosfec",
        "fecrua"    
    ];


    public function relationTrabajador()
	{
        return $this->belongsTo(Subsi20::class, 'cedcon', 'cedcon');
	}

    public function isValid($rules)
    {
        $validator = Validator::make($this->attributes,  $rules, [
            'required' => 'El :attribute campo es requirido.',
            'same'     => 'El :attribute and :other must match.',
            'size'     => 'El :attribute must be exactly :size.',
            'between'  => 'El :attribute valor :input no esta entre :min - :max.',
            'in'       => 'El :attribute must be one of the following types: :values',
            'email.required' => 'Se requiere la dirección de email!',
        ]);
        if ($validator->fails()){
            return $validator->errors();
        }
        return true;
    }

    public function reglas_creacion()
    {
        return array(
            "cedtra" => "required|max:20",
            "cedcon" => "required|max:20",
            "fecafi" => "required|date",
            "comper" => "required"
        );
    }

}
