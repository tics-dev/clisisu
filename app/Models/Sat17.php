<?php
namespace App\Models;

use App\Exceptions\DebugException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

//Responder Solicitud Afiliacion Ccf Primeravez Pruebas
class Sat17 extends Model
{
    use HasFactory;

    protected $table = "sat17";

    public $timestamps = false;
    
    protected $primaryKey = "numtraccf";
    
    public $autoincrement = false;

    protected $fillable = [
        "numtraccf",
        "numtrasat",
        "tipo_documento_empleador",
        "numero_documento_empleador",
        "serial_sat",
        "resultado_tramite",
        "fecha_efectiva_afiliacion",
        "motivo_rechazo",
        "estado"
    ];

    public function getCamposSat17()
    {
        return array(
            "numtraccf"                  => "NumeroRadicadoSolicitud",
            "numtrasat"                  => "NumeroTransaccion",
            "tipo_documento_empleador"   => "TipoDocumentoEmpleador",
            "numero_documento_empleador" => "NumeroDocumentoEmpleador",
            "serial_sat"                 => "SerialSat",
            "resultado_tramite"          => "ResultadoTramite",
            "fecha_efectiva_afiliacion"  => "FechaEfectivaAfiliacion",
            "motivo_rechazo"             => "MotivoRechazo"
        );
    }

    public function isValid($rules)
    {
        $validator = Validator::make($this->attributes,  $rules, [
            'required' => 'El :attribute campo es requirido.',
            'same'     => 'El :attribute and :other must match.',
            'size'     => 'El :attribute must be exactly :size.',
            'between'  => 'El :attribute value :input is not between :min - :max.',
            'in'       => 'El :attribute must be one of the following types: :values',
            'email.required' => 'Se requiere la dirección de email!',
        ]);
        if ($validator->fails()){
            return $validator->errors();
        }
        return true;
    }

    public function getEstadosSat17()
    {
        return [
            'P'=>'Pendiente',
            'F'=>'Finalizado',
            'X'=>'Error' 
        ];
    }
}
