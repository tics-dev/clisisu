<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrescripcionContacto extends Model
{
    use HasFactory;
    
    protected $table ='prescripcion_contacto';
    protected $primaryKey = "id";
    public $timestamps = false;
    protected $fillable = [
        "nombre",
        "telefono",
        "email",
        "usuario",
        "prescripcion_cuota",
        "nota",
        "dia",
        "hora",
        "medio"
    ];

    public function getMediosArray()
    {
        return array(
            "1"=>"Teléfono",
            "2"=>"Email",
            "3"=>"Presencial",
            "4"=>'Otros'
        );
    }
    
    public function getMedioDetalle()
    {
        switch($this->medio)
        {
            case '1': 
                return 'Teléfono'; 
            break;
            case '2': 
                return 'Email'; 
            break;
            case '3': 
                return 'Presencial'; 
            break; 
            case '4': 
                return 'Otros'; 
            break;        
        }
    }
}