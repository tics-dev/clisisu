<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sat31 extends Model
{
    use HasFactory;

    protected $table = "sat31";

    public $timestamps = false;

    protected $primaryKey = "id"; 

    protected $fillable = [
        "tipdocemp",
        "numdocemp",
        "sersat"
    ];

    public function getCamposSat31()
    {
        return array(
            'tipdocemp'   => "TipoDocumentoEmpleador",
            'numdocemp'   => "NumeroDocumentoEmpleador",
            'sersat'      => "SerialSat"
        );
    }
    
}
