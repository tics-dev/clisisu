<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Asopa07 extends Model
{
    use HasFactory;

    protected $table = 'asopa07';

    protected $primaryKey = "numero";

    public $timestamps = false;

    protected $fillable = [
        "numero",
        "nomtit",
        "numtar",
        "saldo",
        "estado",
        "fecha"
    ];

    public function getEstado07Array()
    {
        return array(
            "ACC" => "ACTIVAR CON CAMBIO DE CLAVE",
            "NOR" => "NORMAL"
        );
    }

    public function getEstado07Detalle()
    {
        switch ($this->estado) {
            case 'ACC':
                return 'ACTIVAR CON CAMBIO DE CLAVE';
                break;
            case 'NOR':
                return 'NORMAL';
                break;
        }
    }
}
