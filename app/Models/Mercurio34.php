<?php
namespace App\Models;
use App\Exceptions\DebugException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Mercurio34 extends Model
{
    use HasFactory;
    
    protected $table = "mercurio34";

    public $timestamps = false;
    
    protected $primaryKey = "id";
    
    public $autoincrement = true;

    protected $fillable = [
        "id",
        "log",
        "nit",
        "cedtra",
        "cedcon",
        "numdoc",
        "tipdoc",
        "priape",
        "segape",
        "prinom",
        "segnom",
        "fecnac",
        "ciunac",
        "sexo",
        "parent",
        "huerfano",
        "tiphij",
        "nivedu",
        "captra",
        "tipdis",
        "calendario",
        "usuario",
        "estado",
        "codest",
        "motivo",
        "fecest",
        "codben",
        "tipo",
        "coddoc",
        "documento",
        "cedacu",
        "fecsol"
    ];

    public function __construct()
    {
        parent::__construct();
        $this->setConnection('mercurio');
    }

    public function isValid($rules='')
    {
        if(empty($rules)){
            $rules = $this->getRules();
        }
        $validator = Validator::make($this->attributes,  $rules, [
            'required' => 'El campo :attribute es requirido.',
            'same'     => 'El :attribute y :other deben coincidir.',
            'size'     => 'El :attribute debe ser exactamente :size.',
            'between'  => 'El :attribute valor :input no está entre :min - :max.',
            'in'       => 'El :attribute debe ser uno de los siguientes valores: :values',
            'numeric'  => 'El :attribute el valor no es númerico',
            'email.required' => 'Se requiere la dirección de email!',
            'digits_between' => 'El :attribute el valor debe estar en los rangos predefinidos'
        ]);
        if ($validator->fails()){
            return $validator->errors();
        }
        return true;
    }

    public function getRules()
    {
        return [
            "log" => "numeric|digits_between:1,11",
            "nit" => "numeric|digits_between:1,15",
            "cedtra" => "required|numeric|digits_between:5,15",
            "cedcon" => "required|numeric|digits_between:5,15",
            "tipdoc" => "required|numeric|digits_between:1,11",
            "numdoc" => "required|numeric|digits_between:5,15",
            "priape" => "required|max:20",
            "segape" => "required|max:20",
            "prinom" => "required|max:20",
            "segnom" => "required|max:20",
            "fecnac" => "required|date",
            "ciunac" => "required|max:5",
            "sexo" => "required|max:1|in:F,I,M",
            "nivedu" => "required|numeric|digits_between:1,14",
            "parent" => "required|numeric|in:1,2,3",
            "huerfano"=> "numeric|in:0,1,2",
            "tiphij"=> "required|numeric|in:0,1,2",
            "usuario" => "required|numeric|digits_between:1,5",
            "estado" => "required|max:1",
            "captra" => "required|max:1|in:N,I",
            "tipdis" => "required|numeric|digits_between:0,7",
            "calendario" => "required|in:A,B,N",
            "tipo" => "required|max:2",
            "coddoc" => "required|max:2",
            "documento" => "required|numeric|digits_between:1,15",
            "fecsol" => "required|date",
            "cedacu" =>"numeric|digits_between:0,15",
            "codest" => "max:2",
            "motivo" => "max:100",
            "fecest" => "date",
            "codben" => "numeric|digits_between:1,11"
        ];
    }

    public function mapperAttribute()
    {
        return array(
            "cedula_trabajador"=> "cedtra",
            "cedula_conyuge"=> "cedcon",
            "tipo_documento"=> "tipdoc",
            "primer_apellido"=> "priape",
            "segundo_apellido"=> "segape",
            "primer_nombre"=> "prinom",
            "segundo_nombre"=> "segnom",
            "fecha_nacimiento"=> "fecnac",
            "codigo_ciudad_nacimiento"=> "ciunac",
            "capacidad_trabajar"=> "captra",
            "nivel_educativo"=> "nivedu",
            "fecha_solicitud"=> 'fecsol',
            "fecha_estado"=>"fecest",
            "codigo_documento_empleador" => "coddoc",
            "numero_documento"=> "numdoc",
            "tipo_hijo"=>"tiphij",
            "parentesco"=>"parent",
            "cedula_acudiente"=> "cedacu",
            "tipo_discapacidad" => "tipdis"
        );
    }

}
