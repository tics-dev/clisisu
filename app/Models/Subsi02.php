<?php
namespace App\Models;

use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Thiagoprz\CompositeKey\HasCompositeKey;

class Subsi02 extends Model
{
	use HasCompositeKey;
	use HasFactory;
	
	protected $table = "subsi02";
	
	public $timestamps = false;

	protected $primaryKey = "nit";

	public $autoincrement = false;

	protected $fillable = [
		"nit",
		"digver",
		"tipper",
		"coddoc",
		"razsoc",
		"priape",
		"segape",
		"prinom",
		"segnom",
		"nomemp",
		"sigla",
		"cedrep",
		"repleg",
		"jefper",
		"cedpro",
		"nompro",
		"direccion",
		"codciu",
		"telefono",
		"fax",
		"email",
		"codzon",
		"dirpri",
		"telpri",
		"ciupri",
		"ofiafi",
		"codase",
		"calemp",
		"tipemp",
		"tipsoc",
		"tipapo",
		"forpre",
		"pymes",
		"contratista",
		"colegio",
		"todmes",
		"matmer",
		"codact",
		"codind",
		"feccer",
		"actapr",
		"fecapr",
		"fecafi",
		"fecsis",
		"estado",
		"resest",
		"codest",
		"fecest",
		"tottra",
		"totapo",
		"totcon",
		"tothij",
		"tother",
		"totpad",
		"fecmer",
		"tipdur",
		"feccor",
		"feccam",
		"valmor",
		"permor",
		"fosfec",
		"telt",
		"telr",
		"mailr",
		"mailt",
		"giass",
		"actugp",
		"correo",
		"nota",
		"coddocrepleg",
		"priaperepleg",
		"segaperepleg",
		"prinomrepleg",
		"segnomrepleg",
		"codcaj"
	];

	public function getSucursales()
	{
		return Subsi48::where('nit', $this->nit)->get();
	}

	public function getListas()
	{
		return Subsi73::where('nit', $this->nit)->get();
	}

	public function getTrayectorias()
	{
		return Subsi03::where('nit', $this->nit)->get();
	}
	
	public function trayectoria()
	{
		return $this->belongsToMany(Subsi03::class, 'subsi02', 'nit', 'nit');
	}

	public function actividadEmpresa()
	{
		return $this->belongsTo(Subsi04::class, 'codact', 'codact');
	}

	public function isValid($rules)
    {
        $validator = Validator::make($this->attributes,  $rules, [
            'required' => 'El :attribute campo es requirido.',
            'same'     => 'El :attribute and :other must match.',
            'size'     => 'El :attribute must be exactly :size.',
            'between'  => 'El :attribute valor :input no esta entre :min - :max.',
            'in'       => 'El :attribute must be one of the following types: :values',
            'email.required' => 'Se requiere la dirección de email!',
        ]);
        if ($validator->fails()){
            return $validator->errors();
        }
        return true;
    }

	public function reglas_creacion()
	{
		return array(
            "nit"     => "required|max:15",
            "coddoc"  => "required|max:2",
            "fecafi"  => "required|date",
            "fecsis"  => "required|date",
            "nomemp"  => "required|max:180",
            "codciu"  => "required|max:5",
			"repleg"  => "required|max:60",
			"cedrep"  => "required|max:15",
			"telefono"=> "max:10",	
			"codzon"  => "required|max:5",
			"codact"  => "required|max:4",
			"estado"  => "required|max:1",
			"razsoc"  => "required|max:100",
			"email"   => "email|max:120"
        );
	}
	
}
