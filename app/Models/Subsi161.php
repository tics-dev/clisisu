<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Subsi161 class
 * Agente que posee la novedad
 */
class Subsi161 extends Model
{
    use HasFactory;
    protected $table = "subsi161";
    public $timestamps = false;
    protected $primaryKey = "idreg";
    protected $fillable = [
        "idreg",
        "cedtra",
        "documento",
        "priape",
        "segape",
        "prinom",
        "segnom"   
    ];

    /**
     * novedades function
     * agente posee a muchas novedades
     * @return void
     */
    public function novedades_trabajador()
	{
		return $this->belongsToMany(Subsi160::class, 'subsi161', 'idreg', 'idreg');
	}
}
