<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Thiagoprz\CompositeKey\HasCompositeKey;

class Subsi03 extends Model
{
	use HasCompositeKey;
	use HasFactory;

	protected $table = "subsi03";

	public $timestamps = false;

	protected $primaryKey = ["nit", "fecafi"];

	public $autoincrement = false;

	protected $fillable = [
		"nit",
		"fecafi",
		"fecret",
		"calemp"
	];

	public function empresa()
	{
		return $this->belongsTo(Subsi02::class, 'nit', 'nit');
	}
}
