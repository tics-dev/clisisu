<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Saldos extends Model
{
    use HasFactory;
    protected $table = "saldos";
    public $timestamps = false;
    protected $primaryKey = 'numero';
    protected $fillable = [
        'numero',
        'cedres',
        'periodo',
        'saldo',
        'numtar',
        'fecha',
        'estado',
        'nota',
        'fecgir',
        'fecpro'
    ];

    public function getEstadoSaldosArray()
    {    
        return array(
            "A"=>"Adicionado",
            "P"=>"Procesado"
        );
    }

    public function getEstadoSaldoDetalle()
    {
        switch($this->estado)
        {
            case 'P': 
                return 'Procesado'; 
            break;
            case 'A': 
                return 'Adicionado'; 
            break;        
        }
    }

}