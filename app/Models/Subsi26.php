<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subsi26 extends Model
{
	use HasFactory;
	
	protected $table = "subsi26";
	
	public $timestamps = false;

	protected $primaryKey = "numero";

	public $autoincrement = false;
	
	protected $fillable = [
        'numero',
        'codben',
        'parent',
        'captra',
        'codcer',
        'fecent',
        'fecpre',
        'nomcol',
        'numres',
        'fecres',
        'codciu',
        'codgra',
        'fecsis'
	];

}