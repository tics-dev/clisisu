<?php
namespace App\Models;
use App\Exceptions\DebugException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Mercurio07 extends Model
{
    use HasFactory;
    
    protected $table = "mercurio07";

    public $timestamps = false;
    
    protected $primaryKey = "documento";
    
    protected $fillable = [
        "tipo",
        "coddoc",
        "documento",
        "nombre",
        "email",
        "clave",
        "feccla",
        "autoriza",
        "codciu",
        "fecreg",
        "estado"    
    ];

    public function __construct()
    {
        parent::__construct();
        $this->setConnection('mercurio');
    }

    public function isValid($rules)
    {
        $validator = Validator::make($this->attributes,  $rules, [
            'required' => 'El :attribute campo es requirido.',
            'same'     => 'El :attribute and :other must match.',
            'size'     => 'El :attribute must be exactly :size.',
            'between'  => 'El :attribute value :input is not between :min - :max.',
            'in'       => 'El :attribute must be one of the following types: :values',
            'email.required' => 'Se requiere la dirección de email!',
        ]);
        if ($validator->fails()){
            return $validator->errors();
        }
        return true;
    }

    public function getRules()
    {
        return [
            "tipo" => "required|max:2",
            "coddoc" => "required|max:2",
            "documento" => "required|max:15",
            "nombre" => "required|max:90",
            "email" => "required|max:60",
            "clave" => "required|max:255",
            "feccla" => "required|date",
            "autoriza" => "required|max:1",
            "codciu" => "required|max:5",
            "fecreg" => "required|date",
            "estado" => "required|max:1"
        ];
    }

}
