<?php

namespace App\Models;

use Thiagoprz\CompositeKey\HasCompositeKey;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subsi168 extends Model
{
    use HasFactory;
    use HasCompositeKey;

    protected $table = "subsi168";

    public $timestamps = false;

    protected $primaryKey = ["cedtra", "nit", "codsuc"];

    public $autoincrement = false;

    protected $fillable = [
        "nit",
        "codsuc",
        "codlis",
        "cedtra",
        "agro",
        "codzon",
        "salario",
        "tipsal",
        "fecsal",
        "tipcon",
        "tipjor",
        "feccon",
        "trasin",
        "tipcot",
        "vendedor",
        "empleador",
        "fecpre",
        "fecafi",
        "fecsis",
        "estado",
        "codest",
        "fecest",
        "usuario",
        "codgir2",
        "giro2",
        "horas",
    ];

    public function getTrayectoria()
    {
        return Subsi16::where('cedtra', $this->cedtra)
            ->where('nit', $this->nit)
            ->where('codsuc', $this->codsuc)
            ->get();
    }

    public function getTipoDocumento()
    {
        $trabajador = Subsi15::where('cedtra', $this->cedtra)->get()->first();
        return Gener18::where('coddoc', $trabajador->coddoc)->get()->first();
    }

    public function getTrayectoriaByEmpresa($nit = '')
    {
        if ($nit == '') $nit = $this->nit;
        return Subsi16::where('cedtra', $this->cedtra)->where('nit', $nit)->get();
    }

    public function getSalarios()
    {
        return Subsi17::where('cedtra', $this->cedtra)->get();
    }

    public function reglas_creacion()
    {
        return array(
            "nit"     => "required|max:13",
            "codsuc"  => "required|max:3",
            "codlis"  => "required|max:3",
            "priape"  => "required|max:45",
            "prinom"  => "required|max:45",
            "segape"  => "max:45",
            "segnom"  => "max:45",
            "horas"   => "required",
            "coddoc"  => "required|max:2",
            "fecafi"  => "required|date",
            "fecsis"  => "required|date",
            "codciu"  => "required|max:2",
            "codzon"  => "required|max:2",
            "estado"  => "required|max:1"
        );
    }

    public function isValid($rules)
    {
        $validator = Validator::make($this->attributes,  $rules, [
            'required' => 'El :attribute campo es requirido.',
            'same'     => 'El :attribute and :other must match.',
            'size'     => 'El :attribute must be exactly :size.',
            'between'  => 'El :attribute valor :input no esta entre :min - :max.',
            'in'       => 'El :attribute must be one of the following types: :values',
            'email.required' => 'Se requiere la dirección de email!',
        ]);
        if ($validator->fails()) {
            return $validator->errors();
        }
        return true;
    }
}
