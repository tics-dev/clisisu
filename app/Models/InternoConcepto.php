<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InternoConcepto extends Model
{
    use HasFactory;

    protected $table = "interno_conceptos";

    public $timestamps = false;

    protected $primaryKey = 'id';

    protected $fillable = [
        "id",
        "concepto",
        "tipo"
    ];

    public function getTiposArray()
    {
        return array(
            "A"=>"ABONO",
            "D"=>"DEBITO"
        );
    }

    public function getTipoDetalle()
    {
        switch($this->tipo)
        {
            case 'A': 
                return 'ABONO'; 
            break;
            case 'D': 
                return 'DEBITO'; 
            break;
        }
    }

}