<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subsi169 extends Model
{
    use HasFactory;
    protected $table = "subsi169";
    public $timestamps = false;
    protected $primaryKey = "tipcon";
    protected $fillable = [
        "tipcon",
        "tipmot",
        "dias"
    ];
}