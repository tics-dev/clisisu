<?php

namespace App\Models;

use App\Exceptions\DebugException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CuotaComprobante extends Model
{
    use HasFactory;

    protected $table = "cuotas_comprobantes";

    public $timestamps = false;

    protected $primaryKey = "id";

    protected $fillable = [
        'id',
        'nocobrada',
        'cedula',
        'documento',
        'comprobante',
        'valor_detalle',
        'estado',
        'fecha_detalle'
    ];

    public function getArrayEstados()
    {
        return [
            "C" => 'Cancelado',
            "P" => 'Pendiente',
            "D" => 'Debitado',
            "X" => 'Liquidado previamente'
        ];
    }
}
