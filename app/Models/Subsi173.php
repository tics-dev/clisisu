<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subsi173 extends Model
{
    use HasFactory;

    protected $table = "subsi173";

    public $timestamps = false;

    protected $primaryKey = "idreg";

    protected $fillable = [
        'idreg',
        'usuario',
        'fecdig',
        'nit',
        'codsuc',
        'acccob',
        'fecacc',
        'fecliq',
        'numfir',
        'periodos',
        'valor',
        'nota',
        'nomcon',
        'telcon',
        'concob',
        'ciecas'
    ];
}
