<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subsi04 extends Model
{
	use HasFactory;
	
	protected $table = "subsi04";
	
	public $timestamps = false;

	protected $primaryKey = "codact";

	public $autoincrement = false;
	
	protected $keyType = 'string';
	
	protected $fillable = [
        "codact",
        "detalle",
        "en_uso",
        "esagro"
	];

	public function sucursalesActividad()
	{
		return $this->belongsToMany(Subsi48::class, 'subsi04', 'codact', 'codact');
	}

	public function empresasActividad()
	{
		return $this->belongsToMany(Subsi02::class, 'subsi04', 'codact', 'codact');
	}

}