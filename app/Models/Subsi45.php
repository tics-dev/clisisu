<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Subsi45 class
 * lista de ocupaciones conyugues
 */
class Subsi45 extends Model
{
    use HasFactory;
    
    protected $table = "subsi45";

    public $timestamps = false;

    public $autoincrement = false;

    protected $primaryKey = "codocu";

    protected $keyType = 'string';

    protected $fillable = [
        "codocu",
        "detalle"
    ];
}