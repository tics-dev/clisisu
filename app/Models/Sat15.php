<?php
namespace App\Models;
use App\Exceptions\DebugException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

//estado pago de aportes
class Sat15 extends Model
{
    use HasFactory;

    protected $table = "sat15";

    public $timestamps = false;

    protected $primaryKey = "numtraccf";

    public $autoincrement = false;

    protected $fillable =[
        "numtraccf",
        "tipdocemp",
        "numdocemp",
        "sersat",
        "estpag"
    ];

    public function getCamposSat15()
    {
        return array(
            'numtraccf' => "NumeroRadicado",
            'tipdocemp' => "TipoDocumentoEmpleador",
            'numdocemp' => "NumeroDocumentoEmpleador",
            'sersat'    => "SerialSAT",
            'estpag'    => "EstadoPago"
        );
    }

    public function isValid($rules)
    {
        $validator = Validator::make($this->attributes,  $rules, [
            'required' => 'El :attribute campo es requirido.',
            'same'     => 'El :attribute and :other must match.',
            'size'     => 'El :attribute must be exactly :size.',
            'between'  => 'El :attribute value :input is not between :min - :max.',
            'in'       => 'El :attribute must be one of the following types: :values',
            'email.required' => 'Se requiere la dirección de email!',
        ]);
        if ($validator->fails()){
            return $validator->errors();
        }
        return true;
    }
}