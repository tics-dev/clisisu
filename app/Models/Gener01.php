<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gener01 extends Model
{
    use HasFactory;
    
    protected $table = "gener01";

    protected $primaryKey = "codapl";
    
    public $autoincrement = false;

    protected $fillable = [
        "sigla",
        "direccion",
        "telefono",
        "fax",
        "usrcal",
        "tipcon",
        "retenedor",
        "reteica",
        "reteiva",
        "codciu",
        "control",
        "codapl",
        "audtra",
        "fecha",
        "hora",
        "sofnumlic"
    ];
}