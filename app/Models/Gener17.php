<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gener17 extends Model
{
    use HasFactory;
    
    protected $table = "gener17";

    public $timestamps = false;

    protected $keyType = 'string';

    public $autoincrement = false;

    protected $primaryKey = "codsex";

    protected $fillable = [
        "codsex",
        "detsex",
        "codsex_circular"
    ];

    public function trabajador()
	{
		return $this->belongsTo(Subsi15::class, 'sexo', 'codsex');
	}
}