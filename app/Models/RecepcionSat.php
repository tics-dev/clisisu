<?php
namespace App\Models;
use App\Exceptions\DebugException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class RecepcionSat extends Model
{
    use HasFactory;
    
    protected $table = "recepcionsat";

    public $timestamps = false;
    
    protected $primaryKey = "id";
    
    public $autoincrement = true;

    protected $fillable = [
        'contenido',
        'numero_transaccion',
        'fecha'
    ];

    public function __construct()
    {
        parent::__construct();
        $this->setConnection('mercurio');
    }

}
