<?php

namespace App\Models;
use App\Exceptions\DebugException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sat08 extends Model
{
    use HasFactory;

    protected $table = "sat08";

    public $timestamps = false;

    protected $primaryKey = "numtraccf";

    public $autoincrement = false;

    protected $fillable = [
        "numtraccf",
        "tipper",
        "tipdocemp",
        "numdocemp",
        "sersat",
        "fecper",
        "razsoc",
        "coddep",
        "causa",
        "priape",
        "prinom",
        "estado",
        "resultado",
        "mensaje",
        "codigo"
    ];

    public function getCamposSat08()
    {
        return array(
            'numtraccf' => "NumeroRadicadoSolicitud",
            'tipper'    => "TipoPersona",
            'tipdocemp' => "TipoDocumentoEmpleador",
            'numdocemp' => "NumeroDocumentoEmpleador",
            'sersat'    => "SerialSAT",
            'fecper'    => "FechaPerdidaAfiliacion",
            'razsoc'    => "RazonSocial",
            'causa'     => "CausalRetiro",
            'coddep'    => "Departamento",
            'priape'    => "PrimerApellidoEmpleador",
            'prinom'    => "PrimerNombreEmpleador",
            'estado'    => "EstadoReporte"
        );
    }

    public function isValid($rules)
    {
        $validator = Validator::make($this->attributes,  $rules, [
            'required' => 'El :attribute campo es requirido.',
            'same'     => 'El :attribute and :other must match.',
            'size'     => 'El :attribute must be exactly :size.',
            'between'  => 'El :attribute value :input is not between :min - :max.',
            'in'       => 'El :attribute must be one of the following types: :values',
            'email.required' => 'Se requiere la dirección de email!',
        ]);
        if ($validator->fails()){
            return $validator->errors();
        }
        return true;
    }
}
