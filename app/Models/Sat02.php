<?php
namespace App\Models;

use App\Exceptions\DebugException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sat02 extends Model
{
    use HasFactory;

    protected $table = "sat02";

    public $timestamps = false;
    
    protected $primaryKey = "numtraccf";
    
    public $autoincrement = false;

    protected $fillable =[
        "numtraccf",
        "numtrasat",
        "tipper",
        "tipemp",
        "tipdocemp",
        "numdocemp",
        "sersat",
        "prinom",
        "segnom",
        "priape",
        "segape",
        "fecsol",
        "perafigra",
        "fecafi",
        "razsoc",
        "matmer",
        "coddep",
        "codmun",
        "direccion",
        "telefono",
        "email",
        "tipdocrep",
        "numdocrep",
        "prinom2",
        "segnom2",
        "priape2",
        "segape2",
        "autmandat",
        "autenvnot",
        "noafissfant",
        "resultado",
        "mensaje",
        "codigo"
    ];

    public function getCamposSat02()
    {
        return array(
            'numtraccf'   => "NumeroRadicadoSolicitud",
            'numtrasat'   => "NumeroTransaccion",
            'tipper'      => "TipoPersona",
            'tipemp'      => "NaturalezaJuridicaEmpleador",
            'tipdocemp'   => "TipoDocumentoEmpleador",
            'numdocemp'   => "NumeroDocumentoEmpleador",
            'sersat'      => "SerialSAT",
            'prinom'      => "PrimerNombreEmpleador",
            'segnom'      => "SegundoNombreEmpleador",
            'priape'      => "PrimerApellidoEmpleador",
            'segape'      => "SegundoApellidoEmpleador",
            'fecsol'      => "FechaSolicitud",
            'perafigra'   => "PerdidaAfiliacionCausaGrave",
            'fecafi'      => "FechaEfectivaAfiliacion",
            'razsoc'      => "RazonSocial",
            'matmer'      => "NumeroMatriculaMercantil",
            'coddep'      => "Departamento",
            'codmun'      => "Municipio",
            'direccion'   => "DireccionContacto",
            'telefono'    => "NumeroTelefono",
            'email'       => "CorreoElectronico",
            'tipdocrep'   => "TipoDocumentoRepresentante",
            'numdocrep'   => "NumeroDocumentoRepresentante",
            'prinom2'     => "PrimerNombreRepresentante",
            'segnom2'     => "SegundoNombreRepresentante",
            'priape2'     => "PrimerApellidoRepresentante",
            'segape2'     => "SegundoApellidoRepresentante",
            'autmandat'   => "AutorizacionManejoDatos",
            'autenvnot'   => "AutorizacionNotificaciones",
            'noafissfant' => "Manifestacion"
        );
    }

    public function tiposDocumentoEmpleador()
    {
        return [
            'NI'=> 'Número de identificación tributaria',
            'CC'=> 'Cédula de ciudadanía',
            'CE'=> 'Cédula de extranjería',
            'CD'=> 'Carné diplomático',
            'PE'=> 'Permiso especial de permanencia'
        ];
    }

    public function naturalezasJuridicas()
    {
        return [
            '1'=> 'Pública',
            '2'=> 'Privada',
            '3'=> 'Mixta',
            '4'=> 'Organismos multilaterales',
            '5'=> 'Entidades de derecho público no sometidas a legislación colombiana'
        ];
    }

    public function getTipoPersona()
    {
        return [
            'N'=> 'Natural',
            'J'=> 'Jurídica'
        ];
    }

    public function isValid($rules)
    {
        $validator = Validator::make($this->attributes,  $rules, [
            'required' => 'El :attribute campo es requirido.',
            'same'     => 'El :attribute and :other must match.',
            'size'     => 'El :attribute must be exactly :size.',
            'between'  => 'El :attribute value :input is not between :min - :max.',
            'in'       => 'El :attribute must be one of the following types: :values',
            'email.required' => 'Se requiere la dirección de email!',
        ]);
        if ($validator->fails()){
            return $validator->errors();
        }
        return true;
    }

    public function sat20()
	{
		return $this->belongsTo(Sat20::class, 'numtraccf', 'numtraccf');
	}
}
