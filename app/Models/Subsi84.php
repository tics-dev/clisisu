<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Subsi84 extends Model
{
	use HasFactory;
	
	protected $table ='subsi84';
	
	public $timestamps = false;
	
	protected $primaryKey = "codban";

	public $autoincrement = false;

    protected $keyType = 'string';

	protected $fillable = [
		"codban",
        "detalle",
		"codban1"
	];
}