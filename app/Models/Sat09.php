<?php

namespace App\Models;
use App\Exceptions\DebugException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sat09 extends Model
{
    use HasFactory;

    protected $table = "sat09";
    
    public $timestamps = false;

    protected $primaryKey = "numtraccf";

    public $autoincrement = false;

    protected $fillable = [
        "numtraccf",
        "numtrasat",
        "tipdocemp",
        "numdocemp",
        "sersat",
        "tipini",
        "fecini",
        "tipdoctra",
        "numdoctra",
        "priape",
        "segape",
        "prinom",
        "segnom",
        "sexo",
        "fecnac",
        "coddep",
        "codmun",
        "direccion",
        "telefono",
        "email",
        "salario",
        "tipsal",
        "hortra",
        "autmandat",
        "autenvnot",
        "resultado",
        "mensaje",
        "codigo"
    ];

    public function getCamposSat09()
    {
        return array(
            'tipini'    => "TipoInicio",
            'fecini'    => "FechaInicio",
            'segnom'    => "SegundoNombre",
            'segape'    => "SegundoApellido",
            'sexo'      => "Sexo",
            'fecnac'    => "FechaNacimiento",
            'coddep'    => "Departamento",
            'codmun'    => "Municipio",
            'direccion' => "Direccion",
            'telefono'  => "Telefono",
            'email'     => "Correo",
            'salario'   => "Salario",
            'tipsal'    => "TipoSalario",
            'hortra'    => "HorasTrabajo",
            'numtraccf' => "NumeroRadicadoSolicitud",
            'numtrasat' => "NumeroTransaccion",
            'tipdocemp' => "TipoDocumentoEmpleador",
            'numdocemp' => "NumeroDocumentoEmpleador",
            'sersat'    => "SerialSAT",
            'tipdoctra' => "TipoDocumentoTrabajador",
            'numdoctra' => "NumeroDocumentoTrabajador",
            'prinom'    => "PrimerNombre",
            'priape'    => "PrimerApellido",
            'autmandat' => "AutorizacionManejoDatos",
            'autenvnot' => "AutorizacionNotificaciones"
        );
    }

    public function isValid($rules)
    {
        $validator = Validator::make($this->attributes,  $rules, [
            'required' => 'El :attribute campo es requirido.',
            'same'     => 'El :attribute and :other must match.',
            'size'     => 'El :attribute must be exactly :size.',
            'between'  => 'El :attribute value :input is not between :min - :max.',
            'in'       => 'El :attribute must be one of the following types: :values',
            'email.required' => 'Se requiere la dirección de email!',
        ]);
        if ($validator->fails()){
            return $validator->errors();
        }
        return true;
    }
}
