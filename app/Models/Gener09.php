<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gener09 extends Model
{
    use HasFactory;
    
    protected $table = "gener09";

    public $timestamps = false;

    protected $keyType = 'string';

    protected $fillable = [
        "codzon",
        "detzon"
    ];

    public function trabajador()
	{
		return $this->belongsTo(Subsi15::class, 'codzon', 'codzon');
	}
}