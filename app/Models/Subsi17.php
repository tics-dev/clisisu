<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Thiagoprz\CompositeKey\HasCompositeKey;

class Subsi17 extends Model
{
    use HasCompositeKey;
    use HasFactory;
    
    protected $table = "subsi17";
    
    public $timestamps = false;
    
    protected $primaryKey = ["cedtra", "fecha"];

    public $autoincrement = false;

    protected $fillable = [
        "cedtra",
        "fecha",
        "salario"
    ];
}
