<?php

namespace App\Models;
use App\Exceptions\DebugException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sat10 extends Model
{
    use HasFactory;
    
    protected $table = "sat10";

    public $timestamps = false;

    protected $primaryKey = "numtraccf";

    public $autoincrement = false;

    protected $fillable = [
        "numtraccf",
        "numtrasat",
        "tipdocemp",
        "numdocemp",
        "sersat",
        "tipter",
        "fecter",
        "tipdoctra",
        "numdoctra",
        "priape",
        "prinom",
        "autmandat",
        "autenvnot",
        "resultado",
        "mensaje",
        "codigo"
    ];

    public function getCamposSat10()
    {
        return array(
            'tipter'    => "TipoTerminacion",
            'fecter'    => "FechaTerminacion",
            'numtraccf' => "NumeroRadicadoSolicitud",
            'numtrasat' => "NumeroTransaccion",
            'tipdocemp' => "TipoDocumentoEmpleador",
            'numdocemp' => "NumeroDocumentoEmpleador",
            'sersat'    => "SerialSAT",
            'tipdoctra' => "TipoDocumentoTrabajador",
            'numdoctra' => "NumeroDocumentoTrabajador",
            'prinom'    => "PrimerNombre",
            'priape'    => "PrimerApellido",
            'autmandat' => "AutorizacionManejoDatos",
            'autenvnot' => "AutorizacionNotificaciones"
        );    
    }

    public function isValid($rules)
    {
        $validator = Validator::make($this->attributes,  $rules, [
            'required' => 'El :attribute campo es requirido.',
            'same'     => 'El :attribute and :other must match.',
            'size'     => 'El :attribute must be exactly :size.',
            'between'  => 'El :attribute value :input is not between :min - :max.',
            'in'       => 'El :attribute must be one of the following types: :values',
            'email.required' => 'Se requiere la dirección de email!',
        ]);
        if ($validator->fails()){
            return $validator->errors();
        }
        return true;
    }
}
