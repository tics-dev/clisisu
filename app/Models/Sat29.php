<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sat29 extends Model
{
    use HasFactory;

    protected $table = "sat29";

    public $timestamps = false;

    protected $primaryKey = "codigo"; 

    public $autoincrement = false;

    protected $fillable =[
        "codigo",
        "detser",
        "nomsersat",
        "referencia",
        "client_id",
        "estado",
        "codapl"
    ];
}
