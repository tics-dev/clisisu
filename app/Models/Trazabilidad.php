<?php

namespace App\Models;

use App\Exceptions\DebugException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trazabilidad extends Model
{
    use HasFactory;
    
    protected $table = 'trasabilidad';

    protected $primaryKey = "id";

    public $timestamps = false;

    public $autoincrement = true;

    protected $fillable = [
        'id',
        'usuario',
        'detalle_evento',
        'modulo',
        'medio',
        'estado',
        'hora',
        'create_at'
    ];
}