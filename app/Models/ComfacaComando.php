<?php
namespace App\Models;

use App\Exceptions\DebugException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ComfacaComando extends Model
{
    use HasFactory;

    protected $table = "comandos";
    
    public $timestamps = false;
    
    protected $primaryKey = "id";

    protected $fillable = [
        "id",
        "fecha_runner",
        "hora_runner",
        "usuario",
        "progreso",
        "estado",
        "proceso",
        "linea_comando",
        "estructura",
        "parametros",
        "resultado"
    ];
}