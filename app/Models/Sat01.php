<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sat01 extends Model
{
    use HasFactory;

    protected $table = "sat01";

    public $timestamps = false;

    protected $primaryKey = "codapl";

    protected $keyType = 'string';

    protected $fillable =[
        "codapl",
        "control",
        "audtra",
        "password",
        "grant_type",
        "nit",
        "path"
    ];

}
