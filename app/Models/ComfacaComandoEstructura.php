<?php
namespace App\Models;

use App\Exceptions\DebugException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ComfacaComandoEstructura extends Model
{
    use HasFactory;

    protected $table = "comando_estructuras";
    
    public $timestamps = false;
    
    protected $primaryKey = "id";

    protected $fillable = [
        "id",
        "procesador",
        "estructura",
        "variables",
        "tipo",
        "sistema",
        "env",
        "descripcion",
        "asyncro"     
    ];
}