<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subsi172 extends Model
{
    use HasFactory;
    
    protected $table = "subsi172";

    public $timestamps = false;
    
    protected $primaryKey = "idreg";
    
    protected $fillable = [
        'idreg',
        'usuario',
        'fecgen',
        'horgen',
        'nit',
        'codsuc',
        'ofiafi',
        'email',
        'periodo',
        'valor',
        'fecenv',
        'mecenv',
        'enviado'
    ];
}