<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sat27 extends Model
{
    use HasFactory;

    protected $table = "sat27";

    public $timestamps = false;

    protected $primaryKey = "coddep";

    public $autoincrement = false;
    
    protected $fillable =[
        "coddep",
        "codmun",
        "codciu"
    ];

}