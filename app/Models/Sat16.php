<?php
namespace App\Models;

use App\Exceptions\DebugException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

//Consultar Solicitud Afiliacion Ccf Primera Vez Pruebas
class Sat16 extends Model
{
    use HasFactory;

    protected $table = "sat16";

    public $timestamps = false;
    
    protected $primaryKey = "numero_radicado";
    
    public $autoincrement = false;

    protected $fillable = [
        "numero_radicado",
        "fecha",
        "estado"
    ];

    public function getCamposSat16()
    {
        return array(
            'numero_radicado' => "NumeroTransaccion" 
        );
    }

    public function getEstadosSat16()
    {
        return [
            "Pediente de consultar" => "P",
            "Error"         => "X",
            "Consultado"    => "C" 
        ];
    }

    public function getEstadoSat16($estado=null)
    {
        if($estado){
            $this->estado = $estado;
        }
        switch ($this->estado) {
            case 'P':
                return "Pediente de consultar";
                break;
            case 'X':
                return 'Error';
                break;
            case 'C':
                return 'Consultado';
            break;
        }
    }

    public function isValid($rules)
    {
        $validator = Validator::make($this->attributes,  $rules, [
            'required' => 'El :attribute campo es requirido.',
            'same'     => 'El :attribute and :other must match.',
            'size'     => 'El :attribute must be exactly :size.',
            'between'  => 'El :attribute value :input is not between :min - :max.',
            'in'       => 'El :attribute must be one of the following types: :values',
            'email.required' => 'Se requiere la dirección de email!',
        ]);
        if ($validator->fails()){
            return $validator->errors();
        }
        return true;
    }

    public function getModeloConsultaSat16()
    {
        return [
            "TipoPersona"               => "tipper",
            "NumeroDocumentoEmpleador"  => "nit ",
            "PrimerNombreEmpleador"     => "prinom",
            "SegundoNombreEmpleador"    => "segnom",
            "PrimerApellidoEmpleador"   => "priape",
            "SegundoApellidoEmpleador"  => "segape",
            "FechaEfectivaAfiliacion"   => "fecha_aprobacion_sat",
            "NombreRazonSocial"         => "razsoc",
            "NumeroMatriculaMercantil"  => "matmer",
            "DireccionContacto"         => "direccion",
            "TelefonoContacto"          => "telefono",
            "CorreoElectronicoContacto" => "email",
            "NumeroDocumentoRepresentanteLegal" => "documento_representante_sat",
            "PrimerNombreRepresentanteLegal"    => "prinomrepleg",
            "SegundoNombreRepresentanteLegal"   => "segnomrepleg",
            "PrimerApellidoRepresentanteLegal"  => "priaperepleg",
            "SegundoApellidoRepresentanteLegal" => "segaperepleg"
        ];
    }
}
