<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PinAfiliado extends Model
{
    use HasFactory;
    
    protected $table ='pines_afiliado';

    protected $primaryKey = "id";

    public $timestamps = false;

    protected $fillable = [
        "id",
        "pin_id",
        "fecha",
        "cedtra",
        "numdoc",
        "documento",
        "coddoc",
        "tipo",
        "estado"
    ];

    public function getEstadosArray()
    {
        return [
            'A' => 'ACTIVO',
            'I' => 'INACTIVO'
        ];
    }
}