<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InternoSaldo extends Model
{
    use HasFactory;

    protected $table = "interno_saldos";

    public $timestamps = false;

    protected $primaryKey = 'id';

    protected $fillable = [
        "id",
        "cedula",
        "fecha_inicial",
        "fecha_ultima",
        "valor_inicial",
        "valor_acumulado",
        "estado"
    ];

    public function getEstadosArray()
    {
        return array(
            "P" => "PENDIENTE",
            "L" => "LIQUIDADO"
        );
    }

    public function getEstadoDetalle()
    {
        switch ($this->estado) {
            case 'P':
                return 'PENDIENTE';
                break;
            case 'L':
                return 'LIQUIDADO';
                break;
        }
    }

    public function abonos()
    {
        return $this->hasMany(InternoAbono::class, "interno_saldo", "id");
    }

    public function debitos()
    {
        return $this->hasMany(InternoDebito::class, "interno_saldo", "id");
    }
}
