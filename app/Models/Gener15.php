<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gener15 extends Model
{
    use HasFactory;
    
    protected $table = "gener15";

    public $timestamps = false;

    protected $keyType = 'string';

    protected $fillable = [
        "estciv",
        "detest",
        "estciv_circular"
    ];

    public function trabajador()
	{
		return $this->belongsTo(Subsi15::class, 'estciv', 'estciv');
	}
}