<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subsi56 extends Model
{
	use HasFactory;
	
	protected $table = "subsi56";
	
	public $timestamps = false;

	protected $primaryKey = "codcaj";

	public $autoincrement = false;

	protected $keyType = 'string';
	
	protected $fillable = [
        "codcaj",
        "detalle"
	];

}