<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Thiagoprz\CompositeKey\HasCompositeKey;

class Subsi13 extends Model
{
    use HasCompositeKey;
	use HasFactory;
	
	protected $table = "subsi13";
	
	public $timestamps = false;

	protected $primaryKey = ["nit", "codsuc", "periodo"];

    public $autoincrement = false;

	protected $fillable = [
        "nit",
        "codsuc",
        "periodo",
        "marca",
        "documento",
        "giro",
        "pernom",
        "usuario",
        "estnom"       
	];
    
}