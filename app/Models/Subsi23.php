<?php
namespace App\Models;

use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Thiagoprz\CompositeKey\HasCompositeKey;

class Subsi23 extends Model
{
    use HasCompositeKey;
    use HasFactory;

    protected $table = "subsi23";
    
    public $timestamps = false;
    
    protected $primaryKey = ["codben", "cedtra"];

    public $autoincrement = false;

    protected $fillable = [
        "cedtra",
        "cedcon",
        "fecnac",
        "numhij",
        "codben",
        "pago",
        "fecpre",
        "fecafi",
        "fecsis",
        "ruaf",
        "numrua",
        "fosfec",
        "fecrua",
        "cedacu"      
    ];

    public function relationTrabajador()
    {
        return $this->belongsTo(Subsi22::class, 'cedcon', 'cedcon');
    }

    public function reglas_creacion()
    {
        return array(
            "cedtra" => "required",
            "cedcon" => "required",
            "fecnac" => "required",
            "numhij" => "required",
            "codben" => "required",
            "pago" => "required",
            "fecpre" => "date",
            "fecafi" => "date",
            "fecsis" => "date",
        );
    }

    public function isValid($rules)
    {
        $validator = Validator::make($this->attributes,  $rules, [
            'required' => 'El :attribute campo es requirido.',
            'same'     => 'El :attribute and :other must match.',
            'size'     => 'El :attribute must be exactly :size.',
            'between'  => 'El :attribute valor :input no esta entre :min - :max.',
            'in'       => 'El :attribute must be one of the following types: :values',
            'email.required' => 'Se requiere la dirección de email!',
        ]);
        if ($validator->fails()){
            return $validator->errors();
        }
        return true;
    }

}