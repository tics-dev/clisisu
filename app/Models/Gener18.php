<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gener18 extends Model
{
    use HasFactory;
    
    protected $table = "gener18";

    public $timestamps = false;

    protected $keyType = 'string';

    protected $fillable = [
        "coddoc",
        "detdoc",
        "codrua",
        "coddoc_circular"
    ];

    public function trabajador()
	{
		return $this->belongsTo(Subsi15::class, 'coddoc', 'coddoc');
	}
}