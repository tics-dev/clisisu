<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Thiagoprz\CompositeKey\HasCompositeKey;

class Subsi35 extends Model
{
    use HasCompositeKey;
    use HasFactory;
    
    protected $table = "subsi35";

    public $timestamps = false;

    public $autoincrement = false;

    protected $primaryKey = ["cedcon","fecha"];

    protected $fillable = [
        "cedcon",
        "fecha",
        "salario"
    ];
}