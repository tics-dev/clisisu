<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gener36 extends Model
{
    use HasFactory;
    
    protected $table = "gener36";

    public $timestamps = false;

    protected $keyType = 'string';

    protected $fillable = [
        "codigo",
        "nombre"
    ];

    public function trabajador()
	{
		return $this->belongsTo(Subsi15::class, 'peretn', 'codigo');
	}
}