<?php
namespace App\Models;

use App\Exceptions\DebugException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Sat18 class
 * Solicitudes CCF
 * Donde el codigo_notificacion correponde al valor de respuesta para poder hacer las consultas del detalle
*/
class Sat18 extends Model
{
    use HasFactory;

    protected $table = "sat18";

    public $timestamps = false;
    
    protected $primaryKey = "id";
    
    public $autoincrement = true;

    protected $fillable = [
        "id",
        "numero_transaccion",
        "codigo_novedad",
        "fecha_creacion",
        "fecha_vigencia",
        "estado",
        "estado_flujo",
        "url"
    ];

    public function getCamposSat18()
    {
        return array(
            "numero_transaccion"=>"numero_transaccion",
            "codigo_novedad"=>"codigo_novedad",
            "fecha_creacion"=>"fecha_creacion",
            "fecha_vigencia"=>"fecha_vigencia",
            "estado_flujo"=>"Estado_Fujo",
            "url"=>"url"
        );
    }

    public function isValid($rules)
    {
        $validator = Validator::make($this->attributes,  $rules, [
            'required' => 'El :attribute campo es requirido.',
            'same'     => 'El :attribute and :other must match.',
            'size'     => 'El :attribute must be exactly :size.',
            'between'  => 'El :attribute value :input is not between :min - :max.',
            'in'       => 'El :attribute must be one of the following types: :values',
            'email.required' => 'Se requiere la dirección de email!',
        ]);
        if ($validator->fails()){
            return $validator->errors();
        }
        return true;
    }

    public function getEstadosSat18()
    {
        return [
            'A'=>'Activo',
            'C'=>'Para consulta',
            'P'=>'Pendiente',
            'F'=>'Finalizado',
            'X'=>'Error' 
        ];
    }

    public function getCodigoNovedadesSat18()
    {
        return [
            '1'=> 'Solicitud afiliación primera vez en un departamento',
            '2'=> 'Solicitud afiliación NO primera vez en un departamento',
            '3'=> 'Solicitud de desafiliación a una CCF',
            '4'=> 'Inicio de relación laboral',
            '5'=> 'Terminación de relación laboral',
            '6'=> 'Suspensión temporal del contrato de trabajo',
            '7'=> 'Licencias remuneradas y no remuneradas',
            '8'=> 'Modificación de Salario',
            '9'=> 'Retiro definitivo del empleador al SSF'
        ];
    }

}
