<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subsi41 extends Model
{
    use HasFactory;
    
    protected $table = "subsi41";

    public $timestamps = false;

    public $autoincrement = false;

    protected $primaryKey = "codgir";

    protected $keyType = 'string';

    protected $fillable = [
        "codgir",
        "detalle",
        "tipo"
    ];
}