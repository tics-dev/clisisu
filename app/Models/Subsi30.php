<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subsi30 extends Model
{
    use HasFactory;
    
    protected $table = "subsi30";

    public $timestamps = false;

    public $autoincrement = false;

    protected $primaryKey = "codcat";

    protected $keyType = 'string';

    protected $fillable = [
        "codcat",
        "detalle",
        "cansal",
        "pagsub",
        "codcat_circular"     
    ];
}