<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PuebloIndigena extends Model
{
    use HasFactory;
    
    protected $table = "pueblos_indigenas";
    
    public $timestamps = false;
    
    protected $primaryKey = 'id';
    
    protected $fillable = [
        'id',
        'codigo',
        'detalle'
    ];

}