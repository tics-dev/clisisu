<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gener35 extends Model
{
    use HasFactory;
    
    protected $table = "gener35";

    public $timestamps = false;

    protected $fillable = [
        "codigo",
        "nombre"
    ];

    protected $keyType = 'string';

    public function trabajador()
	{
		return $this->belongsTo(Subsi15::class, 'facvul', 'codigo');
	}
}