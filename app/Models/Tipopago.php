<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tipopago extends Model
{
    use HasFactory;
    
    protected $table = "tipopago";

    public $timestamps = false;

    protected $fillable = [
        "id",
        "detalle",
        "codigo"
    ];

    public function trabajador()
	{
		return $this->belongsTo(Subsi15::class, 'tippag', 'codigo');
	}
}