<?php
namespace App\Models;

use Thiagoprz\CompositeKey\HasCompositeKey;
use App\Exceptions\DebugException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Mercurio08 extends Model
{
    
    use HasCompositeKey;
    use HasFactory;
    
    protected $table = "mercurio08";

    public $timestamps = false;
    
    protected $primaryKey = ['codofi', 'tipopc', 'usuario'];

    public $incrementing = false;

    protected $keyType = 'string';

    protected $fillable = [
        "codofi",
        "tipopc",
        "usuario",
        "orden"
    ];

    public function __construct()
    {
        parent::__construct();
        $this->setConnection('mercurio');
    }

    public function isValid($rules)
    {
        $validator = Validator::make($this->attributes,  $rules, [
            'required' => 'El :attribute campo es requirido.',
            'same'     => 'El :attribute and :other must match.',
            'size'     => 'El :attribute must be exactly :size.',
            'between'  => 'El :attribute value :input is not between :min - :max.',
            'in'       => 'El :attribute must be one of the following types: :values',
            'email.required' => 'Se requiere la dirección de email!',
        ]);
        if ($validator->fails()){
            return $validator->errors();
        }
        return true;
    }

}
