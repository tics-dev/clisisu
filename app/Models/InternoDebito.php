<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InternoDebito extends Model
{
    use HasFactory;

    protected $table = "interno_debitos";

    public $timestamps = false;

    protected $primaryKey = 'id';

    protected $fillable = [
        "id",
        "interno_saldo",
        "fecha",
        "valor_debito",
        "cedula",
        "concepto",
        "usuario",
        "estado",
        "procesado",
        "numero_cuenta",
        "banco",
        "tipo_pago",
        "numero_cheque"
    ];

    public function getEstadosArray(): array
    {
        return array(
            "A" => "Adicionado",
            "P" => "Procesado",
            "X" => "Rechazado"
        );
    }

    public function getEstadoDetalle(): string
    {
        switch ($this->estado) {
            case 'A':
                return 'Adicionado';
                break;
            case 'P':
                return 'Procesado';
                break;
            case 'X':
                return 'Rechazado';
                break;
        }
    }

    public function getProcesadosArray(): array
    {
        return array(
            "S" => "SI",
            "N" => "NO"
        );
    }

    public function getProcesadoDetalle(): string
    {
        switch ($this->procesado) {
            case 'S':
                return 'SI';
                break;
            case 'N':
                return 'NO';
                break;
        }
    }

    /**
     * internoSaldo function
     * los Debitos pertenecen a InternoSaldo pro el interno_saldo key
     * @return void
     */
    public function internoSaldo()
    {
        return $this->belongsTo(InternoSaldo::class, 'interno_saldo');
    }
}
