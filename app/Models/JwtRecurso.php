<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JwtRecurso extends Model
{
    use HasFactory;

    protected $table = "jwt_recursos";

    public $timestamps = false;

    protected $primaryKey = "id";

    protected $fillable = [
        'id',
        'recurso',
        'cliente_id',
        'jwpassword',
        'estado'
    ];
}
