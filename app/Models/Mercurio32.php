<?php

namespace App\Models;

use App\Exceptions\DebugException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Mercurio32 extends Model
{
    use HasFactory;

    protected $table = "mercurio32";

    public $timestamps = false;

    protected $primaryKey = "id";

    public $autoincrement = true;

    protected $fillable = [
        "id",
        "log",
        "cedtra",
        "cedcon",
        "tipdoc",
        "priape",
        "segape",
        "prinom",
        "segnom",
        "fecnac",
        "ciunac",
        "sexo",
        "estciv",
        "comper",
        "tiecon",
        "ciures",
        "codzon",
        "tipviv",
        "direccion",
        "barrio",
        "telefono",
        "celular",
        "email",
        "nivedu",
        "fecing",
        "codocu",
        "salario",
        "tipsal",
        "captra",
        "usuario",
        "estado",
        "codest",
        "motivo",
        "fecest",
        "tipo",
        "coddoc",
        "documento",
        "fecsol",
        "tippag",
        "numcue",
        "empresalab",
        "codban"
    ];

    public function __construct()
    {
        parent::__construct();
        $this->setConnection('mercurio');
    }

    public function isValid($rules = '')
    {
        if (empty($rules)) {
            $rules = $this->getRules();
        }
        $validator = Validator::make($this->attributes,  $rules, [
            'required' => 'El campo :attribute es requirido.',
            'same'     => 'El :attribute y :other deben coincidir.',
            'size'     => 'El :attribute debe ser exactamente :size.',
            'between'  => 'El :attribute valor :input no está entre :min - :max.',
            'in'       => 'El :attribute debe ser uno de los siguientes valores: :values',
            'numeric'  => 'El :attribute el valor no es númerico',
            'email.required' => 'Se requiere la dirección de email!',
            'digits_between' => 'El :attribute el valor debe estar en los rangos predefinidos'
        ]);
        if ($validator->fails()) {
            return $validator->errors();
        }
        return true;
    }

    public function getRules()
    {
        return [
            "log" => "numeric|digits_between:1,11",
            "cedtra" => "required|numeric|digits_between:5,15",
            "cedcon" => "required|numeric|digits_between:5,15",
            "tipdoc" => "required|numeric|digits_between:1,11",
            "priape" => "required|max:20",
            "segape" => "required|max:20",
            "prinom" => "required|max:20",
            "segnom" => "required|max:20",
            "fecnac" => "required|date",
            "ciunac" => "required|max:5",
            "sexo" => "required|max:1|in:F,I,M",
            "estciv" => "required|numeric|digits_between:1,6",
            "comper" => "required|max:1|in:S,N",
            "tiecon" => "required|numeric|digits_between:0,3",
            "ciures" => "required|max:5",
            "codzon" => "required|max:5",
            "tipviv" => "required|max:1|in:F,I,P,A,H",
            "direccion" => "required|max:120",
            "barrio" => "required|max:48",
            "telefono" => "required|numeric|digits_between:1,13",
            "celular" => "required|numeric|digits_between:1,15",
            "email"  => "required|email|max:100",
            "nivedu" => "required|numeric|digits_between:1,14",
            "fecing" => "date",
            "codocu" => "required|numeric|digits_between:0,6",
            "salario" => "required|numeric|digits_between:1,11",
            "tipsal" => "max:1|in:F,I,V",
            "captra" => "required|max:1|in:N,I",
            "usuario" => "required|numeric|digits_between:1,5",
            "estado" => "required|max:1",
            "fecest" => "date",
            "tipo" => "required|max:2",
            "coddoc" => "required|max:2",
            "documento" => "required|numeric|digits_between:1,15",
            "fecsol" => "required|date"
        ];
    }

    public function mapperAttribute()
    {
        return array(
            "cedula_trabajador" => "cedtra",
            "cedula_conyuge" => "cedcon",
            "tipo_documento" => "tipdoc",
            "primer_apellido" => "priape",
            "segundo_apellido" => "segape",
            "primer_nombre" => "prinom",
            "segundo_nombre" => "segnom",
            "fecha_nacimiento" => "fecnac",
            "codigo_ciudad_nacimiento" => "ciunac",
            "estado_civil" => "estciv",
            "codigo_ciudad_residencia" => "ciures",
            "codigo_ciudad_laboral" => "codzon",
            "fecha_ingreso" => "fecing",
            "tipo_salario" => "tipsal",
            "capacidad_trabajar" => "captra",
            "nivel_educativo" => "nivedu",
            "fecha_solicitud" => 'fecsol',
            "companero_permanente" => "comper",
            "tipo_vivienda" => "tipviv",
            "tiempo_convivencia" => "tiecon",
            "fecha_estado" => "fecest",
            "codigo_documento_empleador" => "coddoc",
            "ocupacion" => 'codocu'
        );
    }
}
