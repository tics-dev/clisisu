<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Thiagoprz\CompositeKey\HasCompositeKey;

class Subsi48 extends Model
{
	use HasCompositeKey;
	use HasFactory;
	
	protected $table ='subsi48';
	
	public $timestamps = false;
	
	protected $primaryKey = ["nit", "codsuc"];

	public $autoincrement = false;
	
	
	protected $fillable = [
		"nit",
		"codsuc",
		"detalle",
		"subpla",
		"direccion",
		"codciu",
		"telefono",
		"fax",
		"codzon",
		"ofiafi",
		"nomcon",
		"email",
		"calsuc",
		"codact",
		"codind",
		"traapo",
		"valapo",
		"actapr",
		"fecafi",
		"feccam",
		"estado",
		"resest",
		"codest",
		"fecest",
		"tottra",
		"totapo",
		"totcon",
		"tothij",
		"tother",
		"totpad",
		"tietra",
		"tratot",
		"correo",
		"observacion"
	];

	/**
	 * empresa propietaria del la sucursal
	 */
	public function empresa()
	{
		return $this->belongsTo(Subsi02::class, 'nit', 'nit');
	}

	public function actividadSucursal()
	{
		return $this->belongsTo(Subsi04::class, 'codact', 'codact');
	}

	public function getActividadSucursal()
	{
		return Subsi04::where('codact', $this->codact)->get()->first();
	}
}