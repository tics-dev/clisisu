<?php

namespace App\Models;

use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sat13 extends Model
{
    use HasFactory;

    protected $table = "sat13";

    public $timestamps = false;

    protected $primaryKey = "numtraccf";

    public $autoincrement = false;

    protected $fillable = [
        "numtraccf",
        "numtrasat",
        "tipdocemp",
        "numdocemp",
        "tipdoctra",
        "numdoctra",
        "sersat",
        "prinom",
        "priape",
        "fecmod",
        "salario",
        "tipper",
        "tipsal",
        "autmandat",
        "autenvnot",
        "resultado",
        "mensaje",
        "codigo"       
    ];

    public function getCamposSat13()
    {
        return array(
            'numtraccf' => "NumeroRadicadoSolicitud",
			'numtrasat' => "NumeroTransaccion",
			'tipper'    => "TipoPersona",
			'tipdocemp' => "TipoDocumentoEmpleador",
			'numdocemp' => "NumeroDocumentoEmpleador",
			'sersat'    => "SerialSAT",
			'fecmod'    => "FechaModificacionSalario",
			'tipdoctra' => "TipoDocumentoTrabajador",
			'numdoctra' => "NumeroDocumentoTrabajador",
			'prinom'    => "PrimerNombreTrabajador",
			'priape'    => "PrimerApellidoTrabajador",
			'salario'   => "Salario",
			'tipsal'    => "TipoSalario",
			'autmandat' => "AutorizacionManejoDatos",
			'autenvnot' => "AutorizacionNotificaciones"
        );
    }

    public function isValid($rules)
    {
        $validator = Validator::make($this->attributes,  $rules, [
            'required' => 'El :attribute campo es requirido.',
            'same'     => 'El :attribute and :other must match.',
            'size'     => 'El :attribute must be exactly :size.',
            'between'  => 'El :attribute value :input is not between :min - :max.',
            'in'       => 'El :attribute must be one of the following types: :values',
            'email.required' => 'Se requiere la dirección de email!',
        ]);
        if ($validator->fails()){
            return $validator->errors();
        }
        return true;
    }
}
