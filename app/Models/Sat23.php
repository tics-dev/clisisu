<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sat23 extends Model
{
    use HasFactory;

    protected $table = "sat23";

    public $timestamps = false;

    protected $primaryKey = "tipdoc";

    public $autoincrement = false;
    
    protected $fillable =[
        "tipdoc",
        "detalle",
        "empafi",
        "logini",
        "logfin",
        "tipdat",
        "coddoc"
    ];

}