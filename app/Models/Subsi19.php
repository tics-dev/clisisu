<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subsi19 extends Model
{
	use HasFactory;
	
	protected $table = "subsi19";
	
	public $timestamps = false;

	protected $primaryKey = 'numero';

	protected $fillable = [  
        "numero",
        "cedtra",
        "cedcon",
        "cedres",
        "priape",
        "segape",
        "prinom",
        "segnom",
        "direccion",
        "telefono",
        "codzon",
        "tippag",
        "codcue",
        "ofides",
        "codgru",
        "codban",
        "numcue",
        "tipcue",
        "fecmue",
        "fecsis",
        "numcuo",
        "cuopag",
        "indapl",        
	];

}
