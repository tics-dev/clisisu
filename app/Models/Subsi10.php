<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Thiagoprz\CompositeKey\HasCompositeKey;

class Subsi10 extends Model
{
    use HasCompositeKey;
	use HasFactory;
	
	protected $table = "subsi10";
	
	public $timestamps = false;

	protected $primaryKey = ["nit", "codsuc", "periodo", "cedtra"];

    public $autoincrement = false;

	protected $fillable = [
        "nit",
        "codsuc",
        "periodo",
        "cedtra",
        "porapo",
        "hortra",
        "jushor",
        "vacaciones",
        "apovac",
        "suebas",
        "comision",
        "otrdeb",
        "ibc"
	];

    

}