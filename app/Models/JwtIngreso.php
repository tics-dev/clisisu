<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JwtIngreso extends Model
{
    use HasFactory;

    protected $table = "jwt_ingreso";

    public $timestamps = false;

    protected $primaryKey = "id";

    protected $fillable = [
        'id',
        'jwtoken',
        'dia',
        'hora',
        'http_cliente',
        'http_cliente_origen',
        'consumo',
        'estado'
    ];
}
