<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Asopa03 extends Model
{
    use HasFactory;

    protected $table ='asopa03';

    protected $primaryKey = "numero";
    
    public $timestamps = false;

    protected $fillable = [
		"numero",
        "fecha",
        "numtar",
        "cedres",
        "tipnov",
        "estado"
	];

    public function getTipnov03Array()
    {
        return array(
            "01"=>"Nuevo Afiliado",
            "02"=>"Reexpedicion",
            '03'=>'Aprobacion Cupos',
            "04"=>"Bloqueo de Tarjetas",
            "05"=>"Aprobacion cambios a cupos",
            "06"=>"Modificacion Datos basicos",
            "07"=>"Retiro Afiliado",
            "08"=>"Reactivacion de cupos",
            "09"=>"Bloqueo de Bolsillo",
            "10"=>"Translado de saldos"
        );
    }

    public function getTipnov03Detalle()
    {
        switch($this->tipnov)
        {
            case '01': 
                return 'Nuevo Afiliado'; 
                break;
            case '02': 
                return 'Reexpedicion'; 
                break;
            case '03': 
                return 'Aprobacion Cupos'; 
                break;
            case '04': 
                return 'Bloqueo Tarjetas'; 
                break;
            case '05': 
                return 'Aprobacion cambios a cupos'; 
                break;
            case '06': 
                return 'Modificacion Datos basicos'; 
                break;
            case '07': 
                return 'Retiro Afiliado'; 
                break;
            case '08': 
                return 'Reactivacion de cupos'; 
                break;
            case '09': 
                return 'Bloqueo de Bolsillo'; 
                break;
            case '10': 
                return 'Translado de Saldos'; 
                break;
        }
    }
    
    public function getEstado03Array()
    {
        return array(
            "A"=>"Adicionado",
            "P"=>"Procesado"
        );
    }

    public function getEstado03Detalle()
    {
        switch($this->estado){
            case 'A': 
                return 'Adicionado'; 
            break;
            case 'P': 
                return 'Procesado'; 
            break;
        }
    }

}