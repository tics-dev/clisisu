<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gener33 extends Model
{
    use HasFactory;
    
    protected $table = "gener33";

    public $timestamps = false;

    protected $keyType = 'string';

    protected $fillable = [
        "codocu",
        "detalle"
    ];

    public function trabajador()
	{
		return $this->belongsTo(Subsi15::class, 'cargo', 'codocu');
	}
}