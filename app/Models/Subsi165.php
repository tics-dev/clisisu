<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subsi165 extends Model
{
    use HasFactory;

    protected $table = "subsi165";

    public $timestamps = false;

    protected $primaryKey = "idreg";
    
    public $autoincrement = false;

    protected $fillable = [
        "idreg",
        "fecha",
        "hora",
        "usuario",
        "sec",
        "tipcon",
        "nomcon",
        "procon",
        "ciecas",
        "nota"
    ];
    
}