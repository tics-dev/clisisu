<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banco extends Model
{
    use HasFactory;

    protected $table = 'ba';

    protected $primaryKey = "codban";

    public $autoincrement = false;

    public $timestamps = false;

    protected $keyType = 'string';

    protected $fillable = [
        "codban",
        "detalle",
        "codban1"
    ];

}
