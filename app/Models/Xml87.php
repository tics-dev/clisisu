<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Thiagoprz\CompositeKey\HasCompositeKey;

class Xml87 extends Model
{
    use HasCompositeKey;
    use HasFactory;

    protected $table = "xml87";

    public $timestamps = false;

    protected $primaryKey = ["periodo", "cedtra", "codben"];

    public $autoincrement = false;

    protected $fillable = [
        "periodo",
        "docnit",
        "nit",
        "doctra",
        "cedtra",
        "coddoc",
        "codben",
        "documento",
        "prinom",
        "segnom",
        "priape",
        "segape",
        "fecnac",
        "sexo",
        "parent",
        "conesp",
        "tipcuo",
        "subliq",
        "subpen",
        "numcuo",
        "discap",
        "periodos",
        "tippag",
        "codmun",
        "area"       
    ];

}