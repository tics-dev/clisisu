<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subsi12 extends Model
{
	use HasFactory;
	
	protected $table = "subsi12";
	
	public $timestamps = false;

	protected $primaryKey = "periodo";

	protected $fillable = [
		"periodo",
		"fecini",
		"fecfin",
		"fectar",
		"camley",
		"fecuni",
		"fecreg",
		"pormor",
		"salmin",
		"cansal",
		"saltop",
		"girado"
	];

}
