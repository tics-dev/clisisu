<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subsi63 extends Model
{
	use HasFactory;
	
	protected $table = "subsi63";
	
	public $timestamps = false;

	protected $primaryKey = "ofiafi";

	public $autoincrement = false;

        protected $keyType = 'string';
	
	protected $fillable = [
        "ofiafi",
        "detalle",
        "direccion",
        "telefono",
        "nomrep",
        "email",
        "codzon"
	];

}