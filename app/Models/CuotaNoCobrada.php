<?php

namespace App\Models;

use App\Exceptions\DebugException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CuotaNoCobrada extends Model
{
    use HasFactory;

    protected $table = "cuotas_nocobradas";

    public $timestamps = false;

    protected $primaryKey = "id";


    protected $fillable = [
        'id',
        'cedula',
        'nombre',
        'cuenta_abonar',
        'valor',
        'fecha',
        'detalle',
        'estado',
        'banco'
    ];

    public function getArrayEstados()
    {
        return [
            "C" => 'Cancelado',
            "P" => 'Pendiente',
            "X" => 'Liquidado previamente'
        ];
    }
}
