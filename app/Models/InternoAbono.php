<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InternoAbono extends Model
{
    use HasFactory;

    protected $table = "interno_abonos";

    public $timestamps = false;

    protected $primaryKey = 'id';

    protected $fillable = [
        "id",
        "interno_saldo",
        "fecha",
        "valor_abono",
        "cedula",
        "concepto",
        "usuario",
        "estado",
        "procesado",
        "redime"
    ];

    public function getEstadosArray(): array
    {
        return array(
            "A" => "Adicionado",
            "P" => "Procesado",
            "X" => "Rechazado",
            "R" => "Redimido"
        );
    }

    public function getEstadoDetalle(): string
    {
        switch ($this->estado) {
            case 'A':
                return 'Adicionado';
                break;
            case 'P':
                return 'Procesado';
                break;
            case 'X':
                return 'Rechazado';
                break;
            case 'R':
                return 'Redimido';
                break;
        }
    }

    public function getProcesadosArray(): array
    {
        return array(
            "S" => "SI",
            "N" => "NO"
        );
    }

    public function getProcesadoDetalle(): string
    {
        switch ($this->procesado) {
            case 'S':
                return 'SI';
                break;
            case 'N':
                return 'NO';
                break;
        }
    }

    /**
     * internoSaldo function
     * los Abonos pertenecen a InternoSaldo pro el interno_saldo key
     * @return void
     */
    public function internoSaldo()
    {
        return $this->belongsTo(InternoSaldo::class, 'interno_saldo');
    }
}
