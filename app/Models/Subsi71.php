<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subsi71 extends Model
{
    use HasFactory;
    
    protected $table = "subsi71";

    public $timestamps = false;

    protected $keyType = 'string';

    protected $fillable = [
        "tipcot",
        "detalle"
    ];

    public function trabajador()
	{
		return $this->belongsTo(Subsi15::class, 'tipcot', 'tipcot');
	}
}