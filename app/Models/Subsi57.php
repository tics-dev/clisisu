<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subsi57 extends Model
{
    use HasFactory;
    
    protected $table = "subsi57";

    public $timestamps = false;

    protected $keyType = 'string';

    protected $fillable = [
        "tipdis",
        "detalle"
    ];

    public function trabajador()
	{
		return $this->belongsTo(Subsi15::class, 'tipdis', 'tipdis');
	}
}