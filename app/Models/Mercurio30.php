<?php
namespace App\Models;
use App\Exceptions\DebugException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Mercurio30 extends Model
{
    use HasFactory;
    
    protected $table = "mercurio30";

    public $timestamps = false;
    
    protected $primaryKey = "id";
    
    public $autoincrement = true;

    protected $fillable = [
        "id",
        "log",
        "nit",
        "tipdoc",
        "razsoc",
        "priape",
        "segape",
        "prinom",
        "segnom",
        "sigla",
        "digver",
        "tipper",
        "calemp",
        "cedrep",
        "repleg",
        "direccion",
        "codciu",
        "codzon",
        "telefono",
        "celular",
        "fax",
        "email",
        "codact",
        "fecini",
        "tottra",
        "valnom",
        "tipsoc",
        "estado",
        "codest",
        "motivo",
        "fecest",
        "usuario",
        "dirpri",
        "ciupri",
        "telpri",
        "celpri",
        "emailpri",
        "tipo",
        "coddoc",
        "documento",
        "tipemp",
        "matmer",
        "coddocrepleg",
        "priaperepleg",
        "segaperepleg",
        "prinomrepleg",
        "segnomrepleg",
        "codcaj",
        "fecha_aprobacion_sat",
        "documento_representante_sat",
        "numero_transaccion"
    ];

    public function __construct()
    {
        parent::__construct();
        $this->setConnection('mercurio');
    }

    public function isValid($rules)
    {
        $validator = Validator::make($this->attributes,  $rules, [
            'required' => 'El :attribute campo es requirido.',
            'same'     => 'El :attribute and :other must match.',
            'size'     => 'El :attribute must be exactly :size.',
            'between'  => 'El :attribute value :input is not between :min - :max.',
            'in'       => 'El :attribute must be one of the following types: :values',
            'email.required' => 'Se requiere la dirección de email!',
        ]);
        if ($validator->fails()){
            return $validator->errors();
        }
        return true;
    }

    public function getRules()
    {
        return [
            "nit"    => "required|max:15",
            "tipdoc" => "required|max:2",
            "razsoc" => "required|max:100",
            "calemp" => "required|max:1",
            "cedrep" => "required|max:13",
            "repleg" => "required|max:140",
            "direccion" => "required|max:45",
            "codciu" => "required|max:5",
            "codzon" => "required|max:9",
            "email"  => "required|max:45|email", 
            "codact" => "required|max:6", 
            "tottra" => "required|max:11", 
            "valnom" => "required|max:11", 
            "tipsoc" => "required|max:3", 
            "estado" => "required|max:1", 
            "usuario" => "required|max:11",
            "tipo"   => "required|max:2",
            "coddoc" => "required|max:2",
            "documento" => "required|max:15"
        ];
    }

}
