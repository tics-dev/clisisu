<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Resguardo extends Model
{
    use HasFactory;
    
    protected $table = "resguardos";
    
    public $timestamps = false;
    
    protected $primaryKey = 'id';
    
    protected $fillable = [
        'id',
        'codigo',
        'detalle'
    ];

}