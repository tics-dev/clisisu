<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sat20 extends Model
{
    use HasFactory;

    protected $table = "sat20";

    public $timestamps = false;

    protected $primaryKey = "numtraccf";

    public $autoincrement = false;
    
    protected $fillable =[
        "numtraccf",
        "fecha",
        "hora",
        "usuario",
        "tiptra",
        "gestion",
        "estado",
        "codest",
        "nota",
		"persiste"
    ];

    /**
     * getEstadoDetalle function
     * @return string
     */
    public function getEstadoDetalle()
	{	
		switch(trim($this->estado))
		{
			case 'I':
				return "NO CONTESTADA";
				break;
			case 'CA':
				return "CONTESTADA APROBADA";
				break;
			case 'CR':
				return "CONTESTADA RECHAZADA";
			case 'P':
				return "PENDIENTE";
				break;
			case 'E':
				return "ENVIADA";
				break;
			case 'O':
				return "EXITOSA";
				break;
			case 'U':
				return "EXITOSA CON ALERTA DE NEGOCIO";
				break;
			case 'X':
				return "ERROR GENERAL";
				break;
			case 'G':
				return "ERROR CON GLOSA";
				break;
			default:
				return '';
			break;
		}
	}

	public function sat02()
    {
        return $this->hasMany(Sat02::class, 'numtraccf', 'numtraccf');
    }
	
}
