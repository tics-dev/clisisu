<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Thiagoprz\CompositeKey\HasCompositeKey;

class Gener13 extends Model
{
    use HasCompositeKey;
    use HasFactory;
    
    protected $table = "gener13";

    public $timestamps = false;

    protected $keyType = 'string';

    protected $primaryKey = ['codapl', 'marca'];

    public $incrementing = false;

    protected $fillable = [
        "codapl",
        "marca",
        "detalle",
        "numero"
    ];
    
}