<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subsi54 extends Model
{
	use HasFactory;
	
	protected $table = "subsi54";
	
	public $timestamps = false;

	protected $primaryKey = "tipsoc";

	public $autoincrement = false;

	protected $keyType = 'string';
	
	protected $fillable = [
        "tipsoc",
        "detalle"
	];

}