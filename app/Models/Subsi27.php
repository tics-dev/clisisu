<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subsi27 extends Model
{
	use HasFactory;

	protected $table = "subsi27";

	public $timestamps = false;

	protected $primaryKey = "codcue";

	public $autoincrement = false;

	protected $keyType = 'string';

	protected $fillable = [
		"codcue",
		"numcue",
		"detalle",
		"tipcue",
		"ultche",
		"nit",
		"codzon",
		"auxiliar"
	];
}
