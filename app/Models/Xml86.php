<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Thiagoprz\CompositeKey\HasCompositeKey;

class Xml86 extends Model
{
    use HasCompositeKey;
    use HasFactory;

    protected $table = "xml86";

    public $timestamps = false;

    protected $primaryKey = ["periodo", "cedtra"];

    public $autoincrement = false;

    protected $fillable = [
        'periodo',
        'docnit',
        'nit',
        'coddoc',
        'cedtra',
        'prinom',
        'segnom',
        'priape',
        'segape',
        'fecnac',
        'sexo',
        'conesp',
        'salario',
        'tipafi',
        'codcat',
        'benef',
        'codmun',
        'area',
        'orisex',
        'nivesc',
        'ocupacion',
        'facvul',
        'estciv',
        'peretn',
        'pais',
        'ciulab',
        'arealab'
    ];

}