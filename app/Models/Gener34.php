<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gener34 extends Model
{
    use HasFactory;
    
    protected $table = "gener34";

    public $timestamps = false;

    protected $fillable = [
        "codori",
        "detalle"
    ];

    public function trabajador()
	{
		return $this->belongsTo(Subsi15::class, 'orisex', 'codori');
	}
}