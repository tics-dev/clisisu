<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tercero extends Model
{
	use HasFactory;
	
	protected $table = 'nits';
	
	public $timestamps = false;

	protected $primaryKey = 'nit';

	public $autoincrement = false;

	protected $keyType = 'string';
	
	protected $fillable = [
        'nit',
        'clase',
        'nombre',
        'direccion',
        'telefono',
        'ciudad',
        'autoret',
        'contacto',
        'cupo_g',
        'tipoclie',
        'aaereo',
        'fax',
        'depto',
        'pais',
        'zona',
        'plazo_aut',
        'estado',
        'intereses',
        'codigo',
        'lista',
        'resp_iva',
        'cupo_usado',
        'cargues',
        't_contrib',
        'd_chequeo',
        'id_alterno',
        'prefijo',
        'sexo',
        'zona_postal',
        'celular',
        'grabador',
        'fecha_mod',
        'fecha_crea',
        'resp_cree',
        'y_wxy',
        'y_stk'
	];

    public function __construct()
    {
        parent::__construct();
        $this->setConnection('pgsql');
    }

}