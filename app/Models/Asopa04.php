<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Asopa04 extends Model
{
	use HasFactory;
	protected $table ='asopa04';
	protected $primaryKey = "numero";
	public $timestamps = false;

	protected $fillable = [
		'numero',
		'fecsol',
		'fecapl',
		'numtar',
		'cedres',
		'tipnov',
		'estado'
	];

	public function getTipnov04Array()
	{
		return array(
			"CRE"=>"Creacion Tarjeta",
			"REA"=>"Realce",
			"BLQ"=>"Bloqueo de Tarjetas",
			"CUS"=>"Custodia",
			"ACT"=>"Activacion"
		);
	}

	public function getTipnov04Detalle()
	{
		switch($this->tipnov){
			case 'CRE':
				return 'Creacion Tarjeta';
				break;
			case 'REA':
				return 'Realce';
				break;
			case 'BLQ':
				return 'Bloqueo Tarjetas';
				break;
			case 'CUS':
				return 'Custodia';
				break;
			case 'RET':
				return 'Retiro Afiliado';
				break;
			case 'ACT':
				return 'Activacion';
				break;
		}
	}

	public function getEstado04Array()
    {
        return array(
            "A"=>"Adicionado",
            "P"=>"Procesado"
        );
    }

    public function getEstado04Detalle()
    {
        switch($this->estado){
            case 'A': 
                return 'Adicionado'; 
            break;
            case 'P': 
                return 'Procesado'; 
            break;
        }
    }

}