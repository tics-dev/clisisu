<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Thiagoprz\CompositeKey\HasCompositeKey;

class Subsi70 extends Model
{
    use HasCompositeKey;
	use HasFactory;
	
	protected $table = "subsi70";
	
	public $timestamps = false;

	public $incrementing = false;

    public $autoincrement = false;

	protected $primaryKey = ['ofiafi', 'periodo'];

	protected $fillable = [  
        "ofiafi",
        "periodo",
        "valsub",
        "valagr",
        "subuni",
        "subagr",
	];

}