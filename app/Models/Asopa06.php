<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Asopa06 extends Model
{
    use HasFactory;
    protected $table ='asopa06';
    protected $primaryKey = "numero";
    public $timestamps = false;

    protected $fillable = [
        "numero",
        "fecha",
        "numtar",
        "file",
        "descri",
        "valor",
        "codest",
        "hora"
    ];

}