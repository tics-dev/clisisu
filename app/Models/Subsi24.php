<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Thiagoprz\CompositeKey\HasCompositeKey;

class Subsi24 extends Model
{
    use HasCompositeKey;
	use HasFactory;
	
	protected $table = "subsi24";
	
	public $timestamps = false;

	protected $primaryKey = ["codcer","parent","captra"];

	public $autoincrement = false;

    protected $keyType = 'string';
	
	protected $fillable = [
        'codcer',
        'parent',
        'captra',
        'detalle',
        'cersus',
        'edamin',
        'edamax'
	];

}