<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Gener03 class
 * @detalle 
 * Clase de mantenimiento de opciones de permisos
 */
class Gener03 extends Model
{
    use HasFactory;
    
    protected $table = "gener03";
    protected $primaryKey = "codmen";
    protected $keyType = 'string';

    public $timestamps = false;
    public $autoincrement = false;
    protected $fillable = [
        'codmen',
        'detalle',
        'tipo'
    ];
    
}