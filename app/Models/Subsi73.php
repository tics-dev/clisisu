<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Thiagoprz\CompositeKey\HasCompositeKey;

class Subsi73 extends Model
{
	use HasCompositeKey;
	use HasFactory;
	
	protected $table ='subsi73';
	
	public $timestamps = false;
	
	protected $primaryKey = ["nit", "codlis"];

	public $autoincrement = false;

	protected $fillable = [
		"nit",
		"codlis",
		"detalle",
		"direccion",
		"telefono",
		"fax",
		"coddiv",
		"ofiafi",
		"nomcon",
		"email"
	];

	/**
	 * empresa propietaria del la sucursal
	 */
	public function empresa()
	{
		return $this->belongsTo(Subsi02::class, 'nit', 'nit');
	}
}