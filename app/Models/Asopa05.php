<?php

namespace App\Models;

use App\Exceptions\DebugException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Asopa05 extends Model
{
    use HasFactory;
    protected $table = 'asopa05';

    protected $primaryKey = "numero";

    public $timestamps = false;

    public $autoincrement = true;

    protected $fillable = [
        "numero",
        "fecha",
        "numtar",
        "cedres",
        "valor",
        "tipnov",
        "estado"
    ];

    public function getTipnov05Array()
    {
        return array("0" => "Abono", "1" => "Debito");
    }

    public function getTipnov05Detalle()
    {
        switch ($this->tipnov) {
            case '0':
                return 'Abono';
                break;
            case '1':
                return 'Debito';
                break;
        }
    }

    public function getEstado05Array()
    {
        return array(
            "A" => "Adicionado",
            "P" => "Procesado"
        );
    }

    public function getEstado05Detalle()
    {
        switch ($this->estado) {
            case 'A':
                return 'Adicionado';
                break;
            case 'P':
                return 'Procesado';
                break;
        }
    }
}
