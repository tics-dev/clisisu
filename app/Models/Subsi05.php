<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subsi05 extends Model
{
	use HasFactory;
	
	protected $table = "subsi05";
	
	public $timestamps = false;

	protected $primaryKey = "codind";

	public $autoincrement = false;

	protected $keyType = 'string';
	
	protected $fillable = [
        "codind",
        "detalle"
	];

}