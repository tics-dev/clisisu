<?php
namespace App\Models;

use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subsi20 extends Model
{
    use HasFactory;

    protected $table = "subsi20";
    
    public $timestamps = false;
    
    protected $primaryKey = "cedcon";

    public $autoincrement = false;

    protected $fillable = [
        "cedcon",
        "coddoc",
        "priape",
        "segape",
        "prinom",
        "segnom",
        "direccion",
        "telefono",
        "email",
        "codzon",
        "codcaj",
        "codocu",
        "nivedu",
        "captra",
        "salario",
        "tipsal",
        "fecsal",
        "tippag",
        "codcue",
        "ofides",
        "codgru",
        "codban",
        "numcue",
        "tipcue",
        "sexo",
        "estciv",
        "fecnac",
        "ciunac",
        "estado",
        "fecest",
        "numtar",
        "giass",
        "usuario",
        "fecact"      
    ];

    public function getRelationTrabajadores()
	{
        return Subsi21::where('cedcon', $this->cedcon)->get();
	}
    
    public function isValid($rules)
    {
        $validator = Validator::make($this->attributes,  $rules, [
            'required' => 'El :attribute campo es requirido.',
            'same'     => 'El :attribute and :other must match.',
            'size'     => 'El :attribute must be exactly :size.',
            'between'  => 'El :attribute valor :input no esta entre :min - :max.',
            'in'       => 'El :attribute must be one of the following types: :values',
            'email.required' => 'Se requiere la dirección de email!',
        ]);
        if ($validator->fails()){
            return $validator->errors();
        }
        return true;
    }

    public function reglas_creacion()
    {
        return array(
            "cedcon"  => "required|max:20",
            "coddoc"  => "required|max:2",
            "priape"  => "required|max:45",
            "prinom"  => "required|max:45",
            "segape"  => "max:45",
            "fecsol"  => "date",
            "codzon"  => "required",
            "telefono"=> "required",
            "tippag"  => "required"
        );
    }
    
}
