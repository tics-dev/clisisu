<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Subsi160 class
 * novedades 
 */
class Subsi160 extends Model
{
    use HasFactory;
    protected $table = "subsi160";
    public $timestamps = false;
    protected $primaryKey = "idreg";
    protected $fillable = [
        "idreg",
        "fecha",
        "hora",
        "usuario",
        "numero",
        "tipmot",
        "valcon",
        "estado",
        "fecest",
        "nota",
        "correo"
    ];

    /**
     * agente function
	 * novedad pertenece a   
     * @return void
     */
	public function agente()
	{
		return $this->belongsTo(Subsi161::class, 'idreg', 'idreg');
	}

    /**
     * solicitante function
	 * novedad pertenece a   
     * @return void
     */
	public function solicitante()
	{
		return $this->belongsTo(Subsi162::class, 'idreg', 'idreg');
	}
}
