<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gener02 extends Model
{
    use HasFactory;
    
    protected $table = "gener02";

    public $timestamps = false;

    protected $primaryKey = "usuario";

    public $autoincrement = false;

    protected $fillable = [
        "usuario",
        "cedtra",
        "nombre",
        "tipfun",
        "estacion",
        "impresora",
        "estado",
        "clave",
        "feccla",
        "login",
        "criptada",
        "intentos",
        "confirma_politica",
        "update_at"      
    ];
}