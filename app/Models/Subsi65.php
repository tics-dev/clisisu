<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subsi65 extends Model
{
    use HasFactory;
    
    protected $table = "subsi65";

    public $timestamps = false;

    protected $primaryKey = "numero";

    protected $fillable = [
        "numero",
        "sec",
        "coddoc",
        "cedtra",
        "tipcot",
        "subtip",
        "extpen",
        "colext",
        "coddep",
        "codciu",
        "priape",
        "segape",
        "prinom",
        "segnom",
        "ingtra",
        "novret",
        "novvps",
        "novvts",
        "novstc",
        "novitg",
        "licnom",
        "vacnom",
        "incnom",
        "diatra",
        "salbas",
        "valnom",
        "tarapo",
        "valapo",
        "correc",
        "salint",
        "fecing",
        "fecret",
        "fecinivsp",
        "fecinisln",
        "fecfinsln",
        "feciniige",
        "fecfinige",
        "fecinilma",
        "fecfinlma",
        "fecinivaclr",
        "fecfinvaclr",
        "fecinivct",
        "fecfinvct",
        "feciniirl",
        "fecfinirl",
        "numhor" 
    ];

    public function subsi64()
	{
		return $this->belongsTo(Subsi64::class, 'numero', 'numero');
	}
}