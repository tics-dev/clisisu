<?php
namespace App\Models;

use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subsi22 extends Model
{
    use HasFactory;

    protected $table = "subsi22";
    
    public $timestamps = false;
    
    protected $primaryKey = "codben";

    public $autoincrement = false;

    protected $fillable = [
        "codben",
        "documento",
        "coddoc",
        "priape",
        "segape",
        "prinom",
        "segnom",
        "parent",
        "huerfano",
        "tiphij",
        "captra",
        "tipdis",
        "sexo",
        "fecnac",
        "ciunac",
        "calendario",
        "giro",
        "codgir",
        "estado",
        "codest",
        "fecest",
        "feccon",
        "giass",
        "usuario",        
    ];

    public function getRelationTrabajadores()
	{
        return Subsi23::where('codben', $this->codben)->get();
	}

    public function reglas_creacion()
    {
        return array(
            "codben" => "required",
            "documento" => "required",
            "coddoc" => "required",
            "priape" => "required",
            "segape" => "required",
            "prinom" => "required",
            "segnom" => "required",
            "parent" => "required",
            "tiphij" => "required",
            "captra" => "required",
            "tipdis" => "required",
            "sexo" => "required",
            "fecnac" => "date",
            "ciunac" => "required",
            "calendario" => "required",
            "giro" => "required",
            "estado" => "required",
            "usuario" => "required"
        );
    }

    public function isValid($rules)
    {
        $validator = Validator::make($this->attributes,  $rules, [
            'required' => 'El :attribute campo es requirido.',
            'same'     => 'El :attribute and :other must match.',
            'size'     => 'El :attribute must be exactly :size.',
            'between'  => 'El :attribute valor :input no esta entre :min - :max.',
            'in'       => 'El :attribute must be one of the following types: :values',
            'email.required' => 'Se requiere la dirección de email!',
        ]);
        if ($validator->fails()){
            return $validator->errors();
        }
        return true;
    }

}