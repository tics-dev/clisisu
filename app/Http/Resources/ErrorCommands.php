<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ErrorCommands extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'error' => $this->error,
            'archivo' => $this->archivo,
            'linea' => $this->linea,
            'codigo' => $this->codigo,
            'data' => $this->data,
            'success' => $this->success,
            'message' => $this->message
        ];
    }
}
