<?php

namespace App\Http\Middleware;

use App\Models\Gener02;
use Closure;
use Illuminate\Http\Request;
use stdClass;

class AuthBasic
{
    /**
     * Handle an incoming request.
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            $usuario = null;
            $payload = null;

            if (!$request->hasHeader('Authorization')) {
                return $this->responseUnauthorized('Error no es disponeble de acceso, se requiere de los parametros de (Authorization)', 501);
            }

            // Obtener el token del encabezado de autorización
            $header = $request->header('Authorization');

            if (empty($header) == false || preg_match('/^basic/i', $header)) {
                $tk = base64_decode(trim(preg_replace('/^basic/i', "", $header)));
                $exp = explode(':', $tk);
                if (count($exp) === 2) {
                    $usuario = Gener02::where('usuario', $exp[0])
                        ->get()
                        ->first();

                    if (!$usuario) {
                        return $this->responseUnauthorized('El usuario no es valido para continuar', 401);
                    }

                    if ($usuario->estado != 'A') {
                        return $this->responseUnauthorized('El usuario no está activo para continuar', 401);
                    }

                    if ($usuario->tipfun != 'API') {
                        return $this->responseUnauthorized('El tipo de funcionario no dispone de permisos de acceso', 401);
                    }

                    if (env('APP_MODE') == 'production') {
                        $iv = explode('||', $exp[1]);
                        if (!(count($iv) === 2)) return $this->responseUnauthorized('Access No Authorization', 401);

                        $password = openssl_decrypt($iv[0], 'AES-256-CBC', '8QGTBVnqGh5A4a2WST0ztVU0eB9p2lSHwWWXy1GlSAhPoXyNMw', 0, $iv[1]);
                        if ($password == false) {
                            return $this->responseUnauthorized('Access No Authorization', 401);
                        }
                    } else {
                        $password = trim($exp[1]);
                    }
                    if ($this->clave_verify($password, $usuario->criptada) == false) {
                        return $this->responseUnauthorized('La clave no es correcta para continuar', 401);
                    }
                } else {
                    return $this->responseUnauthorized('La autenticación basica requiere de 3 parametros atob(user:password).', 401);
                }
            } else {
                return $this->responseUnauthorized('Error de (authorization), no está disponible para su validación', 401);
            }

            $payload = new stdClass();
            $payload->usuario = $usuario->usuario;
            $payload->nombre = $usuario->nombre;
            $payload->cedtra = $usuario->cedtra;
            $payload->email = $usuario->estacion;
        } catch (\Exception $e) {
            return $this->responseUnauthorized($e->getMessage(), 503);
        }

        // Almacenar el payload en la solicitud para que esté disponible en el controlador
        return $next($request, ['payload' => $payload]);
    }

    private function responseUnauthorized(string $message, $status = '')
    {
        return response()->json(array(
            'status' => false,
            'message' => $message,
            'code' => $status
        ), 503);
    }

    private function clave_verify($password, $hash)
    {
        /* Regenerating the with an available hash as the options parameter should
         * produce the same hash if the same password is passed.
        */
        return crypt($password, $hash) == $hash;
    }
}
