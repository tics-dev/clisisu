<?php

namespace App\Http\Middleware;

use App\Libraries\JwtLibrary;
use App\Services\JwtService;
use Closure;
use Illuminate\Http\Request;

class Authorization
{
    /**
     * Handle an incoming request.
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $jwtLibrary = new JwtLibrary();
        $jwtService = new JwtService();

        if (!$request->hasHeader('Authorization')) {
            return $this->responseUnauthorized('Error header (authorization)', 501);
        }

        // Obtener el token del encabezado de autorización
        $header = $request->header('Authorization');

        if (empty($header) || !preg_match('/Bearer\s(\S+)/', $header, $matches)) {
            return $this->responseUnauthorized('Error token de (authorization), no está disponible para su validación', 502);
        }
        $jwt = $matches[1];
        try {
            $payload = $jwtLibrary->validateToken($jwt);
            $dataService = $jwtService->getJwtIngresoByToken($jwt);
            if (!$payload) {
                if ($dataService) {
                    $jwtService->updateJwtIngreso($dataService->id, ["estado" => "I"]);
                }
                return $this->responseUnauthorized('Error no es valido el token, esté ya ha expirado', 503);
            } else {
                if ($dataService) {
                    $jwtService->updateJwtIngreso($dataService->id, ["consumo" => $dataService->consumo + 1]);
                }
            }
        } catch (\Exception $e) {
            return $this->responseUnauthorized($e->getMessage(), 503);
        }

        // Almacenar el payload en la solicitud para que esté disponible en el controlador
        return $next($request, ['payload' => $payload]);
    }

    private function responseUnauthorized(string $message, $status = '')
    {
        return response()->json(array(
            'status' => false,
            'message' => $message,
            'code' => $status
        ), 503);
    }
}
