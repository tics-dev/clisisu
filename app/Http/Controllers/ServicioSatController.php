<?php

namespace App\Http\Controllers;

use App\Exceptions\DebugException;
use App\Exceptions\GlozaException;
use App\Exceptions\WsError;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Libraries\MyParams;
use App\Models\Gener18;
use App\Models\Sat02;
use App\Models\Sat03;
use App\Models\Sat06;
use App\Models\Sat08;
use App\Models\Sat09;
use App\Models\Sat10;
use App\Models\Sat13;
use App\Models\Sat15;
use App\Models\Sat20;
use App\Models\Subsi02;
use App\Models\Subsi15;
use App\Services\ServicioSat\Empresa;
use App\Services\ServicioSat\GlosaServices;
use App\Services\ServicioSat\PagoAportes;
use App\Services\ServicioSat\ReprocesarGlosas;
use App\Services\ServicioSat\RestService\RestServiceFactoryImpl;
use App\Services\ServicioSat\Tareas;
use App\Services\ServicioSat\Trabajador;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class ServicioSatController extends Controller
{

    public function empresaNueva(Request $request, int $nit)
    {
        try {
            try {
                $empresa = new Empresa($nit);
                $empresa->nueva();
                $empresa->salvar();
                $data = $empresa->prepara_data_sat();

                $restServiceFactoryImpl = new RestServiceFactoryImpl();
                RestServiceFactoryImpl::setData($data);
                $rqs = $restServiceFactoryImpl->afiliacionPrimeraVezService();

                if ($rqs['codigo']) {
                    Sat02::where('numtraccf', $empresa->numtraccf)->update([
                        'codigo' =>  $rqs['codigo'],
                        'mensaje' => $rqs['mensaje']
                    ]);
                }
                $attr = $empresa->sat->makeVisible('attribute')->toArray();
                $attr['numtraccf'] = $empresa->numtraccf;

                $rqs['data'] = $attr;
                $rqs['modelo'] = $data;
                $rqs['success'] = true;

                return response()->json($rqs, 201);
            } catch (QueryException $ex) {
                throw new DebugException($ex->getMessage(), 1);
            } catch (GlozaException $ge) {
                throw new DebugException($ge->getMessage(), 1);
            }
        } catch (DebugException $err) {
            throw new WsError($err->getMessage(), 501);
        }
    }

    public function empresaReintegro(Request $request, int $nit)
    {
        try {
            try {
                $empresa = new Empresa($nit);
                $empresa->reintegro();
                $empresa->salvar();
                $data = $empresa->prepara_data_sat();

                $restServiceFactoryImpl = new RestServiceFactoryImpl();
                RestServiceFactoryImpl::setData($data);
                $rqs = $restServiceFactoryImpl->afiliacionNoPrimeraVezService();

                if ($rqs['codigo']) {
                    Sat03::where('numtraccf', $empresa->numtraccf)->update([
                        'codigo' =>  $rqs['codigo'],
                        'mensaje' => $rqs['mensaje']
                    ]);
                }

                $attr = $empresa->sat->makeVisible('attribute')->toArray();
                $attr['numtraccf'] = $empresa->numtraccf;

                $rqs['data'] = $attr;
                $rqs['modelo'] = $data;
                $rqs['success'] = true;

                return response()->json($rqs, 201);
            } catch (QueryException $ex) {
                throw new DebugException($ex->getMessage(), 1);
            } catch (GlozaException $ge) {
                throw new DebugException($ge->getMessage(), 1);
            }
        } catch (DebugException $err) {
            throw new WsError($err->getMessage(), 501);
        }
    }

    public function empresaRetiro(Request $request, int $nit)
    {
        try {
            try {
                $empresa = new Empresa($nit);
                $empresa->retiro();
                $empresa->salvar();
                $data = $empresa->prepara_data_sat();

                $restServiceFactoryImpl = new RestServiceFactoryImpl();
                RestServiceFactoryImpl::setData($data);
                $rqs = $restServiceFactoryImpl->desafiliacionService();

                if ($rqs['codigo']) {
                    Sat06::where('numtraccf', $empresa->numtraccf)->update([
                        'codigo' =>  $rqs['codigo'],
                        'mensaje' => $rqs['mensaje']
                    ]);
                }

                $attr = $empresa->sat->makeVisible('attribute')->toArray();
                $attr['numtraccf'] = $empresa->numtraccf;
                $rqs['data'] = $attr;
                $rqs['modelo'] = $data;
                $rqs['success'] = true;

                return response()->json($rqs, 201);
            } catch (QueryException $ex) {
                throw new DebugException($ex->getMessage(), 1);
            } catch (GlozaException $ge) {
                throw new DebugException($ge->getMessage(), 1);
            }
        } catch (DebugException $err) {
            throw new WsError($err->getMessage(), 501);
        }
    }

    public function empresaCausaGrave(Request $request)
    {
        try {
            try {
                $numdocemp = $request->input('numdocemp');
                $detalle = $request->input('detalle');

                $empresa = new Empresa($numdocemp);
                $empresa->causa_grave($detalle);
                $empresa->salvar();
                $data = $empresa->prepara_data_sat();

                $restServiceFactoryImpl = new RestServiceFactoryImpl();
                RestServiceFactoryImpl::setData($data);
                $rqs = $restServiceFactoryImpl->perdidaAfiliacionCausaGraveService();

                if ($rqs['codigo']) {
                    Sat08::where('numtraccf', $empresa->numtraccf)->update([
                        'codigo' =>  $rqs['codigo'],
                        'mensaje' => $rqs['mensaje']
                    ]);
                }

                $attr = $empresa->sat->makeVisible('attribute')->toArray();
                $attr['numtraccf'] = $empresa->numtraccf;
                $rqs['data'] = $attr;
                $rqs['modelo'] = $data;
                $rqs['success'] = true;

                return response()->json($rqs, 201);
            } catch (QueryException $ex) {
                throw new DebugException($ex->getMessage(), 501);
            } catch (GlozaException $ge) {
                throw new DebugException($ge->getMessage(), 501);
            }
        } catch (DebugException $err) {
            throw new WsError($err->getMessage(), 501);
        }
    }

    public function procesaTrabajador(Request $request, $numdoctra)
    {
        try {
            try {
                $trabajador = new Trabajador($numdoctra);
                $trabajador->iniciar();
                $trabajador->salvar();
                $data = $trabajador->prepara_data_sat();

                $restServiceFactoryImpl = new RestServiceFactoryImpl();
                RestServiceFactoryImpl::setData($data);
                $rqs = $restServiceFactoryImpl->inicioRelacionLaboralService();

                if ($rqs['codigo']) {
                    Sat09::where('numtraccf', $trabajador->numtraccf)->update([
                        'codigo' =>  $rqs['codigo'],
                        'mensaje' => $rqs['mensaje']
                    ]);
                }

                $attr = $trabajador->sat->makeVisible('attribute')->toArray();
                $attr['numtraccf'] = $trabajador->numtraccf;
                $rqs['data'] = $attr;
                $rqs['modelo'] = $data;
                $rqs['success'] = true;

                return response()->json($rqs, 201);
            } catch (QueryException $ex) {
                throw new DebugException($ex->getMessage(), 501);
            } catch (GlozaException $ge) {
                throw new DebugException($ge->getMessage(), 501);
            }
        } catch (DebugException $err) {
            throw new WsError($err->getMessage(), 501);
        }
    }

    public function terminaTrabajador(Request $request, int $cedtra)
    {
        try {
            try {
                $trabajador = new Trabajador($cedtra);
                $trabajador->terminar();
                $trabajador->salvar();
                $data = $trabajador->prepara_data_sat();

                $restServiceFactoryImpl = new RestServiceFactoryImpl();
                RestServiceFactoryImpl::setData($data);
                $rqs = $restServiceFactoryImpl->terminacionRelacionLaboralService();

                if ($rqs['codigo']) {
                    Sat10::where('numtraccf', $trabajador->numtraccf)->update([
                        'codigo' =>  $rqs['codigo'],
                        'mensaje' => $rqs['mensaje']
                    ]);
                }

                $attr = $trabajador->sat->makeVisible('attribute')->toArray();
                $attr['numtraccf'] = $trabajador->numtraccf;
                $rqs['data'] = $attr;
                $rqs['modelo'] = $data;
                $rqs['success'] = true;

                return response()->json($rqs, 201);
            } catch (QueryException $ex) {
                throw new DebugException($ex->getMessage(), 1);
            } catch (GlozaException $ge) {
                throw new DebugException($ge->getMessage(), 1);
            }
        } catch (DebugException $err) {
            throw new WsError($err->getMessage(), 501);
        }
    }

    public function getModelo(Request $request)
    {
        try {
            try {
                $numtraccf = $request->input('numtraccf');
                $tablaSat = $request->input('tabla');
                $sat = DB::table($tablaSat)
                    ->select("{$tablaSat}.*")
                    ->join('sat20', 'sat20.numtraccf', '=', "{$tablaSat}.numtraccf")
                    ->where("{$tablaSat}.numtraccf", $numtraccf)
                    ->where("sat20.persiste", '0')
                    ->get()
                    ->first();

                if (!$sat) {
                    return [
                        'success' => false,
                        'data' => [],
                        'mensaje' => "Error el registro no se encuentra {$tablaSat}."
                    ];
                }
                $sat20 = Sat20::where("numtraccf", $numtraccf)->get()->first();
                $sat->sat20 = $sat20;

                return response()->json([
                    'success' => true,
                    'data' => $sat
                ], 201);
            } catch (QueryException $ex) {
                throw new DebugException($ex->getMessage(), 1);
            } catch (GlozaException $ge) {
                throw new DebugException($ge->getMessage(), 1);
            }
        } catch (DebugException $err) {
            throw new WsError($err->getMessage(), 501);
        }
    }

    public function getEmpresa(Request $request, int $nit)
    {
        try {
            try {
                $subsi02 = Subsi02::where("nit", $nit)->get()->first();
                return response()->json([
                    'success' => true,
                    'data' => $subsi02
                ], 201);
            } catch (QueryException $ex) {
                throw new DebugException($ex->getMessage(), 1);
            } catch (GlozaException $ge) {
                throw new DebugException($ge->getMessage(), 1);
            }
        } catch (DebugException $err) {
            throw new WsError($err->getMessage(), 501);
        }
    }

    public function modeloReprocesar(Request $request)
    {
        try {
            try {
                $reprocesarGlosas = new ReprocesarGlosas(
                    new MyParams(
                        array(
                            'post' => $request->input('post'),
                            'tabla' => $request->input('tabla'),
                        )
                    )
                );

                $rqs = $reprocesarGlosas->procesar(new Tareas());
                return response()->json($rqs, 201);
            } catch (QueryException $ex) {
                throw new DebugException($ex->getMessage(), 501);
            } catch (GlozaException $ge) {
                throw new DebugException($ge->getMessage(), 501);
            }
        } catch (DebugException $err) {
            throw new WsError($err->getMessage(), 501);
        }
    }

    public function glozaDelete(Request $request)
    {
        try {
            try {
                $glosaServices = new GlosaServices(
                    new MyParams(
                        array(
                            'numtraccf' => $request->input('numtraccf'),
                            'tabla' => $request->input('tabla')
                        )
                    )
                );
                $rqs = $glosaServices->glozaBorrar();
                return response()->json($rqs, 201);
            } catch (QueryException $ex) {
                throw new DebugException($ex->getMessage(), 501);
            } catch (GlozaException $ge) {
                throw new DebugException($ge->getMessage(), 501);
            }
        } catch (DebugException $err) {
            throw new WsError($err->getMessage(), 501);
        }
    }

    public function actualizaPersistencia(Request $request)
    {
        try {
            try {
                $glosaServices = new GlosaServices(
                    new MyParams(
                        array(
                            'numtraccf' => $request->input('numtraccf'),
                            'tabla' => $request->input('tabla'),
                            'numdocemp' => $request->input('numdocemp'),
                            'numdoctra' => $request->input('numdoctra')
                        )
                    )
                );
                $rqs = $glosaServices->actualizaPersistencia();
                return response()->json($rqs, 201);
            } catch (QueryException $ex) {
                throw new DebugException($ex->getMessage(), 501);
            } catch (GlozaException $ge) {
                throw new DebugException($ge->getMessage(), 501);
            }
        } catch (DebugException $err) {
            throw new WsError($err->getMessage(), 501);
        }
    }

    public function getTrabajador(Request $request, int $cedtra)
    {
        try {
            try {
                $subsi15 = Subsi15::where("cedtra", $cedtra)->get()->first();
                return response()->json([
                    'success' => true,
                    'data' => $subsi15
                ], 201);
            } catch (QueryException $ex) {
                throw new DebugException($ex->getMessage(), 501);
            } catch (GlozaException $ge) {
                throw new DebugException($ge->getMessage(), 501);
            }
        } catch (DebugException $err) {
            throw new WsError($err->getMessage(), 501);
        }
    }

    public function estadoPagoAportes(Request $request)
    {
        try {
            try {
                $aportes = new PagoAportes($request->input('nit'));
                $aportes->estado($request->input('estpag'));
                $aportes->salvar();
                $data = $aportes->prepara_data_sat();

                $restServiceFactoryImpl = new RestServiceFactoryImpl();
                RestServiceFactoryImpl::setData($data);
                $rqs = $restServiceFactoryImpl->estadoPagoEmpleadorService();

                if ($rqs['codigo']) {
                    Sat15::where('numtraccf', $aportes->numtraccf)->update([
                        'codigo' =>  $rqs['codigo'],
                        'mensaje' => $rqs['mensaje']
                    ]);
                }

                $attr = $aportes->sat->makeVisible('attribute')->toArray();
                $attr['numtraccf'] = $aportes->numtraccf;
                $rqs['data'] = $attr;
                $rqs['modelo'] = $data;
                $rqs['success'] = true;
                $rqs['result'] = $aportes->result();

                return response()->json($rqs, 201);
            } catch (QueryException $ex) {
                throw new DebugException($ex->getMessage(), 501);
            } catch (GlozaException $ge) {
                throw new DebugException($ge->getMessage(), 501);
            }
        } catch (DebugException $err) {
            throw new WsError($err->getMessage(), 501);
        }
    }

    public function consultaEmpresaEmpleados(Request $request, int $nit)
    {
        try {
            try {
                $empresa = Subsi02::where("nit", $nit)->get()->first();
                $tipo_documento = Gener18::where("coddoc", $empresa->coddoc)->get()->first();
                $data = [
                    "TipoDocumentoEmpleador" => $tipo_documento->codrua,
                    "NumeroDocumentoEmpleador" => $empresa->nit,
                    "SerialSat" => '0'
                ];

                $restServiceFactoryImpl = new RestServiceFactoryImpl();
                RestServiceFactoryImpl::setData($data);
                $rqs = $restServiceFactoryImpl->consultarEmpresaYEmpleados();

                return response()->json($rqs, 200);
            } catch (QueryException $ex) {
                throw new DebugException($ex->getMessage(), 1);
            } catch (GlozaException $ge) {
                throw new DebugException($ge->getMessage(), 1);
            }
        } catch (DebugException $err) {
            throw new WsError($err->getMessage(), 501);
        }
    }

    public function consultaEmpresaAfiliaciones(Request $request, int $nit)
    {
        try {
            try {
                $empresa = Subsi02::where("nit", $nit)->get()->first();
                $tipo_documento = Gener18::where("coddoc", $empresa->coddoc)->get()->first();
                $data = [
                    "TipoDocumentoEmpleador" => $tipo_documento->codrua,
                    "NumeroDocumentoEmpleador" => $empresa->nit,
                    "SerialSat" => '0'
                ];
                $restServiceFactoryImpl = new RestServiceFactoryImpl();
                RestServiceFactoryImpl::setData($data);
                $rqs = $restServiceFactoryImpl->consultarEmpresaYAfiliaciones();

                return response()->json($rqs, 200);
            } catch (QueryException $ex) {
                throw new DebugException($ex->getMessage(), 1);
            } catch (GlozaException $ge) {
                throw new DebugException($ge->getMessage(), 1);
            }
        } catch (DebugException $err) {
            throw new WsError($err->getMessage(), 501);
        }
    }

    public function trabajadorCambioSalario(Request $request)
    {
        try {
            try {
                $cedtra = $request->input('cedtra');
                $salario = $request->input('salario');

                $trabajador = new Trabajador($cedtra);
                $trabajador->cambio_salario($salario);
                $trabajador->salvar();
                $data = $trabajador->prepara_data_sat();

                $restServiceFactoryImpl = new RestServiceFactoryImpl();
                RestServiceFactoryImpl::setData($data);
                $rqs = $restServiceFactoryImpl->cambioSalarialService();

                if ($rqs['codigo']) {
                    Sat13::where('numtraccf', $trabajador->numtraccf)->update([
                        'codigo' =>  $rqs['codigo'],
                        'mensaje' => $rqs['mensaje']
                    ]);
                }

                $attr = $trabajador->sat->makeVisible('attribute')->toArray();
                $attr['numtraccf'] = $trabajador->numtraccf;
                $rqs['data'] = $attr;
                $rqs['modelo'] = $data;
                $rqs['success'] = true;
                return response()->json($rqs, 201);
            } catch (QueryException $ex) {
                throw new DebugException($ex->getMessage(), 1);
            } catch (GlozaException $ge) {
                throw new DebugException($ge->getMessage(), 1);
            }
        } catch (DebugException $err) {
            throw new WsError($err->getMessage(), 501);
        }
    }
}
