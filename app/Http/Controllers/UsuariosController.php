<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UsuariosController extends Controller
{

    public function buscarUsuario(Request $request, int $user)
    {
        Artisan::call(
            'server:send',
            [
                'clase' => 'Usuarios',
                'metodo' => 'trae_usuario',
                'user' => $request->input('_user'),
                'sistema' => $request->input('_sistema'),
                'env' => $request->input('_env'),
                'params' => $user
            ]
        );
        return response()->json(json_decode(Artisan::output()), 200);
    }

    public function confirmaPolitica(Request $request, int $user)
    {
        Artisan::call(
            'server:send',
            [
                'clase' => 'Usuarios',
                'metodo' => 'confirma_politica',
                'user' => $request->input('_user'),
                'sistema' => $request->input('_sistema'),
                'env' => $request->input('_env'),
                'params' => $user
            ]
        );
        return response()->json(json_decode(Artisan::output()), 200);
    }

    public function resetearIntentos(Request $request, int $user)
    {
        Artisan::call(
            'server:send',
            [
                'clase' => 'Usuarios',
                'metodo' => 'resetear_intentos',
                'user' => $request->input('_user'),
                'sistema' => $request->input('_sistema'),
                'env' => $request->input('_env'),
                'params' => $user
            ]
        );
        return response()->json(json_decode(Artisan::output()), 200);
    }

    public function cargarIntentos(Request $request, int $user)
    {
        Artisan::call(
            'server:send',
            [
                'clase' => 'Usuarios',
                'metodo' => 'cargar_intentos',
                'user' => $request->input('_user'),
                'sistema' => $request->input('_sistema'),
                'env' => $request->input('_env'),
                'params' => $user
            ]
        );
        return response()->json(json_decode(Artisan::output()), 200);
    }
}
