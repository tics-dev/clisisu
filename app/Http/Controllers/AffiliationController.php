<?php

namespace App\Http\Controllers;

use App\Exceptions\DebugException;
use App\Exceptions\WsError;
use App\Http\Resources\ResponseCommands;
use App\Models\Subsi15;
use App\Models\Trazabilidad;
use App\Services\DeshacerAfiliaciones\DeshaceTrabajador;

use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use App\Libraries\MyParams;
use App\Services\Afiliaciones\AfiliaBeneficiario;
use App\Services\Afiliaciones\AfiliaConyuge;
use App\Services\Afiliaciones\AfiliaEmpresa;
use App\Services\Afiliaciones\AfiliaIndependiente;
use App\Services\Afiliaciones\AfiliaTrabajador;
use App\Services\AfiliacionMasivos\CargarBeneficiarios;
use App\Services\AfiliacionMasivos\CargarConyuges;
use App\Services\AfiliacionMasivos\CargarTrabajadores;
use App\Services\AfiliacionParametros\ParametrosBeneficiario;
use App\Services\AfiliacionParametros\ParametrosConyuge;
use App\Services\AfiliacionParametros\ParametrosEmpresa;
use App\Services\AfiliacionParametros\ParametrosIndependiente;
use App\Services\AfiliacionParametros\ParametrosPensionado;
use App\Services\AfiliacionParametros\ParametrosTrabajador;
use App\Services\ConsultasGenerales\ListarCiudadesDepartamentos;
use App\Services\Poblacion\ListarBeneficiarios;
use App\Services\Poblacion\ListarConyuges;
use App\Services\Poblacion\ListarTrabajadores;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;


class AffiliationController extends Controller
{

    public function parametrosTrabajadores(Request $request)
    {
        try {
            $parametrosTrabajador = new ParametrosTrabajador();
            $params = $parametrosTrabajador->getParametros();
            return response()->json($params, 201);
        } catch (DebugException $err) {
            return new WsError($err->getMessage(), 501);
        }
    }

    public function parametrosConyuges(Request $request)
    {
        try {
            $parametrosConyuge = new ParametrosConyuge();
            $params = $parametrosConyuge->getParametros();
            return response()->json($params, 201);
        } catch (DebugException $err) {
            return new WsError($err->getMessage(), 501);
        }
    }

    public function parametrosBeneficiarios(Request $request)
    {
        try {
            $parametrosBeneficiario = new ParametrosBeneficiario();
            $params = $parametrosBeneficiario->getParametros();
            return response()->json($params, 201);
        } catch (DebugException $err) {
            return new WsError($err->getMessage(), 501);
        }
    }

    public function parametrosEmpresa(Request $request)
    {
        try {
            $parametrosEmpresa = new ParametrosEmpresa();
            $params = $parametrosEmpresa->getParametros();
            return response()->json($params, 201);
        } catch (DebugException $err) {
            return new WsError($err->getMessage(), 501);
        }
    }

    public function parametrosPensionado(Request $request)
    {
        try {
            $parametrosPensionado = new ParametrosPensionado();
            $params = $parametrosPensionado->getParametros();
            return response()->json($params, 201);
        } catch (DebugException $err) {
            return new WsError($err->getMessage(), 501);
        }
    }

    public function parametrosIndependiente(Request $request)
    {
        try {
            $parametrosIndependiente = new ParametrosIndependiente();
            $params = $parametrosIndependiente->getParametros();
            return response()->json($params, 201);
        } catch (DebugException $err) {
            return new WsError($err->getMessage(), 501);
        }
    }

    public function listarTrabajadores(Request $request)
    {
        try {
            $listarTrabajadores = new ListarTrabajadores();
            $data = $listarTrabajadores->listarByNit(new MyParams(
                array(
                    'nit' => $request->input('nit'),
                    'estado' => $request->input('estado')
                )
            ));

            return response()->json($data, 201);
        } catch (DebugException $err) {
            return new WsError($err->getMessage(), 501);
        }
    }

    public function listarConyuges(Request $request)
    {
        try {
            $listarConyuges = new ListarConyuges();
            $data = $listarConyuges->listarByNit(
                new MyParams(
                    array(
                        'nit' => $request->input('nit'),
                        'estado' => $request->input('estado')
                    )
                )
            );

            return response()->json($data, 201);
        } catch (DebugException $err) {
            return new WsError($err->getMessage(), 501);
        }
    }

    public function listarConyugesTrabajador(Request $request, int $cedtra)
    {
        try {
            $estados = ($request->input('estado')) ? $request->input('estado') : ['A', 'I', 'M'];
            $listarConyuges = new ListarConyuges();
            $data = $listarConyuges->listarForTrabajador(
                new MyParams(array(
                    'cedtra' => $cedtra,
                    'estados' => $estados
                ))
            );

            return response()->json($data, 201);
        } catch (DebugException $err) {
            return new WsError($err->getMessage(), 501);
        }
    }

    public function listarCiudadesDepartamentos(Request $request)
    {
        try {
            $listarCiudadesDepartamentos = new ListarCiudadesDepartamentos();
            $data = $listarCiudadesDepartamentos->getData();
            return response()->json($data, 201);
        } catch (DebugException $err) {
            return new WsError($err->getMessage(), 501);
        }
    }

    public function trabajadorEmpresa(Request $request)
    {
        try {
            $estados = (empty($request->input('estado'))) ? ['A', 'M', 'I'] : [$request->input('estado')];
            $listarTrabajadores = new ListarTrabajadores();
            $data = $listarTrabajadores->buscarCedtraByNit(
                new MyParams(
                    array(
                        'estados' => $estados,
                        'cedtra' => $request->input('cedtra'),
                        'nit' => $request->input('nit')
                    )
                )
            );

            return response()->json($data, 201);
        } catch (DebugException $err) {
            return new WsError($err->getMessage(), 501);
        }
    }

    public function conyugueTrabajadorBeneficiario(Request $request)
    {
        try {
            $listarBeneficiarios = new ListarBeneficiarios();
            $out = $listarBeneficiarios->buscarBeneficiarioForCedtra(
                new MyParams(
                    array(
                        'cedtra' => $request->input('cedtra'),
                        'documento' => $request->input('documento')
                    )
                )
            );

            return response()->json([
                "success" => ($out != false),
                "data" => $out
            ], 201);
        } catch (DebugException $err) {
            return new WsError($err->getMessage(), 501);
        }
    }

    public function trabajador(Request $request)
    {
        try {
            $estados = (empty($request->input('estado'))) ? ['A', 'M', 'I'] : [$request->input('estado')];
            $listarTrabajadores = new ListarTrabajadores();
            $salida = $listarTrabajadores->buscarByCedtra(
                new MyParams(
                    array(
                        'cedtra' => $request->input('cedtra'),
                        'estados' => $estados
                    )
                )
            );

            return response()->json($salida, 201);
        } catch (DebugException $err) {
            return new WsError($err->getMessage(), 501);
        }
    }

    public function conyuge(Request $request)
    {
        try {
            $estados = (empty($request->input('estado'))) ? ['A', 'M', 'I'] : [$request->input('estado')];
            $listarConyuges = new ListarConyuges();
            $salida = $listarConyuges->buscarByCedcon(
                new MyParams(
                    array(
                        'cedcon' => $request->input('cedcon'),
                        'estados' => $estados
                    )
                )
            );

            return response()->json($salida, 201);
        } catch (DebugException $err) {
            return new WsError($err->getMessage(), 501);
        }
    }

    public function beneficiario(Request $request)
    {
        try {
            $estados = (empty($request->input('estado'))) ? ['A', 'M', 'I'] : [$request->input('estado')];

            $listarBeneficiarios = new ListarBeneficiarios();
            $salida = $listarBeneficiarios->buscarByNumdoc(new MyParams(
                array(
                    'numdoc' => $request->input('numdoc'),
                    'estados' => $estados
                )
            ));

            return response()->json($salida, 201);
        } catch (DebugException $err) {
            return new WsError($err->getMessage(), 501);
        }
    }

    public function afiliaTrabajador(Request $request)
    {
        try {
            $afilia = new AfiliaTrabajador();
            $afilia->procesar($request->input('post'));
            return response()->json($afilia->getResultado(), 201);
        } catch (DebugException $err) {
            return new WsError($err->getMessage(), 501);
        }
    }

    public function afiliaEmpresa(Request $request)
    {
        try {
            $afilia = new AfiliaEmpresa();
            $afilia->procesar($request->input('post'));
            return response()->json($afilia->getResultado(), 201);
        } catch (DebugException $err) {
            return new WsError($err->getMessage(), 501);
        }
    }

    public function afiliaBeneficiario(Request $request)
    {
        try {
            $afilia  = new AfiliaBeneficiario();
            $afilia->procesar($request->input('post'));
            return response()->json($afilia->getResultado(), 201);
        } catch (DebugException $err) {
            return new WsError($err->getMessage(), 501);
        }
    }

    public function afiliaConyuge(Request $request)
    {
        try {
            $afilia = new AfiliaConyuge();
            $afilia->procesar($request->input('post'));
            return response()->json($afilia->getResultado(), 201);
        } catch (DebugException $err) {
            return new WsError($err->getMessage(), 501);
        }
    }

    public function afiliaIndependiente(Request $request)
    {
        try {
            $afilia = new AfiliaIndependiente();
            $afilia->procesar($request->input('post'));
            return response()->json($afilia->getResultado(), 201);
        } catch (DebugException $err) {
            return new WsError($err->getMessage(), 501);
        }
    }

    public function actualizaBeneficiario(Request $request)
    {
        try {
            $afilia = new AfiliaBeneficiario();
            $datos = $request->input('post');
            $afilia->actualizarDatos(
                $datos['numdoc'],
                $datos['modelo']
            );
            return response()->json($afilia->getResultado(), 201);
        } catch (DebugException $err) {
            return new WsError($err->getMessage(), 501);
        }
    }

    public function actualizaEmpresa(Request $request)
    {
        try {
            $afilia = new AfiliaEmpresa();
            $afilia->actualizarDatos($request->input('post'));
            return response()->json($afilia->getResultado(), 201);
        } catch (DebugException $err) {
            return new WsError($err->getMessage(), 501);
        }
    }

    public function deshaceTrabajador(Request $request)
    {
        try {
            $trabajador = Subsi15::where('estado', $request->input('estado'))
                ->where('cedtra', $request->input('cedtra'))
                ->get()
                ->first();

            $nit = $trabajador->nit;
            $tipo_documento = $trabajador->tipdoc;
            $cedtra = $trabajador->cedtra;
            $nota = "se rechazar la afiliación";

            $deshaceTrabajador = new DeshaceTrabajador($nit, $tipo_documento, $cedtra);
            $deshaceTrabajador->procesar();
            $trasa = new Trazabilidad();

            $trasa->create(
                [
                    "id" => Trazabilidad::max('id') + 1,
                    "usuario" => 1,
                    "detalle_evento" => substr("Deshacer aprobación trabajador {$cedtra}, {$nota}", 0, 224),
                    "modulo" =>  "Comfaca en línea, aprobación de trabajador",
                    "medio" => 'Gestión subsidio',
                    "estado" => "Evento generado",
                    "hora" => date('H:i:s'),
                    "create_at" => date('Y-m-d H:i:s')
                ]
            );

            return response()->json(
                ResponseCommands::collection(
                    $deshaceTrabajador->getResultado()
                ),
                200
            );
        } catch (\Exception $err) {
            return new WsError($err->getMessage(), 501);
        }
    }

    public function export()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }

    public function masivoTrabajadores(Request $request)
    {
        try {
            try {
                $cargue = new CargarTrabajadores(
                    new MyParams(
                        array(
                            'filepath' => $request->input('filepath'),
                            'documento' => $request->input('documento'),
                            'nit' => $request->input('nit'),
                            'codsuc' => $request->input('codsuc')
                        )
                    )
                );
                $cargue->procesarDocumento($request->input('filepath'));
                return response()->json($cargue->getResultado(), 201);
            } catch (QueryException $th) {
                throw new DebugException($th->getMessage(), 1);
            }
        } catch (DebugException $err) {
            return new WsError($err->getMessage(), 501);
        }
    }

    public function masivoConyuges(Request $request)
    {
        try {
            try {
                $cargue = new CargarConyuges(
                    new MyParams(
                        array(
                            'filepath' => $request->input('filepath'),
                            'documento' => $request->input('documento'),
                            'nit' => $request->input('nit')
                        )
                    )
                );

                $cargue->procesarDocumento($request->input('filepath'));
                return response()->json($cargue->getResultado(), 201);
            } catch (QueryException $th) {
                throw new DebugException($th->getMessage(), 1);
            }
        } catch (DebugException $err) {
            return new WsError($err->getMessage(), 501);
        }
    }

    public function masivoBeneficiarios(Request $request)
    {
        try {
            try {
                $cargue = new CargarBeneficiarios(
                    new MyParams(
                        array(
                            'filepath' => $request->input('filepath'),
                            'documento' => $request->input('documento'),
                            'nit' => $request->input('nit')
                        )
                    )
                );

                $cargue->procesarDocumento($request->input('filepath'));
                return response()->json($cargue->getResultado(), 201);
            } catch (QueryException $th) {
                throw new DebugException($th->getMessage(), 1);
            }
        } catch (DebugException $err) {
            return new WsError($err->getMessage(), 501);
        }
    }
}
