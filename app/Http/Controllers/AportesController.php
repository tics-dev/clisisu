<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AportesController extends Controller
{

    public function buscarAportesEmpresa(Request $request, int $nit)
    {
        Artisan::call(
            'server:send',
            [
                'clase' => 'AportesEmpresas',
                'metodo' => 'buscarAportesEmpresa',
                'user' => $request->input('_user'),
                'sistema' => $request->input('_sistema'),
                'env' => $request->input('_env'),
                'params' => $nit
            ]
        );
        return response()->json(json_decode(Artisan::output()), 200);
    }

    public function buscarMoraEmpresa(Request $request)
    {
        Artisan::call(
            'server:send',
            [
                'clase' => 'AportesEmpresas',
                'metodo' => 'buscarMoraEmpresa',
                'user' => $request->input('_user'),
                'sistema' => $request->input('_sistema'),
                'env' => $request->input('_env'),
                'params' => $request->input('nit') . '|' . $request->input('codsuc')
            ]
        );
        return response()->json(json_decode(Artisan::output()), 200);
    }
}
