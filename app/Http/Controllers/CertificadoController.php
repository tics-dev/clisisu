<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CertificadoController extends Controller
{

    public function buscarCertificadosBeneficiario(Request $request, string $documento)
    {
        Artisan::call(
            'server:send',
            [
                'clase' => 'Certificados',
                'metodo' => 'buscarCertificadosBeneficiario',
                'user' => $request->input('_user'),
                'sistema' => $request->input('_sistema'),
                'env' => $request->input('_env'),
                'params' => $documento
            ]
        );
        return response()->json(json_decode(Artisan::output()), 200);
    }
}
