<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReprocesoSatController extends Controller
{
    public function depurarEmpresaNoValida(Request $request)
    {
        Artisan::call(
            'server:send',
            [
                'clase' => 'Reprocesos',
                'metodo' => 'depurar_empresa_novalida',
                'user' => $request->input('_user'),
                'sistema' => $request->input('_sistema'),
                'env' => $request->input('_env'),
                'params' => base64_encode(json_encode(
                    [
                        'estado' => $request->input('estado'),
                    ]
                ))
            ]
        );
        return response()->json(json_decode(Artisan::output()), 200);
    }

    public function depurarTrabajadoresNoValidos(Request $request)
    {
        Artisan::call(
            'server:send',
            [
                'clase' => 'Reprocesos',
                'metodo' => 'depurar_trabajadores_novalidos',
                'user' => $request->input('_user'),
                'sistema' => $request->input('_sistema'),
                'env' => $request->input('_env'),
                'params' => base64_encode(json_encode(
                    [
                        'estado' => $request->input('estado'),
                    ]
                ))
            ]
        );
        return response()->json(json_decode(Artisan::output()), 200);
    }

    public function depurarGlozasMasivoEmpresas(Request $request)
    {
        Artisan::call(
            'server:send',
            [
                'clase' => 'Reprocesos',
                'metodo' => 'depurar_glozas_masivo_empresas',
                'user' => $request->input('_user'),
                'sistema' => $request->input('_sistema'),
                'env' => $request->input('_env'),
                'params' => null
            ]
        );
        return response()->json(json_decode(Artisan::output()), 200);
    }

    public function reproEmpresasNuevas(Request $request)
    {
        Artisan::call(
            'server:send',
            [
                'clase' => 'Reprocesos',
                'metodo' => 'reprocesar_empresas_nuevas',
                'user' => $request->input('_user'),
                'sistema' => $request->input('_sistema'),
                'env' => $request->input('_env'),
                'params' => null
            ]
        );
        return response()->json(json_decode(Artisan::output()), 200);
    }

    public function reproTrabajadoresInicioRelacion(Request $request)
    {
        Artisan::call(
            'server:send',
            [
                'clase' => 'Reprocesos',
                'metodo' => 'trabajadores_inicio_relacion',
                'user' => $request->input('_user'),
                'sistema' => $request->input('_sistema'),
                'env' => $request->input('_env'),
                'params' => null
            ]
        );
        return response()->json(json_decode(Artisan::output()), 200);
    }

    public function reproTrabajadoresTerminacionLaboral(Request $request)
    {
        Artisan::call(
            'server:send',
            [
                'clase' => 'Reprocesos',
                'metodo' => 'trabajadores_terminacion_laboral',
                'user' => $request->input('_user'),
                'sistema' => $request->input('_sistema'),
                'env' => $request->input('_env'),
                'params' => null
            ]
        );
        return response()->json(json_decode(Artisan::output()), 200);
    }

    public function reproTrabajadorCambioSalario(Request $request)
    {
        Artisan::call(
            'server:send',
            [
                'clase' => 'Reprocesos',
                'metodo' => 'trabajador_cambio_salario',
                'user' => $request->input('_user'),
                'sistema' => $request->input('_sistema'),
                'env' => $request->input('_env'),
                'params' => null
            ]
        );
        return response()->json(json_decode(Artisan::output()), 200);
    }

    public function reproEmpresasDesafiliaciones(Request $request)
    {
        Artisan::call(
            'server:send',
            [
                'clase' => 'Reprocesos',
                'metodo' => 'reprocesar_empresas_desafiliaciones',
                'user' => $request->input('_user'),
                'sistema' => $request->input('_sistema'),
                'env' => $request->input('_env'),
                'params' => null
            ]
        );
        return response()->json(json_decode(Artisan::output()), 200);
    }

    public function reproEmpresasReintegros(Request $request)
    {
        Artisan::call(
            'server:send',
            [
                'clase' => 'Reprocesos',
                'metodo' => 'empresas_reintegros',
                'user' => $request->input('_user'),
                'sistema' => $request->input('_sistema'),
                'env' => $request->input('_env'),
                'params' => null
            ]
        );
        return response()->json(json_decode(Artisan::output()), 200);
    }

    public function reproEmpresasPendientes(Request $request)
    {
        Artisan::call(
            'server:send',
            [
                'clase' => 'Reprocesos',
                'metodo' => 'enviar_empresas_pendientes',
                'user' => $request->input('_user'),
                'sistema' => $request->input('_sistema'),
                'env' => $request->input('_env'),
                'params' => null
            ]
        );
        return response()->json(json_decode(Artisan::output()), 200);
    }

    public function reproEmpresaAportes(Request $request)
    {
        Artisan::call(
            'server:send',
            [
                'clase' => 'Reprocesos',
                'metodo' => 'reprocesar_empresa_aportes',
                'user' => $request->input('_user'),
                'sistema' => $request->input('_sistema'),
                'env' => $request->input('_env'),
                'params' => null
            ]
        );
        return response()->json(json_decode(Artisan::output()), 200);
    }
}
