<?php

namespace App\Http\Controllers;

use App\Exceptions\DebugException;
use App\Http\Controllers\Controller;
use App\Libraries\MyParams;
use App\Services\Company\InformationCompany;
use App\Services\MercurioServicios\MercurioBeneficiario;
use App\Services\MercurioServicios\MercurioConyuge;
use App\Services\MercurioServicios\MercurioEmpresa;
use App\Services\MercurioServicios\MercurioTrabajador;
use App\Services\Poblacion\InformationBeneficiarios;
use App\Services\Poblacion\InformationConyuges;
use App\Services\Poblacion\InformationTrabajadores;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function informacionEmpresa(Request $request, int $nit = 0)
    {
        try {
            try {
                $informationCompany = new InformationCompany();
                $data = $informationCompany->consultaEmpresa(
                    new MyParams(
                        [
                            'estado' => $request->input('estado'),
                            'nit' => ($nit == 0 || is_null($nit)) ? $request->input('nit') : $nit
                        ]
                    )
                );
                return response()->json($data, 201);
            } catch (QueryException $th) {
                throw new DebugException($th->getMessage(), 501);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'estado'  => 'X',
                'success' => false
            );
        }
        return response()->json($salida, 501);
    }

    public function buscarSucursalesEnEmpresa(Request $request, int $nit)
    {
        try {
            try {
                $informationCompany = new InformationCompany();
                $data = $informationCompany->consultaSucursales(
                    new MyParams(
                        [
                            'nit' => ($nit == 0 || is_null($nit)) ? $request->input('nit') : $nit
                        ]
                    )
                );
                return response()->json($data, 201);
            } catch (QueryException $th) {
                throw new DebugException($th->getMessage(), 501);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'estado'  => 'X',
                'success' => false
            );
        }
        return response()->json($salida, 501);
    }

    public function buscarListasEnEmpresa(Request $request, int $nit)
    {
        try {
            try {
                $informationCompany = new InformationCompany();
                $data = $informationCompany->consultaListas(
                    new MyParams(
                        [
                            'nit' => ($nit == 0 || is_null($nit)) ? $request->input('nit') : $nit
                        ]
                    )
                );
                return response()->json($data, 201);
            } catch (QueryException $th) {
                throw new DebugException($th->getMessage(), 501);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'estado'  => 'X',
                'success' => false
            );
        }
        return response()->json($salida, 501);
    }

    public function informacionTrabajador(Request $request, int $cedtra)
    {
        try {
            try {
                $informationTrabajadores = new InformationTrabajadores();
                $data = $informationTrabajadores->buscar(
                    new MyParams(
                        [
                            'cedtra' => $cedtra
                        ]
                    )
                );
                return response()->json($data, 201);
            } catch (QueryException $th) {
                throw new DebugException($th->getMessage(), 501);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'estado'  => 'X',
                'success' => false
            );
        }
        return response()->json($salida, 501);
    }

    public function informacionConyuge(Request $request, int $cedcon)
    {
        try {
            try {
                $informationConyuges = new InformationConyuges();
                $data = $informationConyuges->buscar(
                    new MyParams(
                        [
                            'cedcon' => $cedcon
                        ]
                    )
                );
                return response()->json($data, 201);
            } catch (QueryException $th) {
                throw new DebugException($th->getMessage(), 501);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'estado'  => 'X',
                'success' => false
            );
        }
        return response()->json($salida, 501);
    }

    public function informacionBeneficiario(Request $request, int $numdoc)
    {
        try {
            try {
                $informationBeneficiarios = new InformationBeneficiarios();
                $data = $informationBeneficiarios->buscar(
                    new MyParams(
                        [
                            'numdoc' => $numdoc
                        ]
                    )
                );
                return response()->json($data, 201);
            } catch (QueryException $th) {
                throw new DebugException($th->getMessage(), 501);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : array(),
                'estado'  => 'X',
                'success' => false
            );
        }
        return response()->json($salida, 501);
    }

    public function actualizaEmpresaEnlinea(Request $request, int $documento)
    {
        try {
            try {
                $mercurio = new MercurioEmpresa();
                $mercurio->validarEstado($documento);
                $resultado = $mercurio->getResultado();
                return response()->json($resultado, 201);
            } catch (QueryException $th) {
                throw new DebugException($th->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : [],
                'estado'  => 'X',
                'success' => false
            );
        }
        return response()->json($salida, 501);
    }

    public function actualizaTrabajadorEnlinea(Request $request, int $documento)
    {
        try {
            try {
                $mercurio = new MercurioTrabajador();
                $mercurio->validarEstado($documento);
                $resultado = $mercurio->getResultado();
                return response()->json($resultado, 201);
            } catch (QueryException $th) {
                throw new DebugException($th->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : [],
                'estado'  => 'X',
                'success' => false
            );
        }
        return response()->json($salida, 501);
    }

    public function actualizaConyugeEnlinea(Request $request, int $documento)
    {
        try {
            try {
                $mercurio = new MercurioConyuge();
                $mercurio->validarEstado($documento);
                $resultado = $mercurio->getResultado();
                return response()->json($resultado, 201);
            } catch (QueryException $th) {
                throw new DebugException($th->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : [],
                'estado'  => 'X',
                'success' => false
            );
        }
        return response()->json($salida, 501);
    }

    public function actualizaBeneficiarioEnlinea(Request $request, int $documento)
    {
        try {
            try {
                $mercurio = new MercurioBeneficiario();
                $mercurio->validarEstado($documento);
                $resultado = $mercurio->getResultado();

                return response()->json($resultado, 201);
            } catch (QueryException $th) {
                throw new DebugException($th->getMessage(), 1);
            }
        } catch (DebugException $err) {
            $salida = array(
                "error"   => $err->getMessage(),
                'archivo' => basename($err->getFile()),
                'linea'   => $err->getLine(),
                'codigo'  => $err->getCode(),
                'data'    => ($err->getCode() === 555) ? DebugException::item("valid") : [],
                'estado'  => 'X',
                'success' => false
            );
        }
        return response()->json($salida, 501);
    }
}
