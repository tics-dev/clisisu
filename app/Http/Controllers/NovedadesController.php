<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NovedadesController extends Controller
{

    public function aportesTrabajadorNoAfiliado(Request $request, int $nit)
    {
        Artisan::call(
            'server:send',
            [
                'clase' => 'Novedades',
                'metodo' => 'aportes_trabajador_noafiliado',
                'user' => $request->input('_user'),
                'sistema' => $request->input('_sistema'),
                'env' => $request->input('_env'),
                'params' => base64_encode(json_encode([
                    'fechaini' => $request->input('fechaini'),
                    'fechafin' => $request->input('fechafin'),
                    'procesar' => $request->input('procesar'),
                ])),
            ]
        );
        return response()->json(json_decode(Artisan::output()), 200);
    }

    public function trabajadorAfiliadoSinAportes(Request $request, int $nit)
    {
        Artisan::call(
            'server:send',
            [
                'clase' => 'Novedades',
                'metodo' => 'trabajador_afiliado_sinaportes',
                'user' => $request->input('_user'),
                'sistema' => $request->input('_sistema'),
                'env' => $request->input('_env'),
                'params' => base64_encode(json_encode([
                    'fechaini' => $request->input('fechaini'),
                    'fechafin' => $request->input('fechafin'),
                    'valida_sucursal' => $request->input('valida_sucursal'),
                    'procesar' => $request->input('procesar'),
                ])),
            ]
        );
        return response()->json(json_decode(Artisan::output()), 200);
    }

    public function novedadTrabajadorSinPlanilla(Request $request, int $nit)
    {
        Artisan::call(
            'server:send',
            [
                'clase' => 'Novedades',
                'metodo' => 'novedad_trabajador_sin_planilla',
                'user' => $request->input('_user'),
                'sistema' => $request->input('_sistema'),
                'env' => $request->input('_env'),
                'params' => base64_encode(json_encode([
                    'estado' => $request->input('estado'),
                    'nit' => $nit
                ])),
            ]
        );
        return response()->json(json_decode(Artisan::output()), 200);
    }

    public function novedadTrabajadorTrayectoria(Request $request, int $nit)
    {
        Artisan::call(
            'server:send',
            [
                'clase' => 'Novedades',
                'metodo' => 'novedad_trabajador_trayectoria',
                'user' => $request->input('_user'),
                'sistema' => $request->input('_sistema'),
                'env' => $request->input('_env'),
                'params' => base64_encode(json_encode([
                    'estado' => $request->input('estado'),
                    'nit' => $nit
                ])),
            ]
        );
        return response()->json(json_decode(Artisan::output()), 200);
    }

    public function pruebaNovedades(Request $request, int $nit)
    {
        Artisan::call(
            'server:send',
            [
                'clase' => 'Novedades',
                'metodo' => 'prueba_novedades',
                'user' => $request->input('_user'),
                'sistema' => $request->input('_sistema'),
                'env' => $request->input('_env'),
                'params' => base64_encode(json_encode([
                    'estado' => $request->input('estado'),
                    'nit' => $nit
                ])),
            ]
        );
        return response()->json(json_decode(Artisan::output()), 200);
    }
}
