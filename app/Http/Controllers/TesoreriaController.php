<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TesoreriaController extends Controller
{

    public function procesarPagosBancos(Request $request, string $filename)
    {
        Artisan::call(
            'server:send',
            [
                'clase' => 'Tesoreria',
                'metodo' => 'procesarPagosBancos',
                'user' => $request->input('_user'),
                'sistema' => $request->input('_sistema'),
                'env' => $request->input('_env'),
                'params' => $filename
            ]
        );
        return response()->json(json_decode(Artisan::output()), 200);
    }
}
