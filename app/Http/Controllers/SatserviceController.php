<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SatserviceController extends Controller
{

    public function listarNotificaciones(Request $request)
    {
        Artisan::call(
            'server:send',
            [
                'clase' => 'Funcionalidades',
                'metodo' => 'listar_notificaciones_sat',
                'user' => $request->input('_user'),
                'sistema' => $request->input('_sistema'),
                'env' => $request->input('_env'),
                'params' => base64_encode(json_encode($request->input('post'))),
            ]
        );
        return response()->json(json_decode(Artisan::output()), 200);
    }
}
