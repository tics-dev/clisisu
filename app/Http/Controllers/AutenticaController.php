<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;
use App\Libraries\JwtLibrary;
use App\Services\HashService;
use App\Services\JwtRecursoService;
use App\Models\Subsi15;

class AutenticaController extends Controller
{

    private $jwtLibrary;
    private $hashService;
    private $recursoService;

    public function __construct()
    {
        $this->jwtLibrary = new JwtLibrary();
        $this->hashService = new HashService();
        $this->recursoService = new JwtRecursoService();
    }

    public function autenticar(Request $request)
    {
        try {
            $cliente = $request->input('cliente_id');
            $clave = $request->input('password');

            if (!isset($cliente)) {
                throw new \Exception('Lo siento, no se dispone de usuario para la autenticación', 404);
            }
            if (!isset($clave)) {
                throw new \Exception('Lo siento, no se dispone de clave para la autenticación', 404);
            }
            $recurso = $this->recursoService->getJwtRecursoClientId($cliente);
            if ($recurso && $this->hashService->VerifyHash($clave, $recurso->cliente_id, $recurso->jwpassword)) {
                $data = array(
                    'id' => $recurso->id,
                    'cliente_id' => $recurso->cliente_id
                );
                $token = $this->jwtLibrary->generateToken($data);
                return response()->json(array(
                    'status' => true,
                    'token' => $token
                ), 202);
            } else {
                throw new \Exception('No son validos los criterios para la autenticación', 404);
            }
        } catch (\Exception $err) {
            return response()->json(array(
                'status' => false,
                'message' => $err->getMessage()
            ), 400);
        }
    }
}
