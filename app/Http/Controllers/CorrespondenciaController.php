<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CorrespondenciaController extends Controller
{

    public function reportePrimerAviso(Request $request)
    {
        Artisan::call(
            'server:send',
            [
                'clase' => 'Correspondencias',
                'metodo' => 'reportePrimerAviso',
                'user' => $request->input('_user'),
                'sistema' => $request->input('_sistema'),
                'env' => $request->input('_env'),
                'params' => $request->input('fecha_generacion')
            ]
        );
        return response()->json(json_decode(Artisan::output()), 200);
    }

    public function reporteAvisoDesafiliacion(Request $request)
    {
        Artisan::call(
            'server:send',
            [
                'clase' => 'Correspondencias',
                'metodo' => 'reporteAvisoDesafiliacion',
                'user' => $request->input('_user'),
                'sistema' => $request->input('_sistema'),
                'env' => $request->input('_env'),
                'params' => $request->input('fecha_generacion')
            ]
        );
        return response()->json(json_decode(Artisan::output()), 200);
    }
}
