<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TipopagoController extends Controller
{

    public function cruzaCuentaDaviplata(Request $request)
    {
        Artisan::call(
            'server:send',
            [
                'clase' => 'CruzarDaviplata',
                'metodo' => 'actualiza_cuentas_daviplata',
                'params' =>  base64_encode(json_encode(['file' => $request->input('filename')])),
                'user' => $request->input('_user'),
                'sistema' => $request->input('_sistema'),
                'env' => $request->input('_env'),
                'comando' => $request->input('comando')
            ]
        );
        return response()->json(json_decode(Artisan::output()), 200);
    }

    public function noCertificaDaviplata(Request $request)
    {
        Artisan::call(
            'server:send',
            [
                'clase' => 'CruzarDaviplata',
                'metodo' => 'cuentas_daviplata_no_certificadas',
                'params' => base64_encode(json_encode(['file' => $request->input('filename')])),
                'user' => $request->input('_user'),
                'sistema' => $request->input('_sistema'),
                'env' => $request->input('_env'),
                'comando' => null
            ]
        );
        return response()->json(json_decode(Artisan::output()), 200);
    }
}
