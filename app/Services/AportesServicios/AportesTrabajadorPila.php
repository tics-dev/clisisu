<?php

namespace App\Services\AportesServicios;

use Illuminate\Support\Facades\DB;
use App\Models\Subsi16;


class AportesTrabajadorPila
{
    private $nit_empresa;
    private $cedula_trabajador;
    private $codigo_sucursal;

    public function setNitEmpresa($nit)
    {
        $this->nit_empresa = $nit;
    }

    public function setCedulaTrabajador($cedtra)
    {
        $this->cedula_trabajador = $cedtra;
    }

    public function setCodigoSucursal($codsuc)
    {
        $this->codigo_sucursal = $codsuc;
    }

    public function buscarAporte($periodo): bool
    {
        $aporte_trabajador = DB::table('subsi65')
            ->select('subsi64.codsuc', 'subsi64.nit', 'subsi65.cedtra', 'subsi65.novret', 'subsi64.numero', 'subsi65.valnom', 'subsi65.valapo', 'subsi64.perapo')
            ->join('subsi64', 'subsi64.numero', '=', 'subsi65.numero')
            ->where("subsi65.cedtra", $this->cedula_trabajador)
            ->where("subsi64.perapo", $periodo)
            ->where("subsi64.nit",    $this->nit_empresa)
            ->where("subsi64.codsuc", $this->codigo_sucursal)
            ->get()
            ->first();

        if ($aporte_trabajador) {
            $trayectos = Subsi16::where("cedtra", $this->cedula_trabajador)
                ->where('nit', $this->nit_empresa)
                ->where('codsuc', $this->codigo_sucursal)
                ->get();

            $has_trayecto = false;
            foreach ($trayectos as $trayecto) {

                //trayecto vigente
                if (is_null($trayecto->fecret) || empty($trayecto->fecret)) {

                    $periodo_ini_trayecto = date('Ym', $trayecto->fecafi);
                    if ($periodo_ini_trayecto <= $periodo) {
                        $has_trayecto = true;
                    }
                } else {
                    //trayecto finalizado
                    $periodo_ini_trayecto = date('Ym', $trayecto->fecafi);
                    $periodo_fin_trayecto = date('Ym', $trayecto->fecret);
                    if ($periodo_ini_trayecto <= $periodo && $periodo_fin_trayecto >= $periodo) {
                        $has_trayecto = true;
                    }
                }
            }
            if ($has_trayecto) return true;
        } else {
            return false;
        }
    }
}
