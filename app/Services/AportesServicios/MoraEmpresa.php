<?php

namespace App\Services\AportesServicios;

use App\Exceptions\DebugException;
use App\Models\Subsi02;
use App\Models\Subsi12;
use Illuminate\Support\Facades\DB;
use App\Models\Subsi16;
use App\Models\Subsi48;
use DateTime;

class MoraEmpresa
{
    protected $console;
    private $nit_empresa;
    private $codigo_sucursal;
    private $sucursal;
    private $empresa;
    private $aportesPeriodo = array();
    private $diferenciaPeriodosAportes = array();
    private $tperiodos;
    private $trayectoTrabajadores = array();

    public function __construct($nit, $codigo_sucursal)
    {
        $this->nit_empresa = $nit;
        $this->codigo_sucursal = $codigo_sucursal;
    }

    public function set_env($console)
    {
        $this->console = $console;
    }

    public function buscarMora()
    {
        $this->sucursal = Subsi48::where('nit', $this->nit_empresa)
            ->where('codsuc', $this->codigo_sucursal)
            ->whereNotIn('codind', ['48', '47'])
            ->get()
            ->first();

        $this->empresa = Subsi02::where('nit', $this->nit_empresa)->get()->first();

        if (!$this->empresa) {
            throw new DebugException("Error la empresa no es valida", 1);
        }

        $this->buscarTrabajadoresRangos();

        $this->getDiferenciaPeriodos();

        $moras = $this->trabajadoresSinAporte();

        return  [
            'periodos' => implode(',', $this->tperiodos),
            'aportes_periodos' => $this->aportesPeriodo,
            'diferencia_peridos_aportes' => implode(',', $this->diferenciaPeriodosAportes),
            'trayectos' => $this->trayectoTrabajadores,
            'moras' => $moras
        ];
    }

    function getPeriodo($fecha)
    {
        if (preg_match('/^(\d{4})-(\d{1,2})-(\d{1,2})$/', $fecha)) {
            $date = new DateTime($fecha);
            return $date->format('Ym');
        } else {
            return false;
        }
    }

    function buscarTrabajadoresRangos()
    {
        $trayectorias = Subsi16::where('nit', $this->nit_empresa)->where('codsuc', $this->sucursal->codsuc)->get();
        if ($trayectorias->count() > 0) {
            foreach ($trayectorias as $trayecto) {
                $periodo_inicio_aportes = $this->getPeriodo($trayecto->fecafi);
                $periodo_final_aportes = $this->getPeriodo($trayecto->fecret);

                if (!$periodo_final_aportes) {
                    $min = Subsi12::where('girado', 'N')->min('periodo');
                    $periodo_final_aportes = $min;
                }

                $this->trayectoTrabajadores[$trayecto->cedtra][] = [
                    'ini' => $periodo_inicio_aportes,
                    'fin' => $periodo_final_aportes
                ];
            }
        }
    }

    function getDiferenciaPeriodos()
    {
        $modPeriodos = [];
        foreach ($this->trayectoTrabajadores as $cedtra => $trayectos) {
            foreach ($trayectos as $item) {
                $dataPeriodos = $this->getPeriodoConfig($item['ini'], $item['fin']);
                foreach ($dataPeriodos  as $periodo) $modPeriodos[$periodo->periodo] = 1;
            }
        }

        $this->tperiodos = array_keys($modPeriodos);
        $periodoPago = array();

        $outs = collect($this->aportes(implode(',', $this->tperiodos)));
        if ($outs->count() > 0) {
            foreach ($outs as $out) {
                $periodoPago[] = $out->periodo;
                $this->aportesPeriodo[$out->periodo] = $out->apoins;
            }
        }
        $this->diferenciaPeriodosAportes = array_values(array_diff($this->tperiodos, $periodoPago));
    }

    function trabajadoresSinAporte()
    {
        $data = array();
        if ($this->diferenciaPeriodosAportes) {
            foreach ($this->diferenciaPeriodosAportes as $periodo) {
                foreach ($this->trayectoTrabajadores as $cedtra => $trayectos) {
                    foreach ($trayectos as $item) {
                        $dataPeriodos = $this->getPeriodoConfig($item['ini'], $item['fin']);
                        if ($dataPeriodos->count() > 0) {
                            foreach ($dataPeriodos as $mperiodo) {
                                if ($mperiodo->periodo == $periodo) {
                                    $data[$periodo][$cedtra] = 1;
                                }
                            }
                        }
                    }
                }
            }
        } else {
            foreach ($this->trayectoTrabajadores as $cedtra => $trayectos) {
                $nominas = new AportesTrabajadorNomina($this->nit_empresa, $cedtra, $this->codigo_sucursal);
                foreach ($trayectos as $item) {
                    $dataPeriodos = $this->getPeriodoConfig($item['ini'], $item['fin']);
                    if ($dataPeriodos->count() > 0) {
                        foreach ($dataPeriodos as $mperiodo) {
                            if ($nominas->buscarAporte($mperiodo->periodo) == false) {
                                $data[$mperiodo->periodo][$cedtra] = 1;
                            }
                        }
                    }
                }
            }
        }
        return $data;
    }

    function aportes($periodos)
    {
        $aporte = DB::select("SELECT 
            SUM(subsi08.apoins) AS apoins,
            SUM(subsi11.valnom) AS codins, 
            subsi08.documento, 
            subsi08.marca,
            subsi11.periodo 
        FROM subsi11 
        INNER JOIN subsi08 ON subsi08.marca = subsi11.marca AND subsi08.documento = subsi11.documento
        WHERE subsi11.nit = '{$this->nit_empresa}' AND
            subsi11.codsuc='{$this->sucursal->codsuc}' AND 
            subsi11.periodo IN({$periodos}) AND 
            subsi08.codins = '1' AND 
            subsi08.apoins > 0 
        GROUP BY subsi11.periodo");
        return $aporte;
    }

    public function aportePeriodo($codsuc, $periodo)
    {
        $sql = "SELECT subsi11.* 
		FROM subsi11  
        INNER JOIN subsi06 ON subsi11.codind = subsi06.codind 
		WHERE 
            subsi11.nit='{$this->nit_empresa}' AND 
            subsi11.codsuc='{$codsuc}' AND
			subsi11.periodo='{$periodo}' AND 
			subsi06.codins='1'";
        return collect(DB::select($sql))->count();
    }

    function getPeriodoConfig($periodo_inicial, $periodo_final)
    {
        return Subsi12::where('periodo', '>=', $periodo_inicial)->where('periodo', '<=', $periodo_final)->get();
    }
}
