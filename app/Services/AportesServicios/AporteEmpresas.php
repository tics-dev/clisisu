<?php

namespace App\Services\AportesServicios;

use Illuminate\Support\Facades\DB;

class AporteEmpresas
{

    public function buscarAportesEmpresa($nit)
    {
        $aportes = DB::table('subsi65')
            ->select(
                'subsi64.fecrec',
                'subsi64.fecsis',
                'subsi64.codsuc',
                'subsi64.nit',
                'subsi65.cedtra',
                'subsi65.novret',
                'subsi64.numero',
                'subsi65.valnom',
                'subsi65.valapo',
                'subsi64.perapo'
            )
            ->join('subsi64', 'subsi64.numero', '=', 'subsi65.numero')
            ->where("subsi64.nit", $nit)
            ->get();

        return [
            'success' => true,
            'data' => $aportes->toArray()
        ];
    }
}
