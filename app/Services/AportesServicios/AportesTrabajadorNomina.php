<?php

namespace App\Services\AportesServicios;

use Illuminate\Support\Facades\DB;

class AportesTrabajadorNomina
{
    private $nit_empresa;
    private $cedula_trabajador;
    private $codigo_sucursal;

    public function __construct($nit = '', $cedtra = '', $codigo_sucursal = '')
    {
        $this->nit_empresa = $nit;
        $this->cedula_trabajador = $cedtra;
        $this->codigo_sucursal = $codigo_sucursal;
    }

    public function setNitEmpresa($nit)
    {
        $this->nit_empresa = $nit;
    }

    public function setCedulaTrabajador($cedtra)
    {
        $this->cedula_trabajador = $cedtra;
    }

    public function setCodigoSucursal($codsuc)
    {
        $this->codigo_sucursal = $codsuc;
    }

    public function buscarAporte($periodo): bool
    {
        $aporte_trabajador = DB::table('subsi10')
            ->where("subsi10.periodo", $periodo)
            ->where("subsi10.nit",    $this->nit_empresa)
            ->where("subsi10.cedtra", $this->cedula_trabajador)
            ->where("subsi10.codsuc", $this->codigo_sucursal);

        $aporte_trabajador = $aporte_trabajador->get()->count();
        if ($aporte_trabajador > 0) {
            return true;
        } else {
            return false;
        }
    }
}
