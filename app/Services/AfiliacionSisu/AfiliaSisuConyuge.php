<?php

namespace  App\Services\AfiliacionSisu;

use Illuminate\Database\QueryException;
use App\Exceptions\DebugException;
use Illuminate\Support\Facades\DB;
use App\Models\Subsi15;
use App\Models\Subsi20;
use App\Models\Subsi21;
use App\Models\Subsi35;

class AfiliaSisuConyuge
{
	protected $resultado;
	protected $hoy;
	private $afiliacion_nueva;
	private $cedcon;

	public function __construct()
	{
		$this->hoy = date('Y-m-d');
	}

	public function procesar($model_conyuge)
	{
		DB::beginTransaction();
		try {
			$this->cedcon = $model_conyuge->cedcon;
			if ($this->validaTrabajador($model_conyuge) == 0) throw new DebugException("El trabajador no está activo en el sistema de subsidio.", 500);

			$conyuge = Subsi20::where('cedcon', $model_conyuge->cedcon)->get()->first();
			if ($conyuge) {

				$this->actualizaConyuge($conyuge, $model_conyuge);

				$relation =  Subsi21::where('cedcon', $this->cedcon)
					->where('cedtra', $model_conyuge->cedtra)
					->get()
					->first();

				if ($relation) {

					$this->actualizaRelacionTrabajador($relation, $model_conyuge);
				} else {

					$otra_relation =  Subsi21::where('cedcon', $this->cedcon)->get()->first();
					if ($otra_relation) {
						$modelo = clone $otra_relation;
						$modelo->comper = 'N';
						$this->actualizaRelacionTrabajador($otra_relation, $modelo);
					}
					$this->crearRelacionTrabajador($model_conyuge);
				}
			} else {

				$this->crearConyuge($model_conyuge);
				$this->crearRelacionTrabajador($model_conyuge);
				$this->registroPrimeraVez($model_conyuge);
			}

			DB::commit();
			$this->resultado = [
				"success" => true,
				"msj" => "El proceso de registro se completo con éxito"
			];
		} catch (QueryException $err) {

			DB::rollBack();
			throw new DebugException($err->getMessage() . ' ' . $err->getLine() . ' ' . basename($err->getFile()), 1);
		} catch (\Exception $ex) {

			DB::rollBack();
			throw new DebugException($ex->getMessage() . ' ' . $ex->getLine() . ' ' . basename($ex->getFile()), 1);
		}
	}

	public function validaTrabajador($params)
	{
		return Subsi15::where('cedtra', $params->cedtra)->where('estado', 'A')->get()->count();
	}

	public function crearConyuge($params)
	{
		$conyuge = new Subsi20();
		$attributes = array_flip($conyuge->getFillable());
		foreach ($params as $prop => $valor) {
			if (array_key_exists($prop, $attributes)) {
				$conyuge->$prop = "$valor";
			}
		}
		$conyuge->coddoc = $params->tipdoc;
		$conyuge->estado = 'A';
		$conyuge->fecest = null;

		if ($conyuge->fecact == '') $conyuge->fecact = null;
		if ($conyuge->fecnac == '') $conyuge->fecnac = null;
		if ($conyuge->fecsal == '') $conyuge->fecsal = $params->fecafi;

		$validator = $conyuge->isValid($conyuge->reglas_creacion());
		if ($validator) {
			if (!$conyuge->save()) {
				throw new DebugException("Error al guardar el modelo {$conyuge->getTable()}", 501);
			}
		} else {
			DebugException::add("valid", $validator);
			throw new DebugException("Error al guardar el modelo {$conyuge->getTable()}", 555);
		}
		$this->afiliacion_nueva = true;
	}

	public function actualizaConyuge($conyuge, $params)
	{
		$attributes = array_flip($conyuge->getFillable());

		if ($conyuge->tippag == 'A' || $conyuge->tippag == 'D') {
			unset($params->numcue);
			unset($params->tippag);
			unset($params->codban);
			if (isset($params->tipcue)) unset($params->tipcue);
		}

		foreach ($params as $prop => $valor) {
			if ($prop == 'fecest') continue;
			if ($prop == 'codest') continue;
			if (array_key_exists($prop, $attributes)) {
				$conyuge->$prop = "$valor";
			}
		}

		$conyuge->coddoc = $params->tipdoc;
		$conyuge->estado = 'A';
		$conyuge->fecest = null;
		if ($conyuge->fecact == '') $conyuge->fecact = null;
		if ($conyuge->fecnac == '') $conyuge->fecnac = null;
		if ($conyuge->fecsal == '') $conyuge->fecsal = $params->fecafi;

		$validator = $conyuge->isValid($conyuge->reglas_creacion());
		if ($validator) {
			if (!$conyuge->save()) {
				throw new DebugException("Error al guardar el modelo {$conyuge->getTable()}", 501);
			}
		} else {
			DebugException::add("valid", $validator);
			throw new DebugException("Error al guardar el modelo {$conyuge->getTable()}", 555);
		}
		$this->afiliacion_nueva = false;
	}

	public function crearRelacionTrabajador($params)
	{
		$relation = new Subsi21();
		$attributes = array_flip($relation->getFillable());
		foreach ($params as $prop => $valor) {
			if (array_key_exists($prop, $attributes)) {
				$relation->$prop = "$valor";
			}
		}
		$relation->fecsis = $this->hoy;

		$validator = $relation->isValid($relation->reglas_creacion());
		if ($validator) {
			if (!$relation->save()) {
				throw new DebugException("Error al guardar el modelo {$relation->getTable()}", 501);
			}
		} else {
			DebugException::add("valid", $validator);
			throw new DebugException("Error al guardar el modelo {$relation->getTable()}", 555);
		}
	}

	public function actualizaRelacionTrabajador($relation, $params)
	{
		if (!$relation) return false;
		$relation->recsub = $params->recsub;
		$relation->fecsis = $this->hoy;
		$relation->comper = $params->comper;

		$validator = $relation->isValid($relation->reglas_creacion());
		if ($validator) {
			if (!$relation->save()) {
				throw new DebugException("Error al guardar el modelo {$relation->getTable()}", 501);
			}
		} else {
			DebugException::add("valid", $validator);
			throw new DebugException("Error al guardar el modelo {$relation->getTable()}", 555);
		}
	}

	public function registroPrimeraVez($params)
	{
		$trayecto = new Subsi35();
		$trayecto->cedcon = $params->cedcon;
		$trayecto->fecha = $this->hoy;
		$trayecto->salario = $params->salario;

		if (!$trayecto->save()) {
			throw new DebugException("Error al guardar el modelo {$trayecto->getTable()}", 501);
		}
	}

	public function getResultado()
	{
		return $this->resultado;
	}

	public function detalleGestion()
	{
		return [
			$this->afiliacion_nueva
		];
	}
}
