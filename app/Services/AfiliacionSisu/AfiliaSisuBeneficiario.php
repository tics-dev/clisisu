<?php

namespace  App\Services\AfiliacionSisu;

use Illuminate\Database\QueryException;
use App\Exceptions\DebugException;
use Illuminate\Support\Facades\DB;
use App\Models\Subsi22;
use App\Models\Subsi23;
use App\Models\Gener13;

class AfiliaSisuBeneficiario
{
    protected $resultado;
    protected $codsuc;

    protected $hoy;
    private $afiliacion_nueva;
    private $afiliacion_principal;
    private $codben;

    public function __construct()
    {
        $this->hoy = date('Y-m-d');
    }

    public function procesar($model_beneficiario)
    {
        DB::beginTransaction();
        try {
            $this->codben = null;
            $beneficiarios = Subsi22::where("documento", $model_beneficiario->numdoc)->get();
            if ($beneficiarios->count() > 0) {
                foreach ($beneficiarios as $mben) {
                    $beneficiarioRelacion = Subsi23::where("cedtra", $model_beneficiario->cedtra)
                        ->where("codben", $mben->codben)
                        ->get();

                    if ($beneficiarioRelacion->count() > 0) {
                        $this->codben = $mben->codben;
                        break;
                    }
                }
            }

            //ya existe y con la relacion con el trabajador
            if ($this->codben) {
                $this->actualizaBeneficiario($model_beneficiario);
                $this->actualizaRelacionTrabajador($model_beneficiario);
            } else {
                //si ya esta registrado usa el codigo de beneficiario para crear la relacion
                if ($beneficiarios->count() == 1) {
                    $mbene = $beneficiarios->first();
                    $this->codben = $mbene->codben;
                } else {
                    $this->generaCodigoBeneficiario();
                    $this->crearBeneficiario($model_beneficiario);
                }
                $this->crearRelacionTrabajador($model_beneficiario);
            }

            DB::commit();

            $this->resultado = [
                "success" => true,
                "msj" => "El proceso de registro se completo con éxito"
            ];
        } catch (QueryException $err) {
            DB::rollBack();
            throw new DebugException($err->getMessage() . ' ' . $err->getLine() . ' ' . basename($err->getFile()), 1);
        } catch (\Exception $ex) {
            DB::rollBack();
            throw new DebugException($ex->getMessage() . ' ' . $ex->getLine() . ' ' . basename($ex->getFile()), 1);
        }
    }

    public function crearBeneficiario($params)
    {
        $beneficiario = new Subsi22();
        $attributes = array_flip($beneficiario->getFillable());
        foreach ($params as $prop => $valor) {
            if (array_key_exists($prop, $attributes)) {
                $beneficiario->$prop = "$valor";
            }
        }
        $beneficiario->fecest = null;
        $beneficiario->codest = null;
        $beneficiario->codben = $this->codben;
        $beneficiario->documento = $params->numdoc;
        $beneficiario->coddoc = $params->tipdoc;
        $beneficiario->codgir = (isset($params->codgir)) ? $params->codgir : 'NU';
        $beneficiario->estado = 'A';

        $validator = $beneficiario->isValid($beneficiario->reglas_creacion());
        if ($validator) {
            if (!$beneficiario->save()) {
                throw new \Exception("Error al guardar el modelo {$beneficiario->getTable()}", 501);
            }
        } else {
            DebugException::add("valid", $validator);
            throw new \Exception("Error al guardar el modelo {$beneficiario->getTable()}", 555);
        }
        $this->afiliacion_nueva = true;
    }

    public function actualizaBeneficiario($params)
    {
        $beneficiario = Subsi22::where('documento', $params->numdoc)
            ->where('codben', $this->codben)
            ->get()
            ->first();

        if (!$beneficiario) return false;

        $attributes = array_flip($beneficiario->getFillable());
        foreach ($params as $prop => $valor) {
            if ($prop == 'fecest') continue;
            if ($prop == 'codest') continue;
            if (array_key_exists($prop, $attributes)) {
                $beneficiario->$prop = "$valor";
            }
        }

        $beneficiario->coddoc = $params->tipdoc;
        $beneficiario->estado = 'A';
        $beneficiario->codgir = (isset($params->codgir)) ? $params->codgir : 'NU';

        $validator = $beneficiario->isValid($beneficiario->reglas_creacion());
        if ($validator) {
            if (!$beneficiario->save()) {
                throw new \Exception("Error al guardar el modelo {$beneficiario->getTable()}", 501);
            }
        } else {
            DebugException::add("valid", $validator);
            throw new \Exception("Error al guardar el modelo {$beneficiario->getTable()}", 555);
        }
        $this->afiliacion_nueva = false;
    }

    public function crearRelacionTrabajador($params)
    {
        $relation = new Subsi23();

        $attributes = array_flip($relation->getFillable());
        foreach ($params as $prop => $valor) {
            if (array_key_exists($prop, $attributes)) {
                $relation->$prop = "$valor";
            }
        }

        $relation->codben = $this->codben;
        $relation->fecpre = $params->fecsol;
        $relation->fecsis = $this->hoy;
        $relation->ruaf   = 'N';

        $validator = $relation->isValid($relation->reglas_creacion());
        if ($validator) {
            if (!$relation->save()) {
                throw new \Exception("Error al guardar el modelo {$relation->getTable()}", 501);
            }
        } else {
            DebugException::add("valid", $validator);
            throw new \Exception("Error al guardar el modelo {$relation->getTable()}", 555);
        }
    }

    public function actualizaRelacionTrabajador($params)
    {
        $relation =  Subsi23::where('codben', $this->codben)
            ->where('cedtra', $params->cedtra)
            ->get()
            ->first();

        if (!$relation) return false;

        $relation->fecnac = $params->fecnac;
        $relation->numhij = $params->numhij;
        $relation->pago   = $params->pago;
        $relation->fecsis = $this->hoy;
        $relation->ruaf   = 'N';
        $relation->cedacu = $params->cedacu;

        $validator = $relation->isValid($relation->reglas_creacion());
        if ($validator) {
            if (!$relation->save()) {
                throw new \Exception("Error al guardar el modelo {$relation->getTable()}", 501);
            }
        } else {
            DebugException::add("valid", $validator);
            throw new \Exception("Error al guardar el modelo {$relation->getTable()}", 555);
        }
    }

    /**
     * generaCodigoBeneficiario function
     * Tomamos el turno antes que lo tome otro para crear el codigo beneficiario
     * @return void
     */
    public function generaCodigoBeneficiario()
    {
        $secuencia = Gener13::where('codapl', 'SU')
            ->where('marca', 'BE')
            ->get()
            ->first();

        $numero = $secuencia->numero + 1;
        $secuencia->numero = $numero;
        $secuencia->save();
        $this->codben = $numero;
    }

    public function getResultado()
    {
        return $this->resultado;
    }

    public function detalleGestion()
    {
        return [
            $this->afiliacion_principal,
            $this->afiliacion_nueva
        ];
    }
}
