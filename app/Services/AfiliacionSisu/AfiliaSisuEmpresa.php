<?php

namespace  App\Services\AfiliacionSisu;

use Illuminate\Database\QueryException;
use App\Exceptions\DebugException;
use Illuminate\Support\Facades\DB;
use App\Console\Servicios\ServicioSat\Tareas;
use App\Models\Subsi02;
use App\Models\Subsi03;
use App\Models\Subsi48;
use App\Models\Subsi73;
use App\Models\Gener18;

class AfiliaSisuEmpresa
{

    protected $resultado;
    protected $codsuc;
    protected $hoy;
    private $afiliacion_nueva;


    public function __construct()
    {
        $this->hoy = date('Y-m-d');
    }

    public function procesar($model_empresa)
    {
        DB::beginTransaction();
        try {
            //valida empresa a registrar
            $empresa = $this->buscarEmpresa($model_empresa->nit);

            //si ya existe la empresa o es reintegro
            if ($empresa) {
                //actualizar la empresa
                $this->actualizaEmpresa($empresa, $model_empresa);
            } else {
                $model_empresa->fosfec = 'N';
                $model_empresa->codase = '09';
                $model_empresa->todmes = 'S';
                //empresa primera vez que se afilia
                $this->crearEmpresa($model_empresa);
            }

            //se registra nueva trayectoria
            $this->guardarTrayectoria($model_empresa);
            //se registra la sucursal

            $cantidad = Subsi48::where('nit', $model_empresa->nit)
                ->get()
                ->count();

            // ya posee una sucursal
            if ($cantidad > 0) {
                $cantidad_sucursal_zona = Subsi48::where('nit', $model_empresa->nit)
                    ->where('codzon', $model_empresa->codzon)
                    ->where('codact', $model_empresa->codact)
                    ->get()
                    ->count();

                //editar sucursal ya existente
                if ($cantidad_sucursal_zona > 0) {

                    $sucursal = $this->buscarSucursal($model_empresa->nit, $model_empresa->codzon, $model_empresa->codact);
                    $this->actualizaSucursal($sucursal, $model_empresa);
                } else {
                    //crear una sucursal

                    $cantidad = $cantidad + 1;
                    $this->codsuc = str_pad("{$cantidad}", 3, '0', STR_PAD_LEFT);
                    $this->guardarSucursal($model_empresa);
                }
            } else {
                $this->codsuc = '001';
                $this->guardarSucursal($model_empresa);
            }

            //se registra lista
            $this->crearLista($model_empresa);

            DB::commit();

            $this->resultado = array(
                "success" => true,
                "msj" => "El proceso de registro se completo con éxito"
            );
        } catch (QueryException $err) {
            DB::rollBack();
            throw new DebugException($err->getMessage() . ' ' . $err->getLine() . ' ' . basename($err->getFile()), 1);
        } catch (\Exception $ex) {
            DB::rollBack();
            throw new DebugException($ex->getMessage() . ' ' . $ex->getLine() . ' ' . basename($ex->getFile()), 1);
        }
    }

    /**
     * preparaData function
     * alista los datos para registro, normaliza las variables para el setter
     * @param [type] $data
     * @return void
     */
    public function preparaData($data)
    {
        $tipo_documento = Gener18::where('coddoc', $data->tipdoc)->get()->first();
        $data->coddoc = substr($tipo_documento->codrua, 0, 2);
        $data->coddocrepleg = (strlen($data->coddocrepleg) > 0) ? trim($tipo_documento->codrua) : 'CC';
        $data->feccam   = $this->hoy;
        $data->estado   = 'A';
        $data->telt     = $data->celpri;
        $data->telr     = $data->celular;
        $data->mailr    = $data->emailpri;
        $data->calsuc   = $data->calemp;
        $data->nomcon   = $data->repleg;
        $data->detalle  = $data->razsoc;
        $data->coddoc   = $data->tipdoc;
        $data->nomemp   = $data->razsoc;
        $data->fecapr   = $data->feccap;
        $data->fecsis   = $this->hoy;
        $data->observacion = $data->nota;
        return $data;
    }

    public function crearEmpresa($params)
    {
        $empresa = new Subsi02();
        $attributes = array_flip($empresa->getFillable());
        foreach ($params as $prop => $valor) {
            if (array_key_exists($prop, $attributes)) {
                $empresa->$prop = "$valor";
            }
        }

        $empresa->estado  = 'A';
        $empresa->totapo  = '0';
        $empresa->totcon  = '0';
        $empresa->tothij  = '0';
        $empresa->tother  = '0';
        $empresa->totpad  = '0';
        $empresa->jefper  = null;
        $empresa->cedpro  = null;
        $empresa->nompro  = null;
        $empresa->feccer  = null;
        $empresa->resest  = null;
        $empresa->codest  = null;
        $empresa->fecest  = null;
        $empresa->fecmer  = null;
        $empresa->feccor  = null;
        $empresa->valmor  = null;
        $empresa->permor  = null;
        $empresa->mailr   = null;
        $empresa->giass   = null;
        $empresa->actugp  = null;
        $empresa->correo  = null;

        $validator = $empresa->isValid($empresa->reglas_creacion());
        if ($validator) {
            if (!$empresa->save()) {
                throw new DebugException("Error al guardar el modelo {$empresa->getTable()}", 501);
            }
        } else {
            DebugException::add("valid", $validator);
            throw new DebugException("Error al guardar el modelo {$empresa->getTable()}", 555);
        }
        $this->afiliacion_nueva = true;
    }

    public function actualizaEmpresa($empresa, $params)
    {
        $attributes = array_flip($empresa->getFillable());
        foreach ($params as $prop => $valor) {
            if (array_key_exists($prop, $attributes)) {
                $empresa->$prop = "$valor";
            }
        }

        $empresa->fecest  = ($empresa->estado == 'I') ? $params->feccam : null;
        $empresa->codest  = ($empresa->estado == 'I') ? ((isset($params->codest)) ? $params->codest : null) : null;

        $validator = $empresa->isValid($empresa->reglas_creacion());
        if ($validator) {
            if (!$empresa->save()) {
                throw new DebugException("Error al guardar el modelo {$empresa->getTable()}", 501);
            }
        } else {
            DebugException::add("valid", $validator);
            throw new DebugException("Error al guardar el modelo {$empresa->getTable()}", 555);
        }

        $this->afiliacion_nueva = false;
    }

    public function guardarSucursal($params)
    {
        $sucursal = new Subsi48();
        $attributes = array_flip($sucursal->getFillable());
        foreach ($params as $prop => $valor) {
            if (array_key_exists($prop, $attributes)) {
                $sucursal->$prop = "$valor";
            }
        }
        $sucursal->codsuc     = $this->codsuc;
        $sucursal->traapo     = '0';
        $sucursal->valapo     = '0';
        $sucursal->tottra     = '0';
        $sucursal->totapo     = '0';
        $sucursal->totcon     = '0';
        $sucursal->tothij     = '0';
        $sucursal->tother     = '0';
        $sucursal->totpad     = '0';
        $sucursal->tietra     = '0';
        $sucursal->tratot     = '0';
        $sucursal->correo     = 'N';
        $sucursal->codest     = null;
        $sucursal->fecest     = null;
        $sucursal->save();
    }

    public function actualizaSucursal($sucursal, $params)
    {
        $attributes = array_flip($sucursal->getFillable());
        foreach ($params as $prop => $valor) {
            if (array_key_exists($prop, $attributes)) {
                $sucursal->$prop = "$valor";
            }
        }

        $this->codsuc = $sucursal->codsuc;
        $sucursal->traapo   = '0';
        $sucursal->valapo   = '0';
        $sucursal->tottra   = '0';
        $sucursal->totapo   = '0';
        $sucursal->totcon   = '0';
        $sucursal->tothij   = '0';
        $sucursal->tother   = '0';
        $sucursal->totpad   = '0';
        $sucursal->tietra   = '0';
        $sucursal->tratot   = '0';
        $sucursal->correo   = 'N';
        $sucursal->resest   = null;
        $sucursal->codest   = null;
        $sucursal->fecest   = null;
        $sucursal->save();
    }

    public function crearLista($params)
    {
        $_lista = Subsi73::where('nit', $params->nit)
            ->where('codlis', $this->codsuc)
            ->get()
            ->first();

        if (!$_lista) {

            $lista = new Subsi73();
            $lista->nit = $params->nit;
            $lista->codlis = $this->codsuc;
            $lista->detalle = substr($params->razsoc, 0, 140);
            $lista->direccion = substr($params->direccion, 0, 60);
            $lista->telefono = $params->telefono;
            $lista->fax = $params->fax;
            $lista->coddiv = $params->codciu;
            $lista->ofiafi = $params->ofiafi;
            $lista->nomcon = $params->repleg;
            $lista->email = $params->email;
            $lista->save();
        } else {

            $_lista->detalle = $params->razsoc;
            $_lista->direccion = $params->direccion;
            $_lista->telefono = $params->telefono;
            $_lista->fax = $params->fax;
            $_lista->coddiv = $params->codciu;
            $_lista->ofiafi = $params->ofiafi;
            $_lista->nomcon = $params->repleg;
            $_lista->email = $params->email;
            $_lista->save();
        }
    }

    public function guardarTrayectoria($params)
    {
        $trayecto = Subsi03::where('nit', $params->nit)
            ->where('fecafi', $params->fecafi)
            ->get()
            ->first();

        if (!$trayecto) {
            $trayectoria = new Subsi03;
            $trayectoria->nit = $params->nit;
            $trayectoria->fecafi = $params->fecafi;
            $trayectoria->calemp = $params->calemp;
            $trayectoria->save();
        }
    }

    public function getResultado()
    {
        return $this->resultado;
    }

    public function buscarEmpresa($nit)
    {
        return Subsi02::where("nit", $nit)->get()->first();
    }

    public function buscarSucursal($nit, $codzon, $codact)
    {
        return  Subsi48::where('nit', $nit)
            ->where('codzon', $codzon)
            ->where('codact', $codact)
            ->get()
            ->first();
    }

    public function detalleGestion()
    {
        return [
            $this->afiliacion_nueva
        ];
    }
}
