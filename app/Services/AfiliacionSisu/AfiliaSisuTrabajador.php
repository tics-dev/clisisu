<?php

namespace  App\Services\AfiliacionSisu;

use Illuminate\Database\QueryException;
use App\Exceptions\DebugException;
use Illuminate\Support\Facades\DB;
use App\Console\Servicios\ServicioSat\Tareas;
use App\Models\Subsi48;
use App\Models\Subsi12;
use App\Models\Subsi15;
use App\Models\Subsi16;
use App\Models\Subsi168;
use App\Models\Subsi17;
use App\Models\Subsi30;

class AfiliaSisuTrabajador
{

	protected $resultado;
	protected $codsuc;
	protected $hoy;

	private $afiliacion_principal;
	private $afiliacion_nueva;

	public function __construct()
	{
		$this->hoy = date('Y-m-d');
	}

	public function procesar($model_trabajador)
	{
		DB::beginTransaction();
		try {
			$trabajador = Subsi15::where("cedtra", $model_trabajador->cedtra)->get()->first();
			$sucursal = $this->validaSucursal($model_trabajador);

			list($carnet, $codcat) = $this->preparaCartentCategoria($model_trabajador);
			$model_trabajador->carnet = $carnet;
			$model_trabajador->codcat = $codcat;
			$actividad = $sucursal->getActividadSucursal();
			$model_trabajador->agro = ($actividad->esagro == 1) ? 'S' : 'N';

			//si ya existe trabajador activo
			if ($trabajador) {

				if ($trabajador->nit == $model_trabajador->nit && $trabajador->estado == 'A') {

					throw new DebugException("Trabajador activo con la misma empresa, no requiere de más acciones", 203);
				}

				//reintegro del trabajador a x empresa como principal
				if ($trabajador->estado == 'I') {

					$this->reintegroTrabajador($trabajador, $model_trabajador);
					//se registra nueva trayectoria	
					$this->guardarTrayectoria($model_trabajador);
				} elseif ($trabajador->nit != $model_trabajador->nit && $trabajador->estado == 'A') {

					//trabajador activo con empresa principal se hace multiafiliacion
					$this->multiafiliacion($model_trabajador);
				} else {

					throw new DebugException("Trabajador activo con la misma empresa, no requiere de más acciones", 203);
				}
			} else {
				// trabajador nuevo
				$this->crearTrabajador($model_trabajador);
				//se registra nueva trayectoria
				$this->guardarTrayectoria($model_trabajador);
			}


			//se registra el salario
			$this->guardarSalario($model_trabajador);
			//se registra lista

			DB::commit();

			$this->resultado = [
				"success" => true,
				"msj" => "El proceso de registro se completo con éxito"
			];
		} catch (QueryException $err) {

			DB::rollBack();
			throw new DebugException($err->getMessage() . ' ' . $err->getLine() . ' ' . basename($err->getFile()), 1);
		} catch (\Exception $ex) {
			DB::rollBack();
			throw new DebugException($ex->getMessage() . ' ' . $ex->getLine() . ' ' . basename($ex->getFile()), 1);
		}
	}

	public function crearTrabajador($params)
	{
		$trabajador = new Subsi15();
		$attributes = array_flip($trabajador->getFillable());
		foreach ($params as $prop => $valor) {
			if (array_key_exists($prop, $attributes)) {
				$trabajador->$prop = "$valor";
			}
		}
		$trabajador->coddoc = $params->tipdoc;
		$trabajador->horas = '240';
		$trabajador->tipcot = $params->tipafi;
		$trabajador->fecsal = $params->fecafi;
		$trabajador->fecpre = $params->fecsol;
		$trabajador->fecsis = $this->hoy;
		$trabajador->fecest = null;
		$trabajador->codest = null;
		$trabajador->benef = 'S';
		$trabajador->ruaf  = 'N';
		$trabajador->totcon = 0;
		$trabajador->tothij = 0;
		$trabajador->tother = 0;
		$trabajador->totpad = 0;
		$trabajador->fosfec = 'N';
		$trabajador->tottra = 0;
		$trabajador->ciulab = $params->codciu;
		$trabajador->pais = '170';
		$trabajador->estado = 'A';
		$trabajador->codgir = (isset($params->codgir)) ? $params->codgir : 'NU';

		$validator = $trabajador->isValid($trabajador->reglas_creacion());
		if ($validator) {
			if (!$trabajador->save()) {
				throw new DebugException("Error al guardar el modelo {$trabajador->getTable()}", 501);
			}
		} else {
			DebugException::add("valid", $validator);
			throw new DebugException("Error al guardar el modelo {$trabajador->getTable()}", 555);
		}
		$this->afiliacion_principal = true;
		$this->afiliacion_nueva = true;
	}

	public function multiafiliacion($params)
	{
		$multiafiliacion =  Subsi168::where('cedtra', $params->cedtra)
			->where('nit', "{$params->nit}")
			->where('codsuc', "{$params->codsuc}")
			->where('fecafi', "{$params->fecafi}")
			->get()
			->first();

		if ($multiafiliacion) {

			$this->reintegroMultiafiliacion($multiafiliacion, $params);
		} else {

			$this->crearMultiafiliacion($params);
		}
		$this->afiliacion_principal = false;
	}

	public function crearMultiafiliacion($params)
	{
		$trabajador = new Subsi168();
		$attributes = array_flip($trabajador->getFillable());
		foreach ($params as $prop => $valor) {
			if (array_key_exists($prop, $attributes)) {
				$trabajador->$prop = "$valor";
			}
		}

		$trabajador->fecsal = $params->fecafi;
		$trabajador->tipcot = $params->tipafi;
		$trabajador->fecpre = $params->fecsol;
		$trabajador->fecsis = $this->hoy;
		$trabajador->fecest = null;
		$trabajador->codest = null;
		$trabajador->estado = 'A';
		$trabajador->giro2 = 'N';
		$trabajador->codgir2 = (isset($params->codgir)) ? $params->codgir : 'NU';

		$validator = $trabajador->isValid($trabajador->reglas_creacion());
		if ($validator) {
			if (!$trabajador->save()) {
				throw new DebugException("Error al guardar el modelo {$trabajador->getTable()}", 501);
			}
		} else {
			DebugException::add("valid", $validator);
			throw new DebugException("Error al guardar el modelo {$trabajador->getTable()}", 555);
		}
		$this->afiliacion_principal = false;
		$this->afiliacion_nueva = true;
	}

	public function reintegroMultiafiliacion($trabajador, $params)
	{
		$attributes = array_flip($trabajador->getFillable());
		foreach ($params as $prop => $valor) {
			if (array_key_exists($prop, $attributes)) {
				$trabajador->$prop = "$valor";
			}
		}

		$trabajador->estado = 'A';
		$trabajador->fecsal = $params->fecafi;
		$trabajador->fecpre = $params->fecsol;
		$trabajador->fecest = null;
		$trabajador->codest = null;
		$trabajador->giro2 = 'N';
		$trabajador->codgir2 = (isset($params->codgir)) ? $params->codgir : 'NU';
		$trabajador->horas = $params->horas;

		$validator = $trabajador->isValid($trabajador->reglas_creacion());
		if ($validator) {
			if (!$trabajador->save()) {
				throw new DebugException("Error al guardar el modelo {$trabajador->getTable()}", 501);
			}
		} else {
			DebugException::add("valid", $validator);
			throw new DebugException("Error al guardar el modelo {$trabajador->getTable()}", 555);
		}

		$this->afiliacion_principal = false;
		$this->afiliacion_nueva = false;
	}

	public function reintegroTrabajador($trabajador, $params)
	{
		$attributes = array_flip($trabajador->getFillable());
		if ($trabajador->tippag == 'A' || $trabajador->tippag == 'D') {
			unset($params->numcue);
			unset($params->tippag);
			unset($params->codban);
			if (isset($params->tipcue)) unset($params->tipcue);
		}

		foreach ($params as $prop => $valor) {
			if (array_key_exists($prop, $attributes)) {
				$trabajador->$prop = "$valor";
			}
		}

		$trabajador->coddoc = $params->tipdoc;
		$trabajador->estado = 'A';
		$trabajador->horas = '240';
		$trabajador->fecsal = $params->fecafi;
		$trabajador->fecpre = $params->fecsol;
		$trabajador->fecest = null;
		$trabajador->codest = null;
		$trabajador->ciulab = $params->codciu;
		$trabajador->codgir = (isset($params->codgir)) ? $params->codgir : 'NU';

		$validator = $trabajador->isValid($trabajador->reglas_creacion());
		if ($validator) {
			if (!$trabajador->save()) {
				throw new DebugException("Error al guardar el modelo {$trabajador->getTable()}", 501);
			}
		} else {
			DebugException::add("valid", $validator);
			throw new DebugException("Error al guardar el modelo {$trabajador->getTable()}", 555);
		}

		$this->afiliacion_principal = true;
		$this->afiliacion_nueva = false;
	}

	public function guardarSalario($params)
	{
		$msalario = Subsi17::where('cedtra', $params->cedtra)
			->where('fecha', $params->fecafi)
			->get()->first();

		if (!$msalario) {
			$msalario = new Subsi17();
			$msalario->cedtra  = $params->cedtra;
			$msalario->fecha  = $params->fecafi;
			$msalario->salario = $params->salario;
			$msalario->save();
		}
	}

	public function guardarTrayectoria($params)
	{
		//si posee una afiliacion con la misma empresa y una fecha menor de afiliacion
		$trayecto = Subsi16::where('cedtra', $params->cedtra)
			->where('nit', $params->nit)
			->where('fecafi', '<', $params->fecafi)
			->whereNull('fecret')
			->get()
			->first();

		//actulizar el trayecto de esta empresa que no posee cierre de afiliación
		if ($trayecto) {
			Subsi16::where("cedtra", $params->cedtra)
				->where('nit', $params->nit)
				->update([
					'fecret' => date('Y-m-d')
				]);
			$trayecto = null;
		}

		//si ya esta afilaido no requiere de más acciones
		$trayecto = Subsi16::where('cedtra', $params->cedtra)
			->where('fecafi', $params->fecafi)
			->get()
			->first();

		if (!$trayecto) {
			$trayectoria = new Subsi16();
			$trayectoria->nit = "$params->nit";
			$trayectoria->fecafi = "$params->fecafi";
			$trayectoria->codsuc = "$params->codsuc";
			$trayectoria->cedtra = $params->cedtra;
			$trayectoria->codlis = "$params->codlis";
			$trayectoria->save();
		}
	}

	public function getResultado()
	{
		return $this->resultado;
	}

	public function preparaCartentCategoria($modelo)
	{
		$salario = $modelo->salario;
		$tipo_afiliado = $modelo->tipafi;

		$mperiodo = Subsi12::where('girado', 'N')->orderBy('periodo', 'asc')->get()->first();
		$periodo = $mperiodo->periodo;
		if (!$periodo) throw new DebugException("No hay periodo no girado vigente", 1);

		$cantidad_salarios = $salario / $mperiodo->salmin;

		//categorias
		$categorias = Subsi30::all()->toArray();
		$codigo_categoria = 'C';
		$carnetiza =  'N';
		foreach ($categorias as $ai => $row) {

			if ($cantidad_salarios <= $row['cansal']) {

				$codigo_categoria  = ($row['codcat']) ? $row['codcat'] : 'C';
				$carnetiza =  'S';
				if (($tipo_afiliado == '3' || $tipo_afiliado == '03') &&  $codigo_categoria == 'A') {
					$codigo_categoria = 'B';
				}
				break;
			}
		}
		return [$carnetiza, $codigo_categoria];
	}

	public function validaSucursal($params)
	{
		$sucursal = Subsi48::where('nit', $params->nit)
			->where('codsuc', $params->codsuc)
			->get()
			->first();

		if (!$sucursal) {
			throw new DebugException("Error el trabajador no le fue asignada la sucursal de la empresa.", 1);
		}

		if ($sucursal->estado != 'A') {
			throw new DebugException("Error la afiliación de la sucursal no está activa.", 1);
		}
		return $sucursal;
	}

	public function detalleGestion()
	{
		return [
			$this->afiliacion_principal,
			$this->afiliacion_nueva
		];
	}
}
