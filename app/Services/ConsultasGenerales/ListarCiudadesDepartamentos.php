<?php

namespace App\Services\ConsultasGenerales;

use App\Models\Gener07;
use App\Models\Gener08;
use App\Models\Gener09;

class ListarCiudadesDepartamentos
{
    protected $departamentos = array();
    protected $ciudades = array();
    protected $zonas = array();

    public function __construct()
    {
        $gener07 = Gener07::all();
        $this->departamentos = $gener07->makeVisible('attribute')->toArray();

        $gener09 = Gener09::all();
        $this->zonas = $gener09->makeVisible('attribute')->toArray();

        $gener08 = Gener08::all();
        $this->ciudades = $gener08->makeVisible('attribute')->toArray();
    }

    public function getCiudades()
    {
        return $this->ciudades;
    }

    public function getDepartamentos()
    {
        return $this->departamentos;
    }

    public function getZonas()
    {
        return $this->zonas;
    }

    public function getData()
    {
        return [
            'zonas' => $this->zonas,
            'departamentos' => $this->departamentos,
            'ciudades' => $this->ciudades
        ];
    }
}
