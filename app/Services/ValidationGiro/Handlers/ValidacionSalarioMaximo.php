<?php

namespace  App\Services\ValidationGiro\Handlers;

use App\Exceptions\DebugException;

class ValidacionSalarioMaximo extends ValidationHandler
{
    private $topeSalarial;

    public function setTope($tope)
    {
        $this->topeSalarial = $tope;
    }

    public function validate($objeto)
    {
        if ($objeto->validaSalarioMaximo() == False) {
            throw new DebugException("El salario del objeto supera el tope salarial");
        }
        return true;
    }
}
