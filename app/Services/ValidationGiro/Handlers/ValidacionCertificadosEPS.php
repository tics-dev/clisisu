<?php

namespace  App\Services\ValidationGiro\Handlers;

use App\Exceptions\DebugException;

class ValidacionCertificadosEPS extends ValidationHandler
{

    public function validate($objeto)
    {
        // Si la validación es exitosa, pasamos al siguiente objeto de validación en la cadena:
        if ($objeto->validaCertificadosEps() == False) {
            throw new DebugException("El objeto no tiene certificado de EPS");
        }
        return true;
    }
}
