<?php

namespace  App\Services\ValidationGiro\Handlers;

use App\Exceptions\DebugException;

class ValidacionEmpresaPrincipal extends ValidationHandler
{

    public function validate($objeto)
    {
        if ($objeto->validaEmpresaPrincipal() == False) {
            throw new DebugException("La empresa principal no es valida");
        }
        return true;
    }
}
