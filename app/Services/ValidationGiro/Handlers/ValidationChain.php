<?php

namespace  App\Services\ValidationGiro\Handlers;

use App\Console\Servicios\GiroSubsidio\GiroActual;
use App\Exceptions\DebugException;

class ValidationChain
{

    protected $validators = [];

    protected ValidationHandler $actualValidador;

    public function addValidator(ValidationHandler $validator)
    {
        $this->validators[] = $validator;
    }

    public function handleValidation($serviceGiro)
    {
        foreach ($this->validators as $this->actualValidador) {

            if ($this->actualValidador->validate($serviceGiro) == False) {
                throw new DebugException('No valido', 501);
            }
        }
        return true;
    }
}
