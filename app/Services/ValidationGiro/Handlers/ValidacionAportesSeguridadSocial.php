<?php

namespace  App\Services\ValidationGiro\Handlers;

use App\Exceptions\DebugException;

class ValidacionAportesSeguridadSocial extends ValidationHandler
{

    public function validate($objeto)
    {
        if ($objeto->validaAportesSeguridadSocial() == False) {
            throw new DebugException("Los aportes no son validos para la empresa");
        }
        return true;
    }
}
