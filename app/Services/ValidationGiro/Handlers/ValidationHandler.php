<?php

namespace  App\Services\ValidationGiro\Handlers;

interface ValidationHandler
{
    public function validate(Object $objecto);
}
