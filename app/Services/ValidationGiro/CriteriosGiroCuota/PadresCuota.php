<?php

declare(strict_types=1);
class PadresCuota implements IValidator
{

    public function validaAportesSeguridadSocial($objecto): bool
    {
        return false;
    }

    public function validaCertificadosEps($objeto): bool
    {
        return false;
    }

    public function validaEmpresaPrincipal($objeto): bool
    {
        return false;
    }

    public function validaSalarioMaximo($objeto): bool
    {
        return false;
    }

    public function validaDerechoEmbargo($objeto): bool
    {
        return false;
    }
}
