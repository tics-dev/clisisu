<?php

declare(strict_types=1);
interface IValidator
{
    public function validaAportesSeguridadSocial($objecto);
    public function validaCertificadosEps($objeto);
    public function validaEmpresaPrincipal($objeto);
    public function validaSalarioMaximo($objeto);
    public function validaDerechoEmbargo($objeto);
}
