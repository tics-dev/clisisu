<?php

declare(strict_types=1);
class EmbargoCuota implements IValidator
{

    public function validaAportesSeguridadSocial($objecto): bool
    {
        return false;
    }

    public function validaCertificadosEps($objeto): bool
    {
        return false;
    }

    public function validaEmpresaPrincipal($objeto): bool
    {
        return false;
    }

    public function validaSalarioMaximo($objeto): bool
    {
        return false;
    }

    public function validaDerechoEmbargo($objeto): bool
    {
        return false;
    }
}
