<?php

declare(strict_types=1);

namespace  App\Services\ValidationGiro;

use App\Exceptions\DebugException;
use App\Models\Subsi02;
use App\Models\Subsi15;
use App\Models\Subsi20;
use App\Models\Subsi21;
use App\Models\Subsi22;
use App\Models\Subsi23;

class ServiceGiro
{
    protected $trabajador;
    protected $conyuges = array();
    protected $beneficiarios = array(); //matriz agrupada por parentescos de beneficiarios
    protected $empresa;
    protected $responsables = array();

    public function __construct(Subsi02 $empresa, Subsi15 $trabajador)
    {
        $this->trabajador = $trabajador;
        $this->empresa = $empresa;
        $this->buscarConyuges();
        $this->buscarBeneficiarios();
        $this->filtrarTipoCuota();
        $this->filtrarCuotaPadres();
        $this->filtrarCuotaHijos();
        $this->filtrarCuotaHermanos();
        $this->filtrarCuotaEmbargos();
        $this->filtrarCuotaMuertos();
    }

    function buscarConyuges()
    {
        $conyuges = Subsi21::where('cedtra', $this->trabajador->cedtra)->get();
        foreach ($conyuges as $conyuge) {
            $this->conyuges[] = Subsi20::where('cedcon', $conyuge->cedcon)->get()->first();
        }
    }

    function buscarBeneficiarios()
    {
        $beneficiariosTra = Subsi23::where('cedtra', $this->trabajador->cedtra)->get();
        if ($beneficiariosTra == false) {
            throw new DebugException("No dispone de beneficiarios", 501);
        }

        foreach ($beneficiariosTra as $beneficiarioTra) {
            $beneficiario = Subsi22::where('codben', $beneficiarioTra->codben)->get()->first();
            if ($beneficiario == false) {
                throw new DebugException("Error el beneficiario no es valido {$beneficiarioTra->codben}", 1);
            }
            if (isset($this->beneficiarios[$beneficiario->parent])) {
                $this->beneficiarios[$beneficiario->parent][] = $beneficiario;
            } else {
                $this->beneficiarios[$beneficiario->parent] = [$beneficiario];
            }
        }
    }

    function addResponsables($responsable)
    {
        $this->responsables[] = $responsable;
    }

    public function crearGiro()
    {
        print_r(__LINE__ . ' creo giro');
    }

    public function filtrarTipoCuota()
    {
        if ($this->trabajador->estado == 'M') {
            //muerte de trabajador
        }
        //custodia de los hijos, embargo por tercero, abuela, tios
    }

    function filtrarCuotaPadres()
    {
        if (isset($this->beneficiarios[3]) == False) return false;
    }

    function filtrarCuotaHijos()
    {
        if (isset($this->beneficiarios[1]) == False) return false;
    }

    function filtrarCuotaHermanos()
    {
        if (isset($this->beneficiarios[2]) == False) return false;
    }

    function filtrarCuotaEmbargos()
    {
    }

    function filtrarCuotaMuertos()
    {
    }

    public function getInstancia()
    {
        $conyuges = [];
        foreach ($this->conyuges as $_conyuge) $conyuges[] = $_conyuge->toArray();
        $beneficiarios = [];
        if (isset($this->beneficiarios[1])) {
            foreach ($this->beneficiarios[1] as $_beneficiario) $beneficiarios[] = $_beneficiario->toArray();
        }
        if (isset($this->beneficiarios[2])) {
            foreach ($this->beneficiarios[2] as $_beneficiario) $beneficiarios[] = $_beneficiario->toArray();
        }
        if (isset($this->beneficiarios[3])) {
            foreach ($this->beneficiarios[3] as $_beneficiario) $beneficiarios[] = $_beneficiario->toArray();
        }
        return [
            'trabajador' => $this->trabajador->toArray(),
            'conyuges' => $conyuges,
            'beneficiarios' => $beneficiarios,
            'empresa' => $this->empresa->toArray(),
            'responsables' => $this->responsables
        ];
    }

    public function getResponsables()
    {
        return $this->responsables;
    }
}
