<?php

namespace App\Services;

use App\Models\JwtRecurso;

class JwtRecursoService
{
    private $jwtRecurso;

    public function __construct()
    {
        $this->jwtRecurso = new JwtRecurso();
    }

    public function getJwtRecurso()
    {
        return $this->jwtRecurso->findAll();
    }

    public function getJwtRecursoById($id)
    {
        return $this->jwtRecurso->find($id);
    }

    public function getJwtRecursoClientId($cliente_id)
    {
        return JwtRecurso::where('cliente_id', $cliente_id)->get()->first();
    }

    public function createJwtRecurso($data)
    {
        return $this->jwtRecurso->insert($data);
    }

    public function updateJwtRecurso($id, $data)
    {
        if (!is_array($data)) return false;

        $recurso = JwtRecurso::where('cliente_id', $id)->get()->first();
        if ($recurso) {
            return JwtRecurso::where('id', $recurso->id)->update($data);
        }
        return false;
    }

    public function deleteJwtRecurso($id)
    {
        return $this->jwtRecurso->delete($id);
    }
}
