<?php

namespace  App\Services\AfiliacionMasivos;

ini_set('memory_limit', '1300M');

use App\Exceptions\DebugException;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Shared_Date;
use App\Console\Servicios\General\General;
use App\Libraries\MyParams;
use App\Models\Mercurio30;
use App\Models\Mercurio31;
use App\Models\Mercurio32;
use App\Models\Mercurio07;
use App\Models\Mercurio34;


class CargarBeneficiarios
{

    protected $resultado;
    protected $console;
    protected $enviroment;
    protected $user_empleador;
    private $filepath;
    private $documento;
    private $empresa;
    private $nit;
    public static $tipopc = 4;
    public static $errors = array();
    public static $success = array();
    public static $registros = 0;


    public function __construct(MyParams $argv)
    {
        $this->documento = $argv->getParam('documento');
        $this->nit = $argv->getParam('nit');
        $this->buscarEmpresa();
        if (!$this->empresa) {
            throw new DebugException("Error la empresa no está disponible.", 500);
        }
    }

    public function procesarDocumento($filepath)
    {
        $this->filepath = $filepath;
        $inputFileType = PHPExcel_IOFactory::identify($this->filepath);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($this->filepath);
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $sheet->getHighestColumn();

        $usuario = General::asignarUsuario(self::$tipopc, $this->user_empleador->codciu);

        General::createLog(
            $this->documento,
            $this->user_empleador->coddoc,
            'CargarBeneficiarios',
            'Se realiza el cargue de beneficiarios de forma masivo usando archivo excel.',
            $this->user_empleador->tipo
        );
        $logger = General::$logger;
        for ($row = 2; $row <= $highestRow; $row++) {
            if (intval($sheet->getCell("A{$row}")->getValue()) == '') continue;
            self::$registros++;
            $cedtra = intval($sheet->getCell("A{$row}")->getValue());
            $cedcon = intval($sheet->getCell("B{$row}")->getValue());
            $numdoc = intval($sheet->getCell("C{$row}")->getValue());

            if (!$this->validaTrabajadorRegistrado($cedtra)) {
                self::$errors[] = [
                    "trama"  => $numdoc,
                    "errors" => "El trabajador con documento {$cedtra} no está disponible para realizar el registro.",
                    "status" => 203
                ];
                continue;
            }

            if ($cedcon) {
                if (!$this->validaConyugeRegistrado($cedcon, $cedtra)) {
                    self::$errors[] = [
                        "trama"  => $numdoc,
                        "errors" => "La conyuge con documento {$cedcon} no está disponible para realizar el registro.",
                        "status" => 203
                    ];
                    continue;
                }
            }

            if ($this->validaBeneficiarioRegistrado($cedtra, $numdoc)) {
                self::$errors[] = [
                    "trama"  => $numdoc,
                    "errors" => "El afiliado con documento {$numdoc} ya se encuentra registrado con éxito, no requiere de más acciones.",
                    "status" => 203
                ];
                continue;
            }

            $data = array(
                "cedula_trabajador" => $cedtra,
                "cedula_conyuge"    => $cedcon,
                "numero_documento"  => $numdoc,
                "tipo_documento" => intval($sheet->getCell("D{$row}")->getValue()),
                "parentesco" => intval($sheet->getCell("E{$row}")->getValue()),
                "primer_apellido" => substr(strtoupper($sheet->getCell("F{$row}")->getValue()), 0, 20),
                "segundo_apellido" => substr(strtoupper($sheet->getCell("G{$row}")->getValue()), 0, 20),
                "primer_nombre" => substr(strtoupper($sheet->getCell("H{$row}")->getValue()), 0, 20),
                "segundo_nombre" => substr(strtoupper($sheet->getCell("I{$row}")->getValue()), 0, 20),
                "fecha_nacimiento" => date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($sheet->getCell("J{$row}")->getValue() + 1)),
                "codigo_ciudad_nacimiento" => intval($sheet->getCell("K{$row}")->getValue()),
                "sexo" => strtoupper($sheet->getCell("L{$row}")->getValue()),
                "cedula_acudiente" => strtoupper($sheet->getCell("M{$row}")->getValue()),
                "huerfano" => strtoupper($sheet->getCell("N{$row}")->getValue()),
                "tipo_hijo" => strtoupper($sheet->getCell("O{$row}")->getValue()),
                "nivel_educativo" => ($sheet->getCell("P{$row}")->getValue()),
                "capacidad_trabajar" => ($sheet->getCell("Q{$row}")->getValue()),
                "tipo_discapacidad" => ($sheet->getCell("R{$row}")->getValue()),
                "tipo" => $this->user_empleador->tipo,
                "fecha_solicitud" => date('Y-m-d'),
                "log" => $logger->log,
                "estado" => 'T',
                "usuario" => $usuario,
                "documento" => intval($this->documento),
                "codigo_documento_empleador" => $this->user_empleador->coddoc
            );

            $beneficiario = new Mercurio34();
            $attrs = $beneficiario->mapperAttribute();
            foreach ($data as $ai => $value) {
                $clave = (isset($attrs[$ai])) ? $attrs[$ai] : trim($ai);
                if (!is_numeric($value)) {
                    $beneficiario->$clave = htmlentities($value);
                } else {
                    $beneficiario->$clave = $value;
                }
            }

            $validator = $beneficiario->isValid();
            if ($validator === true) {
                $beneficiario->save();
                self::$success[] =  [
                    "trama" => $numdoc,
                    "status" => 201
                ];
            } else {
                self::$errors[] =  [
                    "trama"  => $numdoc,
                    "errors"   => $validator,
                    "status" => 501
                ];
            }
        }
    }

    public function buscarEmpresa()
    {
        $this->empresa = Mercurio30::where('nit', $this->nit)
            ->where("estado", 'A')
            ->get()
            ->first();

        $this->user_empleador = Mercurio07::where('documento', $this->documento)
            ->where("estado", 'A')
            ->get()
            ->first();
    }

    public function validaBeneficiarioRegistrado($cedtra, $numdoc): bool
    {
        $beneficiario = Mercurio34::where('numdoc', $numdoc)
            ->where('cedtra', $cedtra)
            ->where('documento', $this->user_empleador->documento)
            ->where('tipo', $this->user_empleador->tipo)
            ->get()
            ->first();
        return ($beneficiario) ? true : false;
    }

    public function validaTrabajadorRegistrado($cedtra): bool
    {
        $trabajador = Mercurio31::where('cedtra', $cedtra)
            ->where('documento', $this->user_empleador->documento)
            ->where('tipo', $this->user_empleador->tipo)
            ->where('estado', 'A')
            ->get()
            ->first();
        return ($trabajador) ? true : false;
    }

    public function validaConyugeRegistrado($cedcon, $cedtra): bool
    {
        $conyuge = Mercurio32::where('cedcon', $cedcon)
            ->where('cedtra', $cedtra)
            ->where('documento', $this->user_empleador->documento)
            ->where('tipo', $this->user_empleador->tipo)
            ->where('estado', 'A')
            ->get()
            ->first();
        return ($conyuge) ? true : false;
    }

    public function getResultado()
    {
        return [
            "success" => true,
            "cantidad" => CargarBeneficiarios::$registros,
            "registros_success" => CargarBeneficiarios::$success,
            "registros_errors" => CargarBeneficiarios::$errors
        ];
    }
}
