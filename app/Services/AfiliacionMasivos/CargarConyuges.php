<?php

namespace  App\Services\AfiliacionMasivos;

ini_set('memory_limit', '1300M');

use Illuminate\Database\QueryException;
use App\Exceptions\DebugException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Shared_Date;
use App\Console\Servicios\General\General;
use App\Libraries\MyParams;
use App\Models\Mercurio30;
use App\Models\Mercurio31;
use App\Models\Mercurio32;
use App\Models\Mercurio07;


class CargarConyuges
{

    protected $resultado;
    protected $console;
    protected $enviroment;
    protected $user_empleador;
    private $filepath;
    private $documento;
    private $empresa;
    private $nit;
    public static $tipopc = 3;
    public static $errors = array();
    public static $success = array();
    public static $registros = 0;


    public function __construct(MyParams $argv)
    {
        $this->documento = $argv->getParam('documento');
        $this->nit = $argv->getParam('nit');
        $this->buscarEmpresa();
        if (!$this->empresa) {
            throw new DebugException("Error la empresa no está disponible.", 500);
        }
    }

    public function procesarDocumento($filepath)
    {
        $this->filepath = $filepath;
        $inputFileType = PHPExcel_IOFactory::identify($this->filepath);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($this->filepath);
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $sheet->getHighestColumn();

        $usuario = General::asignarUsuario(self::$tipopc, $this->user_empleador->codciu);

        General::createLog(
            $this->documento,
            $this->user_empleador->coddoc,
            'CargarConyuges',
            'Se realiza el cargue de conyuges de forma masivo usando archivo excel.',
            $this->user_empleador->tipo
        );
        $logger = General::$logger;
        for ($row = 2; $row <= $highestRow; $row++) {
            if (intval($sheet->getCell("A{$row}")->getValue()) == '') continue;
            self::$registros++;
            $cedtra = intval($sheet->getCell("A{$row}")->getValue());
            $cedcon = intval($sheet->getCell("B{$row}")->getValue());

            if ($this->validaConyugeRegistrado($cedtra, $cedcon) === true) {
                self::$errors[] = [
                    "trama"  => $cedcon,
                    "errors" => "El afiliado ya se encuentra registrado con éxito, no requiere de más acciones.",
                    "status" => 203
                ];
                continue;
            }
            $data = array(
                "cedula_trabajador" => $cedtra,
                "cedula_conyuge" => $cedcon,
                "tipo_documento" => intval($sheet->getCell("C{$row}")->getValue()),
                "primer_apellido" => substr(strtoupper($sheet->getCell("D{$row}")->getValue()), 0, 20),
                "segundo_apellido" => substr(strtoupper($sheet->getCell("E{$row}")->getValue()), 0, 20),
                "primer_nombre" => substr(strtoupper($sheet->getCell("F{$row}")->getValue()), 0, 20),
                "segundo_nombre" => substr(strtoupper($sheet->getCell("G{$row}")->getValue()), 0, 20),
                "fecha_ingreso" => date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($sheet->getCell("H{$row}")->getValue() + 1)),
                "fecha_nacimiento" => date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($sheet->getCell("I{$row}")->getValue() + 1)),
                "codigo_ciudad_nacimiento" => intval($sheet->getCell("J{$row}")->getValue()),
                "sexo" => strtoupper($sheet->getCell("K{$row}")->getValue()),
                "estado_civil" => strtoupper($sheet->getCell("L{$row}")->getValue()),
                "companero_permanente" => strtoupper($sheet->getCell("M{$row}")->getValue()),
                "tiempo_convivencia" =>  strtoupper($sheet->getCell("N{$row}")->getValue()),
                "codigo_ciudad_residencia" => ($sheet->getCell("O{$row}")->getValue()),
                "codigo_ciudad_laboral" => ($sheet->getCell("P{$row}")->getValue()),
                "tipo_vivienda" => ($sheet->getCell("Q{$row}")->getValue()),
                "direccion" => ($sheet->getCell("R{$row}")->getValue()),
                "barrio" => ($sheet->getCell("S{$row}")->getValue()),
                "telefono" => ($sheet->getCell("T{$row}")->getValue()),
                "celular" => ($sheet->getCell("U{$row}")->getValue()),
                "email" => ($sheet->getCell("V{$row}")->getValue()),
                "nivel_educativo" => ($sheet->getCell("W{$row}")->getValue()),
                "ocupacion" => ($sheet->getCell("X{$row}")->getValue()),
                "salario" => ($sheet->getCell("Y{$row}")->getValue()),
                "tipo_salario" => ($sheet->getCell("Z{$row}")->getValue()),
                "capacidad_trabajar" => ($sheet->getCell("AA{$row}")->getValue()),
                "tipo" => $this->user_empleador->tipo,
                "fecha_solicitud" => date('Y-m-d'),
                "log" => $logger->log,
                "estado" => 'T',
                "usuario" => $usuario,
                "documento" => intval($this->documento),
                "codigo_documento_empleador" => $this->user_empleador->coddoc
            );
            $conyuge = new Mercurio32();
            $attrs = $conyuge->mapperAttribute();
            foreach ($data as $ai => $value) {
                $clave = (isset($attrs[$ai])) ? $attrs[$ai] : trim($ai);
                if (!is_numeric($value)) {
                    $conyuge->$clave = htmlentities($value);
                } else {
                    $conyuge->$clave = $value;
                }
            }

            $validator = $conyuge->isValid();
            if ($validator === true) {
                $conyuge->save();
                self::$success[] =  [
                    "trama" => $cedcon,
                    "status" => 201
                ];
            } else {
                self::$errors[] =  [
                    "trama"  => $cedcon,
                    "errors"   => $validator,
                    "status" => 501
                ];
            }
        }
    }

    public function buscarEmpresa()
    {
        $this->empresa = Mercurio30::where('nit', $this->nit)
            ->where("estado", 'A')
            ->get()
            ->first();

        $this->user_empleador = Mercurio07::where('documento', $this->documento)
            ->where("estado", 'A')
            ->get()
            ->first();
    }

    public function validaConyugeRegistrado($cedtra, $cedcon): bool
    {
        $trabajador = Mercurio32::where('cedcon', $cedcon)
            ->where('cedtra', $cedtra)
            ->where('documento', $this->user_empleador->documento)
            ->where('tipo', $this->user_empleador->tipo)
            ->get()
            ->first();
        return ($trabajador) ? true : false;
    }

    public function getResultado()
    {
        return [
            "success" => true,
            "cantidad" => CargarConyuges::$registros,
            "registros_success" => CargarConyuges::$success,
            "registros_errors" => CargarConyuges::$errors
        ];
    }
}
