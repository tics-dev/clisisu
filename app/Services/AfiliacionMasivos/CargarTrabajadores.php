<?php

namespace  App\Services\AfiliacionMasivos;

ini_set('memory_limit', '1300M');

use Illuminate\Database\QueryException;
use App\Exceptions\DebugException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Shared_Date;
use App\Console\Servicios\General\General;
use App\Libraries\MyParams;
use App\Models\Mercurio30;
use App\Models\Mercurio31;
use App\Models\Mercurio07;
use App\Models\Subsi02;

class CargarTrabajadores
{

    protected $resultado;
    protected $console;
    protected $enviroment;
    private $filepath;
    private $documento;
    private $empresa;
    private $user_empleador;
    private $nit;
    private $codsuc;
    public static $tipopc = 1;
    public static $errors = array();
    public static $success = array();
    public static $registros = 0;


    public function __construct(MyParams $argv)
    {
        $this->documento = $argv->getParam('documento');
        $this->nit = $argv->getParam('nit');
        $this->codsuc = $argv->getParam('codsuc');

        $this->buscarEmpresa();
        if (!$this->empresa) {
            throw new DebugException("Error la empresa no está disponible.", 500);
        }
    }

    public function procesarDocumento($filepath)
    {
        $this->filepath = $filepath;
        $inputFileType = PHPExcel_IOFactory::identify($this->filepath);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($this->filepath);
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $sheet->getHighestColumn();

        $usuario = General::asignarUsuario(self::$tipopc, $this->user_empleador->codciu);

        General::createLog(
            $this->documento,
            $this->user_empleador->coddoc,
            'CargarTrabajadores',
            'Se realiza el cargue de trabajadores de forma masivo usando archivo excel.',
            $this->user_empleador->tipo
        );
        $logger = General::$logger;
        for ($row = 2; $row <= $highestRow; $row++) {
            if (intval($sheet->getCell("A{$row}")->getValue()) == '') continue;
            self::$registros++;
            $cedtra = intval($sheet->getCell("A{$row}")->getValue());

            if ($this->validaTrabajadorRegistrado($cedtra) === true) {
                self::$errors[] = [
                    "trama"  => $cedtra,
                    "errors" => "El afiliado ya se encuentra registrado con éxito, no requiere de más acciones.",
                    "status" => 203
                ];
                continue;
            }

            $fecha_nacimiento = $sheet->getCell("G{$row}")->getValue();
            if (strrpos($fecha_nacimiento, '-') !== false || strrpos($fecha_nacimiento, '/') !== false) {
                $fecha_nacimiento = str_replace(["'", "`", "´", "\""], '', $fecha_nacimiento);
            } else {
                $fecha_nacimiento = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($fecha_nacimiento + 1));
            }

            $fecha_ingreso = $sheet->getCell("T{$row}")->getValue();
            if (strrpos($fecha_ingreso, '-') !== false || strrpos($fecha_ingreso, '/') !== false) {
                $fecha_ingreso = str_replace(["'", "`", "´", "\""], '', $sheet->getCell("G{$row}")->getValue());
            } else {
                $fecha_ingreso = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($fecha_ingreso + 1));
            }

            $data = array(
                "cedula" => $cedtra,
                "tipo_documento" => intval($sheet->getCell("B{$row}")->getValue()),
                "primer_apellido" => substr(strtoupper($sheet->getCell("C{$row}")->getValue()), 0, 20),
                "segundo_apellido" => substr(strtoupper($sheet->getCell("D{$row}")->getValue()), 0, 20),
                "primer_nombre" => substr(strtoupper($sheet->getCell("E{$row}")->getValue()), 0, 20),
                "segundo_nombre" => substr(strtoupper($sheet->getCell("F{$row}")->getValue()), 0, 20),
                "fecha_nacimiento" => $fecha_nacimiento,
                "codigo_ciudad_nacimiento" => intval($sheet->getCell("H{$row}")->getValue()),
                "sexo" => strtoupper($sheet->getCell("I{$row}")->getValue()),
                "estado_civil" => strtoupper($sheet->getCell("J{$row}")->getValue()),
                "cabeza_hogar" => strtoupper($sheet->getCell("K{$row}")->getValue()),
                "codigo_ciudad_residencia" => ($sheet->getCell("L{$row}")->getValue()),
                "codigo_ciudad_laboral" => ($sheet->getCell("M{$row}")->getValue()),
                "direccion" => strtoupper($sheet->getCell("N{$row}")->getValue()),
                "barrio" => strtoupper($sheet->getCell("O{$row}")->getValue()),
                "telefono" => intval($sheet->getCell("P{$row}")->getValue()),
                "celular" => intval($sheet->getCell("Q{$row}")->getValue()),
                "fax" => intval($sheet->getCell("R{$row}")->getValue()),
                "email" => substr(strtoupper($sheet->getCell("S{$row}")->getValue()), 0, 100),
                "fecha_ingreso" => $fecha_ingreso,
                "salario" => strtoupper($sheet->getCell("U{$row}")->getValue()),
                "tipo_salario" => strtoupper($sheet->getCell("V{$row}")->getValue()),
                "capacidad_trabajar" => strtoupper($sheet->getCell("W{$row}")->getValue()),
                "tipo_discapacidad" => strtoupper($sheet->getCell("X{$row}")->getValue()),
                "nivel_educativo" => strtoupper($sheet->getCell("Y{$row}")->getValue()),
                "residencia_rural" => strtoupper($sheet->getCell("Z{$row}")->getValue()),
                "horas_trabajar" => strtoupper($sheet->getCell("AA{$row}")->getValue()),
                "tipo_contrato" => strtoupper($sheet->getCell("AB{$row}")->getValue()),
                "sindicalizado" => strtoupper($sheet->getCell("AC{$row}")->getValue()),
                "vivienda" => strtoupper($sheet->getCell("AD{$row}")->getValue()),
                "tipo_afiliado" => strtoupper($sheet->getCell("AE{$row}")->getValue()),
                "profesion" => substr(strtoupper($sheet->getCell("AF{$row}")->getValue()), 0, 45),
                "cargo" => strtoupper($sheet->getCell("AG{$row}")->getValue()),
                "orientacion_sexual" => strtoupper($sheet->getCell("AH{$row}")->getValue()),
                "factor_vulnerabilidad" => strtoupper($sheet->getCell("AI{$row}")->getValue()),
                "etnica" => strtoupper($sheet->getCell("AJ{$row}")->getValue()),
                "direccion_laboral" => strtoupper($sheet->getCell("AK{$row}")->getValue()),
                "tratamiento_datos" => strtoupper($sheet->getCell("AL{$row}")->getValue()),
                "tipo_jornada" => strtoupper($sheet->getCell("AM{$row}")->getValue()),
                "labor_rural" => strtoupper($sheet->getCell("AN{$row}")->getValue()),
                "recibe_comision" => strtoupper($sheet->getCell("AO{$row}")->getValue()),
                "estado" => 'T',
                "fecha_solicitud" => date('Y-m-d'),
                "usuario" => $usuario,
                "documento" => intval($this->documento),
                "razsoc" => $this->empresa->razsoc,
                "coddoc" => $this->user_empleador->coddoc,
                "nit" => intval($this->empresa->nit),
                "tipo" => $this->user_empleador->tipo,
                "log" => $logger->log,
                "codsuc" => $this->codsuc
            );
            $trabajador = new Mercurio31();
            $attrs = $trabajador->mapperAttribute();
            foreach ($data as $ai => $value) {
                $clave = (isset($attrs[$ai])) ? $attrs[$ai] : trim($ai);
                if (!is_numeric($value)) {
                    $trabajador->$clave = htmlentities($value);
                } else {
                    $trabajador->$clave = $value;
                }
            }

            $validator = $trabajador->isValid();
            if ($validator === true) {
                $trabajador->save();
                self::$success[] =  [
                    "trama" => $cedtra,
                    "status" => 201
                ];
            } else {
                self::$errors[] =  [
                    "trama"  => $cedtra,
                    "errors"   => $validator,
                    "status" => 501
                ];
            }
        }
    }

    public function buscarEmpresa()
    {
        $this->empresa = Mercurio30::where('nit', $this->nit)
            ->where("estado", 'A')
            ->get()
            ->first();

        if (!$this->empresa) {
            $this->empresa = Subsi02::where('nit', $this->nit)
                ->whereIn("estado", ['A', 'S', 'D'])
                ->get()
                ->first();
        }

        $this->user_empleador = Mercurio07::where('documento', $this->documento)
            ->where("estado", 'A')
            ->get()
            ->first();
    }

    public function validaTrabajadorRegistrado($cedtra): bool
    {
        $trabajador = Mercurio31::where('cedtra', $cedtra)
            ->where('nit', $this->empresa->nit)
            ->where('documento', $this->user_empleador->documento)
            ->where('tipo', $this->user_empleador->tipo)
            ->get()
            ->first();
        return ($trabajador) ? true : false;
    }

    public function getResultado()
    {
        return [
            "success" => true,
            "cantidad" => CargarTrabajadores::$registros,
            "registros_success" => CargarTrabajadores::$success,
            "registros_errors" => CargarTrabajadores::$errors
        ];
    }
}
