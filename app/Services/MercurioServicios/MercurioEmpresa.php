<?php

namespace  App\Services\MercurioServicios;

use App\Exceptions\DebugException;
use Illuminate\Support\Facades\DB;
use App\Libraries\MyParams;
use App\Models\Mercurio30;
use App\Models\Subsi48;
use Illuminate\Database\QueryException;

class MercurioEmpresa
{
    private $resultado;
    private $hoy;

    public function __construct()
    {
        $this->hoy = date('Y-m-d');
    }

    public function validarEstado($documento)
    {
        DB::beginTransaction();
        try {
            $cantidad = 0;
            $empresas = $this->buscarSolicitante($documento);

            if ($empresas) {
                foreach ($empresas as $empresa) {

                    $cantidad_sucursales = Subsi48::where('nit', trim($empresa->nit))->whereIn('estado', ['A', 'D', 'S'])->count();
                    if ($cantidad_sucursales == 0) {
                        $this->actualizaMercurio($empresa);
                        $cantidad++;
                    }
                }
            }
            $this->resultado = array(
                "success" => true,
                "msj" => "El proceso se completo con éxito",
                "cantidad" => $cantidad
            );
            DB::commit();
        } catch (QueryException $err) {
            DB::rollBack();
            throw new DebugException($err->getMessage() . ' ' . $err->getLine() . ' ' . basename($err->getFile()), 1);
        } catch (\Exception $ex) {
            DB::rollBack();
            throw new DebugException($ex->getMessage() . ' ' . $ex->getLine() . ' ' . basename($ex->getFile()), 1);
        }
    }

    public function actualizaMercurio($empresa)
    {
        Mercurio30::where('documento', $empresa->documento)
            ->where('nit', $empresa->nit)
            ->where('estado', 'A')
            ->update(["estado" => 'I']);
    }

    public function buscarSolicitante($documento)
    {
        return Mercurio30::where('documento', $documento)
            ->where('estado', 'A')
            ->get();
    }

    public function getResultado()
    {
        return $this->resultado;
    }
}
