<?php

namespace  App\Services\MercurioServicios;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Exceptions\DebugException;
use App\Models\Subsi15;
use App\Models\Subsi168;
use App\Models\Mercurio31;

class MercurioTrabajador
{

    protected $resultado;
    protected $hoy;

    public function __construct()
    {
        $this->hoy = date('Y-m-d');
    }

    public function validarEstado($documento)
    {
        DB::beginTransaction();
        try {
            $cantidad = 0;
            $trabajadores = $this->buscarSolicitudes($documento);

            if ($trabajadores) {
                foreach ($trabajadores as $trabajador) {

                    $cantidad_trabajadores = Subsi15::where('cedtra', trim($trabajador->cedtra))
                        ->where('nit', $trabajador->nit)
                        ->where('estado', 'A')
                        ->count();

                    $cantidad_trabajadores += Subsi168::where('cedtra', trim($trabajador->cedtra))
                        ->where('nit', $trabajador->nit)
                        ->where('estado', 'A')
                        ->count();

                    if ($cantidad_trabajadores == 0) {
                        $this->actualizaMercurio($trabajador);
                        $cantidad++;
                    }
                }
            }

            $this->resultado = array(
                "success" => true,
                "msj" => "El proceso se completo con éxito",
                "cantidad" => $cantidad
            );
            DB::commit();
        } catch (QueryException $err) {
            DB::rollBack();
            throw new DebugException($err->getMessage() . ' ' . $err->getLine() . ' ' . basename($err->getFile()), 1);
        } catch (\Exception $ex) {
            DB::rollBack();
            throw new DebugException($ex->getMessage() . ' ' . $ex->getLine() . ' ' . basename($ex->getFile()), 1);
        }
    }

    public function actualizaMercurio($trabajador)
    {
        Mercurio31::where('documento', $trabajador->documento)
            ->where('nit', $trabajador->nit)
            ->where('cedtra', $trabajador->cedtra)
            ->where('estado', 'A')
            ->update(["estado" => 'I']);
    }

    public function buscarSolicitudes($documento)
    {
        return Mercurio31::where('documento', $documento)
            ->where('estado', 'A')
            ->get();
    }

    public function getResultado()
    {
        return $this->resultado;
    }
}
