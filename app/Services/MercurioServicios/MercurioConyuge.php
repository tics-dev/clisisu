<?php

namespace  App\Services\MercurioServicios;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Exceptions\DebugException;
use App\Models\Subsi20;
use App\Models\Subsi21;
use App\Models\Mercurio32;

class MercurioConyuge
{

    protected $resultado;
    protected $hoy;

    public function __construct()
    {
        $this->hoy = date('Y-m-d');
    }

    public function validarEstado($documento)
    {
        DB::beginTransaction();
        try {
            $cantidad = 0;
            $conyuges = $this->buscarSolicitudes($documento);

            if ($conyuges) {
                foreach ($conyuges as $conyuge) {

                    $conyuge_estado = Subsi20::where('cedcon', trim($conyuge->cedcon))
                        ->where('estado', 'A')
                        ->count();

                    if ($conyuge_estado > 0) {

                        $cantidad_conyuges = Subsi21::where('cedcon', trim($conyuge->cedcon))
                            ->where('cedtra', $conyuge->cedtra)
                            ->count();

                        if ($cantidad_conyuges == 0) {
                            $this->actualizaMercurio($conyuge);
                            $cantidad++;
                        }
                    }
                }
            }
            $this->resultado = array(
                "success" => true,
                "msj" => "El proceso se completo con éxito",
                "cantidad" => $cantidad
            );
            DB::commit();
        } catch (QueryException $err) {
            DB::rollBack();
            throw new DebugException($err->getMessage() . ' ' . $err->getLine() . ' ' . basename($err->getFile()), 1);
        } catch (\Exception $ex) {
            DB::rollBack();
            throw new DebugException($ex->getMessage() . ' ' . $ex->getLine() . ' ' . basename($ex->getFile()), 1);
        }
    }

    public function actualizaMercurio($conyuge)
    {
        Mercurio32::where('documento', $conyuge->documento)
            ->where('codcon', $conyuge->codcon)
            ->where('cedtra', $conyuge->cedtra)
            ->where('estado', 'A')
            ->update(["estado" => 'I']);
    }

    public function buscarSolicitudes($documento)
    {
        return Mercurio32::where('documento', $documento)
            ->where('estado', 'A')
            ->get();
    }

    public function getResultado()
    {
        return $this->resultado;
    }
}
