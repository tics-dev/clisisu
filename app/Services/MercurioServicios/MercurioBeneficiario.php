<?php

namespace  App\Services\MercurioServicios;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Exceptions\DebugException;
use App\Models\Subsi22;
use App\Models\Subsi23;
use App\Models\Mercurio34;

class MercurioBeneficiario
{
    protected $resultado;
    protected $hoy;

    public function __construct()
    {
        $this->hoy = date('Y-m-d');
    }

    public function validarEstado($documento)
    {
        DB::beginTransaction();
        try {
            $cantidad = 0;
            $solicitudes = $this->buscarSolicitudes($documento);

            if ($solicitudes) {

                foreach ($solicitudes as $solicitud) {

                    $_beneficiarios = Subsi22::where('documento', trim($solicitud->numdoc))
                        ->where('estado', 'A')
                        ->get();

                    if ($_beneficiarios) {
                        if (count($_beneficiarios) > 0) {

                            $codes = array();
                            foreach ($_beneficiarios as $_beneficiario) $codes[] = $_beneficiario->codben;

                            $cantidad_beneficiarios = Subsi23::whereIn('codben', $codes)
                                ->where('cedtra', $solicitud->cedtra)
                                ->count();

                            if ($cantidad_beneficiarios == 0) {
                                $this->actualizaMercurio($solicitud);
                                $cantidad++;
                            }
                        }
                    }
                }
            }
            $this->resultado = array(
                "success" => true,
                "msj" => "El proceso se completo con éxito",
                "cantidad" => $cantidad
            );
            DB::commit();
        } catch (QueryException $err) {
            DB::rollBack();
            throw new DebugException($err->getMessage() . ' ' . $err->getLine() . ' ' . basename($err->getFile()), 1);
        } catch (\Exception $ex) {
            DB::rollBack();
            throw new DebugException($ex->getMessage() . ' ' . $ex->getLine() . ' ' . basename($ex->getFile()), 1);
        }
    }

    public function actualizaMercurio($beneficiario)
    {
        Mercurio34::where('documento', $beneficiario->documento)
            ->where('numdoc', $beneficiario->numdoc)
            ->where('cedtra', $beneficiario->cedtra)
            ->where('estado', 'A')
            ->update(["estado" => 'I']);
    }

    public function buscarSolicitudes($documento)
    {
        return Mercurio34::where('documento', $documento)
            ->where('estado', 'A')
            ->get();
    }

    public function getResultado()
    {
        return $this->resultado;
    }
}
