<?php

namespace  App\Services\Poblacion;

use App\Libraries\MyParams;
use Illuminate\Support\Facades\DB;
use App\Models\Subsi20;
use App\Models\Subsi21;

class InformationConyuges
{

    public function __construct()
    {
    }

    /**
     * buscar function
     * @changed [2023-12-00]
     * migration informacion_empresa
     * @author elegroag <elegroag@ibero.edu.co>
     * @param [type] $argv
     * @return void
     */
    public function buscar(MyParams $argv)
    {
        $cedcon = $argv->getParam('cedcon');
        $afilia =  self::findByCedcon($cedcon);
        if ($afilia) {

            $conyuges = $afilia['data'];
            if (!is_null($conyuges) && count($conyuges) > 0) {
                $relaciones = Subsi21::where('cedcon', $cedcon)
                    ->orderBy('fecafi', 'desc')
                    ->get();

                return array(
                    "success" => true,
                    "data"    => $conyuges,
                    "multiples_registros" => $afilia['multiples_registros'],
                    "relaciones" => ($relaciones) ? $relaciones->toArray() : []
                );
            } else {
                return array("success" => false, "data" => false);
            }
        } else {
            return array("success" => false, "data" => false);
        }
    }

    public static function findByCedcon($cedcon)
    {
        $conyuges = Subsi20::where('subsi20.cedcon', $cedcon);
        if ($conyuges->count() > 0) {
            $conyuges = $conyuges->get();
            $data = [];
            foreach ($conyuges as $conyuge) {
                if ($conyuge->estado == 'A') {
                    $data = $conyuge->toArray();
                    break;
                }
            }
            //ningun registro de conyugue activa
            if (count($data) == 0) $data = $conyuges->first()->toArray();
            return  [
                "data" => $data,
                "multiples_registros" => $conyuges->toArray()
            ];
        } else {
            return false;
        }
    }
}
