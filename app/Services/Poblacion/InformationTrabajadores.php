<?php

namespace  App\Services\Poblacion;

use App\Libraries\MyParams;
use Illuminate\Support\Facades\DB;
use App\Models\Subsi16;
use App\Models\Subsi168;
use App\Models\Subsi17;

class InformationTrabajadores
{

    public function __construct()
    {
    }

    /**
     * buscarTrabajador function
     * @changed [2023-12-00]
     * migration informacion_empresa
     * @author elegroag <elegroag@ibero.edu.co>
     * @param [type] $argv
     * @return void
     */
    public function buscar(MyParams $argv)
    {
        $cedtra = $argv->getParam('cedtra');
        $trabajador =  self::findByCedtra($cedtra);

        if ($trabajador != false) {
            if ($trabajador && count($trabajador) > 0) {
                $salario = Subsi17::where('cedtra', $cedtra)
                    ->orderBy('fecha', 'desc')
                    ->get();

                $trayectoria = Subsi16::where('cedtra', $cedtra)
                    ->orderBy('fecafi', 'desc')
                    ->get();

                $multiafiliaciones = Subsi168::where('cedtra', $cedtra)->get();

                return array(
                    "success" => true,
                    "data"    => $trabajador,
                    "salario"  => ($salario) ? $salario->toArray() : [],
                    "trayectoria" => ($trayectoria) ? $trayectoria->toArray() : [],
                    "multiafiliaciones" => ($multiafiliaciones) ? $multiafiliaciones->toArray() : []
                );
            } else {
                return [
                    "success" => false, "data" => false
                ];
            }
        } else {
            return [
                "success" => false, "data" => false
            ];
        }
    }

    public static function findByCedtra($cedtra)
    {
        $trabajador = DB::table('subsi15')
            ->select(
                'subsi15.*',
                'subsi02.razsoc',
                'subsi02.repleg',
                'subsi02.direccion as empresa_direccion',
                'subsi02.telefono as empresa_telefono',
                'subsi02.codciu as empresa_codciu'
            )
            ->leftJoin('subsi02', 'subsi02.nit', '=', 'subsi15.nit')
            ->where('subsi15.cedtra', $cedtra)
            ->get()
            ->first();

        if ($trabajador) {
            $data = collect($trabajador);
            $multi_afiliacion = DB::table('subsi168')
                ->select(
                    'subsi168.cedtra',
                    'subsi168.codgir2',
                    'subsi168.codsuc',
                    'subsi168.salario',
                    'subsi168.fecafi',
                    'subsi168.estado',
                    'subsi168.fecest',
                    'subsi15.priape',
                    'subsi15.segape',
                    'subsi15.prinom',
                    'subsi15.segnom',
                    'subsi15.codcat',
                    'subsi168.nit',
                    'subsi168.codlis',
                    DB::raw("CONCAT_WS(' ',subsi15.priape,subsi15.segape,subsi15.prinom,subsi15.segnom) as fullname"),
                    'subsi02.razsoc',
                    'subsi02.repleg',
                    'subsi02.direccion as empresa_direccion',
                    'subsi02.telefono as empresa_telefono',
                    'subsi02.codciu as empresa_codciu'
                )
                ->leftJoin('subsi15', 'subsi15.cedtra', '=', 'subsi168.cedtra')
                ->leftJoin('subsi02', 'subsi02.nit', '=', 'subsi168.nit')
                ->where('subsi168.cedtra', $cedtra)
                ->groupBy('subsi168.cedtra')
                ->get()
                ->first();

            if ($multi_afiliacion) {
                $data['multiafiliacion'] = true;
                $data['multiafiliacion_empresa'] = $multi_afiliacion->nit;
                $data['multiafiliacion_sucursal'] = $multi_afiliacion->codsuc;
                $data['multiafiliacion_fecafi'] = $multi_afiliacion->fecafi;
                $data['multiafiliacion_estado'] = $multi_afiliacion->estado;
                $data['multiafiliacion_salario'] = $multi_afiliacion->salario;
                $data['multiafiliacion_codgir'] =  $multi_afiliacion->codgir2;
                $data['multiafiliacion_razsoc'] =  $multi_afiliacion->razsoc;
                $data['multiafiliacion_repleg'] =  $multi_afiliacion->repleg;
                $data['multiafiliacion_direccion'] =  $multi_afiliacion->empresa_direccion;
                $data['multiafiliacion_telefono'] =  $multi_afiliacion->empresa_telefono;
                $data['multiafiliacion_codciu'] =  $multi_afiliacion->empresa_codciu;
            } else {
                $data['multiafiliacion'] = false;
            }
        } else {
            return false;
        }
        return $data;
    }
}
