<?php

namespace  App\Services\Poblacion;

use App\Libraries\MyParams;
use App\Models\Subsi22;
use Illuminate\Support\Facades\DB;

class ListarBeneficiarios
{

    public function buscarByNumdoc(MyParams $argv)
    {
        $numdoc = $argv->getParam('numdoc');
        $estados = $argv->getParam('estados');

        $beneficiarios = Subsi22::where('documento', $numdoc)
            ->whereIn('estado', $estados)
            ->get();

        $data = [];
        if ($beneficiarios) {
            foreach ($beneficiarios as $beneficiario) {
                if ($beneficiario->estado == 'A') {
                    $data = $beneficiario->toArray();
                    break;
                }
            }
            if (count($data) == 0) {
                $data = collect($beneficiarios->toArray())->first();
            }

            return [
                "success" => true,
                "data" => $data,
                "multiples_registros" => $beneficiarios->toArray()
            ];
        } else {
            return [
                "success" => false,
                "data" => $data
            ];
        }
    }

    public function buscarBeneficiarioForCedtra(MyParams $argv)
    {
        $cedtra = $argv->getParam('cedtra');
        $documento = $argv->getParam('documento');

        $conyuges = DB::table('subsi22')
            ->select(
                'subsi20.*',
                DB::raw("CONCAT_WS(' ', subsi20.priape, subsi20.segape, subsi20.prinom, subsi20.segnom) as nombre"),
                "subsi21.cedtra",
                "subsi21.comper",
                "subsi21.fecafi"
            )
            ->join('subsi23', 'subsi23.codben', '=', 'subsi22.codben')
            ->leftJoin('subsi21', 'subsi21.cedcon', '=', 'subsi23.cedcon')
            ->leftJoin('subsi20', 'subsi20.cedcon', '=', 'subsi23.cedcon')
            ->where('subsi22.documento', $documento)
            ->where('subsi23.cedtra', $cedtra)
            ->where('subsi21.cedtra', $cedtra)
            ->groupBy('subsi20.cedcon');

        if ($conyuges->count() > 0) {
            return $conyuges->get();
        } else {
            return false;
        }
    }
}
