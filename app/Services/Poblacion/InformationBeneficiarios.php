<?php

namespace  App\Services\Poblacion;

use App\Libraries\MyParams;
use App\Models\Subsi22;
use App\Models\Subsi23;
use Illuminate\Support\Facades\DB;

class InformationBeneficiarios
{


    public function __construct()
    {
    }

    /**
     * buscarTrabajador function
     * @changed [2023-12-00]
     * migration informacion_empresa
     * @author elegroag <elegroag@ibero.edu.co>
     * @param [type] $argv
     * @return void
     */
    public function buscar(MyParams $argv)
    {
        $numdoc = $argv->getParam('numdoc');
        $beneficiario =  self::findByNumdoc($numdoc);

        if ($beneficiario) {
            $beneficiario = $beneficiario['data'];

            if (count($beneficiario) > 0) {
                $relaciones = Subsi23::where('codben', $beneficiario['codben'])->get();
                return [
                    "success" => true,
                    "data" => $beneficiario,
                    "relaciones" => ($relaciones) ? $relaciones->toArray() : [],
                    "multiples_registros" => $beneficiario['multiples_registros'],
                ];
            } else {
                return array("success" => false, "data" => false);
            }
        } else {
            return  array("success" => false, "data" => false);
        }
    }

    public static function findByNumdoc($numdoc)
    {
        $subsi22 = Subsi22::where('documento', $numdoc);
        $multiples_registros = false;

        if ($subsi22) {
            $num = $subsi22->count();
            if ($num == 0) {
                return false;
            } else {
                if ($num > 1) {
                    $multiples_registros = $subsi22->get()->toArray();
                    $beneficiario = $subsi22->where('estado', 'A')->get()->first();
                } else {
                    $beneficiario = $subsi22->get();
                }
            }

            if (!$beneficiario) {
                $beneficiario = $subsi22->get()->first();
            }

            return [
                "data" => $beneficiario,
                "multiples_registros" => $multiples_registros
            ];
        } else {
            return false;
        }
    }
}
