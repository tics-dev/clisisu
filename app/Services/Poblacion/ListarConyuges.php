<?php

namespace  App\Services\Poblacion;

use App\Console\Servicios\General\General;
use App\Libraries\MyParams;
use App\Models\Subsi12;
use App\Models\Subsi20;
use Illuminate\Support\Facades\DB;

class ListarConyuges
{

    public function __construct()
    {
    }

    public function buscarByCedcon(MyParams $argv)
    {
        $cedcon = $argv->getParam('cedcon');
        $estados = $argv->getParam('estados');

        $conyuges = Subsi20::where('subsi20.cedcon', $cedcon)
            ->whereIn('estado', $estados);

        if ($conyuges->count() > 0) {
            $conyuges = $conyuges->get();
            $data = [];
            foreach ($conyuges as $conyuge) {
                if ($conyuge->estado == 'A') {
                    $data = $conyuge->toArray();
                    break;
                }
            }

            //ningun registro de conyugue activa
            if (count($data) == 0) $data = $conyuges->first()->toArray();

            return [
                "success" => true,
                "data" => $data,
                "multiples_registros" => $conyuges->toArray()
            ];
        } else {
            return [
                "success" => false,
                "data" => []
            ];
        }
    }

    public function listarByNit(MyParams $argv)
    {
        $nit = $argv->getParam('nit');
        $estados = $argv->getParam('estado');

        $conyuges = DB::table('subsi20')
            ->select(
                "subsi20.cedcon",
                "subsi20.coddoc",
                "subsi20.priape",
                "subsi20.segape",
                "subsi20.prinom",
                "subsi20.segnom",
                "subsi20.direccion",
                "subsi20.telefono",
                "subsi20.email",
                "subsi20.codzon",
                "subsi20.codcaj",
                "subsi20.codocu",
                "subsi20.nivedu",
                "subsi20.captra",
                "subsi20.salario",
                "subsi20.tipsal",
                "subsi20.fecsal",
                "subsi20.tippag",
                "subsi20.codcue",
                "subsi20.ofides",
                "subsi20.codgru",
                "subsi20.codban",
                "subsi20.numcue",
                "subsi20.tipcue",
                "subsi20.sexo",
                "subsi20.estciv",
                "subsi20.fecnac",
                "subsi20.ciunac",
                "subsi20.estado",
                "subsi20.fecest",
                "subsi20.numtar",
                "subsi20.giass",
                "subsi20.usuario",
                "subsi20.fecact",
                DB::raw("CONCAT_WS(' ',subsi20.priape,subsi20.segape,subsi20.prinom,subsi20.segnom) as nombre"),
                "subsi21.cedtra"
            )
            ->leftJoin('subsi21', 'subsi21.cedcon', '=', 'subsi20.cedcon')
            ->leftJoin('subsi15', 'subsi15.cedtra', '=', 'subsi21.cedtra')
            ->where('subsi15.nit', $nit)
            ->whereIn('subsi20.estado', $estados)
            ->whereIn('subsi15.estado', $estados)
            ->groupBy('subsi20.cedcon')
            ->get();

        return $conyuges;
    }

    public function listarForTrabajador(MyParams $argv)
    {
        $cedtra = $argv->getParam('cedtra');
        $estados = $argv->getParam('estados');

        $conyuges = DB::table('subsi20')
            ->select(
                "subsi20.cedcon",
                "subsi20.coddoc",
                "subsi20.priape",
                "subsi20.segape",
                "subsi20.prinom",
                "subsi20.segnom",
                "subsi20.direccion",
                "subsi20.telefono",
                "subsi20.email",
                "subsi20.codzon",
                "subsi20.codcaj",
                "subsi20.codocu",
                "subsi20.nivedu",
                "subsi20.captra",
                "subsi20.salario",
                "subsi20.tipsal",
                "subsi20.fecsal",
                "subsi20.tippag",
                "subsi20.codcue",
                "subsi20.ofides",
                "subsi20.codgru",
                "subsi20.codban",
                "subsi20.numcue",
                "subsi20.tipcue",
                "subsi20.sexo",
                "subsi20.estciv",
                "subsi20.fecnac",
                "subsi20.ciunac",
                "subsi20.estado",
                "subsi20.fecest",
                "subsi20.numtar",
                "subsi20.giass",
                "subsi20.usuario",
                "subsi20.fecact",
                DB::raw("CONCAT_WS(' ',subsi20.priape,subsi20.segape,subsi20.prinom,subsi20.segnom) as nombre"),
                "subsi21.cedtra",
                "subsi21.comper",
                "subsi21.fecafi"
            )
            ->leftJoin('subsi21', 'subsi21.cedcon', '=', 'subsi20.cedcon')
            ->leftJoin('subsi15', 'subsi15.cedtra', '=', 'subsi21.cedtra')
            ->where('subsi15.cedtra', $cedtra)
            ->whereIn('subsi20.estado', $estados)
            ->whereIn('subsi15.estado', $estados)
            ->groupBy('subsi20.cedcon')
            ->get();

        return $conyuges;
    }
}
