<?php

namespace  App\Services\DeshacerAfiliaciones;

use App\Exceptions\DebugException;
use App\Models\Subsi02;
use App\Models\Subsi15;
use App\Models\Subsi168;
use App\Models\Subsi21;
use App\Models\Subsi23;

class DeshaceTrabajador
{

    private $trabajador;
    private $noAction = 0;
    private $isDeleteTrayecto = 0;
    private $isUpdate = 0;
    private $isDelete = 0;
    private $nit;
    private $tipo_documento;
    private $cedula_trabajador;

    public function __construct($nit, $tipo_documento, $cedtra)
    {
        $this->nit = $nit;
        $this->tipo_documento = $tipo_documento;
        $this->cedula_trabajador = $cedtra;
    }

    public function procesar()
    {
        $empresaModel = Subsi02::where("subsi02.nit", $this->nit);
        if ($empresaModel->count() == 0) {
            throw new DebugException("Error la empresa no es está registrada para continuar el proceso.", 501);
        }

        $empresa = $empresaModel->get()->first();

        $trabajadorModel = Subsi15::where("nit", $empresa->nit)
            ->where("coddoc", $this->tipo_documento)
            ->where("cedtra", $this->cedula_trabajador)
            ->where("estado", 'A');

        //buscar multiafiliación
        if ($trabajadorModel->count() == 0) {

            $trabajadorModel = Subsi168::where("nit", $this->nit)
                ->where("cedtra", $this->cedula_trabajador)
                ->where("estado", 'A');
        }

        if ($trabajadorModel->count() > 0) {

            $this->trabajador = $trabajadorModel->get()->first();
            $trayectorias =  $this->trabajador->getTrayectoria();
            $salarios =  $this->trabajador->getSalarios();

            if ($trayectorias) {

                //una trayectoria
                if ($trayectorias->count() == 1) {

                    $trayectoria = $trayectorias->first();

                    if ($this->trabajador->fecafi == $trayectoria->fecafi && $this->trabajador->nit == $trayectoria->nit) {

                        $trayectoria->delete();

                        if ($salarios->count() > 0) {

                            $salario = $salarios->last();
                            if ($salario->fecha == $trayectoria->fecafi) $salarios->delete();
                        }

                        //borrar el trabajador
                        $this->trabajador->delete();
                        $this->isDeleteTrayecto++;
                        $this->isDelete++;
                    } else {

                        //restablece a su trayectoria anterior la ficha del trabajador, dado que las fechas no correspondan
                        $this->trabajador->nit    = $trayectoria->nit;
                        $this->trabajador->fecafi = $trayectoria->fecafi;
                        $this->trabajador->fecest = $trayectoria->fecret;
                        $this->trabajador->fecsis = date('Y-m-d');
                        $this->trabajador->estado = 'I';
                        $this->trabajador->save();
                        $this->isUpdate++;
                    }
                } else {
                    //para muchas trayectorias
                    foreach ($trayectorias as $trayectoria) {

                        if ($this->trabajador->fecafi == $trayectoria->fecafi &&  $this->trabajador->nit == $trayectoria->nit) {

                            $trayectoria->delete();

                            if ($salarios->count() > 0) {

                                $salario = $salarios->last();
                                if ($salario->fecha == $trayectoria->fecafi) $salarios->delete();
                            }
                            $this->isDeleteTrayecto++;

                            break;
                        }
                    }

                    if ($this->isDeleteTrayecto) {


                        $trayectoria =  $this->trabajador->getTrayectoria()->last();

                        if ($trayectoria) {

                            $this->trabajador->fecafi = $trayectoria->fecafi;
                            $this->trabajador->fecest = $trayectoria->fecret;
                            $this->trabajador->nit = $trayectoria->nit;
                        }

                        $this->trabajador->fecsis = date('Y-m-d');
                        $this->trabajador->estado = 'I';
                        $this->trabajador->save();
                        $this->isUpdate++;
                    } else {
                        $this->noAction++;
                    }
                }
            } else {

                //cero trayectorias, el trabajador no es valido
                $this->borrarRelacionBeneficiario();
                $this->borrarRelacionConyuge();
                $this->trabajador->delete();
                $this->isDelete++;
            }
        } else {
            $this->noAction++;
        }
    }

    public function getResultado()
    {
        return [
            'noAction' => $this->noAction,
            'isDeleteTrayecto' => $this->isDeleteTrayecto,
            'isUpdate' => $this->isUpdate,
            'isDelete' => $this->isDelete,
            'trabajador' => ($this->trabajador) ? ["cedula" => $this->trabajador->cedtra, "estado" => $this->trabajador->estado, "nit" => $this->trabajador->nit, "fecha_estado" => $this->trabajador->fecest] : []
        ];
    }

    function borrarRelacionConyuge()
    {
        $relacionTrabajador = Subsi21::where('cedtra', $this->cedula_trabajador);
        if ($relacionTrabajador->count() > 0) {
            $relacionTrabajador = $relacionTrabajador->get()->first();
            $relacionTrabajador->delete();
        }
    }

    function borrarRelacionBeneficiario()
    {
        $relacion = Subsi23::where('cedtra', $this->cedula_trabajador);
        if ($relacion->count() > 0) {
            $relacion = $relacion->get()->first();
            $relacion->delete();
        }
    }
}
