<?php

namespace  App\Services\DeshacerAfiliaciones;

use App\Exceptions\DebugException;
use App\Models\Subsi02;
use App\Models\Subsi03;

class DeshaceEmpresa
{
    private $empresa;
    private $isDeleteTrayecto = 0;
    private $isUpdate = 0;
    private $isDelete = 0;
    private $noAction = 0;
    private $nit;
    private $tipo_documento;

    public function __construct($nit, $tipo_documento)
    {
        $this->nit = $nit;
        $this->tipo_documento = $tipo_documento;
    }

    public function procesar()
    {
        $empresas = Subsi02::where("subsi02.nit", $this->nit)
            ->where("coddoc", $this->tipo_documento)
            ->whereIn("subsi02.estado", ['A', 'D', 'S']);

        if ($empresas->count() > 0) {
            $this->empresa = $empresas->get()->first();
            $trayectorias =  $this->empresa->getTrayectorias();
            if ($trayectorias) {
                //una trayectoria
                if ($trayectorias->count() == 1) {
                    $trayectoria = $trayectorias->first();
                    if ($this->empresa->fecafi == $trayectoria->fecafi) {
                        $this->isDeleteTrayecto++;
                        $this->isDelete++;
                        $trayectoria->delete();
                        $this->empresa->delete();
                    } else {
                        $this->isUpdate++;
                        $this->empresa->fecafi = $trayectoria->fecafi;
                        $this->empresa->fecest = $trayectoria->fecret;
                        $this->empresa->fecsis = date('Y-m-d');
                        $this->empresa->estado = 'I';
                        $this->empresa->save();
                    }
                } else {
                    //para muchas trayectorias
                    foreach ($trayectorias as $trayectoria) {
                        if ($this->empresa->fecafi == $trayectoria->fecafi) {
                            $this->isDeleteTrayecto++;
                            $trayectoria->delete();
                            break;
                        }
                    }

                    if ($this->isDeleteTrayecto) {
                        $this->isUpdate++;
                        $trayectoria = $this->empresa->getTrayectorias()->last();
                        $this->empresa->fecafi = $trayectoria->fecafi;
                        $this->empresa->fecest = $trayectoria->fecret;
                        $this->empresa->fecsis = date('Y-m-d');
                        $this->empresa->estado = 'I';
                        $this->empresa->save();
                    } else {
                        $this->noAction++;
                    }
                }
            } else {
                $this->isDelete++;
                //cero trayectorias
                $this->empresa->delete();
            }
        } else {
            $this->noAction++;
        }
    }

    public function getResultado()
    {
        return [
            'noAction' => $this->noAction,
            'isDeleteTrayecto' => $this->isDeleteTrayecto,
            'isUpdate' => $this->isUpdate,
            'isDelete' => $this->isDelete,
            'empresa' => ($this->empresa) ? $this->empresa->toArray() : []
        ];
    }
}
