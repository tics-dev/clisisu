<?php

namespace  App\Services\DeshacerAfiliaciones;

use App\Exceptions\DebugException;
use App\Models\Subsi02;
use App\Models\Subsi15;
use App\Models\Subsi20;
use App\Models\Subsi21;
use App\Models\Subsi22;
use App\Models\Subsi23;

class DeshaceConyuge
{
    private $trabajador;
    private $conyuge;
    private $isDeleteTrayecto = 0;
    private $isUpdate = 0;
    private $isDelete = 0;
    private $noAction = 0;
    private $cedula_trabajador;
    private $cedula_conyuge;
    private $tipo_documento;

    public function __construct($cedtra, $cedcon, $tipo_documento)
    {
        $this->cedula_trabajador = $cedtra;
        $this->cedula_conyuge = $cedcon;
        $this->tipo_documento = $tipo_documento;
    }

    public function procesar()
    {
        $this->trabajador = Subsi15::where("cedtra", $this->cedula_trabajador);
        $conyugesModel = Subsi20::where('codcon', $this->cedula_conyuge)->where('coddoc', $this->tipo_documento);

        if ($conyugesModel->count() > 0) {
            $this->conyuge = $conyugesModel->get()->first();
            if ($this->conyuge->estado == 'A') {
                $otrasRelaciones = Subsi21::where('cedcon', $this->cedula_conyuge)->where('cedtra', '!=', $this->cedula_trabajador);
                if ($otrasRelaciones->count() == 0) {
                    $this->conyuge->estado = 'I';
                    $this->conyuge->fecest = date('Y-m-d');
                    $this->conyuge->save();
                    $this->isUpdate++;
                }
            }

            $relacionTrabajador = Subsi21::where('cedcon', $this->cedula_conyuge)
                ->where('cedtra', $this->cedula_trabajador);

            if ($relacionTrabajador->count() > 0) {
                $relacionTrabajador = $relacionTrabajador->get()->first();
                $relacionTrabajador->delete();
                $this->isDeleteTrayecto++;
            }
        } else {
            $this->noAction++;
        }
    }

    public function getResultado()
    {
        return [
            'noAction' => $this->noAction,
            'isDeleteTrayecto' => $this->isDeleteTrayecto,
            'isUpdate' => $this->isUpdate,
            'isDelete' => $this->isDelete,
            'empresa' => ($this->conyuge) ? $this->conyuge->toArray() : []
        ];
    }

    function borrarRelacionBeneficiario()
    {
        $relacion = Subsi23::where('cedtra', $this->cedula_trabajador)->where('cedcon', $this->cedula_conyuge);
        if ($relacion->count() > 0) {
            $relacion = $relacion->get()->first();
            $relacion->cedcon = null;
            $relacion->save();
        }
    }
}
