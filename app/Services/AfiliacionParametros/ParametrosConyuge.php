<?php

namespace  App\Services\AfiliacionParametros;

use App\Libraries\MyParams;
use App\Models\Gener08;
use App\Models\Gener09;
use App\Models\Gener15;
use App\Models\Gener17;
use App\Models\Gener18;
use App\Models\Gener36;
use App\Models\PuebloIndigena;
use App\Models\Resguardo;
use App\Models\Subsi27;
use App\Models\Subsi45;
use App\Models\Subsi46;
use App\Models\Subsi57;
use App\Models\Subsi84;
use App\Models\Tipopago;

class ParametrosConyuge
{
    private $parametros;

    public function __construct()
    {
        $params = array();
        $_tipo_documentos = Gener18::all();
        $params['tipo_documentos'] = $_tipo_documentos->makeVisible('attribute')->toArray();

        $_sexos =  Gener17::all();
        $params['sexos'] = $_sexos->makeVisible('attribute')->toArray();

        $_estado_civiles = Gener15::all();
        $params['estado_civiles'] = $_estado_civiles->makeVisible('attribute')->toArray();

        $_ciudades = Gener08::all();
        $params['ciudades'] = $_ciudades->makeVisible('attribute')->toArray();

        $_zonas = Gener09::all();
        $params['zonas'] = $_zonas->makeVisible('attribute')->toArray();

        $_nivel_educativos = Subsi46::all();
        $params['nivel_educativos'] = $_nivel_educativos->makeVisible('attribute')->toArray();

        $_ocupaciones = Subsi45::all();
        $params['ocupaciones'] = $_ocupaciones->makeVisible('attribute')->toArray();

        $_formas_pagos = Tipopago::all();
        $params['formas_pagos'] = $_formas_pagos->makeVisible('attribute')->toArray();

        //codcue
        $codigo_cuenta = Subsi27::all();
        $params['codigo_cuenta'] = $codigo_cuenta->makeVisible('attribute')->toArray();

        $bancos = Subsi84::all();
        $params['bancos'] = $bancos->makeVisible('attribute')->toArray();

        $discapacidades = Subsi57::all();
        $params['discapacidades'] = $discapacidades->makeVisible('attribute')->toArray();

        $resguardos = Resguardo::all();
        $params['resguardos'] = $resguardos->makeVisible('attribute')->toArray();

        $pueblosIndigenas = PuebloIndigena::all();
        $params['pueblos_indigenas'] = $pueblosIndigenas->makeVisible('attribute')->toArray();

        $pertenecia_etnicas = Gener36::all();
        $params['pertenencia_etnicas'] = $pertenecia_etnicas->makeVisible('attribute')->toArray();


        $params['captra'] = [
            ["captra" => "N", "detalle" => "NORMAL"],
            ["captra" => "I", "detalle" => "INCAPACITADO"]
        ];

        $params['vivienda'] = [
            ["vivienda" => "N", "detalle" => "NO"],
            ["vivienda" => "F", "detalle" => "FAMILIAR"],
            ["vivienda" => "P", "detalle" => "PROPIA"],
            ["vivienda" => "A", "detalle" => "ARRENDADA"],
            ["vivienda" => "H", "detalle" => "HIPOTECA"]
        ];

        $params['estado'] = [
            ["estado" => "A", "detalle" => "ACTIVO"],
            ["estado" => "I", "detalle" => "INACTIVO"],
            ["estado" => "M", "detalle" => "MUERTO"]
        ];

        //comper
        $params['companero_permanente'] = [
            ["estado" => "S", "detalle" => "SI"],
            ["estado" => "N", "detalle" => "NO"]
        ];

        //tippag
        $params['tipo_pago'] = [
            ['estado' => "T", 'detalle' => "PENDIENTE FORMA DE PAGO"],
            ['estado' => "A", 'detalle' => "ABONO CUENTA PERSONAL"],
            ['estado' => "D", 'detalle' => "CUENTA DAVIPLATA"]
        ];

        //tipcue
        $params['tipo_cuenta'] = [
            ['estado' => "A", 'detalle' => "AHORROS"],
            ['estado' => "C", 'detalle' => "CORRIENTE"]
        ];

        //recsub
        $params['recibe_subsidio'] = [
            ['estado' => "S", 'detalle' => "SI"],
            ['estado' => "N", 'detalle' => "NO"]
        ];

        $this->parametros = $params;
    }

    public function getParametros()
    {
        return $this->parametros;
    }
}
