<?php

namespace  App\Services\AfiliacionParametros;

use App\Libraries\MyParams;
use App\Models\Gener08;
use App\Models\Gener09;
use App\Models\Gener15;
use App\Models\Gener17;
use App\Models\Gener18;
use App\Models\Gener33;
use App\Models\Gener34;
use App\Models\Gener35;
use App\Models\Gener36;
use App\Models\PuebloIndigena;
use App\Models\Resguardo;
use App\Models\Subsi04;
use App\Models\Subsi05;
use App\Models\Subsi27;
use App\Models\Subsi41;
use App\Models\Subsi46;
use App\Models\Subsi54;
use App\Models\Subsi57;
use App\Models\Subsi71;
use App\Models\Subsi84;
use App\Models\Tipopago;

class ParametrosIndependiente
{
    private $parametros;

    public function __construct()
    {
        $params = array();
        $_tipo_documentos = Gener18::all();
        $params['tipo_documentos'] = $_tipo_documentos->makeVisible('attribute')->toArray();

        $_sexos =  Gener17::all();
        $params['sexos'] = $_sexos->makeVisible('attribute')->toArray();

        $_estado_civiles = Gener15::all();
        $params['estado_civiles'] = $_estado_civiles->makeVisible('attribute')->toArray();

        $_ciudades = Gener08::all();
        $params['ciudades'] = $_ciudades->makeVisible('attribute')->toArray();

        $_zonas = Gener09::all();
        $params['zonas'] = $_zonas->makeVisible('attribute')->toArray();

        $_ocupaciones = Gener33::all();
        $params['ocupaciones'] = $_ocupaciones->makeVisible('attribute')->toArray();

        $_orientacion_sexuales = Gener34::all();
        $params['orientacion_sexuales'] = $_orientacion_sexuales->makeVisible('attribute')->toArray();

        $_vulnerabilidades = Gener35::all();
        $params['vulnerabilidades'] = $_vulnerabilidades->makeVisible('attribute')->toArray();

        $pertenecia_etnicas = Gener36::all();
        $params['pertenencia_etnicas'] = $pertenecia_etnicas->makeVisible('attribute')->toArray();

        $_discapacidades = Subsi57::all();
        $params['discapacidades'] = $_discapacidades->makeVisible('attribute')->toArray();

        $_nivel_educativos = Subsi46::all();
        $params['nivel_educativos'] = $_nivel_educativos->makeVisible('attribute')->toArray();

        $_tipo_cotizantes = Subsi71::where('ocultar', '0')->get();
        $params['tipo_cotizantes'] = $_tipo_cotizantes->makeVisible('attribute')->toArray();

        $_formas_pagos = Tipopago::all();
        $params['formas_pagos'] = $_formas_pagos->makeVisible('attribute')->toArray();

        //codact
        $_actividades = Subsi04::where('en_uso', 1)->get();
        $params['actividades'] = $_actividades->toArray();

        //codcue
        $codigo_cuenta = Subsi27::all();
        $params['codigo_cuenta'] = $codigo_cuenta->makeVisible('attribute')->toArray();

        $bancos = Subsi84::all();
        $params['bancos'] = $bancos->makeVisible('attribute')->toArray();

        $codigo_giro = Subsi41::all();
        $params['codigo_giro'] = $codigo_giro->makeVisible('attribute')->toArray();

        $resguardos = Resguardo::all();
        $params['resguardos'] = $resguardos->makeVisible('attribute')->toArray();

        $pueblosIndigenas = PuebloIndigena::all();
        $params['pueblos_indigenas'] = $pueblosIndigenas->makeVisible('attribute')->toArray();

        $_tipo_sociedades = Subsi54::where('tipsoc', '08')->get();
        $params['tipo_sociedades'] = $_tipo_sociedades->toArray();

        $_codinds = Subsi05::whereIn('codind', ['50', '14'])->get();
        $params['codigo_indice'] = $_codinds->toArray();

        $params['forma_presentacion'] = [
            ["estado" => "U", 'detalle' => "UNICA"],
            ["estado" => "S", 'detalle' => "SUCURSAL"]
        ];

        $params['tipo_duracion'] = [
            ["estado" => "S", 'detalle' => "SI"],
            ["estado" => "N", 'detalle' => "NO"]
        ];

        $params['paga_mes'] = [
            ["estado" => "S", 'detalle' => "SI"],
            ["estado" => "N", 'detalle' => "NO"]
        ];

        //calemp
        $params['calidad_empresa'] = [
            ["estado" => "P", "detalle" => "PENSIONADO"],
        ];

        $params['sidicalizado'] = [
            ["trasin" => "S", "detalle" => "SI"],
            ["trasin" => "N", "detalle" => "NO"]
        ];

        $params['cabhog'] = [
            ["cabhog" => "S", "detalle" => "SI"],
            ["cabhog" => "N", "detalle" => "NO"]
        ];

        $params['captra'] = [
            ["captra" => "N", "detalle" => "NORMAL"],
            ["captra" => "I", "detalle" => "INCAPACITADO"]
        ];

        $params['rural'] = [
            ["rural" => "S", "detalle" => "SI"],
            ["rural" => "N", "detalle" => "NO"]
        ];

        $params['tipcon'] = [
            ["tipcon" => "F", "detalle" => "FIJO"],
            ["tipcon" => "I", "detalle" => "INDEFINIDO"]
        ];

        $params['vivienda'] = [
            ["vivienda" => "N", "detalle" => "NO"],
            ["vivienda" => "F", "detalle" => "FAMILIAR"],
            ["vivienda" => "P", "detalle" => "PROPIA"],
            ["vivienda" => "A", "detalle" => "ARRENDADA"],
            ["vivienda" => "H", "detalle" => "HIPOTECA"]
        ];
        $params['estado'] = [
            ["estado" => "A", "detalle" => "ACTIVO"],
            ["estado" => "I", "detalle" => "INACTIVO"],
            ["estado" => "M", "detalle" => "MUERTO"]
        ];
        $params['tipjor'] = [
            ["tipjor" => "C", "detalle" => "COMPLETA"],
            ["tipjor" => "M", "detalle" => "MEDIA"],
            ["tipjor" => "P", "detalle" => "PARCIAL"]
        ];
        $params['empleador'] = [
            ['estado' => "S", 'detalle' => "SI"],
            ['estado' => "N", 'detalle' => "NO"]
        ];
        //tippag
        $params['tipo_pago'] = [
            ['estado' => "T", 'detalle' => "PENDIENTE FORMA DE PAGO"],
            ['estado' => "A", 'detalle' => "ABONO CUENTA PERSONAL"],
            ['estado' => "D", 'detalle' => "CUENTA DAVIPLATA"]
        ];
        //tipcue
        $params['tipo_cuenta'] = [
            ['estado' => "A", 'detalle' => "AHORROS"],
            ['estado' => "C", 'detalle' => "CORRIENTE"]
        ];

        $params['giro'] = [
            ['estado' => "S", 'detalle' => "SI"],
            ['estado' => "N", 'detalle' => "NO"]
        ];

        $params['vendedor'] = [
            ['estado' => "S", 'detalle' => "SI"],
            ['estado' => "N", 'detalle' => "NO"]
        ];

        $params['labora_otra_empresa'] = [
            ['estado' => "S", 'detalle' => "SI"],
            ['estado' => "N", 'detalle' => "NO"]
        ];

        $params['tipo_empresa'] = [
            ["estado" => "P", 'detalle' => "PRIVADO"]
        ];

        $params['tipo_aportante'] = [
            ['estado' => "I", 'detalle' => "INDENPENDIENTE"]
        ];

        $params['tipo_persona'] = [
            ["estado" => "N", "detalle" => "NATURAL"]
        ];
        $this->parametros = $params;
    }

    public function getParametros()
    {
        return $this->parametros;
    }
}
