<?php

namespace  App\Services\AfiliacionParametros;

use App\Libraries\MyParams;
use App\Models\Gener07;
use App\Models\Gener08;
use App\Models\Gener09;
use App\Models\Gener18;
use App\Models\Subsi04;
use App\Models\Subsi05;
use App\Models\Subsi54;
use App\Models\Subsi56;
use App\Models\Subsi63;

class ParametrosEmpresa
{
    private $parametros;

    public function __construct()
    {
        $params = array();
        $_tipo_documentos = Gener18::all();
        $params['tipo_documentos'] = $_tipo_documentos->makeVisible('attribute')->toArray();

        //ciupri
        $_ciudad_comercial = Gener08::where("codciu", '<', 19001)->where("codciu", '>=', 18001)->get();
        $params['ciudad_comercial'] = $_ciudad_comercial->makeVisible('attribute')->toArray();

        //codciu
        $_ciudades = Gener08::all();
        $params['ciudades']  = $_ciudades->makeVisible('attribute')->toArray();

        //codzon
        $_zonas = Gener09::where("codzon", '<', 19001)->where("codzon", '>=', 18001)->get();
        $params['zonas'] = $_zonas->makeVisible('attribute')->toArray();

        //codact
        $_actividades = Subsi04::where('en_uso', 1)->get();
        $params['actividades'] = $_actividades->toArray();

        //tipsoc
        $_tipo_sociedades = Subsi54::all();
        $params['tipo_sociedades'] = $_tipo_sociedades->makeVisible('attribute')->toArray();

        //coddep
        $_departamentos = Gener07::all();
        $params['departamentos'] = $_departamentos->makeVisible('attribute')->toArray();

        //codcaj
        $_codigo_cajas = Subsi56::all();
        $params['codigo_cajas'] = $_codigo_cajas->makeVisible('attribute')->toArray();

        //codind
        $_codinds = Subsi05::all();
        $params['codigo_indice'] = $_codinds->makeVisible('attribute')->toArray();

        //ofiafi
        $_ofiafi = Subsi63::all();
        $params['oficina'] = $_ofiafi->makeVisible('attribute')->toArray();


        //tipemp
        $params['tipo_empresa'] = [
            ["estado" => "O", "detalle" => "OFICIAL"],
            ["estado" => "P", "detalle" => "PRIVADO"],
            ["estado" => "M", "detalle" => "MIXTO"]
        ];
        $params['estado'] = [
            ["estado" => "A", "detalle" => "ACTIVO"],
            ["estado" => "I", "detalle" => "INACTIVO"],
            ["estado" => "D", "detalle" => "DESACTUALIZADO"],
            ["estado" => "S", "detalle" => "BLOQUEADO"]
        ];
        //calemp
        $params['calidad_empresa'] = [
            ["estado" => "E", "detalle" => "EMPRESA"],
            ["estado" => "I", "detalle" => "INDEPENDIENTE"],
            ["estado" => "P", "detalle" => "PENSIONADO"],
            ["estado" => "F", "detalle" => "FACULTATIVO"]
        ];
        //tipper
        $params['tipo_persona'] = [
            ["estado" => "N", "detalle" => "NATURAL"],
            ["estado" => "J", "detalle" => "JURIDICA"]
        ];

        //tipdur
        $params['tipo_duracion'] = [
            ["estado" => "S", 'detalle' => "SI"],
            ["estado" => "N", 'detalle' => "NO"]
        ];

        //todmes
        $params['paga_mes'] = [
            ["estado" => "S", 'detalle' => "SI"],
            ["estado" => "N", 'detalle' => "NO"]
        ];

        //forpre
        $params['forma_presentacion'] = [
            ["estado" => "U", 'detalle' => "UNICA"],
            ["estado" => "S", 'detalle' => "SUCURSAL"]
        ];

        $params['pymes'] = [
            ["estado" => "S", 'detalle' => "SI"],
            ["estado" => "N", 'detalle' => "NO"]
        ];

        $params['contratista'] = [
            ["estado" => "S", 'detalle' => "SI"],
            ["estado" => "N", 'detalle' => "NO"]
        ];

        //tipemp 
        $params['tipo_empresa'] = [
            ["estado" => "O", 'detalle' => "OFICIAL"],
            ["estado" => "P", 'detalle' => "PRIVADO"],
            ["estado" => "M", 'detalle' => "MIXTA"]
        ];

        //tipapo
        $params['tipo_aportante'] = [
            ['estado' => "G", 'detalle' => "GRANDE"],
            ['estado' => "P", 'detalle' => "PEQUENO"],
            ['estado' => "E", 'detalle' => "ESPECIAL"],
            ['estado' => "I", 'detalle' => "INDENPENDIENTE"],
            ['estado' => "F", 'detalle' => "FACULTATIVO"],
            ['estado' => "O", 'detalle' => "PENSIONADO"]
        ];

        $params['colegio'] = [
            ["estado" => "S", 'detalle' => "SI"],
            ["estado" => "N", 'detalle' => "NO"]
        ];

        $this->parametros = $params;
    }

    public function getParametros()
    {
        return $this->parametros;
    }
}
