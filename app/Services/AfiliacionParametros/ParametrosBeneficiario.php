<?php

namespace  App\Services\AfiliacionParametros;

use App\Libraries\MyParams;
use App\Models\Gener08;
use App\Models\Gener09;
use App\Models\Gener15;
use App\Models\Gener17;
use App\Models\Gener18;
use App\Models\Gener36;
use App\Models\PuebloIndigena;
use App\Models\Resguardo;
use App\Models\Subsi27;
use App\Models\Subsi41;
use App\Models\Subsi45;
use App\Models\Subsi46;
use App\Models\Subsi57;
use App\Models\Subsi84;
use App\Models\Tipopago;

class ParametrosBeneficiario
{
    private $parametros;

    public function __construct()
    {
        $params = array();
        $_tipo_documentos = Gener18::all();
        $params['tipo_documentos'] = $_tipo_documentos->makeVisible('attribute')->toArray();

        $_sexos =  Gener17::all();
        $params['sexos'] = $_sexos->makeVisible('attribute')->toArray();

        $_estado_civiles = Gener15::all();
        $params['estado_civiles'] = $_estado_civiles->makeVisible('attribute')->toArray();

        $_ciudades = Gener08::all();
        $params['ciudades'] = $_ciudades->makeVisible('attribute')->toArray();

        $_zonas = Gener09::all();
        $params['zonas'] = $_zonas->makeVisible('attribute')->toArray();

        $_nivel_educativos = Subsi46::all();
        $params['nivel_educativos'] = $_nivel_educativos->makeVisible('attribute')->toArray();

        $_ocupaciones = Subsi45::all();
        $params['ocupaciones'] = $_ocupaciones->makeVisible('attribute')->toArray();

        $_formas_pagos = Tipopago::all();
        $params['formas_pagos'] = $_formas_pagos->makeVisible('attribute')->toArray();

        $_discapacidades = Subsi57::all();
        $params['discapacidades'] = $_discapacidades->makeVisible('attribute')->toArray();

        $codigo_giro = Subsi41::all();
        $params['codigo_giro'] = $codigo_giro->makeVisible('attribute')->toArray();

        $resguardos = Resguardo::all();
        $params['resguardos'] = $resguardos->makeVisible('attribute')->toArray();

        $pueblosIndigenas = PuebloIndigena::all();
        $params['pueblos_indigenas'] = $pueblosIndigenas->makeVisible('attribute')->toArray();

        $pertenecia_etnicas = Gener36::all();
        $params['pertenencia_etnicas'] = $pertenecia_etnicas->makeVisible('attribute')->toArray();

        $bancos = Subsi84::all();
        $params['bancos'] = $bancos->makeVisible('attribute')->toArray();

        $codigo_cuenta = Subsi27::all();
        $params['codigo_cuenta'] = $codigo_cuenta->makeVisible('attribute')->toArray();

        $params['captra'] = [
            ["captra" => "N", "detalle" => "NORMAL"],
            ["captra" => "I", "detalle" => "INCAPACITADO"]
        ];

        $params['estado'] = [
            ["estado" => "A", "detalle" => "ACTIVO"],
            ["estado" => "I", "detalle" => "INACTIVO"],
            ["estado" => "M", "detalle" => "MUERTO"]
        ];

        $params['parent'] = [
            ["estado" => "1", "detalle" => "HIJO"],
            ["estado" => "2", "detalle" => "HERMANO"],
            ["estado" => "3", "detalle" => "PADRE"],
            ["estado" => "4", "detalle" => "CUSTODIA"],
            ["estado" => "5", "detalle" => "CUIDADOR"]
        ];

        $params['huerfano'] = [
            ["estado" => "0", "detalle" => "NO APLICA"],
            ["estado" => "1", "detalle" => "HUERFANO PADRE"],
            ["estado" => "2", "detalle" => "HUERFANO MADRE"]
        ];

        $params['tiphij'] = [
            ["estado" => "0", "detalle" => "NO APLICA"],
            ["estado" => "1", "detalle" => "HIJO NATURAL"],
            ["estado" => "2", "detalle" => "HIJASTRO"],
            ["estado" => "3", "detalle" => 'CUSTODIA NIETO'],
            ["estado" => "4", "detalle" => 'CUSTODIA SOBRINO'],
            ["estado" => "5", "detalle" => 'CUSTODIA OTRO'],
            ["estado" => "6", "detalle" => 'CUIDADOR DISCAPACITADO'],
        ];

        $params['calendario'] = [
            ["estado" => "A", "detalle" => "A"],
            ["estado" => "B", "detalle" => "B"],
            ["estado" => "N", "detalle" => "NO APLICA"]
        ];

        $params['giro'] = [
            ["estado" => "S", "detalle" => "SI"],
            ["estado" => "N", "detalle" => "NO"],
        ];

        $params['pago'] = [
            ["estado" => "T", "detalle" => "TRABAJADOR"],
            ["estado" => "C", "detalle" => "CONYUGE"]
        ];

        //tippag
        $params['tipo_pago'] = [
            ['estado' => "T", 'detalle' => "PENDIENTE FORMA DE PAGO"],
            ['estado' => "A", 'detalle' => "ABONO CUENTA PERSONAL"],
            ['estado' => "D", 'detalle' => "CUENTA DAVIPLATA"]
        ];

        //tipcue
        $params['tipo_cuenta'] = [
            ['estado' => "A", 'detalle' => "AHORROS"],
            ['estado' => "C", 'detalle' => "CORRIENTE"]
        ];

        $this->parametros = $params;
    }

    public function getParametros()
    {
        return $this->parametros;
    }
}
