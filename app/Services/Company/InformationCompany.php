<?php

namespace  App\Services\Company;

use App\Libraries\MyParams;
use App\Models\Subsi02;
use App\Models\Subsi03;
use App\Models\Subsi48;
use App\Models\Subsi73;

class InformationCompany
{
    /**
     * consultaEmpresa function
     * @changed [2023-12-00]
     * migration ComfacaEmpresas informacion_empresa
     * @author elegroag <elegroag@ibero.edu.co>
     * @param [type] $argv
     * @return void
     */
    public function consultaEmpresa(MyParams $argv)
    {
        $estado = $argv->getParam('estado');
        $nit = $argv->getParam('nit');
        $estados = (!is_null($estado)) ?  ["{$estado}"] : ['A', 'I', 'S', 'D'];

        $empresa = Subsi02::where('nit', $nit)
            ->whereIn('estado', $estados)
            ->get()->first();

        if ($empresa) {
            $sucursales = Subsi48::where('nit', $nit)
                ->whereIn('estado', $estados)
                ->get();

            $actividades = [];
            if ($sucursales) {
                foreach ($sucursales as $sucursal) {
                    $actividad =  $sucursal->getActividadSucursal();
                    $actividades["{$actividad->codact}"] = $actividad->detalle;
                }
            }

            $listas = Subsi73::where('nit', $nit)->get();
            $trayectoria = Subsi03::where('nit', $nit)->get();
            return [
                "success" => true,
                "data"    => $empresa,
                "sucursales"  => ($sucursales) ? $sucursales->toArray() : [],
                "listas"      => ($listas) ? $listas->toArray() : [],
                "trayectoria" => ($trayectoria) ? $trayectoria->toArray() : [],
                "actividades" => $actividades
            ];
        } else {
            return [
                "success" => false,
                "data" => false,
                "message" => "No hay datos de consulta de la empresa {$nit}"
            ];
        }
    }

    /**
     * consultaSucursales function
     * @changed [2023-12-00]
     * migration ComfacaEmpresas buscar_sucursales_en_empresa
     * @author elegroag <elegroag@ibero.edu.co>
     * @param [type] $argv
     * @return void
     */
    public function consultaSucursales(MyParams $argv)
    {
        $empresa = Subsi02::where('nit', $argv->getParam('nit'))->get()->first();
        if ($empresa) {
            return [
                "success" => true,
                "data" => $empresa->getSucursales()->toArray()
            ];
        } else {
            return [
                "success" => false,
                "data" => false,
                "message" => "No hay datos de consulta de la empresa {$argv->getParam('nit')}"
            ];
        }
    }

    /**
     * consultaListas function
     * @changed [2023-12-00]
     * migration ComfacaEmpresas buscar_listas_en_empresa
     * @author elegroag <elegroag@ibero.edu.co>
     * @param [type] $argv
     * @return void
     */
    public function consultaListas(MyParams $argv)
    {
        $empresa = Subsi02::where('nit', $argv->getParam('nit'))->get()->first();
        if ($empresa) {
            return [
                "success" => true,
                "data" => $empresa->getListas()->toArray()
            ];
        } else {
            return [
                "success" => false,
                "data" => false,
                "message" => "No hay datos de consulta de la empresa {$argv->getParam('nit')}"
            ];
        }
    }
}
