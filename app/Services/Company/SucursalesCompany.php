<?php

namespace  App\Services\Company;

use App\Libraries\MyParams;
use App\Models\Subsi02;
use App\Models\Subsi03;
use App\Models\Subsi48;
use App\Models\Subsi73;

class SucursalesCompany
{

    private $resultado;

    public function __construct()
    {
    }

    /**
     * consultaSucursales function
     * @changed [2023-12-00]
     * migration informacion_empresa
     * @author elegroag <elegroag@ibero.edu.co>
     * @param [type] $argv
     * @return void
     */
    public function sucursales(MyParams $argv)
    {
    }

    public function getResultado()
    {
        return $this->resultado;
    }
}
