<?php

namespace App\Services\ReportesGiroCuota;

use Illuminate\Support\Facades\DB;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Border;

class ReporteCruceExcel
{

    public function generarExcel()
    {
        $alfa = "A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|P|Q|R|S|T|U|V|W|X|Y|Z|AA|AB|AC|AD|AE|AF|AG|AH|AI|AJ|AK|AL|AM|AN|AO|AP|AQ|AR|AS|AT|AU|AV|AW|AX|AY|AZ";
        $itemsAlfas = explode("|", $alfa);
        // Crear una nueva instancia de PHPExcel
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("Sistema CLIsisu COMFACA")
            ->setLastModifiedBy("edwin legro agudelo");

        // Seleccionar la hoja activa
        $objPHPExcel->setActiveSheetIndex(0);
        $hoja = $objPHPExcel->getActiveSheet();
        $hoja->setTitle('Pruebas');

        // Arreglo de objetos
        $data = $this->getBuscarDiferencias();
        if ($data) {

            $styleThickBrownBorderOutline = array(
                'borders' => array(
                    'outline' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => '77447744'),
                    ),
                ),
            );

            // Agregar los nombres de columna
            $columnNames = array_keys(get_object_vars($data[0]));
            $row = 1;
            foreach ($columnNames as $column => $value) {
                $hoja->setCellValue($itemsAlfas[$column] . '' . $row, $value)
                    ->getStyle($itemsAlfas[$column] . '' . $row)
                    ->getFont()->setSize(13)
                    ->setBold(true);

                $hoja->setCellValue($itemsAlfas[$column] . '' . $row, $value)
                    ->getStyle($itemsAlfas[$column] . '' . $row)
                    ->getAlignment()
                    ->setVertical(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }

            // Agregar los datos
            $row = 2;
            foreach ($data as $object) {
                $column = 0;
                foreach (get_object_vars($object) as $value) {
                    $hoja->setCellValue($itemsAlfas[$column] . '' . $row, $value);
                    $column++;
                }
                $row++;
            }

            $ai = 0;
            while ($ai < $column) {
                $hoja->getColumnDimension($itemsAlfas[$ai])
                    ->setAutoSize(true);
                $aj = 0;
                while ($aj < count($data)) {
                    $aj++;
                    $objPHPExcel->getActiveSheet(0)->getStyle($itemsAlfas[$ai] . '1:' . $itemsAlfas[$ai] . '' . ($aj + 1))
                        ->applyFromArray($styleThickBrownBorderOutline);
                }
                $ai++;
            }
        }

        // Crear la segunda hoja y llenar las celdas con los datos
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(1);
        $hoja2 = $objPHPExcel->getActiveSheet();
        $hoja2->setTitle('Produccion');

        $data = $this->getBuscarDiferenciasInversa();
        if ($data) {
            // Agregar los nombres de columna
            $row = 1;
            $columnNames = array_keys(get_object_vars($data[0]));
            foreach ($columnNames as $column => $value) {
                $hoja2->setCellValue($itemsAlfas[$column] . '' . $row, $value)
                    ->getStyle($itemsAlfas[$column] . '' . $row)
                    ->getFont()->setSize(13)
                    ->setBold(true);

                $hoja2->setCellValue($itemsAlfas[$column] . '' . $row, $value)
                    ->getStyle($itemsAlfas[$column] . '' . $row)
                    ->getAlignment()
                    ->setVertical(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }

            // Agregar los datos
            $row = 2;
            foreach ($data as $object) {
                $column = 0;
                foreach (get_object_vars($object) as $value) {
                    $hoja2->setCellValue($itemsAlfas[$column] . '' . $row, $value);
                    $column++;
                }
                $row++;
            }

            $ai = 0;
            while ($ai < $column) {
                $hoja2->getColumnDimension($itemsAlfas[$ai])
                    ->setAutoSize(true);
                $aj = 0;
                while ($aj < count($data)) {
                    $aj++;
                    $objPHPExcel->getActiveSheet(0)->getStyle($itemsAlfas[$ai] . '1:' . $itemsAlfas[$ai] . '' . ($aj + 1))
                        ->applyFromArray($styleThickBrownBorderOutline);
                }
                $ai++;
            }
        }


        // Guardar el archivo Excel
        $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $filePath = storage_path('files/reportes/archivo_cruce_giro_cuota_' . strtotime('now') . '.xls');
        $writer->save($filePath);

        dump(json_encode([
            'succcess' => true,
            'msj' => 'Proceso OK',
            'url' => $filePath
        ]));
    }

    public function getBuscarDiferencias()
    {
        $data = DB::select("SELECT pruebas.cedtra, 
        pruebas.cedres, 
        pruebas.codben, 
        pruebas.periodo, 
        pruebas.nit, 
        sum(pruebas.numcuo) as _cuotas, 
        sum(pruebas.valor) as _valor, 
        pruebas.parent 
        FROM subsi14 as pruebas 
        left join subsi14_pro as produccion on 
            produccion.cedres=pruebas.cedres and 
            produccion.codben=pruebas.codben and 
            produccion.cedtra=pruebas.cedtra and 
            produccion.tipgir <> 'A' 
        WHERE pruebas.tipgir <> 'A' and produccion.cedres IS NULL GROUP BY 1,2,3,4;");
        return $data;
    }

    public function getBuscarDiferenciasInversa()
    {
        $data = DB::select("SELECT produccion.cedtra, 
        produccion.cedres, 
        produccion.codben, 
        produccion.periodo, 
        produccion.nit, 
        sum(produccion.numcuo) as _cuotas, 
        sum(produccion.valor) as _valor, 
        produccion.parent 
        FROM subsi14_pro as produccion 
        left join subsi14 as pruebas on 
            pruebas.cedres=produccion.cedres and 
            pruebas.codben=produccion.codben and 
            pruebas.cedtra=produccion.cedtra and 
            pruebas.tipgir <> 'A' 
        WHERE produccion.tipgir <> 'A' and pruebas.cedres IS NULL GROUP BY 1,2,3,4;");
        return $data;
    }
}
