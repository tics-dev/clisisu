<?php

namespace App\Services\ServicioSat;

interface ProcesarTrabajadorInterface
{

    public function iniciar();
    public function terminar();
    public function cambio_salario($salario = 0);
    public function isEnabled($servicio);
    public function prepara_data_sat();
    public function iniciar_pensionados();
    public function iniciar_independientes();
    public function terminar_pensionados();
    public function terminar_independientes();
    public function cambio_salario_independientes($salario = 0);
}
