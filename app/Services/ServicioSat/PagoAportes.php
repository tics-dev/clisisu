<?php

namespace App\Services\ServicioSat;

use App\Exceptions\DebugException;
use Illuminate\Validation\Rule;
use App\Models\Subsi02;
use App\Models\Sat20;
use App\Models\Subsi01;
use App\Models\Subsi64;
use App\Models\Subsi65;
use App\Models\Subsi10;
use App\Models\Subsi11;
use App\Models\Gener18;
use App\Models\Sat15;

class PagoAportes
{
    public $numtraccf;
    protected $usuario;
    protected $subsi02;
    protected $fecha;
    protected $campos;
    protected $rules;
    public $sat;

    public function __construct($nit = null)
    {
        $this->usuario = 1;
        $this->fecha = date('Y-m-d');
        if ($nit) {
            $this->subsi02 = Subsi02::where("nit", $nit)->get()->first();
        }
        $this->numtraccf = Sat20::max("numtraccf") + 1;
    }

    /**
     * estado function
     * reporteEstadoPagoEmpleadorEntrada
     * @return void
     */
    public function estado($estpag)
    {
        $this->valida_empresa();
        $this->createSat20(7);
        $this->sat = new Sat15();
        $this->campos           = $this->sat->getCamposSat15();
        $this->sat->sersat      = "0";
        $this->sat->numtraccf   = $this->numtraccf;
        $this->sat->tipdocemp   = $this->get_coddoc($this->subsi02->coddoc);
        $this->sat->numdocemp   = $this->subsi02->nit;
        $this->sat->estpag      = $estpag;
        //estado 1 = al día
        //estado 2 = en mora por aportes

        $this->rules = array(
            "numtraccf"  => "required|max:11",
            "tipdocemp"  => "required|max:2",
            "estpag"     => "required|max:1",
            "numdocemp"  => "required|max:17"
        );
    }

    public function valida_empresa()
    {
        if (!$this->subsi02) throw new DebugException("Error la empresa no está registrada en el sistema de sisuweb", 1);
    }

    public function createSat20($tiptra)
    {
        $sat20 = new Sat20();
        $sat20->numtraccf = $this->numtraccf;
        $sat20->fecha     = $this->fecha;
        $sat20->hora      = date('H:i:s');
        $sat20->usuario   = $this->usuario;
        $sat20->tiptra    = $tiptra;
        $sat20->gestion   = 'C';
        $sat20->estado    = 'P';
        $sat20->persiste  = '0';
        $sat20->save();
    }

    public function get_coddoc($coddoc)
    {
        $g18 = Gener18::where("coddoc", $coddoc)->get()->first();
        return $g18->codrua;
    }

    public function salvar()
    {
        $validator = $this->sat->isValid($this->rules);
        if ($validator === true) {
            if (!$this->sat->save()) {
                throw new DebugException("Error al guardar el modelo {$this->sat->getTable()} {$this->numtraccf} nit: {$this->sat->numdocemp}", 501);
            }
        } else {
            DebugException::add("valid", $validator);
            throw new DebugException("Error en validation del modelo: {$this->numtraccf} nit: {$this->sat->numdocemp}", 555);
        }
        return $this->numtraccf;
    }

    public function modelSource()
    {
        return strtolower($this->sat->getTable());
    }

    public function result()
    {
        return array(
            "tablasat"  => $this->modelSource(),
            "numtraccf" => $this->numtraccf
        );
    }

    public function prepara_data_sat()
    {
        $data = array();
        $attr =  $this->sat->makeVisible('attribute')->toArray();
        foreach ($this->campos as $key => $value) {
            $data[$value] = (isset($attr["$key"])) ? $attr["$key"] : '';
        }
        $data['NumeroRadicadoSolicitud'] = $this->numtraccf;
        return $data;
    }
}
