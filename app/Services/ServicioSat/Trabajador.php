<?php

namespace App\Services\ServicioSat;

use App\Exceptions\DebugException;
use App\Models\Subsi02;
use App\Models\Subsi15;
use App\Models\Sat20;
use App\Models\Sat09;
use App\Models\Sat10;
use App\Models\Sat13;
use App\Models\Sat29;
use App\Models\Gener18;

class Trabajador implements ProcesarTrabajadorInterface
{
    public $numtraccf;
    protected $usuario;
    protected $subsi15;
    protected $fecha;
    protected $empresa;
    protected $campos;
    protected $rules;
    protected $tipdoctra = '';
    protected $tipdocemp = '';
    public $sat;

    public function __construct($cedtra)
    {
        $this->usuario   = 1;
        $this->fecha     = date('Y-m-d');
        $this->numtraccf = Sat20::max('numtraccf') + 1;
        $this->subsi15   = Subsi15::where('cedtra', $cedtra)->get()->first();
        $this->empresa   = Subsi02::where("nit", $this->subsi15->nit)->get()->first();
    }

    //la fecha de nacimiento debe ir vacia
    public function iniciar()
    {
        $this->createSat20(5);
        $this->sat = new Sat09();
        $this->campos           = $this->sat->getCamposSat09();
        $this->sat->sersat      = '0';
        $this->sat->autmandat   = 'SI';
        $this->sat->autenvnot   = 'SI';
        $this->sat->numtrasat   = '0';
        $this->sat->tipini      = '1';
        $this->sat->numtraccf   = $this->numtraccf;
        $this->sat->fecini      = $this->subsi15->fecafi;
        $this->sat->numdoctra   = $this->subsi15->cedtra;
        $this->sat->priape      = $this->subsi15->priape;
        $this->sat->prinom      = $this->subsi15->prinom;
        $this->sat->segnom      = $this->subsi15->segnom;
        $this->sat->segape      = $this->subsi15->segape;
        $this->sat->tipdocemp   = $this->get_coddoc($this->empresa->coddoc);
        $this->sat->numdocemp   = $this->subsi15->nit;
        $this->sat->tipdoctra   = $this->get_coddoc($this->subsi15->coddoc);
        $this->sat->sexo        = ($this->subsi15->sexo == 'M') ? "H" : "M";
        $this->sat->coddep      = substr($this->subsi15->codzon, 0, 2);
        $this->sat->codmun      = substr($this->subsi15->codzon, 2, 3);
        $this->sat->direccion   = $this->subsi15->direccion;
        $this->sat->telefono    = $this->subsi15->telefono;
        $this->sat->email       = $this->subsi15->email;
        $this->sat->salario     = $this->subsi15->salario;
        $this->sat->tipsal      = ($this->subsi15->tipsal == 'F') ? "1" : "2";
        $this->sat->hortra      = $this->subsi15->horas;
        $this->sat->tipcon      = 'D';
        $this->sat->fecnac      = '';

        $this->rules = array(
            "numtraccf",
            "priape",
            "prinom",
            "tipdocemp",
            "tipdoctra"
        );
    }

    public function terminar()
    {
        $this->createSat20(6);
        $this->sat = new Sat10();
        $this->campos           = $this->sat->getCamposSat10();
        $this->sat->autmandat   = 'SI';
        $this->sat->autenvnot   = 'SI';
        $this->sat->numtrasat   = '0';
        $this->sat->sersat      = '0';
        $this->sat->tipter      = '1';  //tipo de Terminacion labor;
        $this->sat->numtraccf   = $this->numtraccf;
        $this->sat->numdocemp   = $this->subsi15->nit;
        $this->sat->fecter      = $this->subsi15->fecest;
        $this->sat->tipdoctra   = $this->tipdoctra;
        $this->sat->tipdocemp   = $this->tipdocemp;
        $this->sat->numdoctra   = $this->subsi15->cedtra;
        $this->sat->priape      = $this->subsi15->priape;
        $this->sat->prinom      = $this->subsi15->prinom;
        $this->sat->tipcon      = 'D';
        $this->rules = array(
            "fecter",
            "numtraccf",
            "numdoctra",
            "numdocemp",
            "tipdoctra"
        );
    }

    public function cambio_salario($salario = 0)
    {
        $this->createSat20(9);
        $this->sat = new Sat13();
        $this->campos           = $this->sat->getCamposSat13();
        $this->sat->autmandat   = "SI";
        $this->sat->autenvnot   = 'SI';
        $this->sat->numtrasat   = '0';
        $this->sat->sersat      = '0';
        $this->sat->numtraccf   = $this->numtraccf;
        $this->sat->tipper      = $this->get_tipper();
        $this->sat->tipdocemp   = $this->get_coddoc($this->empresa->coddoc);
        $this->sat->numdocemp   = $this->subsi15->nit;
        $this->sat->fecmod      = $this->subsi15->fecsal;
        $this->sat->tipdoctra   = $this->get_coddoc($this->subsi15->coddoc);
        $this->sat->numdoctra   = $this->subsi15->cedtra;
        $this->sat->priape      = $this->subsi15->priape;
        $this->sat->prinom      = $this->subsi15->prinom;
        $this->sat->salario     = $salario;
        $this->sat->tipsal      = ($this->subsi15->tipsal == 'F') ? "1" : "2";
        $this->sat->tipcon      = 'D';
        $this->rules = array(
            "tipper",
            "numtraccf",
            "numdoctra",
            "numdocemp",
            "tipdoctra",
            "salario",
            "tipsal"
        );
    }

    public function createSat20($tiptra)
    {
        $sat20 = new Sat20();
        $sat20->numtraccf = $this->numtraccf;
        $sat20->fecha     = $this->fecha;
        $sat20->hora      = date('H:i:s');
        $sat20->usuario   = $this->usuario;
        $sat20->tiptra    = $tiptra;
        $sat20->gestion   = 'C';
        $sat20->estado    = 'P';
        $sat20->save();
    }

    public function isEnabled($servicio = '')
    {
        $sat29 = Sat29::where("referencia", $servicio)
            ->where("estado", 'A')
            ->where("codapl", 'SA')
            ->count();
        return ($sat29 == 0) ? false : true;
    }

    public function modelSource()
    {
        return strtolower($this->sat->getTable());
    }

    public function get_tipper()
    {
        return $this->empresa->tipper;
    }

    public function get_coddoc($coddoc)
    {
        $g18 = Gener18::where("coddoc", $coddoc)->get()->first();
        return $g18->codrua;
    }

    public function result()
    {
        return array("tablasat" => $this->modelSource(), "numtraccf" => $this->numtraccf);
    }

    public function prepara_data_sat()
    {
        $data = array();
        $attr =  $this->sat->makeVisible('attribute')->toArray();
        foreach ($this->campos as $key => $value) {
            $data[$value] = (isset($attr["$key"])) ? $attr["$key"] : '';
        }
        $data['NumeroRadicadoSolicitud'] = $this->numtraccf;
        return $data;
    }

    public function salvar()
    {
        $validator = $this->sat->isValid($this->rules);
        if ($validator === TRUE) {
            if (!$this->sat->save()) {
                throw new DebugException("Error al guardar el modelo {$this->sat->getTable()}", 501);
            }
        } else {
            DebugException::add("valid", $validator);
            throw new DebugException("Error en validation del modelo.", 555);
        }
        return $this->numtraccf;
    }

    /**
     * pensionados function
     * Inicia relacion laboral pensionados
     * @return void
     */
    public function iniciar_pensionados()
    {
        $this->createSat20(5);
        $this->sat = new Sat09();
        $this->campos           = $this->sat->getCamposSat09();
        $this->sat->sersat      = '0';
        $this->sat->autmandat   = 'SI';
        $this->sat->autenvnot   = 'SI';
        $this->sat->numtrasat   = '0';
        $this->sat->tipini      = '1';
        $this->sat->numtraccf   = $this->numtraccf;
        $this->sat->fecini      = $this->subsi15->fecafi;
        $this->sat->numdoctra   = $this->subsi15->cedtra;
        $this->sat->priape      = $this->subsi15->priape;
        $this->sat->prinom      = $this->subsi15->prinom;
        $this->sat->tipdocemp   = $this->get_coddoc($this->empresa->coddoc);
        $this->sat->numdocemp   = $this->subsi15->nit;
        $this->sat->segnom      = $this->subsi15->segnom;
        $this->sat->tipdoctra   = $this->get_coddoc($this->subsi15->coddoc);
        $this->sat->segape      = $this->subsi15->segape;
        $this->sat->sexo        = ($this->subsi15->sexo == 'M') ? "H" : "M";
        $this->sat->coddep      = substr($this->subsi15->codzon, 0, 2);
        $this->sat->codmun      = substr($this->subsi15->codzon, 2, 3);
        $this->sat->direccion   = $this->subsi15->direccion;
        $this->sat->telefono    = $this->subsi15->telefono;
        $this->sat->email       = $this->subsi15->email;
        $this->sat->salario     = $this->subsi15->salario;
        $this->sat->tipsal      = ($this->subsi15->tipsal == 'F') ? "1" : "2";
        $this->sat->hortra      = $this->subsi15->horas;
        $this->sat->tipcon      = 'P';

        $this->rules = array(
            "numtraccf",
            "priape",
            "prinom",
            "tipdocemp",
            "tipdoctra"
        );
    }

    /**
     * independientes function
     * Inicia relación laboral independientes
     * @return void
     */
    public function iniciar_independientes()
    {
        $this->createSat20(5);
        $this->sat = new Sat09();
        $this->campos           = $this->sat->getCamposSat09();
        $this->sat->sersat      = '0';
        $this->sat->autmandat   = 'SI';
        $this->sat->autenvnot   = 'SI';
        $this->sat->numtrasat   = '0';
        $this->sat->tipini      = '1';
        $this->sat->numtraccf   = $this->numtraccf;
        $this->sat->fecini      = $this->subsi15->fecafi;
        $this->sat->numdoctra   = $this->subsi15->cedtra;
        $this->sat->priape      = $this->subsi15->priape;
        $this->sat->prinom      = $this->subsi15->prinom;
        $this->sat->tipdocemp   = $this->get_coddoc($this->empresa->coddoc);
        $this->sat->numdocemp   = $this->subsi15->nit;
        $this->sat->segnom      = $this->subsi15->segnom;
        $this->sat->tipdoctra   = $this->get_coddoc($this->subsi15->coddoc);
        $this->sat->segape      = $this->subsi15->segape;
        $this->sat->sexo        = ($this->subsi15->sexo == 'M') ? "H" : "M";
        $this->sat->coddep      = substr($this->subsi15->codzon, 0, 2);
        $this->sat->codmun      = substr($this->subsi15->codzon, 2, 3);
        $this->sat->direccion   = $this->subsi15->direccion;
        $this->sat->telefono    = $this->subsi15->telefono;
        $this->sat->email       = $this->subsi15->email;
        $this->sat->salario     = $this->subsi15->salario;
        $this->sat->tipsal      = ($this->subsi15->tipsal == 'F') ? "1" : "2";
        $this->sat->hortra      = $this->subsi15->horas;
        $this->sat->tipcon      = 'I';

        $this->rules = array(
            "numtraccf",
            "priape",
            "prinom",
            "tipdocemp",
            "tipdoctra"
        );
    }

    /**
     * terminar_pensionados function
     * Terminar pensionados
     * @return void
     */
    public function terminar_pensionados()
    {
        $this->createSat20(6);
        $this->sat = new Sat10();
        $this->campos           = $this->sat->getCamposSat10();
        $this->sat->autmandat   = 'SI';
        $this->sat->autenvnot   = 'SI';
        $this->sat->numtrasat   = '0';
        $this->sat->sersat      = '0';
        $this->sat->tipter      = '1';  //tipo de Terminacion labor;
        $this->sat->numtraccf   = $this->numtraccf;
        $this->sat->numdocemp   = $this->subsi15->nit;
        $this->sat->fecter      = $this->subsi15->fecest;
        $this->sat->tipdoctra   = $this->tipdoctra;
        $this->sat->tipdocemp   = $this->tipdocemp;
        $this->sat->numdoctra   = $this->subsi15->cedtra;
        $this->sat->priape      = $this->subsi15->priape;
        $this->sat->prinom      = $this->subsi15->prinom;
        $this->sat->tipcon      = 'P';
        $this->rules = array(
            "fecter",
            "numtraccf",
            "numdoctra",
            "numdocemp",
            "tipdoctra"
        );
    }

    /**
     * terminar_independientes function
     * @return void
     */
    public function terminar_independientes()
    {
        $this->createSat20(6);
        $this->sat = new Sat10();
        $this->campos           = $this->sat->getCamposSat10();
        $this->sat->autmandat   = 'SI';
        $this->sat->autenvnot   = 'SI';
        $this->sat->numtrasat   = '0';
        $this->sat->sersat      = '0';
        $this->sat->tipter      = '1';  //tipo de Terminacion labor;
        $this->sat->numtraccf   = $this->numtraccf;
        $this->sat->numdocemp   = $this->subsi15->nit;
        $this->sat->fecter      = $this->subsi15->fecest;
        $this->sat->tipdoctra   = $this->tipdoctra;
        $this->sat->tipdocemp   = $this->tipdocemp;
        $this->sat->numdoctra   = $this->subsi15->cedtra;
        $this->sat->priape      = $this->subsi15->priape;
        $this->sat->prinom      = $this->subsi15->prinom;
        $this->sat->tipcon      = 'I';
        $this->rules = array(
            "fecter",
            "numtraccf",
            "numdoctra",
            "numdocemp",
            "tipdoctra"
        );
    }

    /**
     * cambio_salario_independientes function
     * @param integer $salario
     * @return void
     */
    public function cambio_salario_independientes($salario = 0)
    {
        $this->createSat20(9);
        $this->sat = new Sat13();
        $this->campos           = $this->sat->getCamposSat13();
        $this->sat->autmandat   = "SI";
        $this->sat->autenvnot   = 'SI';
        $this->sat->numtrasat   = '0';
        $this->sat->sersat      = '0';
        $this->sat->numtraccf   = $this->numtraccf;
        $this->sat->tipper      = $this->get_tipper();
        $this->sat->tipdocemp   = $this->get_coddoc($this->empresa->coddoc);
        $this->sat->numdocemp   = $this->subsi15->nit;
        $this->sat->fecmod      = $this->subsi15->fecsal;
        $this->sat->tipdoctra   = $this->get_coddoc($this->subsi15->coddoc);
        $this->sat->numdoctra   = $this->subsi15->cedtra;
        $this->sat->priape      = $this->subsi15->priape;
        $this->sat->prinom      = $this->subsi15->prinom;
        $this->sat->salario     = $salario;
        $this->sat->tipsal      = ($this->subsi15->tipsal == 'F') ? "1" : "2";
        $this->sat->tipcon      = 'I';
        $this->rules = array(
            "tipper",
            "numtraccf",
            "numdoctra",
            "numdocemp",
            "tipdoctra",
            "salario",
            "tipsal"
        );
    }
}
