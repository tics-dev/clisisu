<?php

namespace App\Services\ServicioSat\RestService;

use App\Services\ServicioSat\TokenGenerador\TokenGeneratorInterface;
use App\Models\Sat01;
use App\Models\Sat29;

class RestServiceSenderFactory
{
    public static function create(TokenGeneratorInterface  $tokenGenerator, Sat01 $proveedor, Sat29 $servicioReference)
    {
        $tokenGenerator->setAtenticar(True);
        $tokenGenerator->generateToken($proveedor, $servicioReference);
        return new RestServiceSender($tokenGenerator);
    }
}
