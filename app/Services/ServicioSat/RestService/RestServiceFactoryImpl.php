<?php

namespace App\Services\ServicioSat\RestService;

use App\Services\ServicioSat\TokenGenerador\TokenGeneratorFactory;
use App\Exceptions\GlozaException;
use App\Models\Sat01;
use App\Models\Sat20;
use App\Models\Sat29;

class RestServiceFactoryImpl implements RestServiceFactory
{
    protected $sat01;
    protected $restServiceSenderFactory;
    public static $codapl = 'SA';
    static $data;

    public function __construct()
    {
        $this->sat01  = Sat01::where("codapl", self::$codapl)->get()->first();
    }

    /**
     * afiliacionPrimeraVezService function
     * @return void
     */
    public function afiliacionPrimeraVezService()
    {
        $sat20 = Sat20::where("numtraccf", self::$data['NumeroRadicadoSolicitud'])->get()->first();
        $servicioReference = self::initializa('sat02', $sat20);
        $resultado = $this->send($servicioReference);

        //procesar resultado
        if (count($resultado) > 0) {
            return self::procesaResultado($sat20, $resultado);
        } else {
            return [
                "codigo" => 401,
                "mensaje" => $resultado,
                "data" => self::$data
            ];
        }
    }

    /**
     * afiliacionNoPrimeraVezService function
     * @return void
     */
    public function afiliacionNoPrimeraVezService()
    {
        $sat20 = Sat20::where("numtraccf", self::$data['NumeroRadicadoSolicitud'])->get()->first();
        $servicioReference = self::initializa('sat03', $sat20);

        //enviar solicitud
        $resultado = $this->send($servicioReference);

        //procesar resultado
        if (count($resultado) > 0) {
            return self::procesaResultado($sat20, $resultado);
        } else {
            return [
                "codigo" => 401,
                "mensaje" => $resultado,
                "data" => self::$data
            ];
        }
    }

    /**
     * desafiliacionService function
     * @return void
     */
    public function desafiliacionService()
    {
        $sat20 = Sat20::where("numtraccf", self::$data['NumeroRadicadoSolicitud'])->get()->first();
        $servicioReference = self::initializa('sat06', $sat20);

        //enviar solicitud
        $resultado = $this->send($servicioReference);

        //procesar resultado
        if (count($resultado) > 0) {
            return self::procesaResultado($sat20, $resultado);
        } else {
            return [
                "codigo" => 401,
                "mensaje" => $resultado,
                "data" => self::$data
            ];
        }
    }

    /**
     * perdidaAfiliacionCausaGraveService function
     * @return void
     */
    public function perdidaAfiliacionCausaGraveService()
    {
        $sat20 = Sat20::where("numtraccf", self::$data['NumeroRadicadoSolicitud'])->get()->first();
        $servicioReference = self::initializa('sat08', $sat20);

        //enviar solicitud
        $resultado = $this->send($servicioReference);

        //procesar resultado
        if (count($resultado) > 0) {
            return self::procesaResultado($sat20, $resultado);
        } else {
            return [
                "codigo" => 401,
                "mensaje" => $resultado,
                "data" => self::$data
            ];
        }
    }

    /**
     * inicioRelacionLaboralService function
     * @return void
     */
    public function inicioRelacionLaboralService()
    {
        $sat20 = Sat20::where("numtraccf", self::$data['NumeroRadicadoSolicitud'])->get()->first();
        $servicioReference = self::initializa('sat09', $sat20);
        $resultado = $this->send($servicioReference);

        //procesar resultado
        if (count($resultado) > 0) {
            return self::procesaResultado($sat20, $resultado);
        } else {
            return [
                "codigo" => 401,
                "mensaje" => $resultado,
                "data" => self::$data
            ];
        }
    }

    /**
     * terminacionRelacionLaboralService function
     * @return void
     */
    public function terminacionRelacionLaboralService()
    {
        $sat20 = Sat20::where("numtraccf", self::$data['NumeroRadicadoSolicitud'])->get()->first();
        $servicioReference = self::initializa('sat10', $sat20);
        $resultado = $this->send($servicioReference);

        //procesar resultado
        if (count($resultado) > 0) {
            return self::procesaResultado($sat20, $resultado);
        } else {
            return [
                "codigo" => 401,
                "mensaje" => $resultado,
                "data" => self::$data
            ];
        }
    }

    /**
     * cambioSalarialService function
     * @return void
     */
    public function cambioSalarialService()
    {
        $sat20 = Sat20::where("numtraccf", self::$data['NumeroRadicadoSolicitud'])->get()->first();
        $servicioReference = self::initializa('sat13', $sat20);
        $resultado = $this->send($servicioReference);

        //procesar resultado
        if (count($resultado) > 0) {
            return self::procesaResultado($sat20, $resultado);
        } else {
            return [
                "codigo" => 401,
                "mensaje" => $resultado,
                "data" => self::$data
            ];
        }
    }


    /**
     * estadoPagoEmpleadorService function
     * @return void
     */
    public function estadoPagoEmpleadorService()
    {
        $sat20 = Sat20::where("numtraccf", self::$data['NumeroRadicadoSolicitud'])->get()->first();
        $servicioReference = self::initializa('sat15', $sat20);
        $resultado = $this->send($servicioReference);

        //procesar resultado
        if (count($resultado) > 0) {
            return self::procesaResultado($sat20, $resultado);
        } else {
            return [
                "codigo" => 401,
                "mensaje" => $resultado,
                "data" => self::$data
            ];
        }
    }

    /**
     * consultarSolicitudAfiliacionCcfPrimeravez function
     * @return void
     */
    public function consultarSolicitudAfiliacionCcfPrimeravez()
    {
    }

    /**
     * responderSolicitudAfiliacionCcfPrimeravez function
     * @return void
     */
    public function responderSolicitudAfiliacionCcfPrimeravez()
    {
    }

    /**
     * consultarEmpresaYEmpleados function
     * @return void
     */
    public function consultarEmpresaYEmpleados()
    {
        $servicioReference = Sat29::where("referencia", 'sat19')->where('codapl', self::$codapl)->get()->first();
        if (!$servicioReference) throw new GlozaException("El servicio no esta configurado tabla sat19", 404);

        if ($servicioReference->estado == 'I') throw new GlozaException("El servicio está inactivo. tabla sat19 ", 404);

        //enviar solicitud
        $resultado = $this->send($servicioReference);

        //procesar resultado
        if (count($resultado) > 0) {
            return [
                "codigo"  => 201,
                "mensaje" => "Ok",
                "success" => true,
                "data" => $resultado[0]
            ];
        } else {
            return [
                "codigo"  => 401,
                "mensaje" => $this->restServiceSenderFactory->getResponseText(),
                "data" => self::$data
            ];
        }
    }

    /**
     * consultarEmpresaYAfiliaciones function
     * @return void
     */
    public function consultarEmpresaYAfiliaciones()
    {
        $servicioReference = Sat29::where("referencia", 'sat31')->where('codapl', self::$codapl)->get()->first();
        if (!$servicioReference) throw new GlozaException("El servicio no esta configurado tabla sat31", 404);

        if ($servicioReference->estado == 'I') throw new GlozaException("El servicio está inactivo. tabla sat31 ", 404);

        //enviar solicitud
        $resultado = $this->send($servicioReference);

        //procesar resultado
        if (count($resultado) > 0) {
            return [
                "codigo"  => 201,
                "mensaje" => "Ok",
                "success" => true,
                "data" => $resultado[0]
            ];
        } else {
            return [
                "codigo"  => 401,
                "mensaje" => $this->restServiceSenderFactory->getResponseText(),
                "data" => self::$data
            ];
        }
    }

    static function initializa($tabla, $sat20): Sat29
    {
        $servicioReference = Sat29::where("referencia", $tabla)->where('codapl', self::$codapl)->get()->first();
        if (!$servicioReference) throw new GlozaException("El servicio no esta configurado tabla {$tabla}", 404);

        if ($servicioReference->estado == 'I') throw new GlozaException("El servicio está inactivo. tabla {$tabla} ", 404);

        $sat20->estado = 'E';
        $sat20->nota = 'Enviado al Sat';
        if (!$sat20->save()) throw new GlozaException("Error al actualizar sat20", 501);

        return $servicioReference;
    }

    static function procesaResultado($sat20, $resultado): array
    {
        if (isset($resultado['codigo'])) {
            switch ($resultado['codigo']) {
                case 200:
                case 'GN02':
                case 'GN04':
                case 'GN07':
                case 'GN15':
                case 'GN33':
                    $sat20->estado = 'O';
                    $sat20->codest = 'P';
                    break;
                default:
                    if (substr($resultado['codigo'], 0, 1) == 'A') {
                        $sat20->estado = 'U';
                        $sat20->codest = 'P';
                    } else {
                        $sat20->estado = 'G';
                        $sat20->codest = 'A';
                    }
                    break;
            }
            $sat20->nota = $resultado['mensaje'];
        } else {
            $sat20->estado = 'X';
            if (isset($resultado['Message'])) {
                $resultado['mensaje'] = $resultado['Message'];
                $resultado['codigo'] = 404;
                $resultado['data'] = self::$data;
                $sat20->nota = $resultado['Message'];
            } else {
                $resultado['codigo'] = 400;
                $sat20->nota = "Error no identificado:";
            }
            $resultado['resultado'] = "";
        }
        $resultado['estado'] = $sat20->estado;

        if (!$sat20->save()) throw new GlozaException("Error al actualizar sat20", 501);

        return $resultado;
    }

    private function send($servicioReference)
    {
        //enviar solicitud
        $this->restServiceSenderFactory = RestServiceSenderFactory::create(
            TokenGeneratorFactory::create(),
            $this->sat01,
            $servicioReference
        );
        $this->restServiceSenderFactory->send($servicioReference->nomsersat, self::$data);
        return $this->restServiceSenderFactory->getResponseArray();
    }

    public static function setData($data)
    {
        self::$data = $data;
    }
}
