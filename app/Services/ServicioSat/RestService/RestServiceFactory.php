<?php

namespace App\Services\ServicioSat\RestService;

interface RestServiceFactory
{

    public function afiliacionPrimeraVezService();
    public function afiliacionNoPrimeraVezService();
    public function desafiliacionService();
    public function perdidaAfiliacionCausaGraveService();
    public function inicioRelacionLaboralService();
    public function terminacionRelacionLaboralService();
    public function estadoPagoEmpleadorService();
    public function consultarSolicitudAfiliacionCcfPrimeravez();
    public function responderSolicitudAfiliacionCcfPrimeravez();
    public function consultarEmpresaYEmpleados();
    public function consultarEmpresaYAfiliaciones();
}
