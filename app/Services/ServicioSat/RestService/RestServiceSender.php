<?php

namespace App\Services\ServicioSat\RestService;

use App\Services\ServicioSat\TokenGenerador\TokenGenerator;
use App\Exceptions\WsException;

class RestServiceSender
{
    private $tokenGenerator;
    private $statusCode;
    private $responseArray;
    private $responseText;

    public function __construct($tokenGenerator)
    {
        $this->tokenGenerator = $tokenGenerator;
    }

    public function send($nomsersat, $data)
    {
        $ch = curl_init();
        $this->tokenGenerator->setAtenticar(False);
        $headers = $this->tokenGenerator->getHeader();
        curl_setopt($ch, CURLOPT_URL, TokenGenerator::$host . '' . $nomsersat);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);

        $this->statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($result === false) {
            $error = curl_error($ch);
            curl_close($ch);
            throw WsException::for($nomsersat, $error);
        } else {
            curl_close($ch);
            if (preg_match('/^\{(.*)\:(.*)\}$/', trim($result)) === False) {
                $this->responseText = $result;
                $this->responseArray = [];
            } else {
                $this->responseText = $result;
                $this->responseArray = json_decode($result, true);
            }
        }
    }

    public function getResponseArray()
    {
        return $this->responseArray;
    }

    public function getResponseText()
    {
        return $this->responseText;
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }
}
