<?php

namespace App\Services\ServicioSat;

use App\Exceptions\DebugException;
use Illuminate\Validation\Rule;
use App\Models\Subsi02;
use App\Models\Sat20;
use App\Models\Sat02;
use App\Models\Sat03;
use App\Models\Sat06;
use App\Models\Sat08;
use App\Models\Sat15;
use App\Models\Subsi01;
use App\Models\Gener18;


class Empresa implements ProcesarEmpresaInterface, ProcesarIndependientesInterface
{
    public $numtraccf;
    protected $usuario;
    protected $subsi02;
    protected $fecha;
    protected $campos;
    protected $rules;
    public $sat;

    public function __construct($nit = null)
    {
        $this->usuario = 1;
        $this->fecha = date('Y-m-d');
        if ($nit) {
            $this->subsi02 = Subsi02::where("nit", $nit)->get()->first();
        }
        $this->numtraccf = Sat20::max("numtraccf") + 1;
    }

    public function nueva()
    {
        $this->valida_empresa();
        $this->createSat20(1);
        $this->sat = new Sat02;
        $this->campos = $this->sat->getCamposSat02();
        $this->sat->autmandat  = 'SI';
        $this->sat->autenvnot = 'SI';
        $this->sat->noafissfant = 'SI';
        $this->sat->numtrasat = '0';
        $this->sat->sersat = "0";
        $this->sat->numtraccf = $this->numtraccf;
        $this->sat->tipdocemp = $this->get_coddoc($this->subsi02->coddoc);
        $this->sat->email = $this->subsi02->email;
        $this->sat->tipper = $this->subsi02->tipper;
        $this->sat->numdocemp = $this->subsi02->nit;
        $this->sat->fecafi = $this->subsi02->fecafi;
        $this->sat->fecsol = $this->getFechaSol();
        $this->sat->direccion = $this->subsi02->direccion;
        $this->sat->telefono = $this->getTelefono();
        $this->sat->coddep = substr($this->subsi02->codzon, 0, 2);
        $this->sat->codmun = substr($this->subsi02->codzon, 2, 3);
        $this->sat->tipcon = 'D';

        if ($this->sat->tipper == 'N') {
            $this->empresaNatural();
        } else {
            $this->empresaJuridica();
        }
    }

    public function getTelefono()
    {
        if (!empty($this->subsi02->telr) && !is_null($this->subsi02->telr)) {
            return $this->subsi02->telr;
        } else {
            if ($this->subsi02->telefono == '') {
                return '0';
            } else {
                return $this->subsi02->telefono;
            }
        }
    }

    public function getFechaSol()
    {
        if (strtotime($this->fecha) > strtotime($this->subsi02->fecafi)) {
            return $this->subsi02->fecafi;
        } else {
            if ($this->fecha > $this->subsi02->fecafi) {
                return $this->subsi02->fecafi;
            } else {
                return $this->fecha;
            }
        }
    }

    public function reintegro()
    {
        $this->valida_empresa();
        $this->createSat20(2);
        $this->sat = new Sat03;
        $this->campos           = $this->sat->getCamposSat03();
        $this->sat->autmandat   = 'SI';
        $this->sat->autenvnot   = 'SI';
        $this->sat->numtrasat   = '0';
        $this->sat->sersat      = "0";
        $this->sat->pazsal      = '';
        $this->sat->siafissfant = 'SI';
        $this->sat->numtraccf   = $this->numtraccf;
        $this->sat->tipdocemp   = $this->get_coddoc($this->subsi02->coddoc);
        $this->sat->email       = $this->subsi02->email;
        $this->sat->tipper      = $this->subsi02->tipper;
        $this->sat->numdocemp   = $this->subsi02->nit;
        $this->sat->fecafi      = $this->subsi02->fecafi;
        $this->sat->fecsol      = (strtotime($this->fecha) > strtotime($this->subsi02->fecafi)) ? $this->subsi02->fecafi : $this->fecha;
        $this->sat->direccion   = $this->subsi02->direccion;
        $this->sat->telefono    = $this->getTelefono();
        $this->sat->coddep      = substr($this->subsi02->codzon, 0, 2);
        $this->sat->codmun      = substr($this->subsi02->codzon, 2, 3);
        $this->sat->tipcon      = 'D';

        $cod_caja_id = $this->subsi02->codcaj;
        if (empty($cod_caja_id) || is_null($cod_caja_id) || $cod_caja_id == '00') {
            $subsi01 = Subsi01::where("codapl", 'SU')->get()->first();
            $codcaj = 'CCF' . $subsi01->codcaj;
        } else {
            $codcaj = 'CCF' . $this->subsi02->codcaj;
        }
        $this->sat->codcaj = $codcaj;
        if ($this->sat->tipper == 'N') {
            $this->empresaNatural();
        } else {
            $this->empresaJuridica();
        }
    }

    public function retiro()
    {
        $this->valida_empresa();
        $this->createSat20(3);
        $this->sat = new Sat06;
        $this->campos         = $this->sat->getCamposSat06();
        $this->sat->autmandat = 'SI';
        $this->sat->autenvnot = 'SI';
        $this->sat->pazsal    = 'SI';
        $this->sat->numtrasat = '0';
        $this->sat->sersat    = '0';
        $this->sat->numtraccf = $this->numtraccf;
        $this->sat->tipdocemp = $this->get_coddoc($this->subsi02->coddoc);
        $this->sat->numdocemp = $this->subsi02->nit;
        $this->sat->fecsol    = $this->fecha;
        $this->sat->fecdes    = $this->subsi02->fecest;
        $this->sat->coddep    = substr($this->subsi02->codzon, 0, 2);
        $this->sat->tipcon    = 'D';
    }

    public function causa_grave($detalle)
    {
        $this->valida_empresa();
        $this->createSat20(4);
        $this->sat = new Sat08;
        $this->campos           = $this->sat->getCamposSat08();
        $this->sat->numtraccf   = $this->numtraccf;
        $this->sat->tipper      = $this->subsi02->tipper;
        $this->sat->tipdocemp   = $this->get_coddoc($this->subsi02->coddoc);
        $this->sat->numdocemp   = $this->subsi02->nit;
        $this->sat->sersat      = '0';
        $this->sat->fecper      = $this->subsi02->fecest;
        $this->sat->razsoc      = $this->subsi02->razsoc;
        $this->sat->coddep      = substr($this->subsi02->codzon, 0, 2);
        $this->sat->causa       = $detalle;

        if ($this->subsi02->tipper == 'N') {
            $this->sat->priape = $this->subsi02->priape;
            $this->sat->prinom = $this->subsi02->prinom;
        }
        //1.Reporte de perdida de la afiliacion por causa grave activo
        //2.Reporte de perdida de la afiliacion por causa grave terminado
        $this->sat->estado = '2';
    }

    public function empresaNatural()
    {
        $this->sat->prinom = (strlen($this->subsi02->prinom) > 1) ? $this->subsi02->prinom : $this->subsi02->prinomrepleg;
        $this->sat->segnom = (strlen($this->subsi02->segnom) > 1) ? $this->subsi02->segnom : $this->subsi02->segnomrepleg;
        $this->sat->priape = (strlen($this->subsi02->priape) > 1) ? $this->subsi02->priape : $this->subsi02->priaperepleg;
        $this->sat->segape = (strlen($this->subsi02->segape) > 1) ? $this->subsi02->segape : $this->subsi02->segaperepleg;
        $this->sat->tipemp = '';
        $this->sat->tipdocemp = ($this->sat->tipdocemp == "NI") ? "CC" : $this->sat->tipdocemp;

        $this->rules = array(
            "numtraccf" => "required|max:11",
            "tipdocemp" => "required|max:2",
            "fecafi"    => "required|date",
            "prinom"    => "required|max:60",
            "priape"    => "required|max:60",
            "fecsol"    => "required|date"
        );
    }

    public function empresaJuridica()
    {
        $this->sat->razsoc    = $this->subsi02->razsoc;
        $this->sat->tipdocrep = (!is_null($this->subsi02->coddocrepleg)) ? $this->subsi02->coddocrepleg : $this->tipo_representante($this->subsi02->coddoc);
        $this->sat->numdocrep = $this->subsi02->cedrep;
        $this->sat->tipdocemp = ($this->sat->tipdocemp == "CC") ? "NI" : $this->sat->tipdocemp;

        $this->sat->prinom2   = (strlen($this->subsi02->priaperepleg) > 1) ? $this->subsi02->priaperepleg : $this->subsi02->priape;
        $this->sat->segnom2   = (strlen($this->subsi02->segaperepleg) > 1) ? $this->subsi02->segaperepleg : $this->subsi02->segape;
        $this->sat->priape2   = (strlen($this->subsi02->prinomrepleg) > 1) ? $this->subsi02->prinomrepleg : $this->subsi02->prinom;
        $this->sat->segape2   = (strlen($this->subsi02->segnomrepleg) > 1) ? $this->subsi02->segnomrepleg : $this->subsi02->segnom;

        if (strlen($this->sat->priape2) == 0 && strlen($this->sat->prinom2) == 0) {
            if (strlen($this->subsi02->repleg) > 0) {
                $exp = explode(" ", trim($this->subsi02->repleg));
                switch (count($exp)) {
                    case 6:
                    case 7:
                    case 8:
                        $this->sat->prinom2 = $exp[0];
                        $this->sat->segnom2 = $exp[1];
                        $this->sat->priape2 = $exp[2];
                        $this->sat->segape2 = $exp[3] . ' ' . $exp[4] . ' ' . $exp[5];
                        break;
                    case 5:
                        $this->sat->prinom2 = $exp[0];
                        $this->sat->segnom2 = $exp[1];
                        $this->sat->priape2 = $exp[2];
                        $this->sat->segape2 = $exp[3] . ' ' . $exp[4];
                        break;
                    case 4:
                        $this->sat->prinom2 = $exp[0];
                        $this->sat->segnom2 = $exp[1];
                        $this->sat->priape2 = $exp[2];
                        $this->sat->segape2 = $exp[3];
                        break;
                    case 3:
                        $this->sat->prinom2 = $exp[0];
                        $this->sat->priape2 = $exp[1] . ' ' . $exp[2];
                        break;
                    case 2:
                        $this->sat->prinom2 = $exp[0];
                        $this->sat->priape2 = $exp[1];
                        break;
                    case 1:
                        $this->sat->prinom2 = $exp[0];
                        $this->sat->priape2 = $exp[0];
                        break;
                    default:
                        break;
                }
            }
        }

        //la naturaleza juridica aplica solo del sat a la caja
        //$this->sat->tipemp = $this->naturaleza_juridica($this->subsi02->tipemp);
        $this->rules = array(
            "tipper" => [
                "required",
                Rule::in(['N', 'J'])
            ],
            "numtraccf" => "required|max:11",
            "tipdocemp" => "required|max:2",
            "fecafi"    => "required|date",
            "razsoc"    => "required|max:255",
            "tipdocrep" => "required|max:2",
            "numdocrep" => "required|max:18",
            "prinom2"   => "required|max:60",
            "priape2"   => "required|max:60",
            "fecsol"    => "required|date"
        );
    }

    public function createSat20($tiptra)
    {
        $sat20 = new Sat20();
        $sat20->numtraccf = $this->numtraccf;
        $sat20->fecha     = $this->fecha;
        $sat20->hora      = date('H:i:s');
        $sat20->usuario   = $this->usuario;
        $sat20->tiptra    = $tiptra;
        $sat20->gestion   = 'C';
        $sat20->estado    = 'P';
        $sat20->persiste  = '0';
        $sat20->save();
    }

    public function get_coddoc($coddoc)
    {
        $g18 = Gener18::where("coddoc", $coddoc)->get()->first();
        return $g18->codrua;
    }

    public function naturaleza_juridica($tipemp)
    {
        switch ($tipemp) {
            case 'O':
                return '1';
                break;
            case 'P':
                return '2';
                break;
            case 'M':
                return '3';
                break;
            case 'N':
                return '4';
                break;
            case 'D':
                return '5';
                break;
        }
        return '';
    }

    public function tipo_representante($coddoc)
    {
        switch ($coddoc) {
            case 1:
                return 'CC'; //CEDULA
                break;
            case 11:
                return 'CD'; //CARNÉ DIPLOMATICO
                break;
            case 4:
                return 'CE'; //CEDULA EXTRANJERIA
                break;
            case 8:
                return 'PE'; //PERMISO ESPECIAL DE PERMANENCIA
                break;
            case 14:
                return 'PT'; //PERMISO PROTECCION TEMPORAL
                break;
            case 3:
                //return 'NI'; //Número De Identificación Tributaria
                return 'CC'; //SAT ERROR USAR CEDULA
                break;
                break;
        }
        return '';
    }

    public function modelSource()
    {
        return strtolower($this->sat->getTable());
    }

    public function result()
    {
        return array(
            "tablasat"  => $this->modelSource(),
            "numtraccf" => $this->numtraccf
        );
    }

    public function prepara_data_sat()
    {
        $data = array();
        $attr =  $this->sat->makeVisible('attribute')->toArray();
        foreach ($this->campos as $key => $value) {
            $data[$value] = (isset($attr["$key"])) ? $attr["$key"] : '';
        }
        $data['NumeroRadicadoSolicitud'] = $this->numtraccf;
        return $data;
    }

    public function salvar()
    {
        $validator = $this->sat->isValid($this->rules);
        if ($validator === true) {
            if (!$this->sat->save()) {
                throw new DebugException("Error al guardar el modelo {$this->sat->getTable()} {$this->numtraccf} nit: {$this->sat->numdocemp}", 501);
            }
        } else {
            DebugException::add("valid", $validator);
            throw new DebugException("Error en validation del modelo: {$this->numtraccf} nit: {$this->sat->numdocemp}", 555);
        }
        return $this->numtraccf;
    }

    /**
     * nueva_independiente function
     * Empresa primera vez como independiente
     * @return void
     */
    public function nueva_independiente()
    {
        $this->valida_empresa();
        $this->createSat20(1);
        $this->sat = new Sat02;
        $this->campos           = $this->sat->getCamposSat02();
        $this->sat->autmandat   = 'SI';
        $this->sat->autenvnot   = 'SI';
        $this->sat->noafissfant = 'SI';
        $this->sat->numtrasat   = '0';
        $this->sat->sersat      = "0";
        $this->sat->numtraccf   = $this->numtraccf;
        $this->sat->tipdocemp   = $this->get_coddoc($this->subsi02->coddoc);
        $this->sat->email       = $this->subsi02->email;
        $this->sat->tipper      = $this->subsi02->tipper;
        $this->sat->numdocemp   = $this->subsi02->nit;
        $this->sat->fecafi      = $this->subsi02->fecafi;
        $this->sat->fecsol      = $this->getFechaSol();
        $this->sat->direccion   = $this->subsi02->direccion;
        $this->sat->telefono    = $this->getTelefono();
        $this->sat->coddep      = substr($this->subsi02->codzon, 0, 2);
        $this->sat->codmun      = substr($this->subsi02->codzon, 2, 3);
        $this->sat->tipcon      = 'I';
        $this->sat->prinom      = $this->subsi02->prinom;
        $this->sat->segnom      = $this->subsi02->segnom;
        $this->sat->priape      = $this->subsi02->priape;
        $this->sat->segape      = $this->subsi02->segape;
        $this->sat->tipemp      = '';

        $this->rules = array(
            "numtraccf" => "required|max:11",
            "tipdocemp" => "required|max:2",
            "fecafi"    => "required|date",
            "prinom"    => "required|max:60",
            "priape"    => "required|max:60",
            "fecsol"    => "required|date"
        );
    }

    /**
     * retiro_independiente function
     * Empleador independiente retirado 
     * @return void
     */
    public function retiro_independiente()
    {
        $this->valida_empresa();
        $this->createSat20(3);
        $this->sat = new Sat06;
        $this->campos         = $this->sat->getCamposSat06();
        $this->sat->autmandat = 'SI';
        $this->sat->autenvnot = 'SI';
        $this->sat->pazsal    = 'SI';
        $this->sat->numtrasat = '0';
        $this->sat->sersat    = '0';
        $this->sat->numtraccf = $this->numtraccf;
        $this->sat->tipdocemp = $this->get_coddoc($this->subsi02->coddoc);
        $this->sat->numdocemp = $this->subsi02->nit;
        $this->sat->fecsol    = $this->fecha;
        $this->sat->fecdes    = $this->subsi02->fecest;
        $this->sat->coddep    = substr($this->subsi02->codzon, 0, 2);
        $this->sat->tipcon = 'I';
    }

    /**
     * causa_grave_independiente function
     * Emplador por causa grave independiente
     * @param [type] $detalle
     * @return void
     */
    public function causa_grave_independiente($detalle)
    {
        $this->valida_empresa();
        $this->createSat20(4);
        $this->sat = new Sat08;
        $this->campos           = $this->sat->getCamposSat08();
        $this->sat->numtraccf   = $this->numtraccf;
        $this->sat->tipper      = $this->subsi02->tipper;
        $this->sat->tipdocemp   = $this->get_coddoc($this->subsi02->coddoc);
        $this->sat->numdocemp   = $this->subsi02->nit;
        $this->sat->sersat      = '0';
        $this->sat->fecper      = $this->subsi02->fecest;
        $this->sat->razsoc      = $this->subsi02->razsoc;
        $this->sat->coddep      = substr($this->subsi02->codzon, 0, 2);
        $this->sat->causa       = $detalle;

        if ($this->subsi02->tipper == 'N') {
            $this->sat->priape = $this->subsi02->priape;
            $this->sat->prinom = $this->subsi02->prinom;
        }
        //1.Reporte de perdida de la afiliacion por causa grave activo
        //2.Reporte de perdida de la afiliacion por causa grave terminado
        $this->sat->estado = '2';
    }

    public function reintegro_independiente()
    {
        $this->valida_empresa();
        $this->createSat20(2);
        $this->sat = new Sat03;
        $this->campos           = $this->sat->getCamposSat03();
        $this->sat->autmandat   = 'SI';
        $this->sat->autenvnot   = 'SI';
        $this->sat->numtrasat   = '0';
        $this->sat->sersat      = "0";
        $this->sat->pazsal      = 'SI';
        $this->sat->siafissfant = 'SI';
        $this->sat->numtraccf   = $this->numtraccf;
        $this->sat->tipdocemp   = $this->get_coddoc($this->subsi02->coddoc);
        $this->sat->email       = $this->subsi02->email;
        $this->sat->tipper      = $this->subsi02->tipper;
        $this->sat->numdocemp   = $this->subsi02->nit;
        $this->sat->fecafi      = $this->subsi02->fecafi;
        $this->sat->fecsol      = (strtotime($this->fecha) > strtotime($this->subsi02->fecafi)) ? $this->subsi02->fecafi : $this->fecha;
        $this->sat->direccion   = $this->subsi02->direccion;
        $this->sat->telefono    = (!empty($this->subsi02->telr) && !is_null($this->subsi02->telr)) ? $this->subsi02->telr : $this->subsi02->telefono;
        $this->sat->coddep      = substr($this->subsi02->codzon, 0, 2);
        $this->sat->codmun      = substr($this->subsi02->codzon, 2, 3);
        $this->sat->tipcon      = 'I';

        $cod_caja_id = $this->subsi02->codcaj;
        if (empty($cod_caja_id) || is_null($cod_caja_id) || $cod_caja_id == '00') {
            $s1 = Subsi01::find()->first();
            $codcaj = 'CCF' . $s1->codcaj;
        } else {
            $codcaj = 'CCF' . $this->subsi02->codcaj;
        }
        $this->sat->codcaj = $codcaj;
        $this->empresaNatural();
    }

    public function valida_empresa()
    {
        if (!$this->subsi02) throw new DebugException("Error la empresa no está registrada en el sistema de sisuweb", 1);
    }
}
