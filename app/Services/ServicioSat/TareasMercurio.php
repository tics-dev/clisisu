<?php

namespace App\Services\ServicioSat;

use Illuminate\Database\QueryException;
use App\Exceptions\DebugException;
use GrahamCampbell\ResultType\Success;
use Illuminate\Support\Facades\DB;
use App\Console\Servicios\General\General;
use App\Models\Gener08;
use App\Models\Mercurio30;
use App\Models\Mercurio07;
use App\Models\Mercurio19;
use App\Models\Gener18;
use App\Models\RecepcionSat;
use App\Models\Sat16;

class TareasMercurio
{
	protected $console;
	public function __construct($console)
	{
		$this->console = $console;
	}

	public function crear_clave()
	{
		$pass = "";
		$seed = str_split('abcdefghijklmnopqrstuvwxyz1234567890');
		shuffle($seed);
		foreach (array_rand($seed, 5) as $k) $pass .= $seed[$k];
		$mclave = '';
		for ($i = 0; $i < strlen($pass); $i++) {
			if ($i % 2 != 0) {
				$x = 6;
			} else {
				$x = -4;
			}
			$mclave .= chr(ord(substr($pass, $i, 1)) + $x + 5);
		}
		return array(md5($mclave), $pass);
	}

	public function buscar_tipo_documento($tipodocumento)
	{
		//usando el codigo RUA
		return Gener18::where("codrua", $tipodocumento)
			->get()
			->first();
	}

	public function buscar_siglas($texto)
	{
		$exp = explode(' ', $texto);
		$ntexto = '';
		if (count($exp) > 1) {
			foreach ($exp as $item) {
				if (strlen(trim($item)) == 0) continue;
				$ntexto .= substr($item, 0, 1);
			}
		} else {
			$ntexto .= substr($texto, 0, 1);
		}
		return $ntexto;
	}

	public function alista_ciudad($departamento, $ciudad)
	{
		return $departamento . '' . ((strlen($ciudad) < 3) ? str_pad($ciudad, 3, '0', STR_PAD_LEFT) : $ciudad);
	}

	public function mercurio_afiliacion_empresa_modelo_sat($detalle, $numero_radicado)
	{
		$tipo = 'P';
		$buscar_documento = $this->buscar_tipo_documento($detalle->TipoDocumentoEmpleador);
		$coddoc = ($buscar_documento) ? $buscar_documento->coddoc : 1;
		$detalle_coddoc = ($buscar_documento) ? $buscar_documento->detdoc : 'NIT';

		$m07 = Mercurio07::where('documento', $detalle->NumeroDocumentoEmpleador)
			->where('coddoc', $coddoc)
			->where('tipo', $tipo)
			->get()
			->count();

		if (!$m07) {
			$mercurio07 = new Mercurio07;
			$data = [
				"tipo"      => $tipo,
				"coddoc"    => $coddoc,
				"documento" => $detalle->NumeroDocumentoEmpleador,
				"nombre"    => $detalle->NombreRazonSocial,
				"email"     => $detalle->CorreoElectronicoContacto,
				"clave"     => $detalle->hash,
				"feccla"    => date('Y-m-d'),
				"autoriza"  => 'S',
				"codciu"    => '18',
				"fecreg"    => date('Y-m-d'),
				"estado"    => 'A'
			];
			General::setModel($mercurio07, $data);
			$validation = $mercurio07->isValid($mercurio07->getRules());
			if ($validation === TRUE) {
				$mercurio07->save();
			} else {
				DebugException::add("valid", $validation);
				throw new DebugException("Error validation mercurio07", 555);
			}
		} else {
			//ya existe asi que se actualiza la clave
			Mercurio07::where('documento', $detalle->NumeroDocumentoEmpleador)
				->where('coddoc', $coddoc)
				->where('tipo', $tipo)
				->update(['clave' => $detalle->hash]);
		}

		General::createLog(
			$detalle->NumeroDocumentoEmpleador,
			$coddoc,
			"consulta_detalle",
			"Inicializa el proceso de consulta detalle SAT, Por medio de la interfaz de linea de comando.",
			$tipo
		);

		$m19 = Mercurio19::where('documento', $detalle->NumeroDocumentoEmpleador)
			->where('tipo', $tipo)
			->get()
			->count();

		if (!$m19) {
			$me19 = new Mercurio19;
			$me19->tipo = $tipo;
			$me19->coddoc = $coddoc;
			$me19->documento = $detalle->NumeroDocumentoEmpleador;
			$me19->codigo = '1';
			$me19->respuesta = 'SAT';
			$me19->save();

			$me19 = new Mercurio19;
			$me19->tipo = $tipo;
			$me19->coddoc = $coddoc;
			$me19->documento = $detalle->NumeroDocumentoEmpleador;
			$me19->codigo = '2';
			$me19->respuesta = 'SAT';
			$me19->save();
		}

		$logger = General::$logger;
		if ($detalle->TipoPersona == 'N') {
			$repleg =
				$detalle->PrimerNombreEmpleador . ' ' .
				$detalle->SegundoNombreEmpleador . ' ' .
				$detalle->PrimerApellidoEmpleador . ' ' .
				$detalle->SegundoApellidoEmpleador;
		} else {
			$repleg =
				$detalle->PrimerNombreRepresentanteLegal . ' ' .
				$detalle->SegundoNombreRepresentanteLegal . ' ' .
				$detalle->PrimerApellidoRepresentanteLegal . ' ' .
				$detalle->SegundoApellidoRepresentanteLegal;
		}

		$direccion =  (strlen($detalle->DireccionContacto) == 0) ? "SAT informa campo pendiente" : $detalle->DireccionContacto;

		$buscar_documento = $this->buscar_tipo_documento($detalle->TipoDocumentoRepresentanteLegal);
		$coddocrepleg = ($buscar_documento) ? $buscar_documento->coddoc : '1';

		$data = [
			"log" => $logger->log,
			"nit" => $detalle->NumeroDocumentoEmpleador,
			"tipdoc" => $coddoc,
			"razsoc" => $detalle->NombreRazonSocial,
			"priape" => $detalle->PrimerApellidoEmpleador,
			"segape" => $detalle->SegundoApellidoEmpleador,
			"prinom" => $detalle->PrimerNombreEmpleador,
			"segnom" => $detalle->SegundoNombreEmpleador,
			"sigla"  => strtoupper($this->buscar_siglas($detalle->NombreRazonSocial)),
			"digver" => '',
			"tipper" => $detalle->TipoPersona,
			"calemp" => 'E',
			"cedrep" => $detalle->NumeroDocumentoRepresentanteLegal,
			"repleg" => $repleg,
			"direccion" => $direccion,
			"codciu" => $this->alista_ciudad($detalle->Departamento, $detalle->Municipio),
			"codzon" => $this->alista_ciudad($detalle->Departamento, $detalle->Municipio),
			"telefono" => $detalle->TelefonoContacto,
			"celular" => $detalle->TelefonoContacto,
			"fax" => '',
			"email" => $detalle->CorreoElectronicoContacto,
			"codact" => '0000',
			"fecini" => ($detalle->FechaEfectivaAfiliacion != "") ? $detalle->FechaEfectivaAfiliacion : NULL,
			"tottra" => '0',
			"valnom" => '0',
			"tipsoc" => '00',
			"estado" => 'D', //devuelto por defecto
			"codest" => '03',
			"motivo" => '',
			"fecest" => date('Y-m-d'),
			"usuario" => General::$usuario->usuario,
			"dirpri" => $direccion,
			"ciupri" => $this->alista_ciudad($detalle->Departamento, $detalle->Municipio),
			"telpri" => $detalle->TelefonoContacto,
			"celpri" => $detalle->TelefonoContacto,
			"emailpri" => $detalle->CorreoElectronicoContacto,
			"tipo" => $tipo,
			"coddoc" => $coddoc,
			"documento" => $detalle->NumeroDocumentoEmpleador,
			"tipemp" => 'E',
			"matmer" => $detalle->NumeroMatriculaMercantil,
			"coddocrepleg" => $coddocrepleg,
			"priaperepleg" => $detalle->PrimerNombreRepresentanteLegal,
			"segaperepleg" => $detalle->SegundoNombreRepresentanteLegal,
			"prinomrepleg" => $detalle->PrimerApellidoRepresentanteLegal,
			"segnomrepleg" => $detalle->SegundoApellidoRepresentanteLegal,
			"codcaj" => ($detalle->ManifestacionNoAfiliacionPrevia == 'SI') ? '13' : '',
			"fecha_aprobacion_sat" => $detalle->FechaEfectivaAfiliacion,
			"documento_representante_sat" => $detalle->NumeroDocumentoEmpleador,
			"numero_transaccion" => $numero_radicado
		];

		$mercurio30 = Mercurio30::where('nit', $detalle->NumeroDocumentoEmpleador)
			->get()
			->first();

		if (!$mercurio30) {
			$mercurio30 = new Mercurio30;
		}
		General::setModel($mercurio30, $data);

		$validation = $mercurio30->isValid($mercurio30->getRules());
		if ($validation === TRUE) {
			$mercurio30->save();
			Sat16::where("numero_radicado", $numero_radicado)->update(["estado" => 'C']);
		} else {
			DebugException::add("valid", $validation);
			throw new DebugException("Error validation mercurio30", 555);
		}
		$data = $mercurio30->toArray();
		$data['clave_ingreso'] = $detalle->clave;
		$data['tipo_documento'] = $detalle_coddoc;
		return $data;
	}

	public function guardar_recepcion_sat($data, $numero)
	{
		$recepcion = new RecepcionSat;
		$recepcion->contenido = $data;
		$recepcion->numero_transaccion = $numero;
		$recepcion->fecha = date('Y-m-d H:i:s');
		$recepcion->save();
	}
}
