<?php

namespace App\Services\ServicioSat;

interface WsSatInterface
{

    public function send(array $data);

    public function sendAlternativo(array $data);
}
