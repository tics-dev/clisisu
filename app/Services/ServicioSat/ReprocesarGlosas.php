<?php

namespace App\Services\ServicioSat;

use App\Exceptions\DebugException;
use App\Libraries\MyParams;
use App\Models\Sat02;
use App\Models\Sat03;
use App\Models\Sat06;
use App\Models\Sat08;
use App\Models\Sat09;
use App\Models\Sat10;
use App\Models\Sat13;
use App\Models\Sat15;

class ReprocesarGlosas
{
    private $modelo;
    private $tabla;

    public function __construct(MyParams $argv)
    {
        $this->modelo = $argv->getParam('post');
        $this->tabla = $argv->getParam('tabla');
    }

    public function procesar(Tareas $Tarea)
    {
        switch ($this->tabla) {
            case 'sat02':
                $sat = Sat02::where("numtraccf", $this->modelo->numtraccf)
                    ->get()
                    ->first();
                $campos = $sat->getCamposSat02();
                break;
            case 'sat03':
                $sat = Sat03::where("numtraccf", $this->modelo->numtraccf)
                    ->get()
                    ->first();
                $campos = $sat->getCamposSat03();
                break;
            case 'sat06':
                $sat = Sat06::where("numtraccf", $this->modelo->numtraccf)
                    ->get()
                    ->first();
                $campos = $sat->getCamposSat06();
                break;
            case 'sat08':
                $sat = Sat08::where("numtraccf", $this->modelo->numtraccf)
                    ->get()
                    ->first();
                $campos = $sat->getCamposSat08();
                break;
            case 'sat09':
                $sat = Sat09::where("numtraccf", $this->modelo->numtraccf)
                    ->get()
                    ->first();
                $campos = $sat->getCamposSat09();
                break;
            case 'sat10':
                $sat = Sat10::where("numtraccf", $this->modelo->numtraccf)
                    ->get()
                    ->first();
                $campos = $sat->getCamposSat10();
                break;
            case 'sat13':
                $sat = Sat13::where("numtraccf", $this->modelo->numtraccf)
                    ->get()
                    ->first();
                $campos = $sat->getCamposSat13();
            case 'sat15':
                $sat = Sat15::where("numtraccf", $this->modelo->numtraccf)
                    ->get()
                    ->first();
                $campos = $sat->getCamposSat15();
            default:
                break;
        }

        foreach ($this->modelo as $row => $valor) {
            $sat->$row = $valor;
        }

        if (!$sat->save()) {
            throw new DebugException("Error al guardar el modelo {$sat->getTable()}", 501);
        }

        $data = array();
        $attr =  $sat->makeVisible('attribute')->toArray();

        foreach ($campos as $key => $value) {
            $data[$value] = (isset($attr["$key"])) ? $attr["$key"] : "";
        }

        if ($this->tabla == 'sat15') {
            $data['NumeroRadicado'] = $sat->numtraccf;
        } else {
            $data['NumeroRadicadoSolicitud'] = $sat->numtraccf;
        }

        $Tarea->principal($data, $this->tabla);
        $resultado = $Tarea->getResultado();

        if (isset($resultado['codigo'])) {
            $sat->codigo = $resultado['codigo'];
            $sat->mensaje = $resultado['mensaje'];
            $sat->save();
        }

        $resultado['success'] = true;
        $resultado['data'] = $sat->makeVisible('attribute')->toArray();
        return $resultado;
    }
}
