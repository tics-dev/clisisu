<?php

namespace App\Services\ServicioSat\TokenGenerador;

use App\Models\Sat01;
use App\Models\Sat29;

interface TokenGeneratorInterface
{

    public function generateToken(Sat01 $proveedor, Sat29 $servicio);

    public function getToken();

    public function setAtenticar($status);
}
