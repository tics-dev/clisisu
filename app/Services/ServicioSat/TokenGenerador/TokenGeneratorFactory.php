<?php

namespace App\Services\ServicioSat\TokenGenerador;

class TokenGeneratorFactory
{
    public static function create()
    {
        return new TokenGenerator();
    }
}
