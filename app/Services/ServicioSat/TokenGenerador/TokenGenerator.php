<?php

namespace App\Services\ServicioSat\TokenGenerador;

use App\Exceptions\WsException;
use App\Models\Sat01;
use App\Models\Sat29;

class TokenGenerator implements TokenGeneratorInterface
{

    public static $host;
    private $grantType;
    private $clientId;
    private $nit;
    private $clave;
    private $tokenType;
    private $statusCode;
    private $autenticar;
    private $token;
    private $nomsersat;

    public function generateToken(Sat01 $proveedor, Sat29 $servicio)
    {
        self::$host = trim($proveedor->path);
        $this->nit = trim($proveedor->nit);
        $this->clave = trim($proveedor->password);
        $this->grantType = trim($proveedor->grant_type);
        $this->clientId = trim($servicio->client_id);
        $this->nomsersat = trim($servicio->nomsersat);
        $this->access();
    }

    private function access()
    {
        $ch = curl_init();
        $cadena_data = http_build_query([
            'grant_type' => $this->grantType,
            'client_id'  => $this->clientId,
            'username'   => $this->nit,
            'password'   => $this->clave
        ]);
        curl_setopt($ch, CURLOPT_URL, self::$host . "token");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $cadena_data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->getHeader());
        $out = curl_exec($ch);

        if ($out === false) {
            $error = curl_error($ch);
            curl_close($ch);
            throw WsException::for($this->nomsersat, $error);
        } else {
            $this->statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            $response = json_decode($out, true);
            $this->token = "{$response['token_type']} {$response['access_token']}";
            $this->tokenType = $response['token_type'];
        }
    }

    public function getHeader()
    {
        if ($this->autenticar == True) {
            return array('Content-Type: application/x-www-form-urlencoded');
        } else {
            return array("Authorization: {$this->token}", "Content-Type: application/json");
        }
    }

    public function setAtenticar($autenticar)
    {
        $this->autenticar = $autenticar;
    }

    public function getAutenticar()
    {
        return $this->autenticar;
    }

    public function getTokenType()
    {
        return $this->tokenType;
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function getToken()
    {
        return $this->token;
    }
}
