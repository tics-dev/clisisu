<?php

namespace App\Services\ServicioSat;

interface ProcesarIndependientesInterface
{
    public function nueva_independiente();
    public function reintegro_independiente();
    public function retiro_independiente();
    public function causa_grave_independiente($detalle);
}
