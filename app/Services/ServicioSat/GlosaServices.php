<?php

namespace App\Services\ServicioSat;

use App\Exceptions\DebugException;
use App\Libraries\MyParams;
use App\Models\Sat02;
use App\Models\Sat03;
use App\Models\Sat06;
use App\Models\Sat08;
use App\Models\Sat09;
use App\Models\Sat10;
use App\Models\Sat13;
use App\Models\Sat15;
use App\Models\Sat20;

class GlosaServices
{
    private $numtraccf;
    private $tabla;
    private $numdocemp;
    private $numdoctra;

    public function __construct(MyParams $argv)
    {
        $this->numtraccf = $argv->getParam('numtraccf');
        $this->tabla = $argv->getParam('tabla');
        $this->numdocemp = $argv->getParam('numdocemp');
        $this->numdoctra = $argv->getParam('numdoctra');
    }

    public function glozaBorrar()
    {
        switch ($this->tabla) {
            case 'sat02':
                Sat02::where("numtraccf", $this->numtraccf)->delete();
                break;
            case 'sat03':
                Sat03::where("numtraccf", $this->numtraccf)->delete();
                break;
            case 'sat06':
                Sat06::where("numtraccf", $this->numtraccf)->delete();
                break;
            case 'sat08':
                Sat08::where("numtraccf", $this->numtraccf)->delete();
                break;
            case 'sat09':
                Sat09::where("numtraccf", $this->numtraccf)->delete();
                break;
            case 'sat10':
                Sat10::where("numtraccf", $this->numtraccf)->delete();
                break;
            case 'sat13':
                Sat13::where("numtraccf", $this->numtraccf)->delete();
                break;
            case 'sat15':
                Sat15::where("numtraccf", $this->numtraccf)->delete();
                break;
            default:
                break;
        }

        $res = Sat20::where("numtraccf", $this->numtraccf)->delete();
        $resultado = [
            "success" => ($res != false),
            "data" => [],
            "mensaje" => ($res) ? "El borrado se ha completado con éxito. {$this->tabla} {$this->numtraccf}" : "No se ha podido borrar el registro. {$this->tabla} {$this->numtraccf}"
        ];
        return $resultado;
    }

    public function actualizaPersistencia()
    {
        $numtraccf = $this->numtraccf;
        $persistencias = [];

        switch ($this->tabla) {
            case 'sat02':
                $numdocemp = $this->numdocemp;
                $solicitudes = Sat02::where("numdocemp", $numdocemp)->get();
                break;
            case 'sat03':
                $numdocemp = $this->numdocemp;
                $solicitudes = Sat03::where("numdocemp", $numdocemp)->get();
                break;
            case 'sat06':
                $numdocemp = $this->numdocemp;
                $solicitudes = Sat06::where("numdocemp", $numdocemp)->get();
                break;
            case 'sat08':
                $numdocemp = $this->numdocemp;
                $solicitudes = Sat08::where("numdocemp", $numdocemp)->get();
                break;
            case 'sat09':
                $numdoctra = $this->numdoctra;
                $solicitudes = Sat09::where("numdoctra", $numdoctra)->get();
                break;
            case 'sat10':
                $numdoctra = $this->numdoctra;
                $solicitudes = Sat10::where("numdoctra", $numdoctra)->get();
                break;
            case 'sat13':
                $numdoctra = $this->numdoctra;
                $solicitudes = Sat13::where("numdoctra", $numdoctra)->get();
                break;
            case 'sat15':
                $numdocemp = $this->numdocemp;
                $solicitudes = Sat15::where("numdocemp", $numdocemp)->get();
                break;
            default:
                break;
        }

        foreach ($solicitudes as $sat) {

            if ($sat->numtraccf != $numtraccf) {

                $solicitud = Sat20::where("tiptra", '1')
                    ->where("numtraccf", $sat->numtraccf)
                    ->get()
                    ->first();

                if ($solicitud) {
                    $solicitud->persiste = "1";
                    $solicitud->save();
                    $persistencias[] = $sat->numtraccf;
                }
            }
        }

        $resultado = [
            "success" => true,
            "data" => $persistencias,
            "mensaje" => "Se ha completado proceso de persistencia."
        ];
        return $resultado;
    }
}
