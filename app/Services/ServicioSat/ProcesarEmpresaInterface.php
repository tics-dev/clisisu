<?php

namespace App\Services\ServicioSat;

interface ProcesarEmpresaInterface
{
    public function nueva();
    public function retiro();
    public function reintegro();
    public function causa_grave($detalle);
    public function empresaNatural();
    public function empresaJuridica();
    public function prepara_data_sat();
    public function valida_empresa();
}
