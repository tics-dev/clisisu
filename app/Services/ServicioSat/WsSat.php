<?php

namespace App\Services\ServicioSat;

use App\Services\ServicioSat\TokenGenerador\TokenGeneratorFactory;
use App\Exceptions\DebugException;
use App\Exceptions\GlozaException;
use App\Exceptions\WsException;
use App\Models\Sat01;
use App\Models\Sat29;
use App\Models\Sat20;
use App\Models\Sat16;

class WsSat implements WsSatInterface
{
    protected $tokenGenerator;
    protected $codapl = 'SA';
    protected $sat01;
    protected $sat29;
    protected $tabla;
    protected $detalle_sercicio;
    protected $servicio_sat;

    private $token;
    private $statusCode;
    private $token_type;

    public static $host;
    public static $nit;
    public static $clave;
    public static $grant_type;
    public static $client_id;
    public $resultado = array();

    public function __construct()
    {
    }

    public function principal($data, $tabla)
    {
        $this->sat01  = Sat01::where("codapl", $this->codapl)->get()->first();
        $this->sat29  = Sat29::where("referencia", $tabla)->where('codapl', $this->codapl)->get()->first();

        if (!$this->sat29) throw new GlozaException("El servicio no esta configurado tabla {$tabla}", 404);
        if ($this->sat29->estado == 'I') throw new GlozaException("El servicio está inactivo. tabla {$tabla} ", 404);

        $this->tokenGenerator = TokenGeneratorFactory::create();
        $this->tokenGenerator->setAtenticar(True);
        $this->tokenGenerator->generateToken($this->sat01, $this->sat29);
        $this->token = $this->tokenGenerator->getToken();
        $this->tabla = $tabla;
        self::$host = $this->sat01->path;
        $this->servicio_sat = $this->sat29->nomsersat;
        $this->send($data);
    }

    function set_code_aplicacion($code)
    {
        $this->codapl = $code;
    }

    public function get_status_code()
    {
        return $this->tokenGenerator->getStatusCode();
    }

    public function get_type_token()
    {
        return $this->tokenGenerator->getTokenType();
    }

    public function get_detalle_servicio()
    {
        return $this->sat29->detser;
    }

    public function get_servicio_sat()
    {
        return $this->servicio_sat;
    }

    public function send($data)
    {
        $sat20 = Sat20::where("numtraccf", @$data['NumeroRadicadoSolicitud'])->get()->first();
        if (!$sat20) {
            $sat20 = Sat20::where("numtraccf", @$data['NumeroRadicado'])->get()->first();
        }
        $sat20->estado = 'E';
        $sat20->nota = 'Enviado al SAT';
        if (!$sat20->save()) throw new DebugException("Error al actualizar sat20", 501);

        $ch = curl_init();
        $this->tokenGenerator->setAtenticar(False);
        $headers  = $this->tokenGenerator->getHeader();
        curl_setopt($ch, CURLOPT_URL, self::$host . $this->servicio_sat);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);

        $this->statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($result === false) {
            $error = curl_error($ch);
            curl_close($ch);
            throw new DebugException($error, 404);
        } else {
            curl_close($ch);
            $this->resultado = (array) json_decode($result, true);

            if (isset($this->resultado['codigo'])) {
                switch ($this->resultado['codigo']) {
                    case 200:
                    case 'GN02':
                    case 'GN04':
                    case 'GN07':
                    case 'GN15':
                    case 'GN33':
                        $sat20->estado = 'O';
                        $sat20->codest = 'P';
                        break;
                    default:
                        if (substr($this->resultado['codigo'], 0, 1) == 'A') {
                            $sat20->estado = 'U';
                            $sat20->codest = 'P';
                        } else {
                            $sat20->estado = 'G';
                            $sat20->codest = 'A';
                        }
                        break;
                }
                $sat20->nota = $this->resultado['mensaje'];
            } else {
                $sat20->estado = 'X';
                if (isset($this->resultado['Message'])) {
                    $this->resultado['mensaje'] = $this->resultado['Message'];
                    $this->resultado['codigo'] = 404;
                    $this->resultado['data'] = $data;
                    $sat20->nota = $this->resultado['Message'];
                } else {
                    $this->resultado['codigo'] = 400;
                    $this->resultado['mensaje'] = "Error de conexión al servicio SAT";
                    $sat20->nota = "Error no identificado:";
                }
                $this->resultado['resultado'] = "";
            }
            $this->resultado['estado'] = $sat20->estado;
            if (!$sat20->save()) {
                throw new DebugException("Error al actualizar sat20", 501);
            }
        }
    }

    public function set_enviroment($ambiente)
    {
        if ($ambiente == '1') {
            $this->set_code_aplicacion('SA');  //producción
        } else {
            $this->set_code_aplicacion('SU');  //pruebas
        }
    }

    public function sendAlternativo(array $data)
    {
        $ch = curl_init();
        $this->tokenGenerator->setAtenticar(False);
        curl_setopt($ch, CURLOPT_URL, self::$host . $this->servicio_sat);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->tokenGenerator->getHeader());
        $result = curl_exec($ch);
        $this->statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($result === false) {
            $error = curl_error($ch);
            curl_close($ch);
            throw new DebugException($error, 404);
        } else {
            curl_close($ch);
            return $result;
        }
    }

    public function alternativo($data, $tabla, $numero_radicado)
    {
        $this->sat01  = Sat01::where("codapl", $this->codapl)->get()->first();
        $this->sat29  = Sat29::where("referencia", $tabla)->where('codapl', $this->codapl)->get()->first();

        if (!$this->sat29) throw new GlozaException("El servicio no esta configurado tabla {$this->tabla}", 404);
        if ($this->sat29->estado == 'I') throw new GlozaException("El servicio está inactivo. tabla {$this->tabla} ", 404);
        $this->servicio_sat = $this->sat29->nomsersat;

        $this->tokenGenerator = TokenGeneratorFactory::create();
        $this->tokenGenerator->setAtenticar(True);
        $this->tokenGenerator->generateToken($this->sat01, $this->sat29);
        $this->token = $this->tokenGenerator->getToken();
        $this->tabla = $tabla;
        $resultado = $this->sendAlternativo($data);

        Sat16::where("numero_radicado", $numero_radicado)->update(["estado" => 'P', 'fecha' => date('Y-m-d H:i:s')]);
        if (preg_match('/^\{(.*)\:(.*)\}$/', trim($resultado))) {
            return $resultado;
        } else {
            Sat16::where("numero_radicado", $numero_radicado)->update(["estado" => 'X', 'fecha' => date('Y-m-d H:i:s')]);
            throw new DebugException("Error, la estructura en recepción no es un JSON {$resultado}", 504);
        }
    }
}
