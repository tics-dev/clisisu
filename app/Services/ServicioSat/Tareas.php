<?php

namespace App\Services\ServicioSat;

use Illuminate\Support\Facades\DB;
use App\Services\ServicioSat\WsSat;
use App\Exceptions\DebugException;
use App\Models\Sat20;
use App\Models\Sat17;
use App\Models\Gener18;
use App\Models\Sat29;

class Tareas extends WsSat
{
    private $debugger = false;

    public function __construct()
    {
    }

    public function set_nombre($repleg)
    {
        $exp = explode(" ", trim($repleg));
        switch (count($exp)) {
            case 6:
            case 7:
            case 8:
                return [
                    $exp[0],
                    $exp[1],
                    $exp[2],
                    $exp[3] . ' ' . $exp[4] . ' ' . $exp[5]
                ];
                break;
            case 5:
                return [
                    $exp[0],
                    $exp[1],
                    $exp[2],
                    $exp[3] . ' ' . $exp[4]
                ];
                break;
            case 4:
                return [
                    $exp[0],
                    $exp[1],
                    $exp[2],
                    $exp[3]
                ];
                break;
            case 3:
                return [
                    $exp[0],
                    '',
                    $exp[1] . ' ' . $exp[2],
                    ''
                ];
                break;
            case 2:
                return [
                    $exp[0],
                    '',
                    $exp[1],
                    ''
                ];
                break;
            case 1:
                return [
                    $exp[0],
                    '',
                    $exp[0],
                    ''
                ];
                break;
            default:
                break;
        }
    }

    public function respuesta_afiliacion_ccf_primera_vez($jd, $usuario)
    {
        try {
            DB::beginTransaction();
            $numtraccf = Sat20::max("numtraccf") + 1;
            $coddoc = Gener18::where('coddoc', $jd->tipdoc)->get()->first();
            $tipo_documento = substr($coddoc->codrua, 0, 2);

            $sat20 = new Sat20();
            $sat20->numtraccf = $numtraccf;
            $sat20->fecha     = date('Y-m-d');
            $sat20->hora      = date('H:i:s');
            $sat20->usuario   = $usuario;
            $sat20->tiptra    = '16';
            $sat20->gestion   = 'C';
            $sat20->estado    = 'P';
            $sat20->persiste  = '0';
            $sat20->save();

            $sat = new Sat17();
            $sat->numtraccf = $numtraccf;
            $sat->numtrasat = $jd->numero_transaccion;
            $sat->tipo_documento_empleador = $tipo_documento;
            $sat->numero_documento_empleador = $jd->nit;
            $sat->serial_sat = "0";
            $sat->resultado_tramite = $jd->resultado_tramite;
            $sat->fecha_efectiva_afiliacion = $jd->fecha_efectiva_afiliacion;
            $sat->motivo_rechazo = ($jd->motivo_rechazo) ? $jd->motivo_rechazo : "";
            $sat->save();
            $campos = $sat->getCamposSat17();

            $data_sat = array();
            $attr =  $sat->makeVisible('attribute')->toArray();
            foreach ($campos as $key => $value) {
                $data_sat[$value] = (isset($attr["$key"])) ? $attr["$key"] : '';
            }
            $data_sat['NumeroRadicadoSolicitud'] = $numtraccf;
            if ($this->debugger == TRUE) {
                $this->resultado['codigo'] = 200;
                $this->resultado['mensaje'] = "Ok la respuesta a notificación";
            } else {
                $this->principal($data_sat, $sat->getTable());
            }

            if (($this->resultado['codigo'] == 200)) {
                $this->resultado['success'] = true;
                Sat17::where("numtraccf", $numtraccf)
                    ->where("numtrasat", $jd->numero_transaccion)
                    ->update(['estado' => 'F']);
            } else {
                $this->resultado['success'] = false;
                Sat17::where("numtraccf", $numtraccf)
                    ->where("numtrasat", $jd->numero_transaccion)
                    ->update(['estado' => 'X']);
            }

            $this->resultado['sat'] =  $data_sat;
            DB::commit();
        } catch (\Exception $th) {
            DB::rollBack();
            throw new DebugException($th->getMessage(), 1);
        }
    }

    public static function isEnabled($servicio, $codapl = 'SA')
    {
        $count = Sat29::where('referencia', $servicio)
            ->where('estado', 'A')
            ->where('codapl', $codapl)
            ->get()
            ->count();
        return ($count == 0) ? false : true;
    }

    public function getResultado()
    {
        return $this->resultado;
    }
}
