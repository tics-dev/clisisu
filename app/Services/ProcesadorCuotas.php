<?php

namespace  App\Services;

use App\Console\Servicios\GiroSubsidio\GiroActual;
use App\Exceptions\DebugException;
use App\Models\Subsi02;
use App\Models\Subsi12;
use App\Services\ValidationGiro\CalculadoraCuotas;
use App\Services\ValidationGiro\Handlers\ValidacionAportesSeguridadSocial;
use App\Services\ValidationGiro\Handlers\ValidacionCertificadosEPS;
use App\Services\ValidationGiro\Handlers\ValidacionEmpresaPrincipal;
use App\Services\ValidationGiro\Handlers\ValidacionSalarioMaximo;
use App\Services\ValidationGiro\Handlers\ValidationChain;
use App\Services\ValidationGiro\ServiceGiro;

class ProcesadorCuotas
{

    private $validationChain;

    public function __construct()
    {
        $this->validationChain = new ValidationChain();
        $this->validationChain->addValidator(new ValidacionEmpresaPrincipal());
        $this->validationChain->addValidator(new ValidacionAportesSeguridadSocial());
        $this->validationChain->addValidator(new ValidacionCertificadosEPS());
        $this->validationChain->addValidator(new ValidacionSalarioMaximo());
    }

    public function procesarCuotas($trabajadores, $empresa, $periodo)
    {
        foreach ($trabajadores as $trabajador) {

            if (isset(CalculadoraCuotas::$trabajadoresCuota[$trabajador->cedtra])) continue;
            $serviceGiro = new ServiceGiro($empresa, $trabajador);
            $responsables =  $serviceGiro->getResponsables();
            foreach ($responsables as $responsable) {
                try {
                    $this->validationChain->handleValidation($responsable);
                    $cuotas = new CalculadoraCuotas();
                    $cuotas->calcularCuotas($responsable, $periodo);
                    $responsable->crearGiro();
                } catch (DebugException $e) {
                    // Si ocurre un error en la validación, se puede registrar o manejar de alguna otra forma.
                }
            }
            CalculadoraCuotas::$trabajadoresCuota[$trabajador->cedtra] = true;
            // Guardar el montoTotalCuotas en la base de datos o realizar otras operaciones necesarias.
        }
    }
}
