<?php

namespace App\Services;

use App\Models\JwtIngreso;

class JwtService
{
    private $jwtIngreso;

    public function __construct()
    {
        $this->jwtIngreso = new JwtIngreso;
    }

    public function getJwtIngreso()
    {
        return $this->jwtIngreso->findAll();
    }

    public function getJwtIngresoById($id)
    {
        return $this->jwtIngreso->find($id);
    }

    public function getJwtIngresoByToken($token)
    {
        return JwtIngreso::where('jwtoken', $token)->get()->first();
    }

    public function createJwtIngreso($data)
    {
        return $this->jwtIngreso->insert($data);
    }

    public function updateJwtIngreso($id, $data)
    {
        return $this->jwtIngreso->find($id)->update($data);
    }

    public function deleteJwtIngreso($id)
    {
        return $this->jwtIngreso->delete($id);
    }
}
