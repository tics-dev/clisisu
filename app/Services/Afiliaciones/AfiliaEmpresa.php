<?php

namespace  App\Services\Afiliaciones;

ini_set('memory_limit', '1300M');

use App\Console\Servicios\ServicioSat\Tareas;
use Illuminate\Database\QueryException;
use App\Exceptions\DebugException;
use App\Models\Subsi15;
use App\Models\Subsi168;
use App\Models\Subsi48;
use App\Services\AfiliacionSisu\AfiliaSisuEmpresa;

class AfiliaEmpresa implements InAfiliation
{

    protected $resultado;
    protected $afilia;

    public function __construct()
    {
        $this->afilia = new AfiliaSisuEmpresa();
    }

    public function procesar($modelo)
    {
        try {
            $modelo = (object) $this->afilia->preparaData($modelo);
            $this->afilia->procesar($modelo);

            $result = $this->afilia->getResultado();
            $numero_transaccion = $modelo->numero_transaccion;

            $sat = false;
            if (empty($numero_transaccion)) {

                list($afiliacion_nueva) = $this->afilia->detalleGestion();

                $modelo->numdocemp = $modelo->nit;
                $modelo->coddep = substr($modelo->codciu, 0, 2);
                $modelo->codmun = substr($modelo->codciu, 2, 5);
                $modelo->codcaj = (!$modelo->codcaj) ? '13' : $modelo->codcaj;
                $modelo->prefijo_codcaj = "CCF{$modelo->codcaj}";

                if (env('APP_MODE') == 'production') {
                    $tareas = new Tareas();
                    if ($afiliacion_nueva) {
                        if (Tareas::isEnabled('sat02', 'SA') == True) {
                            $sat =  $tareas->empresa_nueva_afiliacion($modelo);
                        }
                    } else {
                        if (Tareas::isEnabled('sat03', 'SA') == True) {
                            $sat = $tareas->empresa_segunda_afiliacion($modelo);
                        }
                    }
                }
            }

            $result['sat'] = $sat;
            $this->resultado = $result;
        } catch (QueryException $ex) {
            throw new DebugException($ex->getMessage() . ' ' . $ex->getLine() . ' ' . basename($ex->getFile()), 1);
        }
    }

    public function listar($nit, $estados)
    {
        return [];
    }

    public function getResultado()
    {
        return $this->resultado;
    }

    public function actualizarDatos($modelo)
    {
        $cantidad = Subsi48::where('nit', $modelo->nit)
            ->whereIn('estado', ['A', 'D', 'S'])
            ->get()
            ->count();

        $empresa = $this->afilia->buscarEmpresa($modelo->nit);
        $params = clone $empresa;
        $params->cedrep = $modelo->cedrep;
        $params->telt = $modelo->celpri;
        $params->telr = $modelo->celular;
        $params->ciupri = $modelo->ciupri;
        $params->codciu = $modelo->codciu; //ciudad notificaciones
        $params->coddocrepleg = $modelo->coddocrepleg;
        $params->direccion = $modelo->direccion;  //direccion notificaciones
        $params->dirpri = $modelo->dirpri;  //direccion comercial
        $params->email = $modelo->email;    //email notificaciones
        $params->mailr = $modelo->emailpri; //email comercial
        $params->matmer = $modelo->matmer;
        $params->priape = $modelo->priape;
        $params->prinom = $modelo->prinom;
        $params->razsoc = $modelo->razsoc;
        $params->repleg = $modelo->priape . ' ' . $modelo->segape . ' ' . $modelo->prinom . ' ' . $modelo->segnom;
        $params->segape = $modelo->segape;
        $params->segnom = $modelo->segnom;
        $params->sigla = $modelo->sigla;
        $params->telefono = $modelo->telefono; //notificaciones
        $params->telt = $modelo->telpri; //comercial

        $params->feccam = date('Y-m-d');
        if ($cantidad == 1) {
            $params->codact = $modelo->codact;
            $params->tipsoc = $modelo->tipsoc;
        }

        $this->afilia->actualizaEmpresa($empresa, $params);

        $sucursal = Subsi48::where('nit', $modelo->nit)
            ->where('codsuc', $modelo->codsuc)
            ->get()
            ->first();

        $params = clone $sucursal;
        $params->codsuc = $modelo->codsuc;
        $params->telefono = $modelo->celular;
        $params->codact = $modelo->codact;
        $params->codciu = $modelo->codciu; //ciudad notificaciones
        $params->codzon = $modelo->codzon;
        $params->direccion = $modelo->direccion; //direccion notificaciones
        $params->email = $modelo->email;    //email notificaciones
        $params->feccam = date('Y-m-d');
        $this->afilia->actualizaSucursal($sucursal, $params);

        $this->actualizaTrabajadoresSucursalAgro($sucursal, $modelo->nit, $modelo->codsuc);

        $this->resultado = [
            "success" => true,
            "data" => Subsi48::where('nit', $modelo->nit)->get()->first()->toArray(),
            "msj" => "El proceso se ha completado con éxito"
        ];
    }

    public function actualizaTrabajadoresSucursalAgro($sucursal, $nit, $codsuc)
    {
        $actividad = $sucursal->getActividadSucursal();
        if ($actividad->esagro == 1) {

            Subsi15::where('nit', $nit)
                ->where('codsuc', $codsuc)
                ->where('agro', 'N')
                ->update(["agro" => "S"]);

            Subsi168::where('nit', $nit)
                ->where('codsuc', $codsuc)
                ->where('agro', 'N')
                ->update(["agro" => "S"]);
        } else {
            Subsi15::where('nit', $nit)
                ->where('codsuc', $codsuc)
                ->where('agro', 'S')
                ->update(["agro" => "N"]);

            Subsi168::where('nit', $nit)
                ->where('codsuc', $codsuc)
                ->where('agro', 'S')
                ->update(["agro" => "N"]);
        }
    }
}
