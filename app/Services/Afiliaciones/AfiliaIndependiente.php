<?php

namespace  App\Services\Afiliaciones;

use Illuminate\Support\Facades\DB;
use App\Console\Servicios\General\General;
use App\Console\Servicios\Novedades\GestionNovedades;
use App\Models\Subsi12;
use App\Services\AfiliacionSisu\AfiliaSisuEmpresa;
use App\Services\AfiliacionSisu\AfiliaSisuTrabajador;

class AfiliaIndependiente implements InAfiliation
{
    protected $resultado;
    private $afiliaSisuTrabajador;
    private $afiliaSisuEmpresa;

    public function __construct()
    {
        $this->afiliaSisuTrabajador = new AfiliaSisuTrabajador();
        $this->afiliaSisuEmpresa = new AfiliaSisuEmpresa();
    }

    public function procesar($modelo)
    {
        $modelo = (object) $this->afiliaSisuEmpresa->preparaData($modelo);
        $this->afiliaSisuEmpresa->procesar($modelo);
        $result = $this->afiliaSisuEmpresa->getResultado();

        $this->afiliaSisuTrabajador->procesar($modelo);
        $result = $this->afiliaSisuTrabajador->getResultado();

        $gestionNovedades = new GestionNovedades();
        $gestionNovedades->cerrarNovedad04($modelo->cedtra, $modelo->usuario);

        $this->resultado = $result;
        $this->resultado['modelo'] = $modelo;
    }


    public function listar($nit, $estados_trabajador)
    {
        $trabajadores = DB::table('subsi16')
            ->select(
                'subsi16.cedtra',
                'subsi16.nit',
                'subsi16.fecafi',
                DB::raw("(SELECT subsi17.salario FROM subsi17 WHERE subsi17.cedtra = subsi16.cedtra ORDER BY subsi17.fecha DESC LIMIT 1) as salario"),
                'subsi15.fecest',
                'subsi16.fecret',
                'subsi15.priape',
                'subsi15.segape',
                'subsi15.prinom',
                'subsi15.segnom',
                'subsi15.estado',
                'subsi15.codcat',
                'subsi15.codlis',
                DB::raw("CONCAT_WS(' ',subsi15.priape,subsi15.segape,subsi15.prinom,subsi15.segnom) as fullname")
            )->leftJoin('subsi15', 'subsi15.cedtra', '=', 'subsi16.cedtra')
            ->where('subsi16.nit', $nit)
            ->whereNull('subsi16.fecret')
            ->whereIn('subsi15.estado', $estados_trabajador)
            ->groupBy('subsi16.cedtra')
            ->get();
        //->toSql();

        $data = array();
        foreach ($trabajadores as $ai => $trabajador) {

            $mperiodos = Subsi12::where('fecini', '<=', $trabajador->fecafi)->where('fecfin', '>=', $trabajador->fecafi)->get()->first();
            $periodo = $mperiodos->periodo;
            $salario = $trabajador->salario;
            $rcodcat = General::calcula_categoria($salario, $periodo);
            $codcat = ($rcodcat['success']) ? $rcodcat['valor'] : $trabajador->codcat;

            $data[] = array(
                'multiafiliacion' => 0,
                'cedtra' => trim($trabajador->cedtra),
                'nombre' => trim($trabajador->fullname),
                'salario' => trim($trabajador->salario),
                'fecafi' => trim($trabajador->fecafi),
                'estado' => trim($trabajador->estado),
                'fecest' => trim($trabajador->fecest),
                'nit' => trim($trabajador->nit),
                'codlis' => trim($trabajador->codlis),
                'codcat' => trim($codcat)
            );
        }

        $multi_afiliaciones = DB::table('subsi168')
            ->select(
                'subsi168.cedtra',
                'subsi168.salario',
                'subsi168.fecafi',
                'subsi168.estado',
                'subsi168.fecest',
                'subsi15.priape',
                'subsi15.segape',
                'subsi15.prinom',
                'subsi15.segnom',
                'subsi15.codcat',
                'subsi168.nit',
                'subsi168.codlis',
                DB::raw("CONCAT_WS(' ',subsi15.priape,subsi15.segape,subsi15.prinom,subsi15.segnom) as fullname")
            )->leftJoin('subsi15', 'subsi15.cedtra', '=', 'subsi168.cedtra')
            ->where('subsi168.nit', $nit)
            ->whereIn('subsi168.estado', $estados_trabajador)
            ->groupBy('subsi168.cedtra')
            ->get();

        foreach ($multi_afiliaciones as $ai => $trabajador) {

            $mperiodos = Subsi12::where('fecini', '<=', $trabajador->fecafi)->where('fecfin', '>=', $trabajador->fecafi)->get()->first();
            $periodo = $mperiodos->periodo;
            $salario = $trabajador->salario;

            $rcodcat = General::calcula_categoria($salario, $periodo);
            $codcat = ($rcodcat['success']) ? $rcodcat['valor'] : $trabajador->codcat;

            $data[] = array(
                'multiafiliacion' => 1,
                'cedtra' => trim($trabajador->cedtra),
                'nombre' => trim($trabajador->fullname),
                'salario' => trim($trabajador->salario),
                'fecafi' => trim($trabajador->fecafi),
                'estado' => trim($trabajador->estado),
                'fecest' => trim($trabajador->fecest),
                'nit' => trim($trabajador->nit),
                'codlis' => trim($trabajador->codlis),
                'codcat' => trim($codcat),
            );
        }
        return $data;
    }

    public function getResultado()
    {
        return $this->resultado;
    }

    public function buscar($cedtra, $estados_trabajador)
    {
        $trabajador = DB::table('subsi15')
            ->select(
                'subsi15.*',
                'subsi02.razsoc',
                'subsi02.repleg',
                'subsi02.direccion as empresa_direccion',
                'subsi02.telefono as empresa_telefono',
                'subsi02.codciu as empresa_codciu'
            )
            ->leftJoin('subsi02', 'subsi02.nit', '=', 'subsi15.nit')
            ->where('subsi15.cedtra', $cedtra)
            ->whereIn('subsi15.estado', $estados_trabajador)
            ->get()
            ->first();

        if ($trabajador) {

            $data = collect($trabajador);

            $multi_afiliacion = DB::table('subsi168')
                ->select(
                    'subsi168.cedtra',
                    'subsi168.codgir2',
                    'subsi168.codsuc',
                    'subsi168.salario',
                    'subsi168.fecafi',
                    'subsi168.estado',
                    'subsi168.fecest',
                    'subsi15.priape',
                    'subsi15.segape',
                    'subsi15.prinom',
                    'subsi15.segnom',
                    'subsi15.codcat',
                    'subsi168.nit',
                    'subsi168.codlis',
                    DB::raw("CONCAT_WS(' ',subsi15.priape,subsi15.segape,subsi15.prinom,subsi15.segnom) as fullname"),
                    'subsi02.razsoc',
                    'subsi02.repleg',
                    'subsi02.direccion as empresa_direccion',
                    'subsi02.telefono as empresa_telefono',
                    'subsi02.codciu as empresa_codciu'
                )
                ->leftJoin('subsi15', 'subsi15.cedtra', '=', 'subsi168.cedtra')
                ->leftJoin('subsi02', 'subsi02.nit', '=', 'subsi168.nit')
                ->where('subsi168.cedtra', $cedtra)
                ->whereIn('subsi168.estado', $estados_trabajador)
                ->groupBy('subsi168.cedtra')
                ->get()
                ->first();

            if ($multi_afiliacion) {
                $data['multiafiliacion'] = true;
                $data['multiafiliacion_empresa'] = $multi_afiliacion->nit;
                $data['multiafiliacion_sucursal'] = $multi_afiliacion->codsuc;
                $data['multiafiliacion_fecafi'] = $multi_afiliacion->fecafi;
                $data['multiafiliacion_estado'] = $multi_afiliacion->estado;
                $data['multiafiliacion_salario'] = $multi_afiliacion->salario;
                $data['multiafiliacion_codgir'] =  $multi_afiliacion->codgir2;
                $data['multiafiliacion_razsoc'] =  $multi_afiliacion->razsoc;
                $data['multiafiliacion_repleg'] =  $multi_afiliacion->repleg;
                $data['multiafiliacion_direccion'] =  $multi_afiliacion->empresa_direccion;
                $data['multiafiliacion_telefono'] =  $multi_afiliacion->empresa_telefono;
                $data['multiafiliacion_codciu'] =  $multi_afiliacion->empresa_codciu;
            } else {
                $data['multiafiliacion'] = false;
            }
        } else {
            $data = false;
        }
        $this->resultado = [
            "success" => true,
            "data" =>  $data
        ];
    }

    public function buscar_empresa($cedtra, $estados_trabajador, $nit)
    {
        $trabajador = DB::table('subsi15')
            ->select(
                'subsi15.*',
                'subsi02.razsoc',
                'subsi02.repleg',
                'subsi02.direccion as empresa_direccion',
                'subsi02.telefono as empresa_telefono',
                'subsi02.codciu as empresa_codciu'
            )
            ->leftJoin('subsi02', 'subsi02.nit', '=', 'subsi15.nit')
            ->where('subsi15.cedtra', $cedtra)
            ->whereIn('subsi15.estado', $estados_trabajador)
            ->get()
            ->first();

        if ($trabajador) {
            $data = collect($trabajador)->toArray();

            if ($nit != $data['nit']) {
                $multi_afiliacion = DB::table('subsi168')
                    ->select(
                        'subsi168.cedtra',
                        'subsi168.codgir2',
                        'subsi168.codsuc',
                        'subsi168.salario',
                        'subsi168.fecafi',
                        'subsi168.estado',
                        'subsi168.fecest',
                        'subsi15.priape',
                        'subsi15.segape',
                        'subsi15.prinom',
                        'subsi15.segnom',
                        'subsi15.codcat',
                        'subsi168.nit',
                        'subsi168.codlis',
                        DB::raw("CONCAT_WS(' ',subsi15.priape,subsi15.segape,subsi15.prinom,subsi15.segnom) as fullname"),
                        'subsi02.razsoc',
                        'subsi02.repleg',
                        'subsi02.direccion as empresa_direccion',
                        'subsi02.telefono as empresa_telefono',
                        'subsi02.codciu as empresa_codciu'
                    )
                    ->leftJoin('subsi15', 'subsi15.cedtra', '=', 'subsi168.cedtra')
                    ->leftJoin('subsi02', 'subsi02.nit', '=', 'subsi168.nit')
                    ->where('subsi168.cedtra', $cedtra)
                    ->where('subsi168.nit', $nit)
                    ->whereIn('subsi168.estado', $estados_trabajador)
                    ->get()
                    ->first();

                if ($multi_afiliacion) {
                    $data["nit"] =  $multi_afiliacion->nit;
                    $data["razsoc"] = $multi_afiliacion->razsoc;
                    $data["repleg"] = $multi_afiliacion->repleg;
                    $data["empresa_direccion"] = $multi_afiliacion->empresa_direccion;
                    $data["empresa_telefono"] = $multi_afiliacion->empresa_telefono;
                    $data["empresa_codciu"] = $multi_afiliacion->empresa_codciu;
                }
            }
        } else {
            $data = false;
        }
        $this->resultado = [
            "success" => ($data) ? true : false,
            "data" =>  $data
        ];
    }
}
