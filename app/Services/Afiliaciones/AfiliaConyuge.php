<?php

namespace  App\Services\Afiliaciones;

ini_set('memory_limit', '1300M');

use Illuminate\Support\Facades\DB;
use App\Models\Subsi20;
use App\Services\AfiliacionSisu\AfiliaSisuConyuge;

class AfiliaConyuge implements InAfiliation
{

    protected $resultado;
    private $afiliaSisuConyuge;

    public function __construct()
    {
        $this->afiliaSisuConyuge = new AfiliaSisuConyuge();
    }

    /**
     * procesar function
     * Se encarga de crear los registros en base de datos de la conyuge
     * @param [type] $modelo
     * @return void
     */
    public function procesar($modelo)
    {
        $this->afiliaSisuConyuge->procesar($modelo);
        $this->resultado = $this->afiliaSisuConyuge->getResultado();
    }

    public function getResultado()
    {
        return $this->resultado;
    }
}
