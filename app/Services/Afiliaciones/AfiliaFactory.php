<?php

namespace  App\Services\Afiliaciones;

interface AfiliaFactory
{

    public function make();
}
