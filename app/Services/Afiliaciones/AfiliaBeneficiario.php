<?php

namespace  App\Services\Afiliaciones;

ini_set('memory_limit', '1300M');

use App\Models\Subsi22;
use App\Services\AfiliacionSisu\AfiliaSisuBeneficiario;
use Illuminate\Support\Facades\DB;

class AfiliaBeneficiario implements InAfiliation
{

    protected $resultado;
    private $afiliaSisuBeneficiario;

    public function __construct()
    {
        $this->afiliaSisuBeneficiario = new AfiliaSisuBeneficiario();
    }

    /**
     * procesar function
     * Se encraga de hacer el registro del modelo en la base de datos de sisuweb
     * @param [type] $modelo
     * @return void
     */
    public function procesar($modelo)
    {
        $this->afiliaSisuBeneficiario->procesar($modelo);
        $this->resultado = $this->afiliaSisuBeneficiario->getResultado();
    }

    public function listar($nit, $estados)
    {
        return [];
    }

    public function getResultado()
    {
        return $this->resultado;
    }

    public function actualizarDatos($numdoc, $modelo)
    {
        $modelo = collect($modelo)->toArray();
        Subsi22::where('documento', $numdoc)
            ->update($modelo);

        $this->resultado = [
            "success" => true,
            "data" => Subsi22::where('documento', $numdoc)->get()->first()->toArray()
        ];
    }
}
