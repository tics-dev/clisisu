<?php

namespace  App\Services\Afiliaciones;

interface InAfiliation
{

    public function procesar($modelo);

    public function getResultado();
}
