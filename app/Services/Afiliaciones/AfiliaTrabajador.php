<?php

namespace  App\Services\Afiliaciones;

use Illuminate\Support\Facades\DB;
use App\Console\Servicios\ServicioSat\Tareas;
use App\Console\Servicios\General\General;
use App\Console\Servicios\Novedades\GestionNovedades;
use App\Models\Subsi12;
use App\Services\AfiliacionSisu\AfiliaSisuTrabajador;

class AfiliaTrabajador
{
    protected $resultado;
    private $afiliaSisuTrabajador;

    public function __construct()
    {
        $this->afiliaSisuTrabajador = new AfiliaSisuTrabajador();
    }

    public function procesar($modelo)
    {
        $sat = false;
        $this->afiliaSisuTrabajador->procesar($modelo);
        $result = $this->afiliaSisuTrabajador->getResultado();

        //cerrar las novedades
        $gestionNovedades = new GestionNovedades();
        $gestionNovedades->cerrarNovedad04($modelo->cedtra, $modelo->usuario);


        if (env('APP_MODE') == 'production') {
            //enviar al sat
            $tareas = new Tareas();
            $modelo->numdoctra = $modelo->cedtra;

            list($afiliacion_principal, $afiliacion_nueva) = $this->afiliaSisuTrabajador->detalleGestion();
            if (!!$afiliacion_principal) {

                if ($afiliacion_nueva) {

                    if (Tareas::isEnabled('sat09', 'SA') == True) {
                        $sat = $tareas->trabajador_inicia_relacion($modelo);
                    }
                } else {
                    if (Tareas::isEnabled('sat13', 'SA') == True) {
                        $sat = $tareas->trabajador_cambio_salario($modelo->cedtra, $modelo->salario);
                    }
                }
            }
        }

        $this->resultado = $result;
        $this->resultado['modelo'] = $modelo;
        $this->resultado['sat'] = $sat;
    }

    public function getResultado()
    {
        return $this->resultado;
    }
}
