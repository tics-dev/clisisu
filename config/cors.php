<?php

return [

    'paths' => ['web/*', 'api/*', 'tippag/*'],

    'allowed_methods' => ['GET', 'POST'],

    'allowed_origins' => ['*'],

    'allowed_origins_patterns' => [],

    'allowed_headers' => ['Authorization'],

    'exposed_headers' => [],

    'max_age' => 0,

    'supports_credentials' => false

];
