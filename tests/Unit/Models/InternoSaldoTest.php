<?php

namespace Tests\Unit\Models;

use App\Models\InternoAbono;
use App\Models\InternoDebito;
use App\Models\InternoSaldo;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;

class InternoSaldoTest extends TestCase
{
    /**
     * testInternoSaldoAbonos function
     * Se busca validar la reacion de tablas por hasMany Abonos
     * @return void
     */
    public function testInternoSaldoAbonos()
    {
        $internoSaldo = new InternoSaldo;
        $this->assertInstanceOf(Collection::class, $internoSaldo->abonos);
    }

    /**
     * testInternoSaldoDebitos function
     * Se busca validar la reacion de tablas por hasMany Debitos
     * @return void
     */
    public function testInternoSaldoDebitos()
    {
        $internoSaldo = new InternoSaldo;
        $this->assertInstanceOf(Collection::class, $internoSaldo->debitos);
    }

    /**
     * testInternoDebitosSaldo function
     * @return void
     */
    public function testInternoDebitosSaldo()
    {
        $debito = InternoDebito::factory()->create();
        $this->assertInstanceOf(InternoSaldo::class, $debito->internoSaldo);
        // dd($debito->internoSaldo->toArray());
    }

    /**
     * testInternoAbonosSaldo function
     * @return void
     */
    public function testInternoAbonosSaldo()
    {
        $abono = InternoAbono::factory()->create();
        $this->assertInstanceOf(InternoSaldo::class, $abono->internoSaldo);
        // dd($abono->internoSaldo->toArray());
    }
}
