<?php

namespace Tests\Feature\Services\ReportesGiroCuota;

use App\Services\ReportesGiroCuota\ReporteCruceExcel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ReporteCruceExcelTest extends TestCase
{
    /**
     * testReporteCruce function
     * @return void
     */
    public function testReporteCruce()
    {
        $this->withoutExceptionHandling();
        $reporteCruceExcel = new ReporteCruceExcel();
        $reporteCruceExcel->generarExcel();
        $this->assertNotEmpty('OK');
    }
}
