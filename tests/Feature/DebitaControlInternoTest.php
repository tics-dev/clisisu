<?php

namespace Tests\Feature;

use App\Console\Servicios\Contabilidad\AbonoCuentaPlanos;
use App\Console\Servicios\Contabilidad\DaviplataPagosPlanos;
use App\Models\CuotaComprobante;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Artisan;
use Mockery;
use Tests\TestCase;

use function PHPUnit\Framework\assertJson;

class DebitaControlInternoTest extends TestCase
{


    public function __construct()
    {
        ini_set('memory_limit', '2000M');
        parent::__construct();
    }

    /**
     * A basic feature test example.
     * @return void
     */
    public function testDebitaDaviplataControlInternoOneToOne()
    {
        /* 
        $this->withoutExceptionHandling();

        $fecha = '2023-03-17';

        $cuotasComprobante = CuotaComprobante::where('estado', 'C')->where('fecha_detalle', $fecha)->get()->first();

        DaviplataPagosPlanos::initLog('SYS');

        DaviplataPagosPlanos::$debitar = True;

        $daviplataPagosPlanos = new DaviplataPagosPlanos();

        $daviplataPagosPlanos->set_env(true, $fecha);

        $daviplataPagosPlanos->principal($cuotasComprobante);

        DaviplataPagosPlanos::closeLog();

        $salida = $daviplataPagosPlanos->getResultado();

        $this->assertJson(json_encode($salida), false);

        // dd($salida);
        */
        $this->assertNotEmpty('OK');
    }

    /**
     * A basic feature test example.
     * @return void
     */
    public function testDebitaDaviplataControlInternoAll()
    {
        /* 
        $this->withoutExceptionHandling();

        $fecha = '2023-03-17';

        $cuotasComprobantes = CuotaComprobante::where('estado', 'C')->where('fecha_detalle', $fecha)->get();

        DaviplataPagosPlanos::initLog('SYS');

        $daviplataPagosPlanos = new DaviplataPagosPlanos();
        
        DaviplataPagosPlanos::$debitar = True;

        $daviplataPagosPlanos->set_env(true, $fecha);

        foreach ($cuotasComprobantes as $cuotasComprobante) {
            $daviplataPagosPlanos->principal($cuotasComprobante);
        }

        DaviplataPagosPlanos::closeLog();

        $salida = $daviplataPagosPlanos->getResultado();

        dd($salida); 
        */

        $this->assertNotEmpty('OK');
    }

    /**
     * A basic feature test example.
     * @return void
     */
    public function testDebitaAbonoControlInternoOneToOne()
    {
        /* $this->withoutExceptionHandling();

        $fecha = '2023-03-17';

        $cuotasComprobante = CuotaComprobante::where('estado', 'C')->where('fecha_detalle', $fecha)->get()->first();

        AbonoCuentaPlanos::initLog('SYS');

        AbonoCuentaPlanos::$debitar = True;

        $abonoCuentaPlanos = new AbonoCuentaPlanos();

        $abonoCuentaPlanos->set_env(true, $fecha);

        $abonoCuentaPlanos->principal($cuotasComprobante);

        AbonoCuentaPlanos::closeLog();

        $salida = $abonoCuentaPlanos->getResultado();

        $this->assertJson(json_encode($salida), false);

        dd($salida); */
        $this->assertNotEmpty('OK');
    }

    /**
     * A basic feature test example.
     * @return void
     */
    public function testDebitaAbonoControlInternoAll()
    {
        /* $this->withoutExceptionHandling();

        $fecha = '2023-03-17';

        $cuotasComprobantes = CuotaComprobante::where('estado', 'C')->where('fecha_detalle', $fecha)->get();

        AbonoCuentaPlanos::initLog('SYS');

        $abonoCuentaPlanos = new AbonoCuentaPlanos();

        AbonoCuentaPlanos::$debitar = True;

        $abonoCuentaPlanos->set_env(true, $fecha);

        foreach ($cuotasComprobantes as $cuotasComprobante) {
            $abonoCuentaPlanos->principal($cuotasComprobante);
        }

        AbonoCuentaPlanos::closeLog();

        $salida = $abonoCuentaPlanos->getResultado();

        dd($salida); */

        $this->assertNotEmpty('OK');
    }
}
