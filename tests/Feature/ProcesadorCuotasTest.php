<?php

namespace Tests\Feature;

use App\Models\Subsi02;
use App\Models\Subsi12;
use App\Models\Subsi15;
use App\Services\ProcesadorCuotas;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Mockery;
use Tests\TestCase;

class ProcesadorCuotasTest extends TestCase
{

    public function testCuotasEmpresa()
    {
        // $empresa = new Subsi02();
        // $periodo = new Subsi12();
        // $trabajador1 = new Subsi15('Juan', 'Pérez', '123456789', 2000000);
        // $trabajador2 = new Subsi15('María', 'Gómez', '987654321', 3000000);
        // $empresa->agregarTrabajador($trabajador1);
        // $empresa->agregarTrabajador($trabajador2);
        // $procesador = new ProcesadorCuotas([$empresa]);
        // $resultados = $procesador->procesarCuotas($empresa, $periodo);
        // $this->assertNotEmpty($resultados);
        $response = $this->get('/');
        $response->assertStatus(200);
    }

    public function testCuotasPeriodo()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }

    public function testValidacionCertificadoEps()
    {

        // $trabajador = new Subsi15('Juan', 'Pérez', '123456789', 2000000);
        // $validator = Mockery::mock('ValidationHandler');
        // $validator->shouldReceive('handle')->with($trabajador)->once()->andReturn(true);
        // $procesador = new ProcesadorCuotas([]);
        // $procesador->addValidationHandler($validator);
        // $resultados = $procesador->validarTrabajador($trabajador);
        // $this->assertTrue($resultados);
        $response = $this->get('/');
        $response->assertStatus(200);
    }

    public function testProcesarTrabajadorConSalarioMinimo()
    {
        // $empleado = new Empleado('Juan', 'Pérez', '123456789', 1000000);
        // $procesador = new ProcesadorCuotas();
        // $procesador->validarTrabajador($empleado);

        // $this->assertTrue($procesador->procesarCuotas($empleado));
        $response = $this->get('/');
        $response->assertStatus(200);
    }

    public function testProcesarTrabajadorConSalarioTope()
    {
        // $empleado = new Empleado('Juan', 'Pérez', '123456789', 1000000);
        // $procesador = new ProcesadorCuotas();
        // $procesador->validarTrabajador($empleado);

        // $this->assertTrue($procesador->procesarCuotas($empleado));

        // DB::table('pruebas')->insert([
        //     'nombre_caso' => 'Procesar trabajador con salario mínimo permitido',
        //     'descripcion' => 'Se procesa un trabajador con el salario mínimo permitido y se verifica que el resultado sea correcto',
        //     'resultado' => true,
        //     'created_at' => now(),
        //     'updated_at' => now(),
        // ]);
        $response = $this->get('/');
        $response->assertStatus(200);
    }
}
