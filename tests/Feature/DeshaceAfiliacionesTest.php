<?php

namespace Tests\Feature;

use App\Models\Mercurio31;
use App\Models\Subsi15;
use App\Models\Subsi16;
use App\Models\Trazabilidad;
use App\Services\DeshacerAfiliaciones\DeshaceTrabajador;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;


class DeshaceAfiliacionesTest extends TestCase
{
    //no usa RefreshDatabase en producción esta muy mammon
    //use RefreshDatabase;

    public function __construct()
    {
        ini_set('memory_limit', '2000M');
        parent::__construct();
    }

    public function testBuscarTrayectoriaTrabajador()
    {
        //mode debug laravel
        $this->withoutExceptionHandling();

        $this->artisan('server:send', [
            'clase' => 'ProcesoPrueba',
            'metodo' => 'probar_trayectoria',
            'params' => true,
            'user' => 1,
            'sistema' => 'SYS',
            'env' => 1,
            'comando' => null
        ])->assertSuccessful();
    }

    public function testDeshaceTrabajador()
    {
        //mode debug laravel
        $this->withoutExceptionHandling();

        $trabajadorSolicitud = Mercurio31::where('estado', 'A')->where('fecsol', '>=', '2023-01-01')->get()->random();

        $params = base64_encode(json_encode(
            [
                'nit' => $trabajadorSolicitud->nit,
                'tipo_documento' => $trabajadorSolicitud->tipdoc,
                'cedtra' => $trabajadorSolicitud->cedtra,
                'nota' => 'Se hace rechazo de la solicitud',
            ]
        ));

        $this->artisan('server:send', [
            'clase' => 'DeshacerAfiliaciones',
            'metodo' => 'deshacerAprobacionTrabajador',
            'params' => $params,
            'user' => 1,
            'sistema' => 'SYS',
            'env' => 1,
            'comando' => null
        ])->assertSuccessful();

        /*  $tr31 = Mercurio31::where('cedtra', $trabajadorSolicitud->cedtra)->get()->first();
        $tr15 = Subsi15::where('cedtra', $trabajadorSolicitud->cedtra)->get()->first();
        $tr16 = Subsi16::where('cedtra', $tr31->cedtra)->where('nit', $tr31->nit)->get()->last();

        $out = [
            "subsi15" => [
                "cedtra" => $tr15->cedtra,
                "coddoc" => $tr15->coddoc,
                "nit" => $tr15->nit,
                "fecafi" => $tr15->fecafi,
                "fecest" => $tr15->fecest,
                "estado" => $tr15->estado
            ],
            "mercurio31" => [
                "cedtra" => $tr31->cedtra,
                "tipdoc" => $tr31->tipdoc,
                "nit" => $tr31->nit,
                "fecsol" => $tr31->fecsol,
                "fecest" => $tr31->fecest,
                "estado" => $tr31->estado,
            ],
            "subsi16" => [
                "cedtra" => ($tr16) ? $tr16->cedtra : '',
                "nit" => ($tr16) ? $tr16->nit : '',
                "fecsol" => ($tr16) ? $tr16->fecsol : '',
                "fecest" => ($tr16) ? $tr16->fecest : ''
            ]
        ];
        dd($out); */
    }
}
