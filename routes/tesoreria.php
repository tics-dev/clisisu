<?php

use App\Http\Controllers\TesoreriaController;
use Illuminate\Support\Facades\Route;

Route::middleware('auth.basic')->group(function () {
    /**
     * cruzar cuentas daviplata para actualizar medio de pago, nuemro de cuenta
     */
    Route::post('tesoreria/procesar_pagos_bancos/{filename}', [TesoreriaController::class, 'procesarPagosBancos']);
});
