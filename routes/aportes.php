<?php

use App\Http\Controllers\AportesController;
use Illuminate\Support\Facades\Route;

Route::middleware('auth.basic')->group(function () {

    Route::post('aportes/buscarAportesEmpresa/{nit}', [AportesController::class, 'buscarAportesEmpresa']);

    Route::post('aportes/buscarMoraEmpresa', [AportesController::class, 'buscarMoraEmpresa']);
});
