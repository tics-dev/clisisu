<?php

use App\Http\Controllers\NovedadesController;
use Illuminate\Support\Facades\Route;

Route::middleware('auth.basic')->group(function () {

    Route::post('novedades/aportes_trabajador_noafiliado', [NovedadesController::class, 'aportesTrabajadorNoAfiliado']);

    Route::post('novedades/trabajador_afiliado_sinaportes', [NovedadesController::class, 'trabajadorAfiliadoSinAportes']);

    Route::post('novedades/novedad_trabajador_sin_planilla', [NovedadesController::class, 'novedadTrabajadorSinPlanilla']);

    Route::post('novedades/novedad_trabajador_trayectoria', [NovedadesController::class, 'novedadTrabajadorTrayectoria']);

    Route::post('novedades/prueba_novedades', [NovedadesController::class, 'pruebaNovedades']);
});
