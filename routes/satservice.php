<?php

use App\Http\Controllers\SatserviceController;
use Illuminate\Support\Facades\Route;

Route::middleware('auth.basic')->group(function () {

    Route::post('satservice/listar_notificaciones_sat', [SatserviceController::class, 'listarNotificaciones']);
});
