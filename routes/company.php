<?php

use App\Http\Controllers\CompanyController;
use Illuminate\Support\Facades\Route;

Route::middleware('auth.basic')->group(function () {
    Route::post('company/informacion_empresa/{nit}', [CompanyController::class, 'informacionEmpresa']);

    Route::post('company/informacion_empresa', [CompanyController::class, 'informacionEmpresa']);

    Route::post('company/buscar_sucursales_en_empresa/{nit}', [CompanyController::class, 'buscarSucursalesEnEmpresa']);

    Route::post('company/buscar_listas_en_empresa/{nit}', [CompanyController::class, 'buscarListasEnEmpresa']);

    Route::post('company/informacion_trabajador/{cedtra}', [CompanyController::class, 'informacionTrabajador']);

    Route::post('company/informacion_conyuge/{cedcon}', [CompanyController::class, 'informacionConyuge']);

    Route::post('company/informacion_beneficiario/{numdoc}', [CompanyController::class, 'informacionBeneficiario']);


    /**
     * actualiza mercurio el estado de las solicitudes
     */
    Route::post('company/actualiza_empresa_enlinea/{documento}', [CompanyController::class, 'actualizaEmpresaEnlinea']);

    Route::post('company/actualiza_trabajador_enlinea/{documento}', [CompanyController::class, 'actualizaTrabajadorEnlinea']);

    Route::post('company/actualiza_conyuge_enlinea/{documento}', [CompanyController::class, 'actualizaConyugeEnlinea']);

    Route::post('company/actualiza_beneficiario_enlinea/{documento}', [CompanyController::class, 'actualizaBeneficiarioEnlinea']);
});
