<?php

use App\Http\Controllers\AffiliationController;
use Illuminate\Support\Facades\Route;


Route::middleware('auth.basic')->group(function () {

    Route::post('affiliation/parametros_trabajadores', [AffiliationController::class, 'parametrosTrabajadores']);
    Route::post('affiliation/parametros_conyuges', [AffiliationController::class, 'parametrosConyuges']);
    Route::post('affiliation/parametros_beneficiarios', [AffiliationController::class, 'parametrosBeneficiarios']);
    Route::post('affiliation/parametros_empresa', [AffiliationController::class, 'parametrosEmpresa']);
    Route::post('affiliation/parametros_pensionado', [AffiliationController::class, 'parametrosPensionado']);
    Route::post('affiliation/parametros_independiente', [AffiliationController::class, 'parametrosIndependiente']);

    Route::post('affiliation/listar_trabajadores', [AffiliationController::class, 'listarTrabajadores']);
    Route::post('affiliation/listar_conyuges', [AffiliationController::class, 'listarConyuges']);
    Route::post('affiliation/listar_conyuges_trabajador/{cedtra}', [AffiliationController::class, 'listarConyugesTrabajador']);
    Route::post('affiliation/listar_ciudades_departamentos', [AffiliationController::class, 'listarCiudadesDepartamentos']);

    Route::post('affiliation/trabajador_empresa', [AffiliationController::class, 'trabajadorEmpresa']);
    Route::post('affiliation/conyugue_trabajador_beneficiario', [AffiliationController::class, 'conyugueTrabajadorBeneficiario']);

    Route::post('affiliation/trabajador', [AffiliationController::class, 'trabajador']);
    Route::post('affiliation/conyuge', [AffiliationController::class, 'conyuge']);
    Route::post('affiliation/beneficiario', [AffiliationController::class, 'beneficiario']);

    Route::post('affiliation/afilia_trabajador', [AffiliationController::class, 'afiliaTrabajador']);
    Route::post('affiliation/afilia_empresa', [AffiliationController::class, 'afiliaEmpresa']);
    Route::post('affiliation/afilia_beneficiario', [AffiliationController::class, 'afiliaBeneficiario']);
    Route::post('affiliation/afilia_conyuge', [AffiliationController::class, 'afiliaConyuge']);
    Route::post('affiliation/afilia_independiente', [AffiliationController::class, 'afiliaIndependiente']);

    Route::post('affiliation/actualiza_beneficiario', [AffiliationController::class, 'actualizaBeneficiario']);
    Route::post('affiliation/actualiza_empresa', [AffiliationController::class, 'actualizaEmpresa']);

    // CargueMasivos
    Route::post('affiliation/masivo_trabajadores', [AffiliationController::class, 'masivoTrabajadores']);
    Route::post('affiliation/masivo_conyuges', [AffiliationController::class, 'masivoConyuges']);
    Route::post('affiliation/masivo_beneficiarios', [AffiliationController::class, 'masivoBeneficiarios']);
});

Route::fallback(function ($ruta) {
    $ruta = url($ruta);
    return response()->json([
        "message" => "Ruta {$ruta} no está disponible para acceder.",
        "code" => 404
    ]);
});
