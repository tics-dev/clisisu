<?php

use App\Http\Controllers\CertificadoController;
use Illuminate\Support\Facades\Route;

Route::middleware('auth.basic')->group(function () {
    /**
     * cruzar cuentas daviplata para actualizar medio de pago, nuemro de cuenta
     */
    Route::post('certificados/buscarCertificadosBeneficiario/{documento}', [CertificadoController::class, 'buscarCertificadosBeneficiario']);
});
