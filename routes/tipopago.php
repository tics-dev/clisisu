<?php

use App\Http\Controllers\TipopagoController;
use Illuminate\Support\Facades\Route;

Route::middleware('auth.basic')->group(function () {
    /**
     * cruzar cuentas daviplata para actualizar medio de pago, nuemro de cuenta
     */
    Route::post('tippag/cruza_daviplata', [TipopagoController::class, 'cruzaCuentaDaviplata']);

    /**
     * cruzar cuentas no certificadas para revertir
     */
    Route::post('tippag/nocertifica_daviplata', [TipopagoController::class, 'noCertificaDaviplata']);
});
