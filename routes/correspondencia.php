<?php

use App\Http\Controllers\CorrespondenciaController;
use Illuminate\Support\Facades\Route;

Route::middleware('auth.basic')->group(function () {
    Route::post('correspondencia/reporte_primer_aviso', [CorrespondenciaController::class, 'reportePrimerAviso']);

    Route::post('correspondencia/reporte_aviso_desafiliacion', [CorrespondenciaController::class, 'reporteAvisoDesafiliacion']);
});
