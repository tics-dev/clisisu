<?php

use App\Http\Controllers\AfiliationController;
use App\Http\Controllers\AutenticaController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/token', [AutenticaController::class, 'autenticar']);

Route::middleware('filter')->group(function () {

    Route::get('/afiliation/deshacer', [AfiliationController::class, 'deshaceTrabajador']);

    Route::get('/afiliation/export', [AfiliationController::class, 'export']);

    Route::get('/afiliation', [AfiliationController::class, 'index']);
});

Route::get('/probar_trayecto', function () {
    Artisan::call(
        'server:send',
        [
            'clase' => 'ProcesoPrueba',
            'metodo' => 'probar_trayectoria',
            'params' => true
        ]
    );
    return response()->json(json_decode(Artisan::output()), 200);
});

Route::get('/prueba_comando', function () {
    Artisan::command('server:send {params}', function ($params) {
        return response()->json("Sending email to: {$params}!", 200);
    });
});

Route::fallback(function ($ruta) {
    $ruta = url($ruta);
    return response()->json(array(
        'status' => false,
        'message' =>  "Ruta {$ruta} no está disponible para acceder.",
        'code' => 404
    ), 503);
});
