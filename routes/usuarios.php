<?php

use App\Http\Controllers\UsuariosController;
use Illuminate\Support\Facades\Route;

Route::middleware('auth.basic')->group(function () {
    /**
     * cruzar cuentas daviplata para actualizar medio de pago, nuemro de cuenta
     */
    Route::post('usuarios/trae_usuario/{user}', [UsuariosController::class, 'buscarUsuario']);

    Route::post('usuarios/confirma_politica/{user}', [UsuariosController::class, 'confirmaPolitica']);

    Route::post('usuarios/resetear_intentos/{user}', [UsuariosController::class, 'resetearIntentos']);

    Route::post('usuarios/cargar_intentos/{user}', [UsuariosController::class, 'cargarIntentos']);
});
