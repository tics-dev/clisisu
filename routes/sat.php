<?php

use App\Http\Controllers\ServicioSatController;
use App\Http\Controllers\ReprocesoSatController;
use Illuminate\Support\Facades\Route;

Route::middleware('auth.basic')->group(function () {
    Route::post('sat/empresa_nueva/{numdocemp}', [ServicioSatController::class, 'empresaNueva']);

    Route::post('sat/empresa_reintegro/{numdocemp}', [ServicioSatController::class, 'empresaReintegro']);

    Route::post('sat/empresa_retiro/{numdocemp}', [ServicioSatController::class, 'empresaRetiro']);

    Route::post('sat/empresa_causa_grave', [ServicioSatController::class, 'empresaCausaGrave']);

    Route::post('sat/procesa_trabajador/{numdoctra}', [ServicioSatController::class, 'procesaTrabajador']);

    Route::post('sat/termina_trabajador/{cedtra}', [ServicioSatController::class, 'terminaTrabajador']);

    Route::post('sat/get_modelo', [ServicioSatController::class, 'getModelo']);

    Route::post('sat/get_empresa/{nit}', [ServicioSatController::class, 'getEmpresa']);

    Route::post('sat/modelo_reprocesar', [ServicioSatController::class, 'modeloReprocesar']);

    Route::post('sat/gloza_delete', [ServicioSatController::class, 'glozaDelete']);

    Route::post('sat/actualiza_persistencia', [ServicioSatController::class, 'actualizaPersistencia']);

    Route::post('sat/get_trabajador/{cedtra}', [ServicioSatController::class, 'getTrabajador']);

    Route::post('sat/estado_pago_aportes', [ServicioSatController::class, 'estadoPagoAportes']);

    Route::post('sat/consulta_empresa_empleados/{nit}', [ServicioSatController::class, 'consultaEmpresaEmpleados']);

    Route::post('sat/consulta_empresa_afiliaciones/{nit}', [ServicioSatController::class, 'consultaEmpresaAfiliaciones']);

    Route::post('sat/trabajador_cambio_salario', [ServicioSatController::class, 'trabajadorCambioSalario']);

    #Depurar registros no validos
    Route::post('sat/depurar_empresa_novalida', [ReprocesoSatController::class, 'depurarEmpresaNoValida']);

    Route::post('sat/depurar_trabajadores_novalidos', [ReprocesoSatController::class, 'depurarTrabajadoresNoValidos']);

    Route::post('sat/depurar_glozas_masivo_empresas', [ReprocesoSatController::class, 'depurarGlozasMasivoEmpresas']);

    #Reprocesos
    Route::post('sat/repro_empresas_nuevas', [ReprocesoSatController::class, 'reproEmpresasNuevas']);

    Route::post('sat/repro_trabajadores_inicio_relacion', [ReprocesoSatController::class, 'reproTrabajadoresInicioRelacion']);

    Route::post('sat/repro_trabajadores_terminacion_laboral', [ReprocesoSatController::class, 'reproTrabajadoresTerminacionLaboral']);

    Route::post('sat/repro_trabajador_cambio_salario', [ReprocesoSatController::class, 'reproTrabajadorCambioSalario']);

    Route::post('sat/repro_empresas_desafiliaciones', [ReprocesoSatController::class, 'reproEmpresasDesafiliaciones']);

    Route::post('sat/repro_empresas_reintegros', [ReprocesoSatController::class, 'reproEmpresasReintegros']);

    Route::post('sat/repro_empresas_pendientes', [ReprocesoSatController::class, 'reproEmpresasPendientes']);

    Route::post('sat/repro_empresa_aportes', [ReprocesoSatController::class, 'reproEmpresaAportes']);
});
