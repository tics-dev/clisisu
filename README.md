## API - CLISISU

Proyecto Command Line Interface, CLI, para el procesamiento por medio de bash.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

## Usando tinker

```bash
php artisan vendor:publish --provider="Laravel\Tinker\TinkerServiceProvider"
```

## Desplegar la API CLI-SISU

```bash
p7 artisan serve --host=172.168.0.40 --port=9000
```

## ACCESS API

Se requiere de autenticación AUTH-BASIC base64(user:password), para permitir el ingreso y el uso de la API,  
Solo pueden ingresar usuarios autorizados por el sistema central SISU.
