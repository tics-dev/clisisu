<?php

namespace Database\Factories;

use App\Models\Subsi22;
use Illuminate\Database\Eloquent\Factories\Factory;

class Subsi22Factory extends Factory
{
    protected $model = Subsi22::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
