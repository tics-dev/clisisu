<?php

namespace Database\Factories;

use App\Models\Subsi02;
use Illuminate\Database\Eloquent\Factories\Factory;

class Subsi02Factory extends Factory
{

    protected $model = Subsi02::class;
    /**
     * @return array
     */
    public function definition()
    {
        return [
            'nit' => $this->faker->unique()->randomDigit(),
            'digver' =>  $this->faker->numberBetween(1, 9),
            'coddoc' => $this->faker->numberBetween(1, 2),
            'razsoc' => $this->faker->name(),
            'prinom' => $this->faker->firstNameMale(),
            'segnom' => $this->faker->firstNameMale(),
            'priape' => $this->faker->lastName(),
            'segape' => $this->faker->lastName(),
            'nomemp' => $this->faker->name(),
            'cedrep' => $this->faker->numberBetween(1110291950, 1170491950),
            'repleg' => $this->faker->name(),
            'direccion' =>  $this->faker->address(),
            "tipper" => 'N',
            "sigla" => 'N',
            "codciu" => 18001,
            "telefono" => $this->faker->phoneNumber(),
            "email" => $this->faker->unique()->safeEmail(),
            "codzon" => 18001,
            "dirpri" => $this->faker->address(),
            "telpri" => $this->faker->phoneNumber(),
            "ciupri" => 18001,
            "ofiafi" => '02',
            "calemp" => 'E',
            "tipemp" => 'P',
            "tipsoc" => '02',
            "tipapo" => 'P',
            "codact" => '8560',
            "codind" => "03",
            "feccer" => $this->faker->date(),
            "fecapr" => $this->faker->date(),
            "fecafi" => $this->faker->date(),
            "fecsis" => $this->faker->date(),
            "estado" => 'A'
        ];
    }
}
