<?php

namespace Database\Factories;

use App\Models\InternoAbono;
use App\Models\InternoSaldo;
use Illuminate\Database\Eloquent\Factories\Factory;

class InternoAbonoFactory extends Factory
{

    protected $model = InternoAbono::class;

    /**
     * Define the model's default state.
     * @return array
     */
    public function definition()
    {
        $internoSaldo = InternoSaldo::factory()->create();
        return [
            'interno_saldo' => $internoSaldo->id,
            'fecha' => $this->faker->date(),
            "valor_abono" => 10000,
            "cedula" => $internoSaldo->cedula,
            "concepto" => $this->faker->numberBetween(1, 3),
            "usuario" =>  $this->faker->numberBetween(100, 500),
            "estado" => 'P',
            "procesado" => 'N'
        ];
    }
}
