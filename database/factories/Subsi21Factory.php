<?php

namespace Database\Factories;

use App\Models\Subsi21;
use Illuminate\Database\Eloquent\Factories\Factory;

class Subsi21Factory extends Factory
{
    protected $model = Subsi21::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
