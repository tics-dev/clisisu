<?php

namespace Database\Factories;

use App\Models\Subsi23;
use Illuminate\Database\Eloquent\Factories\Factory;

class Subsi23Factory extends Factory
{
    protected $model = Subsi23::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
