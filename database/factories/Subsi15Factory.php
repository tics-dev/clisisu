<?php

namespace Database\Factories;

use App\Models\Subsi15;
use Illuminate\Database\Eloquent\Factories\Factory;

class Subsi15Factory extends Factory
{
    protected $model = Subsi15::class;

    /**
     * Define the model's default state.
     * @return array
     */
    public function definition()
    {
        return [
            "nit" =>  $this->faker->randomDigit(),
            "codsuc" => '001',
            "codlis" => '001',
            "cedtra" => $this->faker->unique()->randomDigit(),
            "coddoc" => $this->faker->numberBetween(1, 2),
            'prinom' => $this->faker->firstNameMale(),
            'segnom' => $this->faker->firstNameMale(),
            'priape' => $this->faker->lastName(),
            'segape' => $this->faker->lastName(),
            "direccion" => $this->faker->address(),
            "codciu" => 18001,
            "telefono" => $this->faker->phoneNumber(),
            "email" => $this->faker->safeEmail(),
            "codzon" => 18001,
            "rural" => 'N',
            "agro" => 'N',
            "cabhog" => 'N',
            "captra" => 'S',
            "horas" => 240,
            "salario" => 1300000,
            "tipsal" => 'F',
            "fecsal" => $this->faker->date(),
            "sexo" => 'M',
            "tipcot" => '03',
            "vendedor" => 'N',
            "empleador" => 'N',
            "tippag" => 'D',
            "codban" => 51,
            "numcue" => 3157145942,
            "tipcue" => 'A',
            "codcat" => 'A',
            "fecnac" => $this->faker->date(),
            "ciunac" => 18001,
            "fecpre" => $this->faker->date(),
            "fecafi" => $this->faker->date(),
            "fecsis" => $this->faker->date(),
            "giro" => 'S',
            "estado" => 'A',
            "usuario" => 1,
            "correo" => $this->faker->safeEmail()
        ];
    }
}
