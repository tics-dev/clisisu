<?php

namespace Database\Factories;

use App\Models\Subsi20;
use Illuminate\Database\Eloquent\Factories\Factory;

class Subsi20Factory extends Factory
{
    protected $model = Subsi20::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [];
    }
}
