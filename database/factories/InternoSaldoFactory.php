<?php

namespace Database\Factories;

use App\Models\InternoSaldo;
use Illuminate\Database\Eloquent\Factories\Factory;

class InternoSaldoFactory extends Factory
{

    protected $model = InternoSaldo::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "cedula" => $this->faker->numberBetween(100000001, 100000999),
            "fecha_inicial" => $this->faker->date(),
            "fecha_ultima" => $this->faker->date(),
            "valor_inicial" => 10000,
            "valor_acumulado" => 10000,
            "estado" => 'P'
        ];
    }
}
