<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubsi15sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subsi15', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('codsuc');
            $table->integer('nit');
            $table->string('codlis');
            $table->integer("cedtra");
            $table->string("coddoc");
            $table->string("priape");
            $table->string("segape");
            $table->string("prinom");
            $table->string("segnom");
            $table->string("direccion");
            $table->integer("codciu");
            $table->string("telefono");
            $table->string("email");
            $table->integer("codzon");
            $table->string("rural");
            $table->string("agro");
            $table->string("cabhog");
            $table->string("captra");
            $table->string("tipdis");
            $table->integer("horas");
            $table->integer("salario");
            $table->string("tipsal");
            $table->string("fecsal");
            $table->string("sexo");
            $table->string("estciv");
            $table->string("tipcon");
            $table->string("feccon");
            $table->string("trasin");
            $table->string("vivienda");
            $table->string("nivedu");
            $table->string("tipcot");
            $table->string("vendedor");
            $table->string("empleador");
            $table->string("tippag");
            $table->string("codcue");
            $table->string("ofides");
            $table->string("codgru");
            $table->string("codban");
            $table->string("numcue");
            $table->string("tipcue");
            $table->date("feccar");
            $table->string("codcat");
            $table->date("fecnac");
            $table->string("ciunac");
            $table->string("fecpre");
            $table->date("fecafi");
            $table->date("fecsis");
            $table->string("giro");
            $table->string("codgir");
            $table->string("estado");
            $table->string("codest");
            $table->date("fecest");
            $table->string("carnet");
            $table->string("benef");
            $table->string("ruaf");
            $table->string("ruasub");
            $table->integer("totcon");
            $table->integer("tothij");
            $table->integer("tother");
            $table->integer("totpad");
            $table->string("nota");
            $table->string("numtar");
            $table->string("fecrua");
            $table->string("fosfec");
            $table->date("fecact");
            $table->string("tipjor");
            $table->integer("usuario");
            $table->string("correo");
            $table->string("tottra");
            $table->string("cargo");
            $table->string("orisex");
            $table->string("dirlab");
            $table->string("ruralt");
            $table->integer("ciulab");
            $table->integer("pais");
            $table->string("facvul");
            $table->string("peretn");
            $table->date("fecha_giro");
            $table->integer("resguardos_id");
            $table->integer("pub_indigena_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subsi15');
    }
}
